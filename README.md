# Vuexy Laravel Theme  

Given below are the steps you need to follow to install the vuexy-laravel-basic-version / vuexy-laravel-starter-kit on your system:

`Note:`
`System Requirements`
`Node: LTS version`
`PHP: v8.0.2 or Above`
`Composer: v2.0.4 or Above`
`Laravel: v9.0`

## Installation

```sh
git clone https://link-to-project
git branch
git checkout master
git pull origin master
composer install
php artisan migrate
php artisan db:seed
php artisan passport:install
php artisan key:generate
npm install
npm install --legacy-peer-deps
npm run dev
php artisan serve
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`secret`
`refreshTokenSecret`
`expireTime`
`refreshTokenExpireTime`


