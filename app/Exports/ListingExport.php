<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class ListingExport implements FromView
{
    /**
    * @return \Illuminate\Support\Array
    */
    protected $report;
    public function __construct($data)
    {
        $this->report = $data;
    }
    public function view(): View
    {
        return view('reports.excel.listing', [
            'items' => $this->report['data'],
            'columns' => $this->report['columns']
        ]);
    }
}
