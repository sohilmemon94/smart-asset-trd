<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\FieldInputType;
use App\Models\ErrorLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\GeneralTrait;

class ApiController extends Controller
{
    use GeneralTrait;

    function sendResponse($data, $message){
        $response = [
            'success' => true,
            'data' => $data,
            'message' => $message,
        ];
        return response()->json($response, 200);
    }

    function sendError($message, $http_code){
        $response = [
            'success' => false,
            'message' => $message,
        ];
        return response()->json($response, $http_code);
    }

    function deletefields(){
        $arrat = [12, 6, 19];
        FieldInputType::whereIn('field_input_type_id', $arrat)->delete();
        return $this->sendResponse([], 'Deleted Successfully');
    }
    function updatefields(){
        $arrat = [9];
        FieldInputType::where('field_input_type_id', 9)->update(['field_input_type_name' => 'Date-Time']);
        return $this->sendResponse([], 'Updated Successfully');
    }

    
    /**
	 * Get system error log
	 */
	public function getErrorLog(Request $request)
	{
		try {
            $perPage  = isset($request->perPage) ? $request->perPage : null;
            $search   = $request->search;
            $page     = $request->page;
            $sortBy   = isset($request->sortBy) ? $request->sortBy : 'el_id';
            $sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
            $status = $request->status;

			$errorLogData = ErrorLog::with('createdBy')->where('el_name', '!=', ' ');

            // Status filter
            if ($status) {
                $errorLogData = $errorLogData->where('el_status', $status);
            }

            // Search filter
            if ($search) {
                $errorLogData = $errorLogData->where(function ($query) use ($search) {
                    $query->orWhere('el_id', 'LIKE', '%' . $search . '%')
                        ->orWhere('el_name', 'LIKE', '%' . $search . '%')
                        ->orWhere('el_description', 'LIKE', '%' . $search . '%');
                });
            }

            if ($perPage) {
                $errorLogData = $errorLogData->orderBy($sortBy, $sortDesc)->paginate($perPage);
                $pagination = [
                    "total"        => $errorLogData->total(),
                    "current_page" => $errorLogData->currentPage(),
                    "last_page"    => $errorLogData->lastPage(),
                    "from"         => $errorLogData->firstItem(),
                    "to"           => $errorLogData->lastItem()
                ];
                $data = ['errors' => $errorLogData, "total" => $errorLogData->total(), 'pagination' => $pagination,];
            } 

			return $this->returnSuccessMessage('Error List Fetch Successfully.', $data);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}
}
