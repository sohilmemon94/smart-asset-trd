<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Company;
use App\Models\PasswordReset;
use Validator;
use App\Http\Traits\GeneralTrait;
use Exception;
use Illuminate\Support\Facades\Hash;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use Illuminate\Support\Str;
use App\Mail\PasswordResetRequestMail;
use App\Mail\PasswordResetSuccessMail;
use App\Mail\PasswordSetSuccessMail;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
	use GeneralTrait;

	/**
	 * Create user
	 *
	 * @param  [string] name
	 * @param  [string] email
	 * @param  [string] password
	 * @param  [string] password_confirmation
	 * @return [string] message
	 */
	public function register(Request $request)
	{
		$request->validate([
			'email'      => 'required|string|email|unique:users',
			'password'   => 'required|string|',
			'c_password' => 'required|same:password',
		]);

		$user = new User([
			'first_name' => $request->first_name,
			'email'      => $request->email,
			'password'   => bcrypt($request->password)
		]);

		if ($user->save()) {
			return response()->json([
				'message' => 'Successfully created user!'
			], 201);
		} else {
			return response()->json(['error' => 'Provide proper details']);
		}
	}

	/**
	 * Login user and create token
	 *
	 * @param  [string] email
	 * @param  [string] password
	 * @param  [boolean] remember_me
	 * @return [string] access_token
	 * @return [string] token_type
	 * @return [string] expires_at
	 */
	public function login(Request $request)
	{
		// try {
			$validator = Validator::make($request->all(), [
				'email'       => 'required|email|regex: /(.+)@(.+)\.(.+)/i',
				'password'    => 'required',
				'remember_me' => 'boolean'
			]);

			if ($validator->fails()) {
				return $this->returnValidation($validator->errors());
			}

			$credentials = array('email' => $request->email, 'password' => $request->password, 'status' => 'Y');

			if (!Auth::attempt($credentials))
				return $this->returnError(403, 'Invalid login credentials, Please try again...');
            $company_name = "";
            $company_logo = "";
			$user        = $request->user();
			$tokenResult = $user->createToken($user->first_name);
			$token       = $tokenResult->token;
            //dd($user->roles);
            $exit_role = $user->roles->first()->name;
            if($exit_role == 'User'){
                $user_company_id = User::where('id', $user->id)->first()->company_id;
                $companyData = Company::where('id', $user_company_id)->first();
                if($companyData){
                    $company_name = $companyData->name;
                    $company_logo = !empty($companyData->logo) ? $this->getStorageURL($companyData->logo) : null;
                }

				$token->expires_at = Carbon::now()->addHours(2);
            }else{
                $user_company_id = User::where('id', $user->id)->first()->company_id;
                $companyData = Company::where('id', $user_company_id)->first();
                if($companyData){
                    $company_name = $companyData->name;
                    $company_logo = !empty($companyData->logo) ? $this->getStorageURL($companyData->logo) : null;
                }
            }

			if ($request->remember_me)
				$token->expires_at = Carbon::now()->addWeeks(1);
			$token->save();

			$userData = array(
				'id'                  => $user->id,
                'company_id'          => $user->company_id,
				'fullName'            => $user->first_name . " " .  $user->last_name,
				'email'               => $user->email,
				'is_root_admin'       => $user->is_root,
				'avatar'              => !empty($user->avatar) ? $this->getStorageURL($user->avatar) : null,
				'client_company_name' => $company_name,
				'client_company_logo' => $company_logo,
				'is_confined_space'   => isset($user->companyData) ? $user->companyData->is_confined_space : 'N',
				'role'                => $exit_role,
				'permission'          => auth()->user()->jsPermissions(),
				'ability'             => [array(
											'action'  => 'manage',
											'subject' => 'all'
										)]
			);

			$userResponseData = array(
				'loggedUserData'  => $userData,
				'access_token'    => $tokenResult->accessToken,
				'token_type'      => 'Bearer',
				'expires_at'      => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
				'accessToken'     => env('secret'),
				'setRefreshToken' => env('refreshTokenSecret'),
			);

            // With response we need to send the sites & subsites assigned to that user as well.
            $siteOptions = $this->getSiteAndSubSiteList($user->id)['sites'];
            $subSiteOptions = $this->getSiteAndSubSiteList($user->id)['subSites'];

            $data = array(
                'user' => $userResponseData,
                'sites' => $siteOptions,
                'subSites' => $subSiteOptions
            );
			// return $this->returnSuccessMessage('You have successfully logged in as ' . $user->roles->first()->name . '. Now you can start to explore!', $data);

			$return_array = [
                'accessToken'     => $userResponseData['access_token'],
                'data'            => $data,
                'message'         => 'You have successfully logged in as ' . $user->roles->first()->name . '. Now you can start to explore!',
                'setRefreshToken' => $userResponseData['setRefreshToken'],
                'status'          => 200,
            ];

            return response()->json($return_array, 200);
		// } catch (Exception $e) {
		// 	// $e->getMessage();
		// 	return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		// }
	}

	/**
	 * Get the authenticated User
	 *
	 * @return [json] user object
	 */
	public function myProfile(Request $request)
	{

		// try {
			$data = $request->user();

            //var_dump($data);
            //exit;

			if ($data) {
				$data->avatar = !empty($data->avatar) ? $this->getStorageURL($data->avatar) : null;
				$data->client_company_logo = !empty($data->client_company_logo) ? $this->getStorageURL($data->client_company_logo) : null;
				$data->role = $data->roles->first()->name ?? '';

                if($data->company_id){

                    $data->companyData = Company::where('id', $data->company_id)->first();
                    $data->companyData->logo = !empty($data->companyData->logo) ? $this->getStorageURL($data->companyData->logo) : null;

                }

				return $this->returnSuccessMessage(null, $data);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		// } catch (\Exception $e) {
		// 	return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		// }
		return response()->json($request->user());
	}

	/**
	 * Logout user (Revoke the token)
	 *
	 * @return [string] message
	 */
	public function logout(Request $request)
	{
		try {
			$user = Auth::user()->token();
			$user->revoke();

			return $this->returnSuccessMessage('You have successfully logged out.', []);
		} catch (Exception $e) {
			// $e->getMessage();
			return $this->returnError(500, 'Opps! Something went wrong, Please try again...');
		}

		$request->user()->token()->revoke();

		return response()->json([
			'message' => 'Successfully logged out'
		]);
	}

	/**
	 * Create token password reset
	 *
	 * @param  [string] email
	 * @return [string] message
	 */
	public function createPasswordReset(Request $request)
	{
		//try {
		$validator = Validator::make($request->all(), [
			'email' => 'required|email|regex:/(.+)@(.+)\.(.+)/i',
		]);

		if ($validator->fails()) {
			return $this->returnValidation($validator->errors());
		}

		$user = User::where('email', $request->email)->first();

		if (!$user)
			return $this->returnError(404, 'We can\'t find a user with that e-mail address.');

		$passwordReset = PasswordReset::updateOrCreate(
			['email' => $user->email],
			[
				'email' => $user->email,
				'token' => Str::random(60)
			]
		);

		if ($user && $passwordReset) {
			$link = url('/reset-password/' . $passwordReset->token);
			$mail = new PasswordResetRequestMail($request->email, $link, $user->first_name);
			Mail::mailer('support')->to($request->email)->send($mail);
            //Mail::mailer('smtp2')->to($request->email)->send($mail);
		}
		// START :: Notification email functionality
		// $user->notify(
		// 	new PasswordResetRequest($passwordReset->token)
		// );
		// END :: Notification email functionality

		return $this->returnSuccessMessage('We have e-mailed your password reset link!');
		// } catch (Exception $e) {
		// 	return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		// }
	}

	/**
	 * Find token password reset
	 *
	 * @param  [string] $token
	 * @return [string] message
	 * @return [json] passwordReset object
	 */
	public function findTokenPasswordReset(Request $request)
	{
		try {
			$passwordReset = PasswordReset::where('token', $request->token)->first();

			if (!$passwordReset)
				return $this->returnError(404, 'This password reset token is invalid.');

			if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
				$passwordReset->delete();

				return $this->returnError(404, 'This password reset token is invalid.');
			}

			return $this->returnSuccessMessage(200, $passwordReset);
		} catch (Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Reset password
	 *
	 * @param  [string] email
	 * @param  [string] password
	 * @param  [string] password_confirmation
	 * @param  [string] token
	 * @return [string] message
	 * @return [json] user object
	 */
	public function resetPassword(Request $request)
	{
		try {
			$validator = Validator::make($request->all(), [
				'email'    => 'required|email|regex: /(.+)@(.+)\.(.+)/i',
				'password' => 'required|string|confirmed',
				'token'    => 'required|string'
			]);

			if ($validator->fails()) {
				return $this->returnValidation($validator->errors());
			}

			$passwordResetData = PasswordReset::where([
				['token', $request->token],
				['email', $request->email]
			])->first();

			if (!$passwordResetData)
				return $this->returnError(404, 'This password reset token is invalid.');

			$userData = User::where('email', $passwordResetData->email)->first();

			if (!$userData)
				return $this->returnError(404, 'We can\'t find a user with that e-mail address.');

			$userData->password = bcrypt($request->password);
			$userData->save();
			$passwordResetData->delete();

			if ($userData) {
				$mail = new PasswordResetSuccessMail($request->email, $userData->first_name, url('/login'));
				Mail::mailer('support')->to($request->email)->send($mail);
			}

			// START :: Notification email functionality
			// $userData->notify(new PasswordResetSuccess($passwordResetData));
			// END :: Notification email functionality

			return $this->returnSuccessMessage('Your password has been reset successfully.', $userData);
		} catch (Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Reset password
	 *
	 * @param  [string] email
	 * @param  [string] password
	 * @param  [string] password_confirmation
	 * @param  [string] token
	 * @return [string] message
	 * @return [json] user object
	 */
	public function setPassword(Request $request)
	{
		try {
			$validator = Validator::make($request->all(), [
				'email'    => 'required|email|regex: /(.+)@(.+)\.(.+)/i',
				'password' => 'required|string|confirmed',
				'token'    => 'required|string'
			]);

			if ($validator->fails()) {
				return $this->returnValidation($validator->errors());
			}

			$passwordResetData = PasswordReset::where([
				['token', $request->token],
				['email', $request->email]
			])->first();

			if (!$passwordResetData)
				return $this->returnError(404, 'This password reset token is invalid.');

			$userData = User::where('email', $passwordResetData->email)->first();

			if (!$userData)
				return $this->returnError(404, 'We can\'t find a user with that e-mail address.');

			$userData->password = bcrypt($request->password);
			$userData->save();
			$passwordResetData->delete();

			if ($userData) {
				$mail = new PasswordSetSuccessMail($request->email, $userData->first_name, url('/'));
				Mail::to($request->email)->send($mail);
			}

			// START :: Notification email functionality
			// $userData->notify(new PasswordResetSuccess($passwordResetData));
			// END :: Notification email functionality

			return $this->returnSuccessMessage('Your password has been set successfully.', $userData);
		} catch (Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}
}
