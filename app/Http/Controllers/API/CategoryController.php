<?php

namespace App\Http\Controllers\API;

use App\Models\Category;
use App\Models\SubSite;
use App\Models\ClientCloneCategory;
use Illuminate\Http\Request;
use App\Http\Traits\GeneralTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\SubSitePermission;
use App\Models\SubSiteUser;
use App\Models\SubSiteUserPermission;
use Carbon\Carbon;
use Throwable;
use App\Http\Controllers\API\ModuleController;
use App\Models\Module;
use App\Models\ModuleField;
use App\Models\ModuleQuestion;
use App\Models\ModuleAnswer;
use App\Models\Asset;

class CategoryController extends Controller
{
    use GeneralTrait;
    public $catModule = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $perPage  = isset($request->perPage) ? $request->perPage : null;
            $search   = $request->search;
            $page     = $request->page;
            $sortBy   = isset($request->sortBy) ? $request->sortBy : 'category_name';
            $sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
            $status = $request->status;
            $categoryIsDefault = isset($request->categoryIsDefault) ? $request->categoryIsDefault : null;

            $categoryData = Category::where('category_name', '!=', ' ')
                ->where('company_id', Auth::user()->company_id);

            // Category for admin user
            if ($categoryIsDefault) {
                $categoryData = Category::where('category_is_default', $categoryIsDefault);
            }

            // Status filter
            if ($status) {
                $categoryData = $categoryData->where('category_status', $status);
            }

            // Search filter
            if ($search) {
                $categoryData = $categoryData->where(function ($query) use ($search) {
                    $query->orWhere('category_id', 'LIKE', '%' . $search . '%')
                        ->orWhere('category_name', 'LIKE', '%' . $search . '%')
                        ->orWhere('category_desc', 'LIKE', '%' . $search . '%')
                        ->orWhere('created_at', 'LIKE', '%' . $search . '%')
                        ->orWhere('updated_at', 'LIKE', '%' . $search . '%');
                });
            }

            if ($perPage) {
                $categoryData = $categoryData->orderBy($sortBy, $sortDesc)->paginate($perPage);
                $pagination = [
                    "total"        => $categoryData->total(),
                    "current_page" => $categoryData->currentPage(),
                    "last_page"    => $categoryData->lastPage(),
                    "from"         => $categoryData->firstItem(),
                    "to"           => $categoryData->lastItem()
                ];
                $data = ['categories' => $categoryData, "total" => $categoryData->total(), 'pagination' => $pagination,];
            } else {
                $categoryData = Category::where('category_name', '!=', ' ')
                    ->where('company_id', Auth::user()->company_id)->get();
                $categorylist = [];
                $categorylist[] = array(
                    "value" => null,
                    "text" => "Please select Category"
                );

                foreach ($categoryData as $key => $value) {
                    $categorylist[] =  array(
                        "value" => $value['category_id'],
                        "text" => $value['category_name']
                    );
                }


                $data = ['categories' => $categoryData, 'categorieslist' => $categorylist];
            }

            return $this->returnSuccessMessage('Category List Fetch Successfully.', $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|string'
        ]);

        if ($validator->fails()) {
            // get all errors as single string
            return $this->returnValidation($validator->errors());
        }

        try {
            $categoryData = array(
                'category_user_id'    => Auth::user()->id,
                'category_name'       => $request->category_name,
                'category_desc'       => $request->category_desc,
                'category_is_default' => $request->category_is_default ? 'Y' : 'N',
                'category_status'     => $request->category_status ? 'Y' : 'N',
                'created_by'          => Auth::user()->id,
                'company_id'          => Auth::user()->company_id,
            );

            // Save data
            Category::create($categoryData);

            return $this->returnSuccessMessage('Category Created Successfully.', '');
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $Category = Category::where('category_id', $id)->first();

            if ($Category) {
                return $this->returnSuccessMessage('Category retrieved successfully.', $Category);
            } else {
                return $this->returnError(404, 'Oppps! No record found...');
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|string'
        ]);

        if ($validator->fails()) {
            // get all errors as single string
            return $this->returnValidation($validator->errors());
        }

        $categoryData = array(
            'category_user_id'    => Auth::user()->id,
            'category_name'       => $request->category_name,
            'category_desc'       => $request->category_desc,
            'category_is_default' => $request->category_is_default ? 'Y' : 'N',
            'category_status'     => $request->category_status ? 'Y' : 'N',
            'updated_by'          => Auth::user()->id,
        );

        // Update data
        Category::where('category_id', $id)->update($categoryData);

        return $this->returnSuccessMessage('Category Updated Successfully.', '');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        if ($request->id) {

            $exist_category = SubSiteUserPermission::where('ssup_category_id', $request->id)->where('company_id', Auth::user()->company_id)->exists();
            $exist_parent = Module::where('module_category_id', $request->id)->where('company_id', Auth::user()->company_id)->exists();

            if ($exist_category) {
                Category::where('category_id', $request->id)->first()->delete();
                $ModuleId =  Module::where('module_category_id', $request->id)->where('module_user_id', Auth::user()->id)->pluck('module_id')->toArray();
                foreach ($ModuleId as $key => $value) {
                    ModuleField::where('mf_module_id', $value)->delete();
                    ModuleQuestion::where('msq_module_id', $value)->delete();
                    ModuleAnswer::where('msa_module_id', $value)->delete();
                    Asset::where('asset_module_id', $value)->delete();
                    SubSitePermission::where('ssp_module_id', $value)->delete();
                    SubSiteUserPermission::where('ssup_module_id', $value)->delete();
                };
                Module::where('module_category_id', $request->id)->where('module_user_id', Auth::user()->id)->delete();

                return $this->returnSuccessMessage('Category deleted successfully.');
                //return $this->returnError(405, 'Opps! Not allow to Delete, as this Category already assigned to users permission.');
            } else if ($exist_parent) {
                Category::where('category_id', $request->id)->first()->delete();

                $ModuleId =  Module::where('module_category_id', $request->id)->where('module_user_id', Auth::user()->id)->pluck('module_id')->toArray();
                foreach ($ModuleId as $key => $value) {
                    ModuleField::where('mf_module_id', $value)->delete();
                    ModuleQuestion::where('msq_module_id', $value)->delete();
                    ModuleAnswer::where('msa_module_id', $value)->delete();
                };
                Module::where('module_category_id', $request->id)->where('module_user_id', Auth::user()->id)->delete();

                return $this->returnSuccessMessage('Category deleted successfully.');

                //return $this->returnError(405, 'Opps! Not allow to Delete, as this Category already assigned to module.');
            } else {
                $categoryData = Category::where('category_id', $request->id)->first();
                $categoryData->delete();

                return $this->returnSuccessMessage('Category deleted successfully.', $categoryData);
            }
        } else {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Change Category status data
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function changeCategoryStatus(Request $request)
    {
        try {
            if ($request->id) {
                if ($request->status == 'Y') {
                    // In Active category
                    Category::where('category_id', $request->id)->update(['category_status' => 'N', 'updated_by' => Auth::user()->id]);

                    // In Active module which is belongs to this category
                    Module::where('module_category_id', $request->id)->update(['module_status' => 'N', 'updated_by' => Auth::user()->id]);
                } else {
                    // Active category
                    Category::where('category_id', $request->id)->update(['category_status' => 'Y', 'updated_by' => Auth::user()->id]);

                    // Active module which is belongs to this category
                    Module::where('module_category_id', $request->id)->update(['module_status' => 'Y', 'updated_by' => Auth::user()->id]);
                }

                return $this->returnSuccessMessage('You have successfully changed Category status.');
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Category ID On Module List data
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function relatedCategoryModule(Request $request)
    {
        try {
            $subSite = "";
            $selected = "";
            $categoryData = Category::with('module')->where('category_name', '!=', ' ')
                ->where('company_id', Auth::user()->company_id)
                ->orWhere('category_user_id', Auth::user()->id)
                ->get();

            if ($request->subSite_id) {
                $subSite = SubSite::where('sub_site_id', $request->subSite_id)->first();
            }

            $expand = false;
            foreach ($categoryData as $key => $value) {
                $checked = false;
                if ($key == 0) {
                    $expand = true;
                } else {
                    $expand = true;
                }


                $module = [];
                foreach ($value['module'] as $vKey =>  $val) {
                    $selected = SubSitePermission::where('ssp_sub_site_id', $request->subSite_id)->where('ssp_category_id', $val['module_category_id'])->where('ssp_module_id', $val['module_id'])->where('company_id', Auth::user()->company_id)->exists();


                    if (!$checked && $selected) {
                        $checked = $selected;
                    }

                    $module[] = array(
                        "id" => $val['module_id'],
                        "module_category_id" => $val['module_category_id'],
                        "title" => $val['module_name'],
                        "checked" => $selected
                    );
                }
                if (sizeof($module) > 0) {
                    $this->catModule[] =  array(
                        "id" => $value['category_id'],
                        "title" => $value['category_name'],
                        "expanded" => $expand,
                        "async" => true,
                        "checked" => $checked,
                        "children" => $module,
                    );
                }
            }
            $allData[] = array(
                "id" => 0,
                "title" => "Select All",
                "expanded" => true,
                "async" => true,
                "selected" => false,
                "children" => $this->catModule,

            );
            $data = ['categoryWithModules' => $allData, 'subSites' => $subSite];


            return $this->returnSuccessMessage('Category Module List Fetch Successfully.', $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Sub-site Selected Category/Module List data
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function subSiteSelectedCatMod(Request $request)
    {
        $subSite = "";
        $selected = "";
        $categoryData = Category::with('module')->where('category_name', '!=', ' ')
            ->where('company_id', Auth::user()->company_id)
            ->orWhere('category_user_id', Auth::user()->id)
            ->get();

        if ($request->subSite_id) {
            $subSite = SubSite::where('sub_site_id', $request->subSite_id)->first();
        }

        $expand = false;
        foreach ($categoryData as $key => $value) {
            $checked = false;
            if ($key == 0) {
                $expand = true;
            } else {
                $expand = false;
            }


            $module = [];
            foreach ($value['module'] as $vKey =>  $val) {
                $selected = SubSitePermission::where('ssp_sub_site_id', $request->subSite_id)->where('ssp_category_id', $val['module_category_id'])->where('ssp_module_id', $val['module_id'])->where('company_id', Auth::user()->company_id)->exists();

                if (!$checked && $selected) {
                    $checked = $selected;
                }
                if ($selected) {
                    $module[] = array(
                        "id" => $val['module_id'],
                        "module_category_id" => $val['module_category_id'],
                        "title" => $val['module_name'],
                        "checked" => $selected
                    );
                }
            }
            if ($checked) {
                $this->catModule[] =  array(
                    "id" => $value['category_id'],
                    "title" => $value['category_name'],
                    "expanded" => $expand,
                    "async" => true,
                    "checked" => $checked,
                    "children" => $module,
                );
            }
        }
        $allData[] = array(
            "id" => 0,
            "title" => "Select All",
            "expanded" => true,
            "async" => true,
            "selected" => false,
            "children" => $this->catModule,

        );
        $data = ['categoryWithModules' => $allData, 'subSites' => $subSite, 'selectedModules' => $module];


        return $this->returnSuccessMessage('Category Module List Fetch Successfully.', $data);
    }

    /**
     * Get all category for user
     *
     * @param  \App\Models\FieldInputType
     * @return \Illuminate\Http\Response
     */
    public function getAllCategory()
    {
        try {
            $data = Category::where('category_status', 'Y')->where('company_id', Auth::user()->company_id)->orderBy('category_name')->get();

            $categoryOption = [];
            foreach ($data as $key => $value) {
                $categoryOption[$key]['label'] = $value['category_name'];
                $categoryOption[$key]['value'] = $value['category_id'];
            }
            return $this->returnSuccessMessage(null, $categoryOption);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Get all category for user
     *
     * @param  \App\Models\Category
     * @return \Illuminate\Http\Response
     */
    public function getDefaultCategoryList()
    {
        try {
            $data = Category::where('category_status', 'Y')->where('category_is_default', 'Y')->orderBy('category_name')->get();

            $categoryOption = [];
            foreach ($data as $key => $value) {
                $categoryOption[$key]['label'] = $value['category_name'];
                $categoryOption[$key]['value'] = $value['category_id'];
            }

            return $this->returnSuccessMessage(null, $categoryOption);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Clone category, module and module fields table
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function cloneCategory($userId, $companyId, $cloneCategoryArray)
    {
        if ($userId > 0 && sizeOf($cloneCategoryArray) > 0) {

            // Get category data
            $categoryData = Category::whereIn('category_id', $cloneCategoryArray)->get()->keyBy('category_id');

            foreach ($categoryData as $key => $category) {
                $categoryInsertData = array(
                    'category_user_id'    => $userId,
                    'category_name'       => $category['category_name'],
                    'category_desc'       => $category['category_desc'],
                    'category_is_default' => 'N',
                    'category_status'     => $category['category_status'],
                    'created_by'          => Auth::user()->id,
                    'company_id'           => $companyId,
                );

                // Insert data
                $categoryObj = Category::create($categoryInsertData);

                // Mange client clone category
                $this->insertClientCloneCategory($userId, $category->category_id, $categoryObj->category_id, $companyId);

                // Clone module and module fields
                $moduleObj = new ModuleController;
                $moduleObj->cloneModuleAndModuleField($userId, $category->category_id, $categoryObj->category_id, $companyId);
            }

            return true;
        }
        return false;
    }

    /**
     * Insert data into client_clone_category table
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function insertClientCloneCategory($userId, $cloneCategoryId, $newCategoryId, $companyId)
    {
        if ($userId > 0 && $cloneCategoryId > 0 && $newCategoryId > 0) {

            $clientCloneCategoryData = array(
                'cca_user_id'           => $userId,
                'cca_clone_category_id' => $cloneCategoryId,
                'cca_new_category_id'   => $newCategoryId,
                'created_by'            => Auth::user()->id,
                'company_id'            => $companyId,
            );

            // Insert data
            ClientCloneCategory::create($clientCloneCategoryData);

            return true;
        }
        return false;
    }

    /**
     * Get data into client_clone_category table
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getClientCloneCategory($userId)
    {
        $clientCloneCategoryData = ClientCloneCategory::where('cca_user_id', $userId)->pluck('cca_clone_category_id');

        return $clientCloneCategoryData;
    }

    /**
     * Get data into client_category_access table
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserCloneCategory($userId)
    {
        $userCloneCategoryData = Category::where('category_user_id', $userId)->where('created_by', '!=', $userId)->pluck('category_id');

        return $userCloneCategoryData;
    }
}
