<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\GeneralTrait;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Carbon\Carbon;

class CompanyController extends Controller
{
    use GeneralTrait;

    /**
	 * Get all Company List
	 */
	public function index(Request $request)
	{
		try {
			$perPage  = $request->perPage;
			$search   = $request->search;
			$page     = $request->page;
			$sortBy   = isset($request->sortBy) ? $request->sortBy  : 'id';
			$sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
			$status   = $request->status;

            $companyData = Company::where('name', '!=', '');

			// Status filter
			if ($status) {
				$companyData = $companyData->where('status', $status);
			}

			// Search filter
			if ($search) {
				$companyData = $companyData->where(function ($query) use ($search) {
					$query->orWhere('name', 'LIKE', '%' . $search . '%')
						//->orWhere(\DB::raw('CONCAT(first_name, " ", last_name)'), 'LIKE', '%' . $search . '%')
						->orWhere('abn', 'LIKE', '%' . $search . '%')
						->orWhere('client_id', 'LIKE', '%' . $search . '%')
						->orWhere('contact_no', 'LIKE', '%' . $search . '%')
						->orWhere('created_by', 'LIKE', '%' . $search . '%');
						// ->orWhere('contact_number', 'LIKE', '%' . $search . '%');
				});
			}

			$companyData = $companyData->orderBy($sortBy, $sortDesc)->paginate($perPage);

			foreach ($companyData as $key => $company) {
				if ($company->logo) {
					$companyData[$key]->logo = !empty($company->logo) ? $this->getStorageURL($company->logo) : '';
				}
			}

			$pagination = [
				"total"        => $companyData->total(),
				"current_page" => $companyData->currentPage(),
				"last_page"    => $companyData->lastPage(),
				"from"         => $companyData->firstItem(),
				"to"           => $companyData->lastItem()
			];

			$data = ['companys' => $companyData, "total" => $companyData->total(), 'pagination' => $pagination];

			return $this->returnSuccessMessage(null, $data);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

    /**
     * Get all Company for option list
     */
    public function getAllCompany()
    {
        try {
            $data = Company::where('status', 'Y')->orderBy('name')->get();

            $companyOption = [];
            foreach ($data as $key => $value) {
                $companyOption[$key]['label'] = $value['name'];
                $companyOption[$key]['value'] = $value['id'];
            }
            return $this->returnSuccessMessage(null, $companyOption);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /*
	*	Save new Company
	*/
	public function store(Request $request)
	{
			$regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';

			$request->validate([
				'name'          => 'required|max:50|unique:company,name,NULL,id,deleted_at,NULL',
				'abn'           => 'required|max:20|string|unique:company,abn,NULL,id,deleted_at,NULL',
				'client_id'     => 'required|string|max:20|unique:company,client_id,NULL,id,deleted_at,NULL',
				'website'       => 'required|max:120|regex:'.$regex,
				'street'        => 'required|max:50',
				'city'          => 'required|max:50',
				'state'         => 'required|max:50',
				'country'       => 'required|max:50',
				'postal_code'   => 'required|max:20',
				'contact_no'    => 'required|max:17|regex:/^[- +()]*[0-9][- +()0-9]*$/',
				'status'        => 'required',
				'commence_date' => 'required',
				'logo'          => 'required|mimes:jpg,jpeg,png|max:2048'

			]);

		try {
			$company = new Company([
				'name'              => $request->name ?? NULL,
				'abn'               => $request->abn ?? NULL,
				'client_id'         => $request->client_id ?? NULL,
				'website'           => $request->website ?? NULL,
				'contact_no'        => $request->contact_no ?? NULL,
				'status'            => $request->status ?? 'Y',
				'street'            => $request->street ?? NULL,
				'city'              => $request->city ?? NULL,
				'state'             => $request->state ?? NULL,
				'postal_code'       => $request->postal_code ?? NULL,
				'country'           => $request->country ?? NULL,
				'is_confined_space' => $request->is_confined_space ?? 'N',
				'commence_date'     => $request->commence_date,
                'logo'  			=> $request->logo ?? NULL,
				'created_by'        => Auth::user()->id
			]);

			if ($company->save()) {

				// For client logo upload
				if ($company->id && $request->file() && $request->logo) {
					if ($request->file()) {
						$filePath = $this->fileUpload($request->logo, 'image/company');
					}

					$updateCompanyData = array(
						'logo' => $filePath
					);

					$company->update($updateCompanyData);
				}

				return $this->returnSuccessMessage('Company added successfully.');
			} else {
				return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

    /**
	 * Get edit Company data
	 */
	public function edit($id)
	{
		try {
			$data = Company::where('id', $id)->first();

			if ($data) {
				$data->logo = !empty($data->logo) ? $this->getStorageURL($data->logo) : '';

				return $this->returnSuccessMessage(null, $data);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

    /**
	 * Update Company data
	 */
	public function update($id, Request $request)
	{
        //dd($request->all());
            $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';

			$request->validate([
                'name' => 'required|max:50|unique:company,name,' . $id,
				'abn'              => 'required|max:20|string|unique:company,abn,' . $id,
				'client_id'        => 'required|string|max:20|unique:company,client_id,' . $id,
				'website'          => 'required|max:120|regex:'.$regex,
				'street'           => 'required|max:50',
				'city'             => 'required|max:50',
				'state'            => 'required|max:50',
				'country'          => 'required|max:50',
				'postal_code'      => 'required|max:20',
				'contact_no'       => 'required|max:17|regex:/^[- +()]*[0-9][- +()0-9]*$/',
				'status'           => 'required',
				'commence_date'    => 'required',
				// 'logo'          => 'required|mimes:jpg,jpeg,png|max:2048',
			]);


		// try {
			$updateCompanyData = array(
				'name'              => $request->name,
				'abn'               => $request->abn,
				'client_id'         => $request->client_id,
				'website'           => $request->website,
				'contact_no'        => $request->contact_no,
				'is_confined_space' => $request->is_confined_space ?? 'N',
				'commence_date'     => \Carbon\Carbon::parse($request->commence_date)->format('Y-m-d H:i:s'),
				'street'            => $request->street,
				'city'              => $request->city,
				'state'             => $request->state,
				'postal_code'       => $request->postal_code,
				'country'           => $request->country,
				'status'            => $request->status,
				'updated_by'        => Auth::user()->id
			);

			// Update company data
			$company = Company::find($id);
			$company->update($updateCompanyData);

			return $this->returnSuccessMessage('You have successfully updated Company Data.', $company);
		// } catch (\Exception $e) {
		// 	return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		// }
	}

    /**
	 * Update company avatar
	 */
	public function uploadCompanyAvatar($id, Request $request)
	{
		$companyData = Company::where('id', $id)->first();

		if ($request->logo) {
			$request->validate([
				'logo' => 'required|mimes:jpg,jpeg,png|max:2048'
			]);

			if ($request->file()) {
				$filePath = $this->fileUpload($request->logo, 'image/company');
			}

			$updateCompanyData = array(
				'logo' => $filePath
			);

			if (isset($companyData) && $companyData->logo) {
				$this->deleteFile($companyData->logo);
			}

			$companyData->update($updateCompanyData);

			$companyData->logo = !empty($companyData->logo) ? $this->getStorageURL($companyData->logo) : '';

			return $this->returnSuccessMessage('You have successfully updated company logo.', $companyData);
		}
	}

    /**
	 * Remove company avatar
	 */
	public function removeCompanyAvatar(Request $request)
	{
		if ($request->id) {
			$companyData = Company::where('id', $request->id)->first();

			// Remove existing image on server
			if (isset($companyData) && $companyData->logo  != '') {
				$this->deleteFile($companyData->logo);

				// Update user data
				$companyData->update(array('logo' => ''));

				return $this->returnSuccessMessage('You have successfully removed Company logo.');
			}
		}
	}

    /**
	 * Delete Company data
	 *
	 */
	public function destroy(Request $request)
	{
		if ($request->id) {
            ///echo $request->id;
            $exist_companyINuser = User::where('company_id', $request->id) -> exists();

            if($exist_companyINuser){
                return $this->returnError(405, 'Opps! Not allow to delete this company, as some clients assign in this company.');
            }else{
				$companyData = Company::where('id', $request->id)->first();
				$companyData->delete();

			return $this->returnSuccessMessage('You have successfully deleted Company.');

            }
		}
	}

    /**
     * Change Company status data
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function changeCompanyStatus(Request $request)
    {
        try {
            if ($request->id) {
                if ($request->status == 'Y') {
                    Company::where('id', $request->id)->update(['status' => 'N']);
                    User::where('company_id', $request->id)->where('status', 'Y')->update(['status' => 'N']);
                } else {
                    Company::where('id', $request->id)->update(['status' => 'Y']);
                    User::where('company_id', $request->id)->where('status', 'N')->update(['status' => 'Y']);
                }

                return $this->returnSuccessMessage('You have successfully changed Client status.');
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
}
