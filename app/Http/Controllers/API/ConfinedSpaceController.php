<?php

namespace App\Http\Controllers\API;

use App\Models\TemplateField;
use App\Models\FieldInputType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTrait;
use App\Models\Template;
use App\Models\ConfinedSpaceFormResult;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Throwable;

class ConfinedSpaceController extends Controller
{
	use GeneralTrait;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		try {
			$perPage  = isset($request->perPage) ? $request->perPage : null;
			$search   = $request->search;
			$sortBy   = isset($request->sortBy) ? $request->sortBy : 'csfr_id';
			$sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
			$status   = $request->result;
			$siteId = $request->site_id;
			$subSiteId = $request->sub_site_id;

			$confinedSpaceFormResultData = ConfinedSpaceFormResult::with(['location:site_id,site_name', 'area:sub_site_id,sub_site_name', 'user:id,created_by,first_name,last_name'])
											->whereHas('user', function ($query) {
												$query->where('created_by', Auth::id());
											})
											->where('csfr_user_id', '>',  0);
			
			// Location filter
			if ($siteId && $siteId > 0) {
				$confinedSpaceFormResultData = $confinedSpaceFormResultData->where('csfr_site_id', $siteId);
				// $confinedSpaceFormResultData = $confinedSpaceFormResultData->whereHas('location', function ($query) use($siteId) {
				// 									$query->where('site_id', $siteId);
				// 								});
			}

			// Area filter
			if ($subSiteId && $subSiteId > 0) {
				$confinedSpaceFormResultData = $confinedSpaceFormResultData->where('csfr_sub_site_id', $subSiteId);
				// $confinedSpaceFormResultData = $confinedSpaceFormResultData->whereHas('area', function ($query) use($subSiteId) {
				// 									$query->where('sub_site_id', $subSiteId);
				// 								});
			}
			
			// Status filter
			if ($status) {
				$confinedSpaceFormResultData = $confinedSpaceFormResultData->where('csfr_result', $status);
			}

			// Search filter
			if ($search) {
				$confinedSpaceFormResultData = $confinedSpaceFormResultData->where(function ($query) use ($search) {
					$query->orWhere('csfr_department', 'LIKE', '%' . $search . '%')
						->orWhere('csfr_position', 'LIKE', '%' . $search . '%')
						->orWhere('csfr_location_of_space', 'LIKE', '%' . $search . '%')
						->orWhere('csfr_description_of_space', 'LIKE', '%' . $search . '%')
						->orWhere('csfr_confined_space_number', 'LIKE', '%' . $search . '%')
						->orWhere('csfr_result', 'LIKE', '%' . $search . '%')
						->orWhereHas('location', function ($fieldQuery) use ($search) {
							$fieldQuery->where('site_name', 'LIKE', '%' . $search . '%');
						})
						->orWhereHas('area', function ($fieldQuery) use ($search) {
							$fieldQuery->where('sub_site_name', 'LIKE', '%' . $search . '%');
						});
				});
			}

			$confinedSpaceFormResultData = $confinedSpaceFormResultData->orderBy($sortBy, $sortDesc)->paginate($perPage);

			$pagination = [
				"total"        => $confinedSpaceFormResultData->total(),
				"current_page" => $confinedSpaceFormResultData->currentPage(),
				"last_page"    => $confinedSpaceFormResultData->lastPage(),
				"from"         => $confinedSpaceFormResultData->firstItem(),
				"to"           => $confinedSpaceFormResultData->lastItem()
			];

			$data = ['confinedSpace' => $confinedSpaceFormResultData, 'total' => $confinedSpaceFormResultData->total(), 'pagination' => $pagination];

			return $this->returnSuccessMessage(null, $data);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Template  $template
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\Template  $template
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		try {
			$confinedSpaceData = ConfinedSpaceFormResult::with(['answers', 'answers.question','answers.question.childrenQuestion'])->where('csfr_id', $id)->first();

			if ($confinedSpaceData) {
				return $this->returnSuccessMessage('Confined space data retrieved successfully.', $confinedSpaceData);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Template  $template
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Template  $template
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request)
	{

	}


}
