<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\GeneralTrait;
use App\Models\Asset;
use App\Models\Category;
use App\Models\Company;
use App\Models\Inspection;
use App\Models\Module;
use App\Models\ModuleField;
use App\Models\Site;
use App\Models\SubSite;
use App\Models\SubSiteUser;
use App\Models\SubSiteUserPermission;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class DashboardController extends Controller
{
    use GeneralTrait;
    private $subSiteArray = [];
    private $timeZone = 'UTC';

    public function getFilterOptions(Request $request)
    {
        try {
            \Config::set('constant.timeZone', $this->timeZone);
            $filters = [];
            if (Auth::user()->roles->first()->name == 'Super Admin') {
                $filters = $this->getSuperAdminFilterOptions();
            } else if (Auth::user()->roles->first()->name == 'Admin') {
                $filters = $this->getClientAdminFilterOptions();
            } else if (Auth::user()->roles->first()->name == 'User') {
                $filters = $this->getSiteManagerFilterOptions();
            }
            $responseData = $filters;
            return $this->returnData('data', $responseData);
        } catch (\Exception $e) {
            return $this->returnError(500, $e->getMessage());
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    public function getSuperAdminFilterOptions()
    {
        // Get all the company list with all locations and areas
        $companies = Company::has('admins')->select('id as value', 'name as label');

        $clientArr = $companies->pluck('value')->toArray();
        $clientAdmins = $companies->orderBy('name')->get();

        // Get all sites created by all the client admins
        // $sites = Site::select('site_id as value', 'site_name as label', 'created_by as client')->whereIn('created_by', $clientArr);
        $sites = Site::select('site_id as value', 'site_name as label', 'company_id as client')->whereIn('company_id', $clientArr);
        $sitesArr = $sites->pluck('value')->toArray();
        $sitesOptions = $sites->orderBy('site_name')->get();

        // Get all the subsites of all the sites
        $subsites = SubSite::select('sub_site_id as value', 'sub_site_name as label', 'site_id as location', 'company_id as client')->whereIn('site_id', $sitesArr)->orderBy('sub_site_name')->get();

        $allC = [
            'value' => 0,
            'label' => 'All Companies',
            'client' => 0,
            'location' => 0,
        ];
        $allL = [
            'value' => 0,
            'label' => 'All Locations',
            'client' => 0,
            'location' => 0,
        ];
        $allA = [
            'value' => 0,
            'label' => 'All Areas',
            'client' => 0,
            'location' => 0,
        ];

        $clientAdmins->prepend($allC);
        $sitesOptions->prepend($allL);
        $subsites->prepend($allA);

        return [
            'clientAdmins' => $clientAdmins,
            'locations'    => $sitesOptions,
            'areas'        => $subsites,
        ];
    }

    public function getClientAdminFilterOptions()
    {
        // Get the user instance of login client admin
        $client = Auth::user();
        $company = Company::find($client->company_id);

        // Get all sites created by the login client admins
        $sites = Site::select('site_id as value', 'site_name as label', 'company_id as client')->where('company_id', $client->company_id);
        $sitesArr = $sites->pluck('value')->toArray();
        $sitesOptions = $sites->get();


        // Get all the subSites of all the sites
        $subSites = SubSite::select('sub_site_id as value', 'sub_site_name as label', 'site_id as location', 'company_id as client')->whereIn('site_id', $sitesArr)->get();

        $allLocation = [
            'value' => 0,
            'label' => 'All Locations',
            'client' => $company->id,
            // 'company' => $client->company_id,
            'location' => 0,
        ];

        $allArea = [
            'value' => 0,
            'label' => 'All Areas',
            'client' => $company->id,
            // 'company' => $client->company_id,
            'location' => 0,
        ];

        $clientAdmins = [
            [
                'value' => $company->id,
                'label' => $company->name,
                'client' => $company->id,
                'location' => 0,
            ]
        ];

        $sitesOptions->prepend($allLocation);
        $subSites->prepend($allArea);

        return [
            'clientAdmins' => $clientAdmins,
            'locations'    => $sitesOptions,
            'areas'        => $subSites,
        ];
    }

    public function getSiteManagerFilterOptions()
    {
        // Get the user instance of login User
        $siteManager = Auth::user();

        // Get all the sites that are permitted to the login User
        $sites = $this->getSiteAndSubSiteList($siteManager->id)['sites'];

        // Get all the subsites of that are permitted to the login User
        $subsites = $this->getSiteAndSubSiteList($siteManager->id)['subSites'];

        $clientAdmins = [
            [
                'value' => $siteManager->id,
                'label' => $siteManager->first_name . ' ' . $siteManager->last_name,
                'client' => $siteManager->id,
                'location' => 0,
            ]
        ];

        $siteOptions[] = [
            'value' => 0,
            'label' => 'All Locations',
            'client' => $siteManager->id,
            'location' => 0,
        ];
        foreach ($sites as $site) {
            $siteOptions[] = [
                'value' => $site['value'],
                'label' => $site['label'],
                'client' => $siteManager->id,
            ];
        }

        $subsiteOptions[] = [
            'value' => 0,
            'label' => 'All Areas',
            'location' => 0,
        ];
        foreach ($subsites as $subsite) {
            $subsiteOptions[] = [
                'value' => $subsite['value'],
                'label' => $subsite['label'],
                'location' => $subsite['site'],
                'client' => $siteManager->id,
            ];
        }

        return [
            'clientAdmins' => $clientAdmins,
            'locations'    => $siteOptions,
            'areas'        => $subsiteOptions,
        ];
    }

    /*
        NOTE :: Here clientId is a companyID 
    */
    public function index(Request $request)
    {
        try {
            $this->subSiteArray = $request->areaArr;
            $this->timeZone = $request->timeZone;
            \Config::set('constant.timeZone', $this->timeZone);
            $responseData = [
                'summary'                     => $this->generateAssetSummary(),
                'currentInspectionCompliance' => $this->generateCurrentInspectionCompliance(),
                'lastInspection'              => $this->generateLastInspection(),
                'lastRegisteredAsset'         => $this->generateLastRegisteredAsset(),
                'lastFailedAsset'             => $this->generateLastFailedAsset(),
            ];
            if (Auth::user()->roles->first()->name == 'Super Admin' || Auth::user()->roles->first()->name == 'Admin') {
                $responseData['detailedInfo'] = $this->generateDetailedInfo($request->clientId, $request->locationId, $request->areaId);
            }

            return $this->returnData('data', $responseData);
        } catch (\Exception $e) {
            return $this->returnError(500, $e->getMessage());
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
    
    private function generateAssetSummary()
    {
        return [
            'info'                    => $this->generateInfo(),
            'assets_due_to_expire'    => $this->assetsDueToExpire(),
            'assets_inspected'        => $this->assetsInspected(),
            'assets_registered'       => $this->assetsRegistered(),
            'total_assets_inspection' => $this->totalAssetsInspection(),
        ];
    }
    private function generateCurrentInspectionCompliance()
    {
        $currentInspection = $this->getInspectionData('current');
        $overdueInspection = $this->getInspectionData('overdue');
        $inspectionDue     = $this->getInspectionData('due');

        $inspectionDue24hrs  = $this->getInspectionData('24hrs');
        $inspectionDue7days  = $this->getInspectionData('7days');
        $inspectionDue30days = $this->getInspectionData('30days');

        return [
            'current_inspections'   => $currentInspection,
            'overdue_inspections'   => $overdueInspection,
            'inspections_due'       => $inspectionDue,
            'inspection_due_24hrs'  => $inspectionDue24hrs,
            'inspection_due_7days'  => $inspectionDue7days,
            'inspection_due_30days' => $inspectionDue30days,
        ];
    }
    private function getInspectionData($status)
    {
        $assets = Asset::whereNull('deleted_at');
        if (count($this->subSiteArray) > 0) {
            $assets = $assets->whereIn('asset_sub_site_id', $this->subSiteArray);
        }
        $assets = $assets->whereHas('module', function ($query) {
            $query->whereNotNull('module_inspection_schedule');
        })->with('inspections', function ($query) {
            $query->latest();
        })->get()->filter(function ($asset) use ($status) {
            if ($status == 'current') {
                // ONE cases possible with two Condition (AND)
                // 1. Inspection is done (AND) current date - Inspection done date <= InspectionSchedule day value
                if ($asset->asset_q_color == NULL && $asset->inspections->count() > 0 && Carbon::parse($asset->inspections[0]->created_at)->diffInDays(Carbon::now()) <= $asset->module->module_inspection_schedule) {
                    return $asset;
                }

                // RGBY - Inspection is done (AND) Inspection expiry date <= current date value
                // if ($asset->asset_q_color != NULL && $asset->inspections->count() > 0 && $asset->inspections()->whereDate('inspection_q_expiry', '<=', Carbon::now()->toDateString())->count() > 0) {
                //     return $asset;
                // }
                if ($asset->asset_q_color != NULL && $asset->inspections->count() > 0) {
                    $latestInspection = $asset->inspections()->latest()->first();
                    if ($latestInspection && $latestInspection->created_at <= $latestInspection->inspection_q_expiry) {
                        return $asset;
                    }
                    return $asset;
                }
            } else if ($status == 'overdue') {
                // TWO cases possible
                // 1. Inspection is done (AND) current date - Inspection done date > InspectionSchedult day value
                // 2. Inspection is not done (AND) current date - Asset created date > InspectionSchedult day value
                if ($asset->asset_q_color == NULL && $asset->inspections->count() > 0 && Carbon::parse($asset->inspections[0]->created_at)->diffInDays(Carbon::now()) > $asset->module->module_inspection_schedule) {
                    return $asset;
                } else if ($asset->asset_q_color == NULL && $asset->inspections->count() == 0 && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) > $asset->module->module_inspection_schedule) {
                    return $asset;
                }

                // RGBY - TWO cases possible
                // 1. Inspection is done (AND) inspection_q_expiry > current day value
                // 2. Inspection is not done (AND) inspection_q_expiry > current day value
                if ($asset->asset_q_color != null && $asset->inspections->count() > 0 ) {
                    $latestInspection = $asset->inspections()->latest()->first();
                    if ($latestInspection && $latestInspection->inspection_q_expiry <= Carbon::now()->toDateString()) {
                        return $asset;
                    }
                }  else if ($asset->asset_q_color != null && $asset->inspections->count() == 0) {
                    if(Carbon::now() > $this->getQcolorExpiry($asset->asset_q_color)) {
                        return $asset;
                    }
                }
            } else if ($status == 'due') {
                // ONE cases possible
                // 1. Inspection is not done (AND) current date - Asset created date <= InspectionSchedult day value
                if ($asset->asset_q_color == NULL && $asset->inspections->count() == 0 && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) <= $asset->module->module_inspection_schedule) {
                    return $asset;
                }

                // RGBY color selected
                if($asset->asset_q_color != NULL && $asset->inspections->count() == 0) {
                    return $asset;
                }
            } else if($status == '24hrs') {
                // In this case the "inspection due" in 24 hrs will be listed
                // New Assets inspection due in 24 hrs
                // Old Assets inspection due in 24 hrs
                if ($asset->asset_q_color == NULL && $asset->inspections->count() == 0 && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) <= $asset->module->module_inspection_schedule && Carbon::parse($asset->created_at)->diffInDays(Carbon::now())<=1) {
                    return $asset;
                } elseif($asset->asset_q_color == NULL && $asset->inspections->count() > 0 && $asset->module->module_inspection_schedule - Carbon::parse($asset->inspections[0]->created_at)->diffInDays(Carbon::now()) <= 1) {
                    return $asset;
                }

                // RGBY - In this case the "inspection due" in 24 hrs will be listed
                // New Assets inspection due in 24 hrs - RGBY
                // Old Assets inspection due in 24 hrs - RGBY
                if ($asset->asset_q_color != NULL && $asset->inspections->count() == 0) {
                    return $asset;
                } elseif($asset->asset_q_color != NULL &&  $asset->inspections->count() > 0 && Carbon::parse($asset->inspections[0]->inspection_q_expiry)->diffInDays(Carbon::now()) <= 1) {
                    return $asset;
                }
            } elseif($status == '7days') {
                // In this case the "inspection due" in 7 days will be listed
                if ($asset->asset_q_color == NULL && $asset->inspections->count() == 0 && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) <= $asset->module->module_inspection_schedule && Carbon::parse($asset->created_at)->diffInDays(Carbon::now())<=7) {
                    return $asset;
                } elseif($asset->asset_q_color == NULL && $asset->inspections->count() > 0 && $asset->module->module_inspection_schedule - Carbon::parse($asset->inspections[0]->created_at)->diffInDays(Carbon::now()) <= 7) {
                    return $asset;
                }

                // RGBY - In this case the "inspection due" in 7 days will be listed
                if ($asset->asset_q_color != NULL && $asset->inspections->count() == 0) {
                    return $asset;
                } elseif($asset->asset_q_color != NULL && $asset->inspections->count() > 0 && Carbon::parse($asset->inspections[0]->inspection_q_expiry)->diffInDays(Carbon::now()) <= 7) {
                    return $asset;
                }
            } elseif($status == '30days') {
                // In this case the "inspection due" in 30 days will be listed
                if ($asset->asset_q_color == NULL && $asset->inspections->count() == 0 && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) <= $asset->module->module_inspection_schedule && Carbon::parse($asset->created_at)->diffInDays(Carbon::now())<=30) {
                    return $asset;
                } elseif($asset->asset_q_color == NULL && $asset->inspections->count() > 0 && $asset->module->module_inspection_schedule - Carbon::parse($asset->inspections[0]->created_at)->diffInDays(Carbon::now()) <= 30) {
                    return $asset;
                }

                // RGBY - In this case the "inspection due" in 30 days will be listed
                if ($asset->asset_q_color != NULL && $asset->inspections->count() == 0) {
                    return $asset;
                } elseif( $asset->asset_q_color != NULL && $asset->inspections->count() > 0 && Carbon::parse($asset->inspections[0]->inspection_q_expiry)->diffInDays(Carbon::now()) <= 30) {
                    return $asset;
                }
            }
        });
        return $assets->count();
    }
    private function generateLastInspection()
    {
        $lastInspection = Inspection::join('assets', 'assets.asset_id', 'inspections.inspection_asset_id')->whereIn('assets.asset_sub_site_id', $this->subSiteArray)->orderBy('inspections.created_at', 'desc')->first();
        if (!$lastInspection) {
            return [];
        }
        $lastInspectionDate = Inspection::find($lastInspection->inspection_id)->created_at;
        $lastInspectionDate = $this->convertTimeZone('UTC', $this->timeZone, $lastInspectionDate);

        return [
            'date' => Carbon::parse($lastInspectionDate)->dayName . ', ' . Carbon::parse($lastInspectionDate)->format('F d, Y'),
            'time' => Carbon::parse($lastInspectionDate)->format('h:i A'),
            'asset_id' => $lastInspection->asset_id,
            'asset_uid' => $lastInspection->asset_uid,
            'status' => $lastInspection->inspection_result,
            'latitude' => $lastInspection->asset_latitude,
            'longitude' => $lastInspection->asset_longitude,
            'last_inspection_photo' => $lastInspection->inspection_image_1 ? $this->getStorageURL($lastInspection->inspection_image_1) : null,
        ];
    }
    private function generateLastRegisteredAsset()
    {
        $lastRegisteredAsset = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)->orderBy('created_at', 'desc')->first();
        if (!$lastRegisteredAsset) {
            return [];
        }
        $lastRegisteredAssetInspection = $lastRegisteredAsset->inspections->last();
        $lastRegisteredAssetDate = $lastRegisteredAsset->created_at;
        $lastRegisteredAssetDate = $this->convertTimeZone('UTC', $this->timeZone, $lastRegisteredAssetDate);
        return [
            'date' => Carbon::parse($lastRegisteredAssetDate)->dayName . ', ' . Carbon::parse($lastRegisteredAssetDate)->format('F d, Y'),
            'time' => Carbon::parse($lastRegisteredAssetDate)->format('h:i A'),
            'asset_id' => $lastRegisteredAsset->asset_id,
            'asset_uid' => $lastRegisteredAsset->asset_uid,
            'status' => $lastRegisteredAssetInspection ? $lastRegisteredAssetInspection->inspection_result : null,
            'latitude' => $lastRegisteredAsset->asset_latitude,
            'longitude' => $lastRegisteredAsset->asset_longitude,
            'asset_photo' => $lastRegisteredAsset && $lastRegisteredAsset->asset_image ? $this->getStorageURL($lastRegisteredAsset->asset_image) : null,
        ];
    }
    private function generateLastFailedAsset()
    {
        $lastFailed = Inspection::join('assets', 'assets.asset_id', 'inspections.inspection_asset_id')->whereIn('assets.asset_sub_site_id', $this->subSiteArray)->where('inspection_result', 'fail')->orderBy('inspections.created_at', 'desc')->first();

        if (!$lastFailed) {
            return [];
        }
        $lastFailedDate = Inspection::find($lastFailed->inspection_id)->created_at;
        $lastFailedDate = $this->convertTimeZone('UTC', $this->timeZone, $lastFailedDate);
        return [
            'date' => Carbon::parse($lastFailedDate)->dayName . ', ' . Carbon::parse($lastFailedDate)->format('F d, Y'),
            'time' => Carbon::parse($lastFailedDate)->format('h:i A'),
            'asset_id' => $lastFailed->asset_id,
            'asset_uid' => $lastFailed->asset_uid,
            'status' => $lastFailed->inspection_result,
            'latitude' => $lastFailed->asset_latitude,
            'longitude' => $lastFailed->asset_longitude,
            'last_inspection_photo' => $lastFailed->inspection_image_1 ? $this->getStorageURL($lastFailed->inspection_image_1) : null,
        ];
    }
    private function generateDetailedInfo($clientId, $siteId, $subSiteId)
    {
        $activeClients                = null;
        $inactiveClients              = null;
        $last_location_added          = [];
        $last_client_admin_user_added = [];
        $activeSites                  = null;
        $activeSubSites               = null;
        $activeAdminUsers             = null;
        $activeUsers                  = null;
        $activeCategories             = null;
        $activeModules                = null;
        $inactiveSites                = null;
        $inactiveSubSites             = null;
        $inactiveAdminUsers           = null;
        $inactiveUsers                = null;
        $inactiveCategories           = null;
        $inactiveModules              = null;

        if (Auth::user()->roles->first()->name == 'Super Admin') {
            // NOTE :: After company integration we change the code
            // $activeClients = User::where('status', 'Y')->whereHas('roles', function ($query) {
            //     $query->where('name', 'Admin');
            // })->count();
            // $inactiveClients = User::where('status', 'N')->whereHas('roles', function ($query) {
            //     $query->where('name', 'Admin');
            // })->count();

            $activeClients   = Company::where('status', 'Y')->count();
            $inactiveClients = Company::where('status', 'N')->count();

            $last_location = Site::orderBy('created_at', 'desc')->first();

            if ($last_location) {
                $last_location_added = [
                    'date'                 => $this->getFormattedDate($this->convertTimeZone('UTC', $this->timeZone, $last_location->created_at)),
                    'time'                 => $this->getFormattedTime($this->convertTimeZone('UTC', $this->timeZone, $last_location->created_at)),
                    'client_name'          => $last_location->company->name,
                    'created_by_user_name' => $last_location->createdBy->first_name . ' ' .  $last_location->createdBy->last_name,
                    'location_name'        => $last_location->site_name,
                    'location_count'       => Site::where('created_by', $last_location->createdBy->id)->count(),
                    'area_count'           => SubSite::where('created_by', $last_location->createdBy->id)->count(),
                ];
            }

            // When Super Admin is login user then the data here will be showing all the stats for the Super Admin.
            $activeAdminUsers = User::where('status', 'Y')->whereHas('roles', function ($query) {
                $query->where('name', 'Admin');
            })->count();

            $last_client_admin_user = User::whereHas('roles', function ($query) {
                $query->where('name', 'Admin');
            })->orderBy('created_at', 'desc')->first();
            $last_client_admin_user_added = [
                'date'                 => $this->getFormattedDate($this->convertTimeZone('UTC', $this->timeZone, $last_client_admin_user->created_at)),
                'time'                 => $this->getFormattedTime($this->convertTimeZone('UTC', $this->timeZone, $last_client_admin_user->created_at)),
                'client_name'          => $last_client_admin_user->companyData->name,
                'created_by_user_name' => $last_client_admin_user->createdBy ? $last_client_admin_user->createdBy->first_name . ' ' .  $last_client_admin_user->createdBy->last_name : '',
                'client_admin_name'    => $last_client_admin_user->first_name . ' ' .  $last_client_admin_user->last_name,
            ];

            $inactiveAdminUsers = User::where('status', 'N')->whereHas('roles', function ($query) {
                $query->where('name', 'Admin');
            })->count();
        }

        if ($clientId == 0) {

            $allClientAdminId = User::whereHas('roles', function ($query) {
                $query->where('name', 'Admin');
            })->pluck('id')->toArray();
            // for all clients...
            $activeSites = Site::where('site_status', 'Y')->count();
            $activeSubSites = SubSite::where('sub_site_status', 'Y')->whereHas('site', function ($query) {
                $query->whereNull('deleted_at');
            })->count();
            $activeUsers = User::where('status', 'Y')->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('roles')
                    ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->whereColumn('users.id', 'model_has_roles.model_id')
                    ->where('name', 'User')
                    ->whereNotExists(function ($query) {
                        $query->select(DB::raw(1))
                                ->from('roles')
                                ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                                ->whereColumn('users.id', 'model_has_roles.model_id')
                                ->where('name', 'Admin');
                    });
            })->count();
            $activeCategories = Category::whereIn('category_user_id', $allClientAdminId)->where('category_status', 'Y')->count();
            $activeModules = Module::whereIn('module_user_id', $allClientAdminId)->where('module_status', 'Y')->count();
            // $activeCategories = Category::where('category_status', 'Y')->count();
            // $activeModules = Module::where('module_status', 'Y')->count();

            $inactiveSites = Site::where('site_status', 'N')->count();
            $inactiveSubSites = SubSite::where('sub_site_status', 'N')->whereHas('site', function ($query) {
                $query->whereNull('deleted_at');
            })->count();
            $inactiveUsers = User::where('status', 'N')->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('roles')
                    ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->whereColumn('users.id', 'model_has_roles.model_id')
                    ->where('name', 'User')
                    ->whereNotExists(function ($query) {
                        $query->select(DB::raw(1))
                                ->from('roles')
                                ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                                ->whereColumn('users.id', 'model_has_roles.model_id')
                                ->where('name', 'Admin');
                    });
            })->count();
            $inactiveCategories = Category::whereIn('category_user_id', $allClientAdminId)->where('category_status', 'N')->count();
            $inactiveModules = Module::whereIn('module_user_id', $allClientAdminId)->where('module_status', 'N')->count();
        } else {
            $activeClients = User::where('status', 'Y')->where('company_id', $clientId)->whereHas('roles', function ($query) {
                $query->where('name', 'Admin');
            })->count();
            $inactiveClients = User::where('status', 'N')->where('company_id', $clientId)->whereHas('roles', function ($query) {
                $query->where('name', 'Admin');
            })->count();

            // for selected client admin...
            // last site/location added to that client admin common for every site - subsites | location-areas
            $last_location = Site::where('company_id', $clientId)->orderBy('created_at', 'desc')->first();
            
            if ($last_location) {
                $last_location_added = [
                    'date'                 => $this->getFormattedDate($last_location->created_at),
                    'time'                 => $this->getFormattedTime($last_location->created_at),
                    'client_name'          => $last_location->company->name,
                    'created_by_user_name' => $last_location->createdBy->first_name . ' ' .  $last_location->createdBy->last_name,
                    'location_name'        => $last_location->site_name,
                    'location_count'       => Site::where('created_by', $last_location->createdBy->id)->count(),
                    'area_count'           => SubSite::where('created_by', $last_location->createdBy->id)->count(),
                ];
            } else {
                $last_location_added = [];
            }
            $activeSites = Site::where('site_status', 'Y')->where('company_id', $clientId)->count();
            $activeSubSites = SubSite::where('sub_site_status', 'Y')->whereHas('site', function ($query) {
                $query->whereNull('deleted_at');
            })->where('company_id', $clientId)->count();

            $last_client_admin_user = User::whereHas('roles', function ($query) {
                $query->where('name', 'Admin');
            })->where('company_id', $clientId)->orderBy('created_at', 'desc')->first();
            
            $last_client_admin_user_added = [
                'date'                 => $this->getFormattedDate($this->convertTimeZone('UTC', $this->timeZone, $last_client_admin_user->created_at)),
                'time'                 => $this->getFormattedTime($this->convertTimeZone('UTC', $this->timeZone, $last_client_admin_user->created_at)),
                'client_name'          => $last_client_admin_user->companyData->name,
                'created_by_user_name' => $last_client_admin_user->createdBy ? $last_client_admin_user->createdBy->first_name . ' ' .  $last_client_admin_user->createdBy->last_name : '',
                'client_admin_name'    => $last_client_admin_user->first_name . ' ' .  $last_client_admin_user->last_name,
            ];

            // When CLient Admin is the login user then the record here will be the users who are the admin of the same company as the login user.
            $activeAdminUsers = User::where('status', 'Y')->where('company_id', Auth::user()->company_id)->whereHas('roles', function ($query) {
                $query->where('name', 'Admin');
            })->count();
            $activeUsers = User::where('status', 'Y')->where('company_id', $clientId)->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('roles')
                        ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                        ->whereColumn('users.id', 'model_has_roles.model_id')
                        ->where('name', 'User')
                        ->whereNotExists(function ($query) {
                            $query->select(DB::raw(1))
                                    ->from('roles')
                                    ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                                    ->whereColumn('users.id', 'model_has_roles.model_id')
                                    ->where('name', 'Admin');
                        });
                })->count();
            $activeCategories = Category::where('category_status', 'Y')->where('company_id', $clientId)->count();
            $activeModules = Module::where('module_status', 'Y')->where('company_id', $clientId)->count();

            $inactiveSites = Site::where('site_status', 'N')->where('company_id', $clientId)->count();
            $inactiveSubSites = SubSite::where('sub_site_status', 'N')->whereHas('site', function ($query) {
                $query->whereNull('deleted_at');
            })->where('company_id', $clientId)->count();
            $inactiveAdminUsers = User::where('status', 'N')->where('company_id', Auth::user()->company_id)->whereHas('roles', function ($query) {
                $query->where('name', 'Admin');
            })->count();
            $inactiveUsers = User::where('status', 'N')->where('company_id', $clientId)->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('roles')
                    ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->whereColumn('users.id', 'model_has_roles.model_id')
                    ->where('name', 'User')
                    ->whereNotExists(function ($query) {
                        $query->select(DB::raw(1))
                                ->from('roles')
                                ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                                ->whereColumn('users.id', 'model_has_roles.model_id')
                                ->where('name', 'Admin');
                    });
            })->count();
            $inactiveCategories = Category::where('category_status', 'N')->where('company_id', $clientId)->count();
            $inactiveModules = Module::where('module_status', 'N')->where('company_id', $clientId)->count();



            // for cases an site is !0 that is only one subsite is selected then changes will be reflected in Active Users, Active Categories, Active Modules and Inactive Users, Inactive Categories, Inactive Modules
            if ($siteId != 0) {
                // Here all operation will be done for the selected subsiteArray.
                $activeUsers = SubSiteUser::select('ssu_user_id')->whereIn('ssu_sub_site_id', $this->subSiteArray)->whereHas('siteManager', function ($query) {
                    $query->where('status', 'Y');
                })->distinct('ssu_user_id')->count();
                $inactiveUsers = SubSiteUser::select('ssu_user_id')->whereIn('ssu_sub_site_id', $this->subSiteArray)->whereHas('siteManager', function ($query) {
                    $query->where('status', 'N');
                })->distinct('ssu_user_id')->count();

                $activeCategories = SubSiteUserPermission::select('ssup_category_id')->whereIn('ssup_sub_site_id', $this->subSiteArray)->whereHas('category', function ($query) {
                    $query->where('category_status', 'Y');
                })->distinct('ssup_category_id')->count();
                $inactiveCategories = SubSiteUserPermission::select('ssup_category_id')->whereIn('ssup_sub_site_id', $this->subSiteArray)->whereHas('category', function ($query) {
                    $query->where('category_status', 'N');
                })->distinct('ssup_category_id')->count();

                $activeModules = SubSiteUserPermission::select('ssup_module_id')->whereIn('ssup_sub_site_id', $this->subSiteArray)->whereHas('module', function ($query) {
                    $query->where('module_status', 'Y');
                })->distinct('ssup_module_id')->count();
                $inactiveModules = SubSiteUserPermission::select('ssup_module_id')->whereIn('ssup_sub_site_id', $this->subSiteArray)->whereHas('module', function ($query) {
                    $query->where('module_status', 'N');
                })->distinct('ssup_module_id')->count();
            }
        }
        if (Auth::user()->roles->first()->name != 'Super Admin') {
            // Hide the details for the client admin user and User user
            $last_location_added = [];
            $last_client_admin_user_added = [];
            $activeClients = null;
            $inactiveClients = null;
        }

        return [
            'active_client'                => $activeClients,
            'inactive_client'              => $inactiveClients,
            'last_location_added'          => $last_location_added,
            'last_client_admin_user_added' => $last_client_admin_user_added,
            'active_locations'             => $activeSites,
            'active_areas'                 => $activeSubSites,
            'active_admin_users'           => $activeAdminUsers,
            'active_users'                 => $activeUsers,
            'active_categories'            => $activeCategories,
            'active_modules'               => $activeModules,
            'inactive_locations'           => $inactiveSites,
            'inactive_areas'               => $inactiveSubSites,
            'inactive_admin_users'         => $inactiveAdminUsers,
            'inactive_users'               => $inactiveUsers,
            'inactive_categories'          => $inactiveCategories,
            'inactive_modules'             => $inactiveModules,
        ];
    }

    private function generateInfo()
    {
        if (sizeOf($this->subSiteArray) > 0) {
            return [
                'pass'                 => $this->getGeneralData('pass'),
                'fail'                 => $this->getGeneralData('fail'),
                'expired'              => $this->expiredAssetCount(),
                'maintanance_required' => $this->getGeneralData('maintenance_required'),
                'inspection_due'       => $this->getGeneralData('due'),
                'total'                => $this->getGeneralData('total'),
            ];
        } else {
            return [
                'pass'                 => 0,
                'fail'                 => 0,
                'expired'              => 0,
                'maintanance_required' => 0,
                'inspection_due'       => 0,
                'total'                => 0,
            ];
        }

    }
    private function expiredAssetCount()
    {
        $mfIds = $this->getExpiryMFids();
        $assets = Asset::whereNull('deleted_at');
        if (count($this->subSiteArray) > 0) {
            $assets = $assets->whereIn('asset_sub_site_id', $this->subSiteArray);
        }
        $assets = $assets->whereHas('values', function ($query) use ($mfIds) {
            $query->whereIn('cmfv_mf_id', $mfIds)->whereDate('cmfv_value_date', '<=', Carbon::now());
        })->count();

        return $assets;
    }
    private function getGeneralData($result)
    {
        $assets = Asset::with(['module'])
                    ->whereNull('deleted_at')
                    ->whereHas('module', function ($query){
                        $query->where('module_status', 'Y');
                        $query->whereNull('deleted_at');
                    });

        if (count($this->subSiteArray) > 0) {
            $assets = Asset::with(['module'])
                        ->whereIn('asset_sub_site_id', $this->subSiteArray)
                        ->whereHas('module', function ($query){
                            $query->where('module_status', 'Y');
                            $query->whereNull('deleted_at');
                        });
        }

        $assets = $assets->with('inspections', function ($query1) {
            $query1->latest();
        })->get()->filter(function ($asset) use ($result) {
            if ($result == 'total') {
                return $asset;
            } elseif ($result != 'due' && $asset->inspections->count() > 0 && $asset->inspections->first()->inspection_result == $result) {
                return $asset;
            } elseif ($result == 'due' && $asset->inspections->count() == 0) {
                return $asset;
            }
        });
        $currentRecord = $assets;
        $count = $assets->count();
        if($result == 'fail' || $result == 'maintenance_required') {
            $newAssets = $assets->filter(function ($asset) {
                $inspection = $asset->inspections->first();
                if($inspection->is_notified == 0) {
                    return $asset;
                }
            });
            if($newAssets->count()>0) {
                // Have to Send Mail Since new Asset is found in the collection.
                $previousRecord = $assets->filter(function ($asset) {
                    $inspection = $asset->inspections->first();
                    if($inspection->is_notified == 1) {
                        return $asset;
                    }
                });
                $title = "Assets Requiring Maintenance";
                if($result=='fail') {
                    $title = "Failed Assets Details";
                }
                // Initiating mail method from EMailNotification COntroller.
                $emailNotification = new EmailNotificationController();
                $emailNotification->sendNotificationMail($emailNotification->generateAssetListData($previousRecord), $emailNotification->generateAssetListData($currentRecord), $title, 'inspections');
            }

        }
        return $count;
    }
    private function assetsDueToExpire()
    {
        $mfIds = $this->getExpiryMFids();

        $assets24hrs = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->whereHas('values', function ($query) use ($mfIds) {
                $query->whereIn('cmfv_mf_id', $mfIds)->whereDate('cmfv_value_date', '>=', Carbon::now())->whereDate('cmfv_value_date', '<=', Carbon::now()->addDay());
            });
        $assets7days = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->whereHas('values', function ($query) use ($mfIds) {
                $query->whereIn('cmfv_mf_id', $mfIds)->whereDate('cmfv_value_date', '>=', Carbon::now())->whereDate('cmfv_value_date', '<=', Carbon::now()->addDays(7));
            });
        $assets30days = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->whereHas('values', function ($query) use ($mfIds) {
                $query->whereIn('cmfv_mf_id', $mfIds)->whereDate('cmfv_value_date', '>=', Carbon::now())->whereDate('cmfv_value_date', '<=', Carbon::now()->addDays(30));
            });
        $assets90days = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->whereHas('values', function ($query) use ($mfIds) {
                $query->whereIn('cmfv_mf_id', $mfIds)->whereDate('cmfv_value_date', '>=', Carbon::now())->whereDate('cmfv_value_date', '<=', Carbon::now()->addDays(90));
            });
        $assets180days = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->whereHas('values', function ($query) use ($mfIds) {
                $query->whereIn('cmfv_mf_id', $mfIds)->whereDate('cmfv_value_date', '>=', Carbon::now())->whereDate('cmfv_value_date', '<=', Carbon::now()->addDays(180));
            });

        //
        // Sending mail notification for the asset due to expire cards.
        $emailNotification = new EmailNotificationController();
        $emailNotification->assetToExpire(1, $this->subSiteArray);
        $emailNotification->assetToExpire(7, $this->subSiteArray);
        $emailNotification->assetToExpire(30, $this->subSiteArray);
        $emailNotification->assetToExpire(90, $this->subSiteArray);

        return [
            'in24hrs'     => $assets24hrs->count(),
            'in7days'     => $assets7days->count(),
            'in30days'    => $assets30days->count(),
            'in90days'    => $assets90days->count(),
            'in_this_year' => $assets180days->count()
        ];
    }
    private function assetsInspected()
    {
        // Asset Inspection count in last 24 hours, 7 days, 30 days, 90 days, this year. considering only one inspection (last) per asset
        $assets24hrs = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->whereHas('inspections', function ($query) {
                $query->where('created_at', '>=', Carbon::now()->subDay());
            })->count();
        $assets7days = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->whereHas('inspections', function ($query) {
                $query->where('created_at', '>=', Carbon::now()->subDays(7));
            })->count();
        $assets30days = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->whereHas('inspections', function ($query) {
                $query->where('created_at', '>=', Carbon::now()->subDays(30));
            })->count();
        $assets90days = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->whereHas('inspections', function ($query) {
                $query->where('created_at', '>=', Carbon::now()->subDays(90));
            })->count();
        $assetsThisYear = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->whereHas('inspections', function ($query) {
                // $query->where('created_at', '>=', Carbon::now()->subDays(180));
                $query->where('created_at', '>=', Carbon::now()->startOfYear());
            })->count();

        return [
            'in24hrs'     => $assets24hrs,
            'in7days'     => $assets7days,
            'in30days'    => $assets30days,
            'in90days'    => $assets90days,
            'in_this_year' => $assetsThisYear
        ];
    }
    private function assetsRegistered()
    {
        $assets24hrs = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->where('created_at', '>=', Carbon::now()->subDay())
            ->count();
        $assets7days = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->where('created_at', '>=', Carbon::now()->subDays(7))
            ->count();
        $assets30days = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->where('created_at', '>=', Carbon::now()->subDays(30))
            ->count();
        $assets90days = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->where('created_at', '>=', Carbon::now()->subDays(90))
            ->count();
        $assetsThisYear = Asset::whereIn('asset_sub_site_id', $this->subSiteArray)
            ->where('created_at', '>=', Carbon::now()->startOfYear())
            ->count();
        return [
            'in24hrs'     => $assets24hrs,
            'in7days'     => $assets7days,
            'in30days'    => $assets30days,
            'in90days'    => $assets90days,
            'in_this_year' => $assetsThisYear
        ];
    }
    private function totalAssetsInspection()
    {
        // Count of Inspections in last 24 hours, 7 days, 30 days, 90 days, this year where assets subsite id is in $this->subSiteArray
        $assets24hrs = Inspection::join('assets', 'assets.asset_id', 'inspections.inspection_asset_id')
            ->whereIn('assets.asset_sub_site_id', $this->subSiteArray)
            ->where('inspections.created_at', '>=', Carbon::now()->subDay())
            ->count();
        $assets7days = Inspection::join('assets', 'assets.asset_id', 'inspections.inspection_asset_id')
            ->whereIn('assets.asset_sub_site_id', $this->subSiteArray)
            ->where('inspections.created_at', '>=', Carbon::now()->subDays(7))
            ->count();
        $assets30days = Inspection::join('assets', 'assets.asset_id', 'inspections.inspection_asset_id')
            ->whereIn('assets.asset_sub_site_id', $this->subSiteArray)
            ->where('inspections.created_at', '>=', Carbon::now()->subDays(30))
            ->count();
        $assets90days = Inspection::join('assets', 'assets.asset_id', 'inspections.inspection_asset_id')
            ->whereIn('assets.asset_sub_site_id', $this->subSiteArray)
            ->where('inspections.created_at', '>=', Carbon::now()->subDays(90))
            ->count();
        $assetsThisYear = Inspection::join('assets', 'assets.asset_id', 'inspections.inspection_asset_id')
            ->whereIn('assets.asset_sub_site_id', $this->subSiteArray)
            ->where('inspections.created_at', '>=', Carbon::now()->startOfYear())
            ->count();

        return [
            'in24hrs'     => $assets24hrs,
            'in7days'     => $assets7days,
            'in30days'    => $assets30days,
            'in90days'    => $assets90days,
            'in_this_year' => $assetsThisYear
        ];
    }

    /**
     * Get global search asset data
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getGlobalAssetSearch(Request $request)
    {
        try {
            $clientId     = $request->client;
            $siteId       = $request->location;
            $subSiteId    = $request->area;
            $subSiteArray = ($request->areaArr == 0) ? [] : $request->areaArr;
            $search       = $request->search;

            if ($search != '') {
                $assetData = Asset::select('assets.asset_id', 'assets.asset_nfc_uuid', 'assets.asset_module_id', 'assets.asset_uid', 'asset_site_id', 'asset_sub_site_id', 'assets.asset_image', 'assets.asset_latitude', 'assets.asset_longitude')
                                ->with(['values', 'module', 'site' => function ($query) {
                                        $query->select('site_id', 'site_name');
                                    }, 'subSite' => function ($query) {
                                        $query->select('sub_site_id', 'sub_site_name');
                                    }])
                                ->whereHas('site', function ($query) {
                                    $query->where('site_status', 'Y');
                                    $query->whereNull('deleted_at');
                                })
                                ->whereHas('subSite', function ($query) {
                                    $query->where('sub_site_status', 'Y');
                                    $query->whereNull('deleted_at');
                                })
                                ->whereHas('module', function ($query){
                                    $query->where('module_status', 'Y');
                                    $query->whereNull('deleted_at');
                                })
                                ->whereNull('deleted_at');

                $clientSiteIdArray = array();
                if ($clientId > 0) {
                    //If end user id then find out client id base on his user id
                    if(Auth::user()->roles->first()->name == 'User') {
                        $clientId = Auth::user()->company_id;
                    }

                    $clientSiteIdArray = Site::where('site_status', 'Y')->where('company_id', $clientId)->get('site_id')->toArray();

                    // If client have not subsites, At that case no need to check in asset
                    if(empty($clientSiteIdArray)) {
                        return $this->returnError(204, 'Oppps! Asset not found, Please try again...');
                    }
                }

                // If client select - show all area data
                if (sizeOf($clientSiteIdArray) > 0) {
                    $siteIdArray = array_column($clientSiteIdArray, 'site_id');
                    $assetData = $assetData->whereIn('asset_site_id', $siteIdArray);
                }

                // If location select - show only this location area data
                if (sizeOf($subSiteArray) > 0) {
                    $assetData = $assetData->whereIn('asset_sub_site_id', $subSiteArray);
                }

                // Filter data with specific site id
                if ($siteId > 0) {
                    $assetData = $assetData->where('asset_site_id', $siteId);
                }

                // Filter data with specific sub site id
                if ($subSiteId > 0) {
                    $assetData = $assetData->where('asset_sub_site_id', $subSiteId);
                }

                // For end user check user module permission
                if(Auth::user()->roles->first()->name == 'User') {
                    // Get module id based on user permission
                    $moduleIdArray = SubSiteUserPermission::where('ssup_user_id', Auth::id())->whereNull('deleted_at')->groupBy('ssup_module_id')->pluck('ssup_module_id')->toArray();
                    $assetData = $assetData->whereIn('asset_module_id', $moduleIdArray);
                }

                $assetData = $assetData->where(function ($masterQuery) use ($search) {
                    $masterQuery->orWhere('assets.asset_uid', 'like', '%' . $search . '%');
                    $masterQuery->orWhereHas('values', function ($query) use ($search) {
                        $query->where('cmfv_value_text', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_mediumtext', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_longtext', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_int', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_double', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_decimal', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_char', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_varchar', 'like', '%' . $search . '%');
                    });
                });

                // Get asset data
                $assetList = $assetData->get();
                $assetCount = $assetList->count();

                if ($assetCount > 0) {
                    $data = array(
                        'total' => $assetCount,
                        'result' => array()
                    );

                    foreach ($assetList as $key => $asset) {
                        // Site name
                        $siteName = isset($asset->site->site_name) ? $asset->site->site_name : '';

                        // Sub site name
                        $subSiteName = isset($asset->subSite->sub_site_name) ? $asset->subSite->sub_site_name : '';

                        // Module name
                        $categoryName = isset($asset->module->module_category_id) ? optional(Category::find($asset->module->module_category_id))->category_name ?? 'Unknown category' : 'No module ID';

                        // Module name
                        $moduleName = isset($asset->module->module_name) ? $asset->module->module_name : '';

                        $data['result'][$key]['asset_id']            = $asset->asset_id;
                        $data['result'][$key]['asset_category']      = $categoryName;
                        $data['result'][$key]['asset_module']        = $moduleName;
                        $data['result'][$key]['asset_site_id']       = $asset->asset_site_id;
                        $data['result'][$key]['asset_site_name']     = $siteName;
                        $data['result'][$key]['asset_sub_site_id']   = $asset->asset_sub_site_id;
                        $data['result'][$key]['asset_sub_site_name'] = $subSiteName;
                        $data['result'][$key]['asset_uid']           = $asset->asset_uid;
                        $data['result'][$key]['asset_image']         = $asset->asset_image;
                        $data['result'][$key]['asset_latitude']      = $asset->asset_latitude;
                        $data['result'][$key]['asset_longitude']     = $asset->asset_longitude;
                        $data['result'][$key]['asset_info_url']      = !empty($asset->asset_nfc_uuid) ? '/asset-info/' . $asset->asset_nfc_uuid : '';
                    }

                    return $this->returnSuccessMessage('Asset Listed Successfully.', $data);
                } else {
                    return $this->returnWarning(204, 'Oppps! Asset not found, Please try again...');
                }
            } else {
                return $this->returnError(204, 'Oppps! Asset not found, Please try again...');
            }
        } catch (Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
}
