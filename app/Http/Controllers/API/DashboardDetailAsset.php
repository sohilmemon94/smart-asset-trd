<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTrait;
use App\Models\Asset;
use App\Models\CategoryModuleFieldValue;
use App\Models\FieldInputType;
use App\Models\Site;
use App\Models\User;
use App\Models\SubSite;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade\Pdf;
use File;
use Mail;

class DashboardDetailAsset extends Controller
{
    use GeneralTrait;
    private $timeZone = 'UTC';
    public function getAssetDetail($assetID, Request $request)
    {
        try {
            $this->timeZone = $request->has('timeZone') ? $request->timeZone : 'UTC';
            $asset = Asset::find($assetID);

            if(!$asset) {
                return $this->returnError(404, 'Asset not found');
            }

            $status = 'Inspection Pending';
            $lastInspection = $asset->inspections->last();
            if($lastInspection) {
                if($lastInspection->inspection_result == 'pass'){
                    $status = 'Pass';
                }
                else if($lastInspection->inspection_result == 'fail'){
                    $status = 'Fail';
                }
                else if($lastInspection->inspection_result == 'maintenance_required'){
                    $status = 'Maintenance Required';
                }
            }

            $documents = [];
            foreach ($asset->documents as $document) {
                // if($document->ad_file || $document->ad_link) {
                    $documents[] = [
                        'name' => $document->ad_name,
                        'url'  => $document->ad_file ? $this->getStorageURL($document->ad_file): null,
                        'link' => $document->ad_link
                    ];
                // }
            }

            $data = [];
            $data['asset_id'] = $asset->asset_id;
            $data['asset_uid'] = $asset->asset_uid;
            $data['status'] = $status;

            $data['dynamic_fields'] = $this->getDynamicFieldsData($asset->asset_id);

            $data['inspection_schedule'] = $asset->module->module_inspection_schedule;
            if($asset->module->module_inspection_schedule_rgby == 'Y'){
                $data['inspection_schedule'] = 'RGBY';
            }

            $data['last_inspection']           = $lastInspection && $lastInspection->created_at ? $this->getFormattedDate($this->convertTimeZone('UTC', $this->timeZone, $lastInspection->created_at)) : '';
            $data['last_inspection_by']        = ($lastInspection && !is_null($lastInspection->createdBy)) ? $lastInspection->createdBy->full_name : '';
            $data['last_inspection_notes']     = $lastInspection ? $lastInspection->inspection_note : '';
            $data['last_inspection_photo']     = $lastInspection && $lastInspection->inspection_image_1 ? $this->getStorageURL($lastInspection->inspection_image_1): null;
            $data['last_inspection_photo_act'] = $lastInspection && $lastInspection->inspection_image_1 ? 'storage/'.$lastInspection->inspection_image_1 : null;
            $data['last_inspection_result']    = $status;
            $data['documents']                 = $documents;
            $data['photo']                     = $asset->asset_image ? $this->getStorageURL($asset->asset_image): null;
            $data['photo_act']                 = $asset->asset_image ? 'storage/'.$asset->asset_image           : null;
            $data['latitude']                  = $asset->asset_latitude;
            $data['longitude']                 = $asset->asset_longitude;
            $data['location']                  = Site::find($asset->asset_site_id)->site_name;
            $data['area']                      = SubSite::find($asset->asset_sub_site_id)->sub_site_name;
            $data['category']                  = $asset->module->category->category_name;
            $data['module']                    = $asset->module->module_name;

            return $this->returnData('data', $data);

        } catch (\Exception $e) {
            return $this->returnError(500, $e->getMessage());
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    private function getCMFVtype($inputFieldTypeId) {
        $type = FieldInputType::find($inputFieldTypeId)->field_input_type;
        $var  = 'cmfv_value_'.$type;
        return $var;
    }

    private function isCMFVtypeColor($inputFieldTypeId) {
        $typeName = FieldInputType::find($inputFieldTypeId)->field_input_type_name;
        return $typeName == 'Color';
    }

    private function getDynamicFieldsData($assetId) {
        $values = CategoryModuleFieldValue::where('cmfv_asset_id', $assetId)->with('moduleField')->get();
        $data = [];
        foreach($values as $value) {
            if(isset($value->moduleField)) {
                if($value->moduleField->mf_status == 'Y'){
                    $type = $this->getCMFVtype($value->moduleField->mf_input_field_type_id);
                    $fieldValue = $value->$type;
                    // formatting date, time & datetime
                    if($type == 'cmfv_value_time') {
                        $fieldValue = $value->$type ? $this->getFormattedTime($this->convertTimeZone('UTC', $this->timeZone, $fieldValue)): '';
                    } else if($type == 'cmfv_value_date') {
                        $fieldValue = $value->$type ? $this->getFormattedDate($fieldValue): '';
                    } else if($type == 'cmfv_value_datetime') {
                        $fieldValue = $value->$type ? $this->getFormattedDateTime($this->convertTimeZone('UTC', $this->timeZone, $fieldValue)): '';
                    } else if($type == 'cmfv_value_boolean'){
                        $fieldValue = ($value->$type == 'Y')?'Yes':'No';
                    }
                    $data[] = [
                        'label' => $value->moduleField->mf_name,
                        'value' => $fieldValue,
                        'isColor' => $this->isCMFVtypeColor($value->moduleField->mf_input_field_type_id)
                    ];
                }
            }
        }
        return $data;
    }

    public function exportDashboardAssetDetailData(Request $request) {
        try {
            // dd($request->all());
            $message = '';
            $data = [];
            if($request->type=='mail') {
                $data = $this->sendMail(json_decode($request->data), $request->dateTime, $request->email);
                $message = 'Mail sent successfully';
            } elseif($request->type=='pdf') {
                $data = $this->exportPDF(json_decode($request->data), $request->dateTime, $request->data_photo_mode, $request->inspection_photo_mode);
                $message = 'PDF downloaded successfully';
            }
            return $this->returnData('data', $data);
        } catch (\Exception $e) {
            return $this->returnError(500, $e->getMessage());
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
    private function sendMail($data, $dateTime, $mail) {
        $title = 'Asset Info';
        $isClientAdmin = Auth::user()->roles->first()->name == 'Admin' ? true : false;
        $isUserAdmin = Auth::user()->roles->first()->name == 'User' ? true : false;
        if($isUserAdmin){
            $clientCompanyName  = User::find(Auth::user()->created_by)->client_company_name??'';
            $clientLogo = 'storage/'.User::find(Auth::user()->created_by)->client_company_logo ?? 'pdf_image/logo/sa_logo.png';

        }else{
            $clientLogo = Auth::user()->client_company_logo ? 'storage/'.Auth::user()->client_company_logo : 'pdf_image/logo/sa_logo.png';
            $clientCompanyName = Auth::user()->client_company_name;
        }
        $mail = Mail::send('reports.pdf.detailemail', array(
            'data'              => $data,
            'datetime'          => $dateTime,
            'head'              => $title,
            'isClientAdmin'     => $isClientAdmin,
            'isUserAdmin'     =>   $isUserAdmin,
            'clientLogo'        => $clientLogo,
            'clientCompanyName' => $clientCompanyName,
        ), function($message) use ($mail, $title){
            $message->to($mail)->subject($title);
        });
        return ['success' => true];
    }
    private function exportPDF($data, $dateTime, $data_photo_mode, $inspection_photo_mode) {
        //print_r($data);
        $title = 'Asset Info';
        $isClientAdmin = Auth::user()->roles->first()->name == 'Admin' ? true : false;
        $isUserAdmin = Auth::user()->roles->first()->name == 'User' ? true : false;
        if($isUserAdmin){
            $clientCompanyName  = User::find(Auth::user()->created_by)->client_company_name??'';
            $clientLogo = 'storage/'.User::find(Auth::user()->created_by)->client_company_logo ?? 'pdf_image/logo/sa_logo.png';

        }else{
            $clientLogo = Auth::user()->client_company_logo ? 'storage/'.Auth::user()->client_company_logo : 'pdf_image/logo/sa_logo.png';
            $clientCompanyName = Auth::user()->client_company_name;
        }

        $path = storage_path('app/public/pdf');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }

        $pdf = PDF::loadView('reports.pdf.detail', array(
            'data'              => $data,
            'datetime'          => $dateTime,
            'head'              => $title,
            'isClientAdmin'     => $isClientAdmin,
            'isUserAdmin'     =>   $isUserAdmin,
            'clientLogo'        => $clientLogo,
            'clientCompanyName' => $clientCompanyName,
            'data_photo_mode'   => $data_photo_mode,
            'inspection_photo_mode' => $inspection_photo_mode,
        ));
        $fileName = str_pad(mt_rand(1, 99999999), 8, '0', STR_PAD_LEFT) . '.pdf';
        // $fileName = $request->apiHead.'.pdf';
        $filePath = $path . '/' . $fileName;
        $pdf->save($filePath);

        return ['success' => true, 'fileName' => $fileName];
    }

}
