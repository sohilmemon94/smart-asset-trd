<?php

namespace App\Http\Controllers\API;

use App\Exports\ListingExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\GeneralTrait;
use App\Models\Asset;
use App\Models\Category;
use App\Models\FieldInputType;
use App\Models\Inspection;
use App\Models\Module;
use App\Models\ModuleField;
use App\Models\Site;
use App\Models\SubSite;
use App\Models\SubSiteUser;
use App\Models\SubSiteUserPermission;
use App\Models\User;
use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade\Pdf;
use Maatwebsite\Excel\Facades\Excel;
use File;
use Mail;
use URL;

class DashboardDetailListing extends Controller
{
    use GeneralTrait;
    private $subSiteArray = [], $clientId = 0, $locationId = 0, $areaId = 0, $timeZone;
    public function fetchDashboardListing(Request $request)
    {
        // if(areaArray) is present in request data then consider the assets whose subsites is in areaArray else consider all asset
        try {
            $this->subSiteArray = $request->has('areaArray') ? $request->areaArray  : [];
            $this->clientId     = $request->has('clientId') ? (int)$request->clientId    : 0;
            $this->locationId   = $request->has('locationId') ? (int)$request->locationId : 0;
            $this->areaId       = $request->has('areaId') ? (int)$request->areaId        : 0;
            $this->timeZone     = $request->has('timeZone') ? $request->timeZone          : 'UTC';
            $time = (int)$request->time;
            $response = [
                'pageTitle' => $this->getPageTitle($request->apiKey, $time),
                'assetList' => $this->generateDashboardListing($request->apiKey, $time)
            ];
            return $this->returnData('data', $response);
        } catch (\Exception $e) {
            return $this->returnError(500, $e->getMessage());
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    private function generateDashboardListing($api, $time)
    {

        $data = [];
        if ($api == 'assets_pass') {
            $data = $this->assetsGeneral('pass');
        } elseif ($api == 'assets_fail') {
            $data = $this->assetsGeneral('fail');
        } elseif ($api == 'assets_expired') {
            $data = $this->assetsExpired();
        } elseif ($api == 'assets_maintanance_required') {
            $data = $this->assetsGeneral('maintenance_required');
        } elseif ($api == 'assets_inspection_due') {
            $data = $this->assetsGeneral('due');
        } elseif ($api == 'assets_total') {
            $data = $this->assetsGeneral('total');
        } elseif ($api == 'assets_due_to_expire') {
            $data = $this->assetsDueToExpire($time);
        } elseif ($api == 'assets_inspected') {
            $data = $this->assetsInspected($time);
        } elseif ($api == 'assets_registered') {
            $data = $this->assetsRegistered($time);
        } elseif ($api == 'total_assets_inspection') {
            $data = $this->totalAssetsInspection($time);
        } elseif ($api == 'current_inspections') {
            $data = $this->getInspectionData('current');
        } elseif ($api == 'overdue_inspections') {
            $data = $this->getInspectionData('overdue');
        } elseif ($api == 'inspections_due') {
            $data = $this->getInspectionData('due');
        } elseif ($api == 'inspections_due_24hrs') {
            $data = $this->getInspectionData('24hrs');
        } elseif ($api == 'inspections_due_7days') {
            $data = $this->getInspectionData('7days');
        } elseif ($api == 'inspections_due_30days') {
            $data = $this->getInspectionData('30days');
        } elseif ($api == 'active_clients') {
            $data = $this->getClients('Y');
        } elseif ($api == 'inactive_clients') {
            $data = $this->getClients('N');
        } elseif ($api == 'active_locations') {
            $data = $this->getLocations('Y');
        } elseif ($api == 'active_areas') {
            $data = $this->getAreas('Y');
        } elseif ($api == 'active_admin_users') {
            $data = $this->getSuperAdminUsers('Y');
        } elseif ($api == 'active_users') {
            $data = $this->getUsers('Y');
        } elseif ($api == 'active_categories') {
            $data = $this->getCategories('Y');
        } elseif ($api == 'active_modules') {
            $data = $this->getModules('Y');
        } elseif ($api == 'inactive_locations') {
            $data = $this->getLocations('N');
        } elseif ($api == 'inactive_areas') {
            $data = $this->getAreas('N');
        } elseif ($api == 'inactive_admin_users') {
            $data = $this->getSuperAdminUsers('N');
        } elseif ($api == 'inactive_users') {
            $data = $this->getUsers('N');
        } elseif ($api == 'inactive_categories') {
            $data = $this->getCategories('N');
        } elseif ($api == 'inactive_modules') {
            $data = $this->getModules('N');
        }
        return $data;
    }

    private function getPageTitle($apiKey, $time)
    {
        $title = '';
        if ($apiKey == 'assets_pass') {
            $title = 'Pass Asset Details';
        } elseif ($apiKey == 'assets_fail') {
            $title = 'Failed Assets Details';
        } elseif ($apiKey == 'assets_expired') {
            $title = 'Expired Assets Details';
        } elseif ($apiKey == 'assets_maintanance_required') {
            $title = 'Assets Requiring Maintenance';
        } elseif ($apiKey == 'assets_inspection_due') {
            $title = 'New Asset Registration Inspection Due';
        } elseif ($apiKey == 'assets_total') {
            $title = 'Total Assets';
        } elseif ($apiKey == 'assets_due_to_expire') {
            $title = 'Assets Expiring in ' . $time . ' Days';
            if ($time == 1) {
                $title = 'Assets Expiring in 24 Hours';
            }
        } elseif ($apiKey == 'assets_inspected') {
            $title = 'Assets Inspected in the past ' . $time . ' Days';
            if ($time == 1) {
                $title = 'Assets Inspected in the past 24 Hours';
            } elseif ($time == 0) {
                $title = 'Assets Inspected in This Year';
            }
        } elseif ($apiKey == 'assets_registered') {
            $title = 'Assets registered in the past ' . $time . ' Days';
            if ($time == 1) {
                $title = 'Assets registered in the past 24 Hours';
            } elseif ($time == 0) {
                $title = 'Assets registered in This Year';
            }
        } elseif ($apiKey == 'total_assets_inspection') {
            $title = 'Total Asset Inspections in the past ' . $time . ' Days';
            if ($time == 1) {
                $title = 'Total Asset Inspections in the past 24 Hours';
            } elseif ($time == 0) {
                $title = 'Total Asset Inspections in This Year';
            }
        } elseif ($apiKey == 'current_inspections') {
            $title = 'Current Inspections';
        } elseif ($apiKey == 'overdue_inspections') {
            $title = 'Overdue Inspections';
        } elseif ($apiKey == 'inspections_due') {
            $title = 'New Registration Inspections Due';
        } elseif ($apiKey == 'inspections_due_24hrs') {
            $title = 'Inspections Due in 24hrs';
        } elseif ($apiKey == 'inspections_due_7days') {
            $title = 'Inspections Due in 7 Days';
        } elseif ($apiKey == 'inspections_due_30days') {
            $title = 'Inspections Due in 30 Days';
        } elseif ($apiKey == 'active_clients') {
            $title = "Active Client Admins";
        } elseif ($apiKey == 'inactive_clients') {
            $title = "Inctive Client Admins";
        } elseif ($apiKey == 'active_locations') {
            $title = "Active Locations";
        } elseif ($apiKey == 'active_areas') {
            $title = "Active Areas";
        } elseif ($apiKey == 'active_admin_users') {
            $title = "Active Super Admins";
            if (Auth::user()->roles->first()->name == 'Admin') {
                $title = "Active Admin Users";
            }
        } elseif ($apiKey == 'active_users') {
            $title = "Active Client Users";
        } elseif ($apiKey == 'active_categories') {
            $title = "Active Categories";
        } elseif ($apiKey == 'active_modules') {
            $title = "Active Modules";
        } elseif ($apiKey == 'inactive_locations') {
            $title = "Inactive Locations";
        } elseif ($apiKey == 'inactive_areas') {
            $title = "Inactive Areas";
        } elseif ($apiKey == 'inactive_admin_users') {
            $title = "Inactive Super Admins";
            if (Auth::user()->roles->first()->name == 'Admin') {
                $title = "Inactive Admin Users";
            }
        } elseif ($apiKey == 'inactive_users') {
            $title = "Inactive Client Users";
        } elseif ($apiKey == 'inactive_categories') {
            $title = "Inactive Categories";
        } elseif ($apiKey == 'inactive_modules') {
            $title = "Inactive Modules";
        }

        return $title;
    }

    private function assetsGeneral($result)
    {
        // for total, pass, fail, inspection due and maintanance required
        $moduleFieldsColorId = $this->getModuleFieldColorId();
        $assets = Asset::whereNull('deleted_at');
        if (count($this->subSiteArray) > 0) {
            $assets = Asset::whereIn('asset_sub_site_id', $this->subSiteArray);
        }
        $assets = $assets->with('inspections', function ($query1) {
                        $query1->latest();
                    })
                    ->with(['module', 'values'])
                    ->whereHas('module', function ($query){
                        $query->where('module_status', 'Y');
                        $query->whereNull('deleted_at');
                    })
                    ->get()
                    ->filter(function ($asset) use ($result) {
                        if ($result == 'total') {
                            return $asset;
                        } elseif ($result != 'due' && $asset->inspections->count() > 0 && $asset->inspections->first()->inspection_result == $result) {
                            return $asset;
                        } elseif ($result == 'due' && $asset->inspections->count() == 0) {
                            return $asset;
                        }
                    });
        //print_r($assets);
        //exit;


        return $this->generateAssetListData($assets);
    }

    private function assetsExpired()
    {
        $mfIds = $this->getExpiryMFids();
        $assets = Asset::whereNull('deleted_at');
        if (count($this->subSiteArray) > 0) {
            $assets = $assets->whereIn('asset_sub_site_id', $this->subSiteArray);
        }
        $assets = $assets->whereHas('values', function ($query) use ($mfIds) {
            $query->whereIn('cmfv_mf_id', $mfIds)->whereDate('cmfv_value_date', '<', Carbon::now());
        })->with('module')->with('inspections', function ($query) {
            $query->latest();
        })->get();

        return $this->generateAssetListData($assets);
    }


    private function assetsDueToExpire($time)
    {
        $moduleFieldsColorId = $this->getModuleFieldColorId();
        $expiryMFids = $this->getExpiryMFids();
        $mfIds = array_merge($moduleFieldsColorId, $expiryMFids);
        $assets = Asset::whereNull('deleted_at');
        if (count($this->subSiteArray) > 0) {
            $assets = Asset::whereIn('asset_sub_site_id', $this->subSiteArray);
        }

        if ($time == 1) {
            $assets = $assets->whereHas('values', function ($query) use ($mfIds) {
                $query->whereIn('cmfv_mf_id', $mfIds)->whereDate('cmfv_value_date', '>=', Carbon::now())->whereDate('cmfv_value_date', '<=', Carbon::now()->addDay());
            })->with('module')->with('inspections', function ($query) {
                $query->latest();
            })->orderBy('asset_id', 'desc')->get();
            // $assets = $assets->whereHas('values', function ($query) use ($mfIds, $time, $moduleFieldsColorId) {
            //     $query->whereIn('cmfv_mf_id', $mfIds)->whereIn('cmfv_mf_id', $moduleFieldsColorId)->whereDate('cmfv_value_date','>',Carbon::now())->whereDate('cmfv_value_date', '<=', Carbon::now()->addDay());
            // })->with('inspections', function ($query) {
            //     $query->latest();
            // })->orderBy('asset_id', 'desc')->get();
        } elseif ($time > 1) {
            $assets = $assets->whereHas('values', function ($query) use ($mfIds, $time) {
                $query->whereIn('cmfv_mf_id', $mfIds)->whereDate('cmfv_value_date', '>=', Carbon::now())->whereDate('cmfv_value_date', '<=', Carbon::now()->addDays($time));
            })->with('module')->with('inspections', function ($query) {
                $query->latest();
            })->orderBy('asset_id', 'desc')->get();
        }
        return $this->generateAssetListData($assets);
    }

    private function assetsInspected($time)
    {
        $moduleFieldsColorId = $this->getModuleFieldColorId();
        $expiryMFids = $this->getExpiryMFids();
        $mfIds = array_merge($moduleFieldsColorId, $expiryMFids);
        $assets = Asset::whereNull('deleted_at');
        if (count($this->subSiteArray) > 0) {
            $assets = Asset::whereIn('asset_sub_site_id', $this->subSiteArray);
        }
        $assets = $assets = $assets->with('inspections', function ($query) {
            $query->latest();
        })->with('module')->with('values', function ($query) use ($mfIds) {
            $query->whereIn('cmfv_mf_id', $mfIds);
        })->orderBy('asset_id', 'desc')->get();
        ///dd($assets -> toArray());

        $assets = $assets->filter(function ($asset) use ($time) {
            if ($asset->inspections->count() > 0) {
                $inspections = $asset->inspections->first();

                if ($time == 1) {
                    return ($inspections && !is_null($inspections->created_at)) ? $inspections->created_at->gte(Carbon::now()->subDay()) : '';
                } elseif ($time > 0) {
                    return ($inspections && !is_null($inspections->created_at)) ? $inspections->created_at->gte(Carbon::now()->subDays($time)) : '';
                } else {
                    return ($inspections && !is_null($inspections->created_at)) ? $inspections->created_at->gte(Carbon::now()->startOfYear()) : '';
                }
            }
        });
        // print_r($assets);
        // exit;
        //dd($assets);
        return $this->generateAssetListData($assets);
    }

    private function assetsRegistered($time)
    {
        $moduleFieldsColorId = $this->getModuleFieldColorId();
        $expiryMFids = $this->getExpiryMFids();
        $mfIds = array_merge($moduleFieldsColorId, $expiryMFids);

        $assets = Asset::whereNull('deleted_at');
        if (count($this->subSiteArray) > 0) {
            $assets = Asset::whereIn('asset_sub_site_id', $this->subSiteArray);
        }
        if ($time == 1) {
            $assets = Asset::where('created_at', '>=', Carbon::now()->subDay());
        } elseif ($time > 1) {
            $assets = Asset::where('created_at', '>=', Carbon::now()->subDays($time));
        } else {
            $assets = Asset::where('created_at', '>=', Carbon::now()->startOfYear());
        }
        if (count($this->subSiteArray) > 0) {
            $assets = $assets->whereIn('asset_sub_site_id', $this->subSiteArray);
        }

        $assets = $assets->with('inspections', function ($query) {
            $query->latest();
        })->with('module')->with('values', function ($query) use ($mfIds) {
            $query->whereIn('cmfv_mf_id', $mfIds);
        })->orderBy('asset_id', 'desc')->get();
        //dd($assets);
        return $this->generateAssetListData($assets);
    }

    private function totalAssetsInspection($time)
    {
        $sub_site_array = $this->subSiteArray;

        $moduleFieldsColorId = $this->getModuleFieldColorId();
        $expiryMFids = $this->getExpiryMFids();
        $mfIds = array_merge($moduleFieldsColorId, $expiryMFids);

        $assetInspections = Inspection::whereNull('deleted_at');
        if (count($sub_site_array) > 0) {
            $assetInspections = Inspection::whereHas('assetInfo', function ($query) use ($sub_site_array) {
                $query->whereIn('asset_sub_site_id', $sub_site_array);
            });
        }
        if ($time == 1) {
            $assetInspections = $assetInspections->where('created_at', '>=', Carbon::now()->subDay());
        } elseif ($time > 1) {
            $assetInspections = $assetInspections->where('created_at', '>=', Carbon::now()->subDays($time));
        } else {
            $assetInspections = $assetInspections->where('created_at', '>=', Carbon::now()->startOfYear());
        }
        $assetInspections = $assetInspections->orderBy('created_at', 'desc')->get();
        //print_r($assetInspections);
        //exit;
        $data = [];
        foreach ($assetInspections as $key => $inspection) {
            $color = '';
            $variant = '';
            $status = '';
            $lastInspection = $inspection->assetWiseInspection->inspections->last();
            $inspectedBy = $inspection->createdBy ? $inspection->createdBy->full_name : '';

            if ($inspection->inspection_result == 'pass') {
                $variant = 'success';
                $status = 'Pass';
            } elseif ($inspection->inspection_result == 'fail') {
                $variant = 'danger';
                $status = 'Fail';
            } else {
                $variant = 'warning';
                $status = 'Maintenance Required';
            }
            $color = $inspection->assetWiseInspection->values->filter(function ($value) use ($moduleFieldsColorId) {
                if (in_array($value->cmfv_mf_id, $moduleFieldsColorId)) {
                    return $value;
                }
            })->first();
            $expiry = $inspection->assetWiseInspection->values->filter(function ($value) use ($expiryMFids) {
                if (in_array($value->cmfv_mf_id, $expiryMFids)) {
                    return $value;
                }
            })->first();
            $color = $color ? $color->cmfv_value_char : '';
            $expiry = $expiry && $expiry->cmfv_value_date ? $this->getFormattedDate($expiry->cmfv_value_date) : '';

            $data[] = [
                'uid'             => $inspection->assetWiseInspection->asset_uid,
                'location'        => $inspection->assetWiseInspection->site->site_name ?? '',
                'area'            => $inspection->assetWiseInspection->subSite->sub_site_name,
                'category'        => $inspection->assetWiseInspection->module->category->category_name ?? '',
                'module'          => $inspection->assetWiseInspection->module->module_name ?? '',
                'asset_q_color'   => $inspection->assetWiseInspection->asset_q_color,
                'q'               => $inspection->assetWiseInspection->module->module_inspection_schedule ?? '',
                'status'          => $status,
                'variant'         => $variant,
                'expires'         => $expiry,
                'hasExpired'      => $expiry ? Carbon::parse($expiry)->isPast() : false,
                'raised'          => $inspection->assetWiseInspection->created_at ? $this->getFormattedDate($inspection->assetWiseInspection->created_at) : '',
                'last_inspection' => $lastInspection ? $this->getFormattedDate($this->convertTimeZone('UTC', $this->timeZone, $lastInspection->created_at)) : '',
                'inspected_by'    => $inspectedBy
            ];
        }
        return $data;
    }

    private function getInspectionData($status)
    {
        $moduleFieldsColorId = $this->getModuleFieldColorId();
        $expiryMFids = $this->getExpiryMFids();
        $mfIds = array_merge($moduleFieldsColorId, $expiryMFids);

        $assets = Asset::whereNull('deleted_at');
        if (count($this->subSiteArray) > 0) {
            $assets = $assets->whereIn('asset_sub_site_id', $this->subSiteArray);
        }
        $assets = $assets->whereHas('module', function ($query) {
            $query->whereNotNull('module_inspection_schedule');
        })->with('inspections', function ($query) {
            $query->latest();
        })->with('module')->with('values', function ($query) use ($mfIds) {
            $query->whereIn('cmfv_mf_id', $mfIds);
        })->get()->filter(function ($asset) use ($status) {
            if ($status == 'current') {
                // ONE cases possible with two Condition (AND)
                // 1. Inspection is done (AND) current date - Inspection done date <= InspectionSchedult day value
                if ($asset->asset_q_color == NULL && $asset->inspections->count() > 0 && Carbon::parse($asset->inspections[0]->created_at)->diffInDays(Carbon::now()) <= $asset->module->module_inspection_schedule) {
                    return $asset;
                }

                // RGBY - Inspection is done (AND) Inspection expiry date <= current date value
                // if ($asset->asset_q_color != NULL && $asset->inspections->count() > 0 && $asset->inspections[0]->whereDate('inspection_q_expiry', '<=', Carbon::now()->toDateString())->count() > 0) {
                //     return $asset;
                // }
                if ($asset->asset_q_color != NULL && $asset->inspections->count() > 0) {
                    $latestInspection = $asset->inspections()->latest()->first();
                    if ($latestInspection && $latestInspection->created_at <= $latestInspection->inspection_q_expiry) {
                        return $asset;
                    }
                    return $asset;
                }
            } else if ($status == 'overdue') {
                // TWO cases possible
                // 1. Inspection is done (AND) current date - Inspection done date > InspectionSchedult day value
                // 2. Inspection is not done (AND) current date - Asset created date > InspectionSchedult day value
                if ($asset->asset_q_color == NULL && $asset->inspections->count() > 0 && Carbon::parse($asset->inspections[0]->created_at)->diffInDays(Carbon::now()) > $asset->module->module_inspection_schedule) {
                    return $asset;
                } else if ($asset->asset_q_color == NULL && $asset->inspections->count() == 0 && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) > $asset->module->module_inspection_schedule) {
                    return $asset;
                }

                // RGBY - TWO cases possible
                // 1. Inspection is done (AND) inspection_q_expiry > current day value
                // 2. Inspection is not done (AND) inspection_q_expiry > current day value
                if ($asset->asset_q_color != null && $asset->inspections->count() > 0 ) {
                    $latestInspection = $asset->inspections()->latest()->first();
                    if ($latestInspection && $latestInspection->inspection_q_expiry <= Carbon::now()->toDateString()) {
                        return $asset;
                    }
                } else if ($asset->asset_q_color != null && $asset->inspections->count() == 0) {
                    if(Carbon::now() > $this->getQcolorExpiry($asset->asset_q_color)) {
                        return $asset;
                    }
                }
            } else if ($status == 'due') {
                // ONE cases possible
                // 1. Inspection is not done (AND) current date - Asset created date <= InspectionSchedult day value
                if ($asset->asset_q_color == NULL && $asset->inspections->count() == 0 && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) <= $asset->module->module_inspection_schedule) {
                    return $asset;
                }

                // RGBY color selected
                if ($asset->asset_q_color != NULL && $asset->inspections->count() == 0) {
                    return $asset;
                }
            } else if ($status == '24hrs') {
                // In this case the "inspection due" in 24 hrs will be listed
                // New Assets inspection due in 24 hrs
                // Old Assets inspection due in 24 hrs
                if ($asset->asset_q_color == NULL &&  $asset->inspections->count() == 0 && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) <= $asset->module->module_inspection_schedule && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) <= 1) {
                    return $asset;
                } elseif ($asset->inspections->count() > 0 && $asset->module->module_inspection_schedule - Carbon::parse($asset->inspections[0]->created_at)->diffInDays(Carbon::now()) <= 1) {
                    return $asset;
                }

                // RGBY - In this case the "inspection due" in 24 hrs will be listed
                // New Assets inspection due in 24 hrs - RGBY
                // Old Assets inspection due in 24 hrs - RGBY
                if ($asset->asset_q_color != NULL && $asset->inspections->count() == 0) {
                    return $asset;
                } elseif ($asset->asset_q_color != NULL &&  $asset->inspections->count() > 0 && Carbon::parse($asset->inspections[0]->inspection_q_expiry)->diffInDays(Carbon::now()) <= 1) {
                    return $asset;
                }
            } elseif ($status == '7days') {
                // In this case the "inspection due" in 7 days will be listed
                if ($asset->asset_q_color == NULL && $asset->inspections->count() == 0 && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) <= $asset->module->module_inspection_schedule && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) <= 7) {
                    return $asset;
                } elseif ($asset->inspections->count() > 0 && $asset->module->module_inspection_schedule - Carbon::parse($asset->inspections[0]->created_at)->diffInDays(Carbon::now()) <= 7) {
                    return $asset;
                }

                // RGBY - In this case the "inspection due" in 7 days will be listed
                if ($asset->asset_q_color != NULL && $asset->inspections->count() == 0) {
                    return $asset;
                } elseif ($asset->asset_q_color != NULL && $asset->inspections->count() > 0 && Carbon::parse($asset->inspections[0]->inspection_q_expiry)->diffInDays(Carbon::now()) <= 7) {
                    return $asset;
                }
            } elseif ($status == '30days') {
                // In this case the "inspection due" in 30 days will be listed
                if ($asset->asset_q_color == NULL && $asset->inspections->count() == 0 && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) <= $asset->module->module_inspection_schedule && Carbon::parse($asset->created_at)->diffInDays(Carbon::now()) <= 30) {
                    return $asset;
                } elseif ($asset->inspections->count() > 0 && $asset->module->module_inspection_schedule - Carbon::parse($asset->inspections[0]->created_at)->diffInDays(Carbon::now()) <= 30) {
                    return $asset;
                }

                // RGBY - In this case the "inspection due" in 30 days will be listed
                if ($asset->asset_q_color != NULL && $asset->inspections->count() == 0) {
                    return $asset;
                } elseif ($asset->asset_q_color != NULL && $asset->inspections->count() > 0 && Carbon::parse($asset->inspections[0]->inspection_q_expiry)->diffInDays(Carbon::now()) <= 30) {
                    return $asset;
                }
            }
        });
        return $this->generateAssetListData($assets);
    }

    private function generateAssetListData($assets)
    {
        // inspections relation has to be passed with assets.
        $data = [];
        $expiryMFids = $this->getExpiryMFids();
        foreach ($assets as $key => $asset) {
            // to find expiry date
            $expiryDate = '';
            foreach ($asset->values as $value) {
                if (in_array($value->cmfv_mf_id, $expiryMFids)) {
                    $expiryDate = $value->cmfv_value_date ? $this->getFormattedDate($value->cmfv_value_date) : '';
                }
            }
            $status = 'Inspection Pending';
            $variant = 'secondary';
            $lastInspection = null;
            $inspectedBy = null;

            if ($asset->inspections->count() > 0) {
                $lastInspection = $asset->inspections[0]->created_at;
                $inspectedBy = $asset->inspections[0]->createdBy ? $asset->inspections[0]->createdBy->full_name : '';
                if ($asset->inspections[0]->inspection_result == 'pass') {
                    $variant = 'success';
                    $status = 'Pass';
                } elseif ($asset->inspections[0]->inspection_result == 'fail') {
                    $variant = 'danger';
                    $status = 'Fail';
                } elseif ($asset->inspections[0]->inspection_result == 'maintenance_required') {
                    $variant = 'warning';
                    $status = 'Maintenance Required';
                }
            }
            $color = null;
            if ($asset->values->count() > 0) {
                $color = $asset->values[0]->cmfv_value_char;
            }
            $data[] = [
                'id'              => $asset->asset_id,
                'uid'             => $asset->asset_uid,
                'client'          => User::find(User::find($asset->asset_user_id)->created_by)->client_company_name ?? '',
                'location'        => $asset->site->site_name ?? '',
                'area'            => $asset->subSite->sub_site_name ?? '',
                'category'        => $asset->module->category->category_name ?? '',
                'module'          => $asset->module->module_name ?? '',
                'asset_q_color'   => $asset->asset_q_color,
                'q'               => $asset->module->module_inspection_schedule ?? '',
                'status'          => $status,
                'variant'         => $variant,
                'expires'         => $expiryDate,
                'hasExpired'      => $expiryDate ? Carbon::parse($expiryDate)->isPast() : false,
                // 'raised'       => $asset->created_at ? $this->getFormattedDate($asset->created_at) : '',
                'last_inspection' => $lastInspection ? $this->getFormattedDate($this->convertTimeZone('UTC', $this->timeZone, $lastInspection)) : '',
                'inspected_by'    => $inspectedBy
            ];
        }
        return $data;
    }

    private function getModuleFieldColorId()
    {
        $fieldColorTypeId = FieldInputType::where('field_input_type_name', 'Color')->first()->field_input_type_id;
        $moduleFieldsColorId = ModuleField::where('mf_input_field_type_id', $fieldColorTypeId)->pluck('mf_id')->toArray();
        return $moduleFieldsColorId;
    }

    private function getClients($status)
    {
        // NOTE :: After company integration we change the code
        // $clients = User::where('status', $status)->where('company_id', $this->clientId)->whereHas('roles', function ($query) {
        //     $query->where('name', 'Admin');
        // })->with('companyData')->orderBy('created_at', 'desc')->get();
        
        $data = [];
        $clients = Company::with('firstAdminUser')->where('status', $status)->orderBy('created_at', 'desc')->get();

        foreach ($clients as $client) {
            $data[] = [
                'name'         => $client->firstAdminUser->full_name,
                'email'        => $client->firstAdminUser->email,
                'contact'      => $client->contact_no,
                'company_name' => $client->name,
                'abn'          => $client->abn,
                'created_by'   => $client->firstAdminUser->createdBy ? $client->firstAdminUser->createdBy->full_name : '',
            ];
        }
        return $data;
    }

    private function getLocations($status)
    {
        $data = [];
        $locations = Site::where('site_status', $status)->orderBy('created_at', 'desc');

        if ($this->clientId != 0) {
            $locations = $locations->where('company_id', $this->clientId);
        }

        $locations = $locations->get();

        foreach ($locations as $location) {
            $data[] = [
                'name'       => $location->site_name,
                'id'         => $location->site_uuid,
                'created_by' => $location->createdBy ? $location->createdBy->full_name : '',
            ];
        }

        return $data;
    }
    private function getAreas($status)
    {
        $areas = SubSite::where('sub_site_status', $status)->whereHas('site', function ($query) {
            $query->whereNull('deleted_at');
        });
        if ($this->clientId != 0 && $this->locationId == 0) {
            $areas = $areas->where('company_id', $this->clientId);
        }
        if ($this->locationId != 0) {
            $areas = $areas->where('site_id', $this->locationId);
        }
        $areas = $areas->orderBy('created_at', 'desc')->get();
        $data = [];
        foreach ($areas as $area) {
            $data[] = [
                'name'       => $area->sub_site_name,
                'location_name' => $area->site ? $area->site->site_name : '',
                'created_by' => $area->createdBy ? $area->createdBy->full_name : '',
            ];
        }
        return $data;
    }
    private function getSuperAdminUsers($status)
    {
        if (Auth::user()->roles->first()->name == 'Super Admin') {
            $admins = User::where('status', $status)->whereHas('roles', function ($query) {
                $query->where('name', 'Admin');
            })->orderBy('created_at', 'desc')->get();
            $data = [];
            foreach ($admins as $admin) {
                $data[] = [
                    'name'         => $admin->full_name,
                    'position'     => $admin->client_representative_position,
                    'email'        => $admin->email,
                    'contact'      => $admin->contact_number,
                    'created_by'   => $admin->createdBy ? $admin->createdBy->full_name : '',
                ];
            }
        } else {
            $adminUsers = User::with('companyData')->where('status', $status)->where('company_id', Auth::user()->company_id)->whereHas('roles', function ($query) {
                $query->where('name', 'Admin');
            })->orderBy('created_at', 'desc')->get();
            $data = [];
            foreach ($adminUsers as $au) {
                $data[] = [
                    'name'         => $au->full_name,
                    'email'        => $au->email,
                    'contact'      => $au->contact_number,
                    'company_name' => $au->companyData->name,
                    'abn'          => $au->companyData->abn,
                    'created_by'   => $au->createdBy ? $au->createdBy->full_name : '',
                ];
            }
        }

        return $data;
    }
    private function getUsers($status)
    {
        $data = [];
        $clients = User::with(['companyData'])->where('status', $status)->whereExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('roles')
                ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                ->whereColumn('users.id', 'model_has_roles.model_id')
                ->where('name', 'User')
                ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('roles')
                        ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                        ->whereColumn('users.id', 'model_has_roles.model_id')
                        ->where('name', 'Admin');
                });
        })->orderBy('created_at', 'desc');

        if ($this->clientId != 0 & $this->locationId == 0) {
            $clients = User::with(['companyData'])->where('status', $status)->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('roles')
                    ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->whereColumn('users.id', 'model_has_roles.model_id')
                    ->where('name', 'User')
                    ->whereNotExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('roles')
                            ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                            ->whereColumn('users.id', 'model_has_roles.model_id')
                            ->where('name', 'Admin');
                    });
            });
            $clients = $clients->where('company_id', $this->clientId)->orderBy('created_at', 'desc');
        } elseif ($this->locationId != 0) {
            $users = SubSiteUser::select('ssu_user_id')->whereIn('ssu_sub_site_id', $this->subSiteArray)->whereHas('siteManager', function ($query) use ($status) {
                $query->where('status', $status);
            })->distinct('ssu_user_id')->pluck('ssu_user_id')->toArray();
            $clients = User::with(['companyData'])->whereIn('id', $users)->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('roles')
                    ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->whereColumn('users.id', 'model_has_roles.model_id')
                    ->where('name', 'User')
                    ->whereNotExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('roles')
                            ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                            ->whereColumn('users.id', 'model_has_roles.model_id')
                            ->where('name', 'Admin');
                    });
            })->orderBy('created_at', 'desc');
        }

        foreach ($clients->get() as $client) {
            $data[] = [
                'name'        => $client->full_name,
                'email'       => $client->email,
                'companyName' => ($client->companyData)?$client->companyData->name:null,
                'contact'     => $client->contact_number,
                'created_by'  => $client->createdBy ? $client->createdBy->full_name : '',
                'created_at'  => $this->getFormattedDate($client->created_at),
            ];
        }
        
        return $data;
    }
    private function getCategories($status)
    {
        $allClientAdminId = User::whereHas('roles', function ($query) {
            $query->where('name', 'Admin');
        })->pluck('id')->toArray();

        $categories = Category::where('category_status', $status)->whereIn('category_user_id', $allClientAdminId);
        if ($this->clientId != 0 && $this->locationId == 0) {
            $categories = $categories->where('company_id', $this->clientId);
        } elseif ($this->locationId != 0) {
            $categoryIds = SubSiteUserPermission::select('ssup_category_id')->whereIn('ssup_sub_site_id', $this->subSiteArray)->whereHas('category', function ($query) {
                $query->where('category_status', 'Y');
            })->distinct('ssup_category_id')->pluck('ssup_category_id')->toArray();
            $categories = Category::whereIn('category_id', $categoryIds)->where('category_status', $status);
        }

        $categories = $categories->with('companyData')->orderBy('created_at', 'desc');
        $data = [];
        foreach ($categories->get() as $category) {
            $data[] = [
                'name'       => $category->category_name,
                'created_by' => $category->createdBy ? $category->createdBy->full_name : 'N/A',
                'created_at' => $this->getFormattedDate($category->created_at),
                'company_name' => $category->companyData ? $category->companyData->name : 'N/A',
            ];
        }
        return $data;
    }
    private function getModules($status)
    {
        $allClientAdminId = User::whereHas('roles', function ($query) {
            $query->where('name', 'Admin');
        })->pluck('id')->toArray();

        $modules = Module::where('module_status', $status)->whereIn('module_user_id', $allClientAdminId)->whereHas('category', function ($query) {
            $query->whereNull('deleted_at');
        });
        if ($this->clientId != 0 && $this->locationId == 0) {
            $modules = $modules->where('company_id', $this->clientId);
        } elseif ($this->locationId != 0) {
            $moduleIds = SubSiteUserPermission::select('ssup_module_id')->whereIn('ssup_sub_site_id', $this->subSiteArray)->whereHas('module', function ($query) {
                $query->where('module_status', 'Y');
            })->distinct('ssup_module_id')->pluck('ssup_module_id')->toArray();
            $modules = Module::whereIn('module_id', $moduleIds)->where('module_status', $status);
        }
        $modules = $modules->orderBy('created_at', 'desc');
        $data = [];
        foreach ($modules->get() as $module) {
            $data[] = [
                'name'       => $module->module_name,
                'category_name'   => $module->category ? $module->category->category_name : '',
                'created_by' => $module->createdBy ? $module->createdBy->full_name : '',
            ];
        }
        return $data;
    }

    public function exportDashboardListingData(Request $request)
    {
        try {
            $data = json_decode($request->data);
            $column = json_decode($request->column);
            $title = $request->title;
            $response = [];
            if ($request->type == 'pdf') {
                $response = $this->generatePDF($data, $column, $title, $request->clientName, $request->locationName, $request->areaName, $request->datetime);
            } elseif ($request->type == 'csv') {
                $response = $this->generateCSV($data, $column, $title);
            } elseif ($request->type == 'mail') {
                $response = $this->sendToMail($data, $column, $title, $request->clientName, $request->locationName, $request->areaName, $request->datetime, $request->email);
            }
            return $this->returnData('data', $response);
        } catch (\Exception $e) {
            return $this->returnError(500, $e->getMessage());
            // return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
    private function generatePDF($data, $column, $title, $client, $location, $area, $dateTime)
    {
        //print_r($data);
        // $dateTime = $this->getFormattedDateTime(Carbon::now());
        //echo "now". Carbon::now() ;
        //echo "add Day". Carbon::now()->addDay();

        $path = storage_path('app/public/pdf');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
        $isClientAdmin = Auth::user()->roles->first()->name == 'Admin' ? true : false;
        $isSuperAdmin = Auth::user()->roles->first()->name == 'Super Admin' ? true : false;
        $isUserAdmin = Auth::user()->roles->first()->name == 'User' ? true : false;
        if ($isUserAdmin) {
            $clientCompanyName  = User::find(Auth::user()->created_by)->client_company_name ?? '';
            $companyLogo = User::find(Auth::user()->created_by)->company_id;
            
            $clientLogo = $companyLogo ? 'storage/' . Company::find($companyLogo)->logo : 'pdf_image/logo/sa_logo.png';
        } else {
            $clientLogo = Auth::user()->company_id ? 'storage/' . Company::find(Auth::user()->company_id)->logo : 'pdf_image/logo/sa_logo.png';
            $clientCompanyName = Auth::user()->client_company_name;
        }
        //$clientWithLogo = 'storage/'.$clientLogo;

        $pdf = PDF::loadView('reports.pdf.listing', array(
            'items'             => $data,
            'datetime'          => $dateTime,
            'head'              => $clientCompanyName . ' ' . $title,
            'columns'           => $column,
            'client'            => $client,
            'displayClient'     => $isSuperAdmin,
            'location'          => $location,
            'area'              => $area,
            'isClientAdmin'     => $isClientAdmin,
            'isUserAdmin'     =>   $isUserAdmin,
            'clientLogo'        => $clientLogo,
            'clientCompanyName' => $clientCompanyName,
        ));

        $fileName = str_pad(mt_rand(1, 99999999), 8, '0', STR_PAD_LEFT) . '.pdf';
        // $fileName = $request->apiHead.'.pdf';
        $filePath = $path . '/' . $fileName;
        $pdf->save($filePath);

        return ['success' => true, 'fileName' => $fileName];
    }
    private function generateCSV($data, $column, $title)
    {
        $path = storage_path('app/public/csv');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }

        $exportData = [
            'data' => $data,
            'columns' => $column,
        ];

        $export = new ListingExport($exportData);
        $fileName = str_pad(mt_rand(1, 99999999), 8, '0', STR_PAD_LEFT) . '.csv';
        Excel::store($export, $fileName, 'csv');

        return ['success' => true, 'fileName' => $fileName];
    }
    private function sendToMail($data, $column, $title, $client, $location, $area, $dateTime, $mail)
    {
        $isClientAdmin = Auth::user()->roles->first()->name == 'Admin' ? true : false;

        $isSuperAdmin = Auth::user()->roles->first()->name == 'Super Admin' ? true : false;
        $isUserAdmin = Auth::user()->roles->first()->name == 'User' ? true : false;
        if ($isUserAdmin) {
            $clientCompanyName  = User::find(Auth::user()->created_by)->client_company_name ?? '';
            $clientLogo = 'storage/' . Company::find(Auth::user()->company_id)->logo ?? 'pdf_image/logo/sa_logo.png';
        } else {
            $clientLogo = Auth::user()->company_id ? 'storage/' . Company::find(Auth::user()->company_id)->logo : 'pdf_image/logo/sa_logo.png';
            $clientCompanyName = Auth::user()->client_company_name;
        }
        $mail = Mail::send('reports.pdf.listingemail', array(
            'items'             => $data,
            'datetime'          => $dateTime,
            'head'              => $clientCompanyName . ' ' . $title,
            'columns'           => $column,
            'client'            => $client,
            'location'          => $location,
            'area'              => $area,
            'displayClient'     => $isSuperAdmin,
            'isUserAdmin'       => $isUserAdmin,
            'isClientAdmin'     => $isClientAdmin,
            'clientLogo'        => $clientLogo,
            'clientCompanyName' => $clientCompanyName,
        ), function ($message) use ($mail, $title) {
            $message->to($mail)->subject($title);
        });
        return ['success' => true];
    }
}
