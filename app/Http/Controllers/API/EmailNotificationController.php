<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\FieldInputType;
use App\Models\ModuleField;
use App\Models\User;
use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\GeneralTrait;
use App\Models\Inspection;
use Mail;

class EmailNotificationController extends Controller
{
    use GeneralTrait;

    public function assetToExpire($time, $subSiteArray)
    {
        $title = "Assets Due to Expire in {$time} days";
        if($time==1) {
            $title = "Assets Due to Expire in 24 hrs";
        }
        $moduleFieldsColorId = $this->getModuleFieldColorId();
        $expiryMFids = $this->getExpiryMFids();
        $mfIds = array_merge($moduleFieldsColorId, $expiryMFids);
        $assets = Asset::whereNull('deleted_at');
        if(count($subSiteArray)>0) {
            $assets = Asset::whereIn('asset_sub_site_id', $subSiteArray);
        }
        if($time==1){
            $assets = $assets->whereHas('values', function ($query) use ($mfIds) {
                $query->whereIn('cmfv_mf_id', $mfIds)->whereDate('cmfv_value_date','>=',Carbon::now())->whereDate('cmfv_value_date', '<=', Carbon::now()->addDay());
            })->with('inspections', function ($query) {
                $query->latest();
            })->orderBy('asset_id', 'desc')->get();
        } elseif($time>1){
            $assets = $assets->whereHas('values', function ($query) use ($mfIds, $time) {
                $query->whereIn('cmfv_mf_id', $mfIds)->whereDate('cmfv_value_date','>=',Carbon::now())->whereDate('cmfv_value_date', '<=', Carbon::now()->addDays($time));
            // })->with('inspections', function ($query) {
            //     $query->latest();
            })->orderBy('asset_id', 'desc')->get();
        }

        $currentRecord = $assets;
        // Adding is_notified in that time frame filter
        $newAssets = $assets->filter(function($asset) use ($time) {
            if($time==1 && $asset->is_notified_24hrs==0) {
                return $asset;
            } elseif($time==7 && $asset->is_notified_7days==0) {
                return $asset;
            } elseif($time==30 && $asset->is_notified_30days==0) {
                return $asset;
            } elseif($time==90 && $asset->is_notified_90days==0) {
                return $asset;
            }
        });

        if(count($newAssets)>0) {
            $previousRecord = $assets->filter(function($asset) use ($time) {
                if($time==1 && $asset->is_notified_24hrs==1) {
                    return $asset;
                } elseif($time==7 && $asset->is_notified_7days==1) {
                    return $asset;
                } elseif($time==30 && $asset->is_notified_30days==1) {
                    return $asset;
                } elseif($time==90 && $asset->is_notified_90days==1) {
                    return $asset;
                }
            });

            $this->sendNotificationMail($this->generateAssetListData($previousRecord), $this->generateAssetListData($currentRecord), $title, 'assets', $time);
        }
    }

    public function sendNotificationMail($oldData, $newData, $title, $model, $time=0)
    {
        $isAdmin = Auth::user()->roles->first()->name == 'Admin';
        if(!$isAdmin) {
            return false;
        }
        // all newData records' is_notified column as true
        if($model == 'inspections') {
            foreach($newData as $data) {
                $inspection = Inspection::find($data['last_inspection_id']);
                $inspection->is_notified = 1;
                $inspection->save();
            }
        } elseif($model == 'assets') {
            foreach($newData as $data) {
                $asset = Asset::find($data['id']);
                if($time == 1) {
                    $asset->is_notified_24hrs = 1;
                } elseif($time == 7) {
                    $asset->is_notified_7days = 1;
                } elseif($time == 30) {
                    $asset->is_notified_30days = 1;
                } elseif($time == 90) {
                    $asset->is_notified_90days = 1;
                } elseif($time == 180) {
                    $asset->is_notified_180days = 1;
                }
                $asset->save();
            }
        }

        // Get company data
        $companyId = Auth::user()->company_id;
        $companyData = Company::find($companyId);

        // Get root admin user data
        $rootAdminUserEmail  = User::select('email','first_name')
                                    ->where('company_id', $companyId)
                                    ->where('is_root', '0')
                                    ->whereHas('roles', function ($query) {
                                        $query->where('name', 'Admin');
                                    })
                                    ->first();

        // Get all admin data from company
        $adminUserEmailArray  = User::where('company_id', $companyId)
                                    ->where('is_root', '1')
                                    ->whereHas('roles', function ($query) {
                                        $query->whereIn('name', ['Admin']);
                                    })
                                    ->pluck('email')
                                    ->toArray();

        // Mail Sending Work below onwards
        $columns = [[
                "key"=>"row",
                "label"=>"Row #"
            ],[
                "key"=>"uid",
                "label"=>"UID",
                "sortable"=>true
            ],
            // [
            //     "key"=>"client",
            //     "label"=>"Client"
            // ],
            [
                "key"=>"location",
                "label"=>"Location"
            ],[
                "key"=>"area",
                "label"=>"Area"
            ],[
                "key"=>"category",
                "label"=>"Category"
            ],[
                "key"=>"module",
                "label"=>"Module"
            ],[
                "key"=>"q",
                "label"=>"Inspection Schedule"
            ],[
                "key"=>"status",
                "label"=>"Status"
            ],[
                "key"=>"expires",
                "label"=>"Expires"
            ],[
                "key"=>"last_inspection",
                "label"=>"Last Inspection"
            ],[
                "key"=>"inspected_by",
                "label"=>"Inspected By"
            ],[
                "key"=>"actions",
                "label"=>"Actions",
                "thClass"=>"text-center"
            ]];

        $dateTime = $this->getFormattedDate($this->convertTimeZone('UTC', request()->timeZone, Carbon::now()));
        $clientLogo = $companyData->logo ? 'storage/'.$companyData->logo : 'pdf_image/logo/sa_logo.png';

        $mail = Mail::send('reports.email.notification', array(
            // 'items'             => $data,
            'datetime'          => $dateTime,
            'head'              => $title,
            'clientLogo'        => $clientLogo,
            'oldData'           => $oldData,
            'newData'           => $newData,
            'columns'           => $columns
        ), function($message) use ($rootAdminUserEmail, $adminUserEmailArray, $title){
            (count($adminUserEmailArray) > 0)?$message->to($rootAdminUserEmail['email'])->bcc($adminUserEmailArray)->subject($title):$message->to($rootAdminUserEmail['email'])->subject($title);
        });

        if($mail){
            return true;
        }else{
            return false;
        }
    }

    private function getModuleFieldColorId()
    {
        $fieldColorTypeId = FieldInputType::where('field_input_type_name', 'Color')->first()->field_input_type_id;
        $moduleFieldsColorId = ModuleField::where('mf_input_field_type_id', $fieldColorTypeId)->pluck('mf_id')->toArray();
        return $moduleFieldsColorId;
    }

    public function generateAssetListData($assets)
    {
        // inspections relation has to be passed with assets.
        $data = [];
        $expiryMFids = $this->getExpiryMFids();
        foreach ($assets as $key => $asset) {
            // to find expiry date
            $expiryDate = '';
            foreach($asset->values as $value) {
                if(in_array($value->cmfv_mf_id, $expiryMFids)) {
                    $expiryDate = $value->cmfv_value_date ? $this->getFormattedDate($value->cmfv_value_date) : '';
                }
            }
            $status = 'Inspection Pending';
            $variant = 'secondary';
            $lastInspection = null;
            $lastInspectionId = null;
            $inspectedBy = null;

            if ($asset->inspections->count() > 0) {
                $lastInspection = $asset->inspections[0]->created_at;
                $lastInspectionId = $asset->inspections[0]->inspection_id;
                $inspectedBy = $asset->inspections[0]->createdBy ? $asset->inspections[0]->createdBy->full_name:'';
                if ($asset->inspections[0]->inspection_result == 'pass') {
                    $variant = 'success';
                    $status = 'Pass';
                } elseif ($asset->inspections[0]->inspection_result == 'fail') {
                    $variant = 'danger';
                    $status = 'Fail';
                } elseif ($asset->inspections[0]->inspection_result == 'maintenance_required') {
                    $variant = 'warning';
                    $status = 'Maintenance Required';
                }
            } else {
                $status = 'Inspection Pending';
                $variant = 'secondary';
            }

            $color = null;
            if ($asset->values->count() > 0) {
                $color = $asset->values[0]->cmfv_value_char;
            }
            
            $data[] = [
                'id'                 => $asset->asset_id,
                'uid'                => $asset->asset_uid,
                'client'             => Company::find($asset->company_id)->name ?? '',
                'location'           => $asset->site->site_name??'',
                'area'               => $asset->subSite->sub_site_name??'',
                'category'           => $asset->module->category->category_name??'',
                'module'             => $asset->module->module_name??'',
                'q'                  => $color,
                'status'             => $status,
                'variant'            => $variant,
                'expires'            => $expiryDate,
                'hasExpired'         => $expiryDate ? Carbon::parse($expiryDate)->isPast() : false,
                // 'raised'          => $asset->created_at ? $this->getFormattedDate($asset->created_at) : '',
                'last_inspection'    => $lastInspection ? $this->getFormattedDate($lastInspection) : '',
                'inspected_by'       => $inspectedBy,
                'last_inspection_id' => $lastInspectionId,
            ];
        }
        return $data;
    }
}
