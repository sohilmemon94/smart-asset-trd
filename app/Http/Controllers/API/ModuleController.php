<?php

namespace App\Http\Controllers\API;

use Throwable;
use Carbon\Carbon;
use App\Models\Module;
use App\Models\Category;
use App\Models\ModuleField;
use App\Models\SubSitePermission;
use Illuminate\Http\Request;
use App\Models\TemplateField;
use App\Http\Traits\GeneralTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\SubSiteUserPermission;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\ModuleFieldController;
use App\Models\Asset;
use App\Models\ModuleAnswer;
use App\Models\ModuleQuestion;


class ModuleController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $perPage    = isset($request->perPage) ? $request->perPage : null;
            $search     = $request->search;
            $sortBy     = isset($request->sortBy) ? $request->sortBy  : 'module_name';
            $sortDesc   = ($request->sortDesc == 'true') ? 'desc' : 'asc';
            $status     = $request->status;
            $categoryId = $request->categoryId;
            $subSiteId = $request->sub_site_id;
            $moduleIsDefault = isset($request->moduleIsDefault) ? $request->moduleIsDefault : null;

            // Get module data
            $moduleData = Module::with('category')->where('module_name', '!=', ' ')->where('company_id', Auth::user()->company_id);

            // Module for admin user
            if ($moduleIsDefault) {
                $moduleData = Module::with('category')->where('module_is_default', $moduleIsDefault);
            }

            if ($subSiteId) {
                $ids = SubSiteUserPermission::where('ssup_sub_site_id', $subSiteId)->groupBy('ssup_module_id')->pluck('ssup_module_id')->toArray();
                $moduleData = Module::with('category')->whereIn('module_id', $ids);
            }

            $categoryName = '';
            if ($categoryId > 0) {
                // Get Category data
                $categoryData = Category::find($categoryId);
                $categoryName = $categoryData->category_name;

                $moduleData = $moduleData->where('module_category_id', $categoryId);
            }

            // Status filter
            if ($status) {
                $moduleData = $moduleData->where('module_status', $status);
            }

            // Search filter
            if ($search) {
                $moduleData = $moduleData->where(function ($query) use ($search) {
                    $query->where('module_id', 'LIKE', '%' . $search . '%')
                        ->orWhere('module_name', 'LIKE', '%' . $search . '%')
                        ->orWhereHas('category', function ($categoryQuery) use ($search) {
                            $categoryQuery->where('category_name', 'LIKE', '%' . $search . '%');
                        });
                });
            }

            if ($perPage) {
                $moduleData = $moduleData->orderBy($sortBy, $sortDesc)->paginate($perPage);
                $pagination = [
                    "total"        => $moduleData->total(),
                    "current_page" => $moduleData->currentPage(),
                    "last_page"    => $moduleData->lastPage(),
                    "from"         => $moduleData->firstItem(),
                    "to"           => $moduleData->lastItem()
                ];
                $data = ['categoryName' => $categoryName, 'modules' => $moduleData, "total" => $moduleData->total(), 'pagination' => $pagination,];
            } else {

                if ($request->sub_site_id && $request->category_id && $request->user_id) {
                    $modules  = SubSiteUserPermission::where('ssup_user_id', $request->user_id)->where('ssup_sub_site_id', $request->sub_site_id)->where('ssup_category_id', $request->category_id)->distinct()->pluck('ssup_module_id')->toArray();

                    $moduleList = [];
                    foreach ($modules as $index => $moduleId) {
                        $module = Module::find($moduleId);

                        $moduleList[] =  array(
                            "value" => $module->module_id,
                            "text" => $module->module_name
                        );
                    }
                    // $moduleCatData = Module::whereNotNull('module_name')
                    // ->where('module_category_id', $request -> category_id)
                    // -> where(function ($query){
                    //     $query
                    //     ->where('created_by', Auth::user() -> id)
                    //     ->orWhere('module_user_id', Auth::user()->id);
                    // }) -> get();


                    // foreach($moduleCatData as $key => $value) {
                    //    $moduleList[]=  Array (
                    //     "value" => $value['module_id'],
                    //     "text" => $value['module_name']
                    //    );
                    // }
                    //    dd($moduleList);
                } else {
                    $moduleData = Module::with('category')->where('module_name', '!=', ' ')
                        ->where('company_id', Auth::user()->company_id)
                        ->orWhere('module_user_id', Auth::user()->id)->get();
                }
                $data = ['modules' => $moduleData, 'moduleList' => $moduleList];
            }

            return $this->returnSuccessMessage('Module List Fetch Successfully.', $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'module_name'   => 'required|string',
            // 'module_prefix' => 'required|string'
        ]);

        if ($validator->fails()) {
            // get all errors as single string
            return $this->returnValidation($validator->errors());
        }

        try {
            if($request->module_inspection_schedule == 'RGBY'){
                $inspectionScheduleDays = 90;
            } else {
                $inspectionScheduleDays = ($request->module_inspection_schedule != 'RGBY') ? $request->module_inspection_schedule : NULL;
            }

            $moduleData = array(
                'module_user_id'                  => Auth::user()->id,
                'module_name'                     => $request->module_name,
                // 'module_prefix'                => $request->module_prefix,
                'module_desc'                     => $request->module_desc,
                'module_inspection_schedule'      => $inspectionScheduleDays,
                'module_inspection_schedule_rgby' => ($request->module_inspection_schedule == 'RGBY') ? 'Y' : 'N',
                'module_category_id'              => $request->module_category_id,
                'module_is_default'               => ($request->module_is_default == 'Y') ? 'Y' : 'N',
                'module_has_activity'             => ($request->module_has_activity == 'true') ? 'Y' : 'N',
                'module_has_image'                => ($request->module_has_image == 'true') ? 'Y' : 'N',
                'module_allow_duplicates'         => ($request->module_allow_duplicates == 'true') ? 'Y' : 'N',
                'module_status'                   => ($request->module_status == 'true') ? 'Y' : 'N',
                'created_by'                      => Auth::user()->id,
                'company_id'                      => Auth::user()->company_id,
            );

            if ($request->module_icon) {
                $filePath = $this->fileUpload($request->module_icon, 'image/modules');
                $moduleData['module_icon'] = $filePath;
            }

            // Save data
            $moduleObj = Module::create($moduleData);

            // Clone template data
            if ($request->module_template_id > 0 && $moduleObj->module_id) {
                $templateFieldData = TemplateField::where('template_field_template_id', $request->module_template_id)->where('template_field_status', 'Y')->get();

                if (sizeOf($templateFieldData) > 0) {
                    $moduleFieldData = [];
                    foreach ($templateFieldData as $key => $templateField) {
                        $moduleFieldData[$key]['mf_user_id']             = Auth::user()->id;
                        $moduleFieldData[$key]['mf_module_id']           = $moduleObj->module_id;
                        $moduleFieldData[$key]['mf_input_field_type_id'] = $templateField->template_field_input_field_type_id;
                        $moduleFieldData[$key]['mf_name']                = $templateField->template_field_name;
                        $moduleFieldData[$key]['mf_default_val']         = $templateField->template_field_default_val;
                        $moduleFieldData[$key]['mf_is_show_on_grid']     = $templateField->template_field_is_show_on_grid;
                        $moduleFieldData[$key]['mf_is_show_on_details']  = $templateField->template_field_is_show_on_details;
                        $moduleFieldData[$key]['mf_is_required']         = $templateField->template_field_is_required;
                        $moduleFieldData[$key]['mf_status']              = $templateField->template_field_status;
                        $moduleFieldData[$key]['created_by']             = Auth::user()->id;
                        $moduleFieldData[$key]['company_id']             = Auth::user()->company_id;
                        $moduleFieldData[$key]['created_at']             = Carbon::now();
                        $moduleFieldData[$key]['updated_at']             = Carbon::now();
                    }

                    // Create module field
                    ModuleField::insert($moduleFieldData);
                }
            }

            return $this->returnSuccessMessage('Module Created Successfully.', '');
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $moduleData = Module::where('module_id', $id)->first();

            if ($moduleData) {
                $moduleData->module_icon  = $moduleData->module_icon ? $this->getStorageURL($moduleData->module_icon) : '';

                return $this->returnSuccessMessage('Module retrieved successfully.', $moduleData);
            } else {
                return $this->returnError(404, 'Oppps! No record found...');
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'module_name'   => 'required|string'
            ]);

            if ($validator->fails()) {
                // get all errors as single string
                return $this->returnValidation($validator->errors());
            }

            if($request->module_inspection_schedule == 'RGBY'){
                $inspectionScheduleDays = 90;
            } else {
                $inspectionScheduleDays = ($request->module_inspection_schedule != 'RGBY') ? $request->module_inspection_schedule : NULL;
            }

            $moduleData = array(
                'module_name'                     => $request->module_name,
                // 'module_prefix'                => $request->module_prefix ?? NULL,
                'module_inspection_schedule'      => $inspectionScheduleDays,
                'module_inspection_schedule_rgby' => ($request->module_inspection_schedule == 'RGBY') ? 'Y' : 'N',
                'module_desc'                     => $request->module_desc ?? NULL,
                'module_category_id'              => $request->module_category_id,
                'module_is_default'               => ($request->module_is_default == 'Y') ? 'Y' : 'N',
                'module_has_image'                => ($request->module_has_image == 'true') ? 'Y' : 'N',
                'module_has_activity'             => $request->module_has_activity == 'true' ? 'Y' : 'N',
                'module_allow_duplicates'         => $request->module_allow_duplicates == 'true' ? 'Y' : 'N',
                'module_status'                   => ($request->module_status == 'true') ? 'Y' : 'N',
                'updated_by'                      => Auth::user()->id,
            );

            if ($request->file() && $request->module_icon) {
                $filePath = $this->fileUpload($request->module_icon, 'image/modules');
                $moduleData['module_icon'] = $filePath;
            }

            // Clone template data
            if ($request->module_template_id > 0 && $id) {
                $templateFieldData = TemplateField::where('template_field_template_id', $request->module_template_id)->where('template_field_status', 'Y')->get();

                if (sizeOf($templateFieldData) > 0) {
                    $moduleFieldData = [];
                    foreach ($templateFieldData as $key => $templateField) {
                        $moduleFieldData[$key]['mf_user_id']             = Auth::user()->id;
                        $moduleFieldData[$key]['mf_module_id']           = $id;
                        $moduleFieldData[$key]['mf_input_field_type_id'] = $templateField->template_field_input_field_type_id;
                        $moduleFieldData[$key]['mf_name']                = $templateField->template_field_name;
                        $moduleFieldData[$key]['mf_default_val']         = $templateField->template_field_default_val;
                        $moduleFieldData[$key]['mf_is_show_on_grid']     = $templateField->template_field_is_show_on_grid;
                        $moduleFieldData[$key]['mf_is_show_on_details']  = $templateField->template_field_is_show_on_details;
                        $moduleFieldData[$key]['mf_is_required']         = $templateField->template_field_is_required;
                        $moduleFieldData[$key]['mf_status']              = $templateField->template_field_status;
                        $moduleFieldData[$key]['created_by']             = Auth::user()->id;
                        $moduleFieldData[$key]['company_id']             = Auth::user()->company_id;
                        $moduleFieldData[$key]['created_at']             = Carbon::now();
                        $moduleFieldData[$key]['updated_at']             = Carbon::now();
                    }

                    // Create module field
                    ModuleField::insert($moduleFieldData);
                }
            }

            // Update data
            Module::where('module_id', $id)->update($moduleData);

            return $this->returnSuccessMessage('Module Updated Successfully.');
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            //dd($request->id);
            Module::where('module_id', $request->id)->first()->delete();
            ModuleField::where('mf_module_id', $request->id)->delete();
            SubSitePermission::where('ssp_module_id', $request->id)->delete();
            SubSiteUserPermission::where('ssup_module_id', $request->id)->delete();
            Asset::where('asset_module_id', $request->id)->delete();
            ModuleAnswer::where('msa_module_id', $request->id)->delete();
            ModuleQuestion::where('msq_module_id', $request->id)->delete();


            return $this->returnSuccessMessage('Module deleted successfully.');
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Change module status
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function changeModuleStatus(Request $request)
    {
        try {
            if ($request->id) {
                if ($request->status == 'Y') {
                    Module::where('module_id', $request->id)->update(['module_status' => 'N']);
                } else {
                    Module::where('module_id', $request->id)->update(['module_status' => 'Y']);
                }

                return $this->returnSuccessMessage('You have successfully changed module status.');
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Upload module icon
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadModuleIcon(Request $request)
    {
        try {
            $moduleData = Module::where('module_id', $request->module_id)->first();

            if ($request->module_icon) {

                if ($request->file()) {
                    $filePath = $this->fileUpload($request->module_icon, 'image/module');
                }

                // Remove existing image on server
                if (isset($moduleData) && $moduleData->module_icon) {
                    $this->deleteFile($moduleData->module_icon);
                }

                $moduleData->update(['module_icon' => $filePath]);

                // Set full URL
                $moduleData->module_icon = !empty($moduleData->module_icon) ? $this->getStorageURL($moduleData->module_icon) : '';

                return $this->returnSuccessMessage('You have successfully updated module icon.', $moduleData);
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Remove module icon
     */
    public function removeModuleIcon(Request $request)
    {
        try {
            if ($request->module_id) {
                $moduleData = Module::where('module_id', $request->module_id)->first();

                // Remove existing image on server
                if (isset($moduleData) && $moduleData->module_icon != '') {
                    $this->deleteFile($moduleData->module_icon);

                    // Update user data
                    $moduleData->update(array('module_icon' => ''));

                    return $this->returnSuccessMessage('You have successfully removed module icon.');
                }
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Clone module and module fields table
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function cloneModuleAndModuleField($userId, $oldCategoryId, $newCategoryId, $companyId)
    {
        if ($userId > 0 && $oldCategoryId > 0 && $newCategoryId > 0) {
            // Get module data
            $moduleData = Module::where('module_category_id', $oldCategoryId)->get()->keyBy('module_id');

            if (sizeof($moduleData) > 0) {
                $moduleInsertData = array();
                foreach ($moduleData as $module) {
                    $moduleInsertData = array(
                        'module_user_id'                  => $userId,
                        'module_name'                     => $module['module_name'],
                        // 'module_prefix'                => $module['module_prefix'],
                        'module_inspection_schedule'      => $module['module_inspection_schedule'],
                        'module_inspection_schedule_rgby' => $module['module_inspection_schedule_rgby'],
                        'module_desc'                     => $module['module_desc'],
                        'module_category_id'              => $newCategoryId,
                        'module_is_default'               => 'N',
                        'module_has_image'                => 'Y',
                        'module_has_activity'             => $module['module_has_activity'],
                        'module_allow_duplicates'         => $module['module_allow_duplicates'],
                        'module_status'                   => $module['module_status'],
                        'created_by'                      => Auth::user()->id,
                        'company_id'                      => $companyId,
                    );

                    // Insert data
                    $moduleObj = Module::create($moduleInsertData);

                    // Get module ids and clone module related data
                    if ($module->module_id > 0 && $moduleObj->module_id > 0) {
                        // Clone module field
                        $moduleFieldObj = new ModuleFieldController;
                        $moduleFieldObj->cloneModuleField($userId, $module->module_id, $moduleObj->module_id, $companyId);

                        // Clone module question
                        $moduleQuestionObj = new ModuleQuestionController;
                        $moduleQuestionObj->cloneModuleQuestion($userId, $module->module_id, $moduleObj->module_id, $companyId);
                    }
                }
            }

            return true;
        }
        return false;
    }
}
