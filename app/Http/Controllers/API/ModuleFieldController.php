<?php

namespace App\Http\Controllers\API;

use App\Models\Module;
use App\Models\ModuleField;
use App\Models\FieldInputType;
use App\Models\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTrait;
use App\Models\TemplateField;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

use Throwable;

class ModuleFieldController extends Controller
{
	use GeneralTrait;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		//try {
		$perPage  = isset($request->perPage) ? $request->perPage : null;
		$search   = $request->search;
		$sortBy   = isset($request->sortBy) ? $request->sortBy : 'mf_id';
		$sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
		$status   = $request->status;
		$moduleId = $request->moduleId;

		// Get Module data
		$moduleData = Module::find($moduleId);

		$moduleFieldData = ModuleField::with('fieldInputType')->where('mf_name', '!=', ' ')->where('mf_module_id', $moduleId);

		// Status filter
		if ($status) {
			$moduleFieldData = $moduleFieldData->where('mf_status', $status);
		}

		// Search filter
		if ($search) {
			$moduleFieldData = $moduleFieldData->where(function ($query) use ($search) {
				$query->orWhere('mf_name', 'LIKE', '%' . $search . '%')
					->orWhereHas('fieldInputType', function ($fieldQuery) use ($search) {
						$fieldQuery->where('field_input_type_name', 'LIKE', '%' . $search . '%');
					});
			});

			// $moduleFieldData = $moduleFieldData->where('mf_name', 'LIKE', '%' . $search . '%')
			// 	->orWhereHas('fieldInputType', function ($query) use ($search) {
			// 		$query->where('field_input_type_name', 'LIKE', '%' . $search . '%');
			// 	})->where('mf_module_id', $moduleId);
		}

		$moduleFieldData = $moduleFieldData->orderBy($sortBy, $sortDesc)->paginate($perPage);

		$pagination = [
			"total"        => $moduleFieldData->total(),
			"current_page" => $moduleFieldData->currentPage(),
			"last_page"    => $moduleFieldData->lastPage(),
			"from"         => $moduleFieldData->firstItem(),
			"to"           => $moduleFieldData->lastItem()
		];

		$data = ['moduleName' => $moduleData->module_name, 'moduleField' => $moduleFieldData, 'total' => $moduleFieldData->total(), 'pagination' => $pagination];

		return $this->returnSuccessMessage(null, $data);
		// } catch (\Exception $e) {
		// 	return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		// }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'mf_name'          => 'required',
			'mf_input_field_type_id' => 'required'
		]);

		if ($validator->fails()) {
			// get all errors as single string
			return $this->returnValidation($validator->errors());
		}

		$moduleFieldData = array(
			'mf_module_id'           => $request->mf_module_id,
			'mf_user_id'             => Auth::user()->id,
			'mf_name'                => $request->mf_name,
			'mf_input_field_type_id' => $request->mf_input_field_type_id,
			'mf_is_show_on_grid'     => $request->mf_is_show_on_grid ? 'Y'   : 'N',
			'mf_is_show_on_details'  => $request->mf_is_show_on_details ? 'Y' : 'N',
			'mf_has_expiry_date'  => $request->mf_has_expiry_date ? 'Y' : 'N',
			'mf_is_required'         => $request->mf_is_required ? 'Y'       : 'N',
			'mf_status'              => $request->mf_status ? 'Y'            : 'N',
			'created_by'             => Auth::user()->id,
            'company_id'             => Auth::user()->company_id,
		);

		// Save data
		ModuleField::create($moduleFieldData);

		return $this->returnSuccessMessage('Module Field Created Successfully.', '');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Module  $module
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		try {
			$module = Module::where('module_id', $id)->first();

			if ($module) {
				return $this->returnSuccessMessage('Module retrieved successfully.', $module);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\Module  $module
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		try {
			$moduleFieldData = ModuleField::where('mf_id', $id)->first();

			if ($moduleFieldData) {
				return $this->returnSuccessMessage('Module retrieved successfully.', $moduleFieldData);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Module  $module
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$moduleFieldObj = ModuleField::find($id);

		if ($moduleFieldObj == null) {
			return $this->returnError(404, "Oppps! Module field not found");
		}

		$validator = Validator::make($request->all(), [
			'Module_name' => 'required|string',
		]);

		$validator = Validator::make($request->all(), [
			'mf_name'            => 'required',
			'mf_is_show_on_grid' => 'required'
		]);

		if ($validator->fails()) {
			// get all errors as single string
			return $this->returnValidation($validator->errors());
		}

		$moduleFieldData = array(
			'mf_module_id'           => $request->mf_module_id,
			'mf_user_id'             => Auth::user()->id,
			'mf_name'                => $request->mf_name,
			'mf_default_val'         => $request->mf_default_val,
			'mf_input_field_type_id' => $request->mf_input_field_type_id,
			'mf_is_show_on_grid'     => $request->mf_is_show_on_grid ? 'Y'   : 'N',
			'mf_is_show_on_details'  => $request->mf_is_show_on_details ? 'Y' : 'N',
			'mf_has_expiry_date'     => $request->mf_has_expiry_date ? 'Y' : 'N',
			'mf_is_required'         => $request->mf_is_required ? 'Y'       : 'N',
			'mf_status'              => $request->mf_status ? 'Y'            : 'N',
			'updated_by'             => Auth::user()->id,
		);

		// Update data
		$moduleFieldObj->update($moduleFieldData);

		return $this->returnSuccessMessage('Module Field Updated Successfully.', $moduleFieldData);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Module  $module
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request)
	{
		if ($request->id) {
			$moduleData = ModuleField::where('mf_id', $request->id)->first();
			$moduleData->delete();

			return $this->returnSuccessMessage('Module Field deleted successfully.', $moduleData);
		} else {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	public function changeModuleFieldStatus(Request $request)
	{
		try {
			if ($request->id) {
				if ($request->status == 'Y') {
					ModuleField::where('mf_id', $request->id)->update(['mf_status' => 'N']);
				} else {
					ModuleField::where('mf_id', $request->id)->update(['mf_status' => 'Y']);
				}

				return $this->returnSuccessMessage('You have successfully changed Module field status.');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Get all input field
	 *
	 * @param  \App\Models\FieldInputType
	 * @return \Illuminate\Http\Response
	 */
	public function getAllInputField()
	{
		try {
			$data = FieldInputType::select('field_input_type_id', 'field_input_type_name', 'field_input_type')->where('field_input_type_status', 'Y')->get();

			$inputFieldTypeOption = [];
			foreach ($data as $key => $value) {
				$inputFieldTypeOption[$key]['label'] = $value['field_input_type_name'];
				$inputFieldTypeOption[$key]['value'] = $value['field_input_type_id'];
			}
			return $this->returnSuccessMessage(null, $inputFieldTypeOption);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * save template field for module
	 *
	 * @param  \App\Models\ModuleField
	 * @return \Illuminate\Http\Response
	 */
	public function saveTemplateFieldForModule(Request $request)
	{
		try {
			if ($request->moduleId && $request->templateId && sizeOf($request->templateFieldIdArray) > 0) {
				$templateFieldData = TemplateField::where('template_field_template_id', $request->templateId)->whereIn('template_field_id', $request->templateFieldIdArray)->get();

				$moduleFieldData = [];
				foreach ($templateFieldData as $key => $templateField) {
					$moduleFieldData[$key]['mf_user_id']             = Auth::user()->id;
					$moduleFieldData[$key]['mf_module_id']           = $request->moduleId;
					$moduleFieldData[$key]['mf_input_field_type_id'] = $templateField->template_field_input_field_type_id;
					$moduleFieldData[$key]['mf_name']                = $templateField->template_field_name;
					$moduleFieldData[$key]['mf_default_val']         = $templateField->template_field_default_val;
					$moduleFieldData[$key]['mf_is_show_on_grid']     = $templateField->template_field_is_show_on_grid;
					$moduleFieldData[$key]['mf_has_expiry_date']     = $templateField->template_field_has_expiry_date;
					$moduleFieldData[$key]['mf_is_show_on_details']  = $templateField->template_field_is_show_on_details;
					$moduleFieldData[$key]['mf_is_required']         = $templateField->template_field_is_required;
					$moduleFieldData[$key]['mf_status']              = $templateField->template_field_status;
					$moduleFieldData[$key]['created_by']             = Auth::user()->id;
					$moduleFieldData[$key]['company_id']             = Auth::user()->company_id;
					$moduleFieldData[$key]['created_at']             = Carbon::now();
					$moduleFieldData[$key]['updated_at']             = Carbon::now();
				}

				// Create module field
				ModuleField::insert($moduleFieldData);

				return $this->returnSuccessMessage('You have successfully clone fields from template.');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Clone Module Field based on module ids
	 *
	 * @param  \App\Models\ModuleField
	 * @return \Illuminate\Http\Response
	 */
	public function cloneModuleField($userId, $oldModuleId, $newModuleId, $companyId)
	{
		try {
			if ($userId > 0 && $oldModuleId > 0 && $newModuleId > 0) {
				$moduleFieldData = ModuleField::where('mf_module_id', $oldModuleId)->get();

				foreach ($moduleFieldData as $moduleField) {
					$moduleFieldData = array(
						'mf_user_id'             => $userId,
						'mf_module_id'           => $newModuleId,
						'mf_input_field_type_id' => $moduleField->mf_input_field_type_id,
						'mf_name'                => $moduleField->mf_name,
						'mf_default_val'         => $moduleField->mf_default_val,
						'mf_is_show_on_grid'     => $moduleField->mf_is_show_on_grid,
						'mf_has_expiry_date'     => $moduleField->mf_has_expiry_date,
						'mf_is_show_on_details'  => $moduleField->mf_is_show_on_details,
						'mf_is_required'         => $moduleField->mf_is_required,
						'mf_status'              => $moduleField->mf_status,
						'created_by'             => Auth::user()->id,
						'company_id'             => $companyId,
					);

					// Create module field
					ModuleField::create($moduleFieldData);
				}

				return true;
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}
}
