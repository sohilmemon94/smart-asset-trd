<?php

namespace App\Http\Controllers\API;

use Throwable;
use Carbon\Carbon;
use App\Models\Module;
use App\Models\ModuleAnswer;
use Illuminate\Http\Request;
use App\Models\ModuleQuestion;
use App\Http\Traits\GeneralTrait;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ModuleQuestionController extends Controller
{
	use GeneralTrait;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		//try {
		$perPage  = isset($request->perPage) ? $request->perPage : null;
		$search   = $request->search;
		$sortBy   = isset($request->sortBy) ? $request->sortBy : 'msq_id';
		$sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
		$status   = $request->status;
		$moduleId = $request->moduleId;
		$msqIsDefault = isset($request->msqIsDefault) ? $request->msqIsDefault : null;

		// Get Module data
		$moduleData = Module::find($moduleId);

		$moduleQuestionData = ModuleQuestion::where('msq_title', '!=', ' ')->where('msq_module_id', $moduleId)->where('company_id', Auth::user()->company_id);

		// Module for admin user
		if ($msqIsDefault) {
			$moduleQuestionData = ModuleQuestion::where('msq_module_id', $moduleId)->where('msq_is_default', $msqIsDefault);
		}

		// Status filter
		if ($status) {
			$moduleQuestionData = $moduleQuestionData->where('msq_status', $status);
		}

		// Search filter
		if ($search) {
			$moduleQuestionData = $moduleQuestionData->where(function ($query) use ($search) {
				$query->orWhere('msq_title', 'LIKE', '%' . $search . '%');
			});
		}

		$moduleQuestionData = $moduleQuestionData->orderBy($sortBy, $sortDesc)->paginate($perPage);

		$pagination = [
			"total"        => $moduleQuestionData->total(),
			"current_page" => $moduleQuestionData->currentPage(),
			"last_page"    => $moduleQuestionData->lastPage(),
			"from"         => $moduleQuestionData->firstItem(),
			"to"           => $moduleQuestionData->lastItem()
		];

		$data = ['moduleName' => $moduleData->module_name, 'moduleQuestion' => $moduleQuestionData, 'total' => $moduleQuestionData->total(), 'pagination' => $pagination];

		return $this->returnSuccessMessage(null, $data);
		// } catch (\Exception $e) {
		// 	return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		// }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// Remove blank array and get options
		$options = array_filter(array_column($request->msq_option, 'option'));
		$request->merge(['msq_option' => $options]);

		//Change validation message
		$message = array(
			'msq_title' => 'The question title field is required.',
			'msq_type.required' => 'The question type field is required.',
			'msq_option.required' => 'The question option field is required.',
		);

		$request->validate([
			'msq_module_id' => 'required',
			'msq_title'     => 'required',
			'msq_type'      => 'required',
			'msq_option'    => 'required',
			'msq_status'    => 'required',
		], $message );

		$moduleQuestionData = array(
			'msq_module_id'       => $request->msq_module_id,
			'msq_user_id'         => Auth::user()->id,
			'msq_title'           => $request->msq_title,
			'msq_desc'            => $request->msq_desc,
			'msq_type'            => $request->msq_type,
			'msq_options'         => json_encode($options, JSON_FORCE_OBJECT),
			'msq_is_default'      => $request->msq_is_default,
			'msq_type_has_button' => ($request->msq_type_has_button == true) ? 'Y': 'N',
			'msq_has_required'    => ($request->msq_has_required == true) ? 'Y': 'N',
			'msq_status'          => ($request->msq_status == 'Y') ? 'Y' : 'N',
			'created_by'          => Auth::user()->id,
			'company_id'          => Auth::user()->company_id,
		);

		// Save data
		ModuleQuestion::create($moduleQuestionData);

		return $this->returnSuccessMessage('Module Survey Answer Created Successfully.', '');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Module  $module
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		try {
			$module = Module::where('module_id', $id)->first();

			if ($module) {
				return $this->returnSuccessMessage('Module retrieved successfully.', $module);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\Module  $module
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		try {
			$moduleQuestionData = ModuleQuestion::where('msq_id', $id)->first();

			if ($moduleQuestionData) {
				$moduleQuestionData->msq_options = json_decode($moduleQuestionData->msq_options, JSON_FORCE_OBJECT);

				$option = array();
				foreach ($moduleQuestionData->msq_options as $key => $value) {
					$option[$key]['id'] = $key+1;
					$option[$key]['option'] = $value;
					$option[$key]['prevHeight'] = 1000;
				}

				$moduleQuestionData->msq_options = $option;

				return $this->returnSuccessMessage('Module retrieved successfully.', $moduleQuestionData);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Module  $module
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$moduleQuestionObj = ModuleQuestion::find($id);

		// Remove blank array and get options
		$options = array_filter(array_column($request->msq_options, 'option'));

		$request->merge(['msq_option' => $options]);

		//Change validation message
		$message = array(
			'msq_title' => 'The question title field is required.',
			'msq_type.required' => 'The question type field is required.',
			'msq_option.required' => 'The question option field is required.',
		);

		$request->validate([
			'msq_module_id' => 'required',
			'msq_title'     => 'required',
			'msq_type'      => 'required',
			'msq_option'    => 'required',
			'msq_status'    => 'required',
		], $message );

		$moduleQuestionData = array(
			'msq_module_id'       => $request->msq_module_id,
			'msq_user_id'         => Auth::user()->id,
			'msq_title'           => $request->msq_title,
			'msq_desc'            => $request->msq_desc,
			'msq_type'            => $request->msq_type,
			'msq_options'         => json_encode($options, JSON_FORCE_OBJECT),
			'msq_is_default'      => $request->msq_is_default,
			'msq_type_has_button' => ($request->msq_type == 'radio') ? (($request->msq_type_has_button == true) ? 'Y': 'N') : 'N',
			'msq_has_required'    => ($request->msq_has_required == 'true') ? 'Y': 'N',
			'msq_status'          => ($request->msq_status == 'Y') ? 'Y' : 'N',
			'updated_by'          => Auth::user()->id,
		);

		// Update data
		$moduleQuestionObj->update($moduleQuestionData);

		return $this->returnSuccessMessage('Module question Updated Successfully.', $moduleQuestionData);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Module  $module
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request)
	{
		if ($request->id) {
			$moduleData = ModuleQuestion::where('msq_id', $request->id)->first();
			$moduleData->delete();

			return $this->returnSuccessMessage('Module Question deleted successfully.', $moduleData);
		} else {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	public function changeModuleQuestionStatus(Request $request)
	{
		try {
			if ($request->id) {
				if ($request->status == 'Y') {
					ModuleQuestion::where('msq_id', $request->id)->update(['msq_status' => 'N']);
				} else {
					ModuleQuestion::where('msq_id', $request->id)->update(['msq_status' => 'Y']);
				}

				return $this->returnSuccessMessage('You have successfully changed Module Question status.');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Clone Module Field based on module ids
	 *
	 * @param  \App\Models\ModuleQuestion
	 * @return \Illuminate\Http\Response
	 */
	public function cloneModuleQuestion($userId, $oldModuleId, $newModuleId, $companyId)
	{
		try {
			if ($userId > 0 && $oldModuleId > 0 && $newModuleId > 0) {
				$moduleQuestionData = ModuleQuestion::where('msq_module_id', $oldModuleId)->get();

				foreach ($moduleQuestionData as $moduleQuestion) {
					$moduleQuestionData = array(
						'msq_module_id'       => $newModuleId,
						'msq_user_id'         => $userId,
						'msq_title'           => $moduleQuestion->msq_title,
						'msq_desc'            => $moduleQuestion->msq_desc,
						'msq_type'            => $moduleQuestion->msq_type,
						'msq_options'         => $moduleQuestion->msq_options,
						'msq_is_default'      => 'N',
						'msq_type_has_button' => $moduleQuestion->msq_type_has_button,
						'msq_has_required'    => $moduleQuestion->msq_has_required,
						'msq_status'          => $moduleQuestion->msq_status,
						'created_by'          => Auth::user()->id,
                        'company_id'          => $companyId,
					);

					// Create module field
					ModuleQuestion::create($moduleQuestionData);
				}

				return true;
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

    /**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getAnswerList(Request $request)
	{

		//try {
		$perPage  = isset($request->perPage) ? $request->perPage : null;
		$search   = $request->search;
		$sortBy   = isset($request->sortBy) ? $request->sortBy : 'msq_id';
		$sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
		$moduleId = $request->moduleId;

		// Get Module data
		$moduleName = Module::find($moduleId)->module_name;

		$moduleAnswerData = ModuleAnswer::where('msa_module_id', $moduleId)->with(['site:site_id,site_name', 'subSite:sub_site_id,sub_site_name', 'module:module_id,module_name', 'user:id,first_name,last_name', 'question:msq_id,msq_title']);

		// Search filter
		if ($search) {
			$moduleAnswerData = $moduleAnswerData->where(function ($query) use ($search) {
				$query->orWhere('msa_value', 'LIKE', '%' . $search . '%')
                ->orWhere('created_at', 'LIKE', '%' . $search . '%')
				->orWhereHas('site', function ($siteQuery) use ($search) {
                    $siteQuery->where('site_name', 'LIKE', '%' . $search . '%');
                })
				->orWhereHas('subSite', function ($subSiteQuery) use ($search) {
                    $subSiteQuery->where('sub_site_name', 'LIKE', '%' . $search . '%');
                })
                ->orWhereHas('module', function ($moduleQuery) use ($search) {
                    $moduleQuery->where('module_name', 'LIKE', '%' . $search . '%');
                })->orWhereHas('user', function ($userQuery) use ($search) {
                    $userQuery->where('first_name', 'LIKE', '%' . $search . '%')->orWhere('last_name', 'LIKE', '%' . $search . '%');
                })->orWhereHas('question', function ($queQuery) use ($search) {
                    $queQuery->where('msq_title', 'LIKE', '%' . $search . '%');
                });
			});
		}

		$moduleAnswerData = $moduleAnswerData->orderBy($sortBy, $sortDesc)->paginate($perPage);

		$pagination = [
			"total"        => $moduleAnswerData->total(),
			"current_page" => $moduleAnswerData->currentPage(),
			"last_page"    => $moduleAnswerData->lastPage(),
			"from"         => $moduleAnswerData->firstItem(),
			"to"           => $moduleAnswerData->lastItem()
		];

		$data = ['moduleName' => $moduleName, 'moduleAnswer' => $moduleAnswerData, 'total' => $moduleAnswerData->total(), 'pagination' => $pagination];

		return $this->returnSuccessMessage(null, $data);
		// } catch (\Exception $e) {
		// 	return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		// }
	}
}
