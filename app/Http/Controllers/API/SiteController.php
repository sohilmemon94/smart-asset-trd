<?php

namespace App\Http\Controllers\API;

use App\Models\Site;
use App\Models\SubSite;
use Illuminate\Http\Request;
use App\Http\Traits\GeneralTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Throwable;

// Site word is replaced with Location whereever it is text and not related to any operation or transaction to DB.
class SiteController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        try {
            $perPage  = isset($request->perPage) ? $request->perPage : null;
            $search   = $request->search;
            $page     = $request->page;
            $sortBy   = isset($request->sortBy) ? $request->sortBy : 'site_id';
            $sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
            $status = $request->status;

            $siteData = Site::where('site_name', '!=', ' ')
                ->where('company_id', Auth::user()->company_id);

            // Status filter
            if ($status) {
                $siteData = $siteData->where('site_status', $status);
            }

            // Search filter
            if ($search) {
                $siteData = $siteData->where(function ($query) use ($search) {
                    $query->orWhere('site_id', 'LIKE', '%' . $search . '%')
                        ->orWhere('site_uuid', 'LIKE', '%' . $search . '%')
                        ->orWhere('site_name', 'LIKE', '%' . $search . '%')
                        ->orWhere('site_description', 'LIKE', '%' . $search . '%')
                        ->orWhere('site_status', 'LIKE', '%' . $search . '%')
                        ->orWhere('created_at', 'LIKE', '%' . $search . '%');
                });
            }

            if ($perPage) {
                $siteData = $siteData->orderBy($sortBy, $sortDesc)->paginate($perPage);
                $pagination = [
                    "total"        => $siteData->total(),
                    "current_page" => $siteData->currentPage(),
                    "last_page"    => $siteData->lastPage(),
                    "from"         => $siteData->firstItem(),
                    "to"           => $siteData->lastItem()
                ];
                $data = ['sites' => $siteData, "total" => $siteData->total(), 'pagination' => $pagination,];
            } else {
                $sitelist = $siteList = [];
                $sitelist[] = array(
                    "value" => null,
                    "text" => "Please select Location"
                );
                
                $siteData = Site::where('site_name', '!=', ' ')
                ->where('company_id', Auth::user()->company_id)
                ->get();
                
                foreach ($siteData as $key => $value) {
                    if($value->site_status == 'Y'){
                        $sitelist[] =  array(
                            "value" => $value['site_id'],
                            "text" => $value['site_name']
                        );
                    }

                    $siteList[] =  array(
                        "value" => $value['site_id'],
                        "text" => $value['site_name']
                    );
                }

                $data = ['sites' => $siteData, 'sitelist' => $sitelist, 'siteList' => $siteList];
            }

            return $this->returnSuccessMessage(null, $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'site_name' => 'required|string',
            // 'site_uuid' => 'required|unique:sites,site_uuid',
        ],[
            //'site_uuid.unique' => 'This Location ID has already been taken.'
        ]);


        // try {
            $siteData = array(
                'site_name'        => $request->site_name,
                'site_uuid'        => $request->site_uuid ?? NULL,
                'site_description' => $request->site_description ?? NULL,
                'site_status'      => ($request->site_status == false) ? 'N' : 'Y',
                'created_by'       => Auth::user()->id,
                'company_id'       => Auth::user()->company_id,
            );

            // Save data
            Site::create($siteData);
            return $this->returnSuccessMessage('Location Created Successfully.');
        // } catch (\Exception $e) {
        //     return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($site_id)
    {
        $site = Site::find($site_id);
        if ($site == null) {
            return $this->returnError(404, "Oppps! Site not found");
        }
        return $this->returnSuccessMessage('Location retrieved successfully.', collect($site)->only(['site_id', 'site_name', 'site_uuid', 'site_description', 'site_status', 'created_by']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $site = Site::where('site_id', $id)->first();

            if ($site) {
                return $this->returnSuccessMessage('Location retrieved successfully.', $site);
            } else {
                return $this->returnError(404, 'Oppps! No record found...');
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $site_id)
    {
        $request->validate([
            'site_name' => 'required|string',
            //'site_uuid' => 'required|unique:sites,site_uuid,' . $site_id . ',site_id',
        ],[
            //'site_uuid.unique' => 'The location ID has already been taken'
        ]);

        try {
            $siteData = array(
                'site_name' => $request->site_name,
                'site_uuid' => $request->site_uuid ?? NULL,
                'site_description' => $request->site_description ?? NULL,
                'site_status' => ($request->site_status == false) ? 'N' : 'Y',
                'updated_by'  => Auth::user()->id,
            );

            // Update data
            Site::where('site_id', $site_id)->update($siteData);

            return $this->returnSuccessMessage('Location Updated Successfully.');
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->id) {

            $exist_location = SubSite::where('site_id', $request->id)->where('company_id', Auth::user()->company_id) -> exists();
            if($exist_location){
                return $this->returnError(405, 'Opps! Not allow to Delete, as this Location already assigned to area.');
            }else{
                $siteData = Site::where('site_id', $request->id)->first();
                $siteData->delete();
                return $this->returnSuccessMessage('Location deleted successfully.', $siteData);
            }

        } else {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Change Site status data
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function changeSiteStatus(Request $request)
    {
        //try {
            if ($request->id) {
                if ($request->status == 'Y') {
                    Site::where('site_id', $request->id)->update(['site_status' => 'N']);

                    $subSiteData = SubSite::where('site_id', $request->id)->get();
                    if(sizeOf($subSiteData) > 0) {
                        // InActive respected subsite
                        SubSite::where('site_id', $request->id)->update(['sub_site_status' => 'N']);
                    }

                } else {
                    Site::where('site_id', $request->id)->update(['site_status' => 'Y']);

                    $subSiteData = SubSite::where('site_id', $request->id)->get();
                    if(sizeOf($subSiteData) > 0) {
                        // Active respected subsite
                        SubSite::where('site_id', $request->id)->update(['sub_site_status' => 'Y']);
                    }
                }

                return $this->returnSuccessMessage('You have successfully changed Location status.');
            }
        // } catch (\Exception $e) {
        //     return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        // }
    }
}
