<?php

namespace App\Http\Controllers\API;

use App\Models\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Throwable;

class TemplateController extends Controller
{
    use GeneralTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $perPage  = isset($request->perPage) ? $request->perPage : null;
            $search   = $request->search;
            $page     = $request->page;
            $sortBy   = isset($request->sortBy) ? $request->sortBy : 'template_id';
            $sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
            $status   = $request->status;
            $templateIsDefault = isset($request->templateIsDefault) ? $request->templateIsDefault : null;
            $templateData = Template::where('template_name', '!=', ' ')
                ->where('company_id', Auth::user()->company_id);

            // // Template for admin user
            if ($templateIsDefault == 'Y') {
                $templateData = Template::where('template_is_default', $templateIsDefault);
            }

            // Status filter
            if ($status) {
                $templateData = $templateData->where('template_status', $status);
            }

            // Search filter
            if ($search) {
                $templateData = $templateData->where(function ($query) use ($search) {
                    $query->orWhere('template_id', 'LIKE', '%' . $search . '%')
                        ->orWhere('template_name', 'LIKE', '%' . $search . '%')
                        ->orWhere('template_description', 'LIKE', '%' . $search . '%')
                        ->orWhere('template_status', 'LIKE', '%' . $search . '%');
                });
            }

            if ($perPage) {
                $templateData = $templateData->orderBy($sortBy, $sortDesc)->paginate($perPage);
                $pagination = [
                    "total"        => $templateData->total(),
                    "current_page" => $templateData->currentPage(),
                    "last_page"    => $templateData->lastPage(),
                    "from"         => $templateData->firstItem(),
                    "to"           => $templateData->lastItem()
                ];
                $data = ['templates' => $templateData, "total" => $templateData->total(), 'pagination' => $pagination];
            } else {
                $templateData = Template::where('template_name', '!=', ' ')
                    ->where('created_by', Auth::user()->id);

                $data = ['templates' => $templateData];
            }

            return $this->returnSuccessMessage(null, $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'template_name' => 'required|string',
            ]);
            if ($validator->fails()) {
                // get all errors as single string
                return $this->returnValidation($validator->errors());
            }
            $templateData = array(
                'template_name' => $request->template_name,
                'template_description' => $request->template_description ?? NULL,
                'template_is_default' => $request->template_is_default,
                'template_status' => ($request->template_status == false) ? 'N' : 'Y',
                'created_by'  => Auth::user()->id,
                'company_id'    => Auth::user()->company_id,
            );
            // Save data
            Template::create($templateData);
            return $this->returnSuccessMessage('Template Created Successfully.');
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $template = Template::where('template_id', $id)->first();

            if ($template) {
                return $this->returnSuccessMessage('Template retrieved successfully.', $template);
            } else {
                return $this->returnError(404, 'Oppps! No record found...');
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $template = Template::where('template_id', $id)->first();

            if ($template) {
                return $this->returnSuccessMessage('Template retrieved successfully.', $template);
            } else {
                return $this->returnError(404, 'Oppps! No record found...');
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'template_name' => 'required|string',
            ]);

            if ($validator->fails()) {
                // get all errors as single string
                return $this->returnValidation($validator->errors());
            }

            $templateData = array(
                'template_name' => $request->template_name,
                'template_description' => $request->template_description ?? NULL,
                'template_is_default' => $request->template_is_default,
                'template_status' => ($request->template_status == false) ? 'N' : 'Y',
                'updated_by'  => Auth::user()->id,
            );
            // Save data
            Template::where('template_id', $id)->update($templateData);
            return $this->returnSuccessMessage('Template Updated Successfully.');
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->id) {
            $templateData = Template::where('template_id', $request->id)->first();
            $templateData->delete();

            return $this->returnSuccessMessage('Template deleted successfully.', $templateData);
        } else {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    public function changeTemplateStatus(Request $request)
    {
        try {
            if ($request->id) {
                if ($request->status == 'Y') {
                    Template::where('template_id', $request->id)->update(['template_status' => 'N']);
                } else {
                    Template::where('template_id', $request->id)->update(['template_status' => 'Y']);
                }

                return $this->returnSuccessMessage('You have successfully changed template status.');
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Get all template for user
     *
     * @param  \App\Models\FieldInputType
     * @return \Illuminate\Http\Response
     */
    public function getUserTemplateOptions()
    {
        try {
            $data = Template::with('templateField')
                        ->whereHas('templateField', function ($query) {
                            $query->whereNull('template_fields.deleted_at');
                        })
                        ->whereNull('templates.deleted_at')
                        ->where('templates.template_status', 'Y');

            // If super admin then show all templates of super admin
            if (Auth::user()->roles->first()->id == 1) {
                $data = $data->where('templates.template_is_default', 'Y');
            } else {
                $data = $data->where('templates.company_id', Auth::user()->company_id);
            }

            $data = $data->get();

            $templateOption = [];
            foreach ($data as $key => $value) {
                if(sizeOf($value->templateField) > 0) {
                    $templateOption[$key]['label'] = $value['template_name'];
                    $templateOption[$key]['value'] = $value['template_id'];
                }
            }
            return $this->returnSuccessMessage(null, $templateOption);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Duplicate template and template .
     *
     * @param  \App\Models\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function duplicateTemplate(Request $request)
    {
        if ($request->id) {
            $templateData = Template::where('template_id', $request->id)->with('templateField')->first();

            // Create a new instance of the model with the same data
            $newTemplate = new Template($templateData->toArray());

            // Clear the ID to create a new row in the database
            $newTemplate->template_id   = null;
            $newTemplate->template_name = $templateData->template_name .' Duplicate';
            $newTemplate->created_by    = Auth::id();

            // Save the new row to the database
            $newTemplate->save();

            // Duplicate the template fields and associate with the new template
            foreach ($templateData->templateField as $field) {
                $newField = $field->replicate();
                $newField->template_field_template_id = $newTemplate->template_id;
                $newField->created_by = Auth::id();
                $newField->save();
            }

            return $this->returnSuccessMessage('Template duplicate successfully.', $newTemplate);
        } else {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }   
    
}
