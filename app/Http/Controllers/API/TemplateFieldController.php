<?php

namespace App\Http\Controllers\API;

use App\Models\TemplateField;
use App\Models\FieldInputType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\GeneralTrait;
use App\Models\Template;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Throwable;

class TemplateFieldController extends Controller
{
	use GeneralTrait;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		try {
			$perPage  = isset($request->perPage) ? $request->perPage : null;
			$search   = $request->search;
			$sortBy   = isset($request->sortBy) ? $request->sortBy : 'template_field_id';
			$sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
			$status   = $request->status;
			$templateId = $request->templateId;

			// Get Template data
			$templateData = Template::find($templateId);

			$templateFieldData = TemplateField::with('fieldInputType')->where('template_field_name', '!=', ' ')
				->where('template_field_template_id', $templateId);

			// Status filter
			if ($status) {
				$templateFieldData = $templateFieldData->where('template_field_status', $status);
			}

			// Search filter
			if ($search) {
				$templateFieldData = $templateFieldData->where(function ($query) use ($search) {
					$query->orWhere('template_field_name', 'LIKE', '%' . $search . '%')
						->orWhere('template_field_default_val', 'LIKE', '%' . $search . '%')
						->orWhereHas('fieldInputType', function ($fieldQuery) use ($search) {
							$fieldQuery->where('field_input_type_name', 'LIKE', '%' . $search . '%');
						});
				});

				// $templateFieldData = $templateFieldData->where('template_field_name', 'LIKE', '%' . $search . '%')
				// 	->orWhere('template_field_default_val', 'LIKE', '%' . $search . '%')
				// 	->orWhereHas('fieldInputType', function ($query) use ($search) {
				// 		$query->where('field_input_type_name', 'LIKE', '%' . $search . '%');
				// 	})
				// 	->where('template_field_template_id', $templateId);
			}

			$templateFieldData = $templateFieldData->orderBy($sortBy, $sortDesc)->paginate($perPage);

			$pagination = [
				"total"        => $templateFieldData->total(),
				"current_page" => $templateFieldData->currentPage(),
				"last_page"    => $templateFieldData->lastPage(),
				"from"         => $templateFieldData->firstItem(),
				"to"           => $templateFieldData->lastItem()
			];

			$data = ['templateName' => $templateData->template_name, 'templateField' => $templateFieldData, 'total' => $templateFieldData->total(), 'pagination' => $pagination];

			return $this->returnSuccessMessage(null, $data);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'template_field_name'                => 'required',
			'template_field_input_field_type_id' => 'required'
		]);

		if ($validator->fails()) {
			// get all errors as single string
			return $this->returnValidation($validator->errors());
		}

		try {
			$templateFieldData = array(
				'template_field_template_id'         => $request->template_field_template_id,
				'template_field_name'                => $request->template_field_name,
				'template_field_default_val'         => $request->template_field_default_val,
				'template_field_input_field_type_id' => $request->template_field_input_field_type_id,
				'template_field_is_show_on_grid'     => $request->template_field_is_show_on_grid ? 'Y' : 'N',
				'template_field_is_show_on_details'  => $request->template_field_is_show_on_details ? 'Y' : 'N',
				'template_field_is_required'         => $request->template_field_is_required ? 'Y' : 'N',
				'template_field_has_expiry_date'     => $request->template_field_has_expiry_date ? 'Y' : 'N',
				'template_field_status'              => $request->template_field_status ? 'Y' : 'N',
				'created_by'                         => Auth::user()->id,
				'company_id'                         => Auth::user()->company_id,

			);

			// Save data
			TemplateField::create($templateFieldData);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}

		return $this->returnSuccessMessage('Template Field Created Successfully.', '');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Template  $template
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		try {
			$template = Template::where('template_id', $id)->first();

			if ($template) {
				return $this->returnSuccessMessage('Template retrieved successfully.', $template);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\Template  $template
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		try {
			$templateFieldData = TemplateField::where('template_field_id', $id)->first();

			if ($templateFieldData) {
				return $this->returnSuccessMessage('Template retrieved successfully.', $templateFieldData);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Template  $template
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$templateFieldObj = TemplateField::find($id);

		if ($templateFieldObj == null) {
			return $this->returnError(404, "Oppps! Template field not found");
		}

		$validator = Validator::make($request->all(), [
			'template_name' => 'required|string',
		]);

		$validator = Validator::make($request->all(), [
			'template_field_name'                => 'required',
			'template_field_input_field_type_id' => 'required'
		]);

		if ($validator->fails()) {
			// get all errors as single string
			return $this->returnValidation($validator->errors());
		}

		try {
			$templateFieldData = array(
				'template_field_template_id'         => $request->template_field_template_id,
				'template_field_name'                => $request->template_field_name,
				'template_field_default_val'         => $request->template_field_default_val,
				'template_field_input_field_type_id' => $request->template_field_input_field_type_id,
				'template_field_is_show_on_grid'     => $request->template_field_is_show_on_grid ? 'Y' : 'N',
				'template_field_is_show_on_details'  => $request->template_field_is_show_on_details ? 'Y' : 'N',
				'template_field_is_required'         => $request->template_field_is_required ? 'Y' : 'N',
				'template_field_has_expiry_date'     => $request->template_field_has_expiry_date ? 'Y' : 'N',
				'template_field_status'              => $request->template_field_status ? 'Y' : 'N',
				'updated_by'                         => Auth::user()->id,
			);

			// Update data
			$templateFieldObj->update($templateFieldData);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}

		return $this->returnSuccessMessage('Template Field Updated Successfully.', $templateFieldData);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Template  $template
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request)
	{
		try {
			if ($request->id) {
				$templateData = TemplateField::where('template_field_id', $request->id)->first();
				$templateData->delete();

				return $this->returnSuccessMessage('Template Field deleted successfully.', $templateData);
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Change template field status
	 *
	 * @param  \App\Models\TemplateField
	 * @return \Illuminate\Http\Response
	 */
	public function changeTemplateFieldStatus(Request $request)
	{
		try {
			if ($request->id) {
				if ($request->status == 'Y') {
					TemplateField::where('template_field_id', $request->id)->update(['template_field_status' => 'N']);
				} else {
					TemplateField::where('template_field_id', $request->id)->update(['template_field_status' => 'Y']);
				}

				return $this->returnSuccessMessage('You have successfully changed template field status.');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Get all input field
	 *
	 * @param  \App\Models\FieldInputType
	 * @return \Illuminate\Http\Response
	 */
	public function getAllInputField()
	{
		try {
			$data = FieldInputType::select('field_input_type_id', 'field_input_type_name', 'field_input_type')->where('field_input_type_status', 'Y')->get();

			$inputFieldTypeOption = [];
			foreach ($data as $key => $value) {
				$inputFieldTypeOption[$key]['label'] = $value['field_input_type_name'];
				$inputFieldTypeOption[$key]['value'] = $value['field_input_type_id'];
			}
			return $this->returnSuccessMessage(null, $inputFieldTypeOption);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Get template all field
	 *
	 * @param  \App\Models\FieldInputType
	 * @return \Illuminate\Http\Response
	 */
	public function getTemplateAllFields(Request $request)
	{
		try {
			$data = TemplateField::where('template_field_template_id', $request->templateId)->where('template_field_status', 'Y')->get();

			$templateAllFieldOption = [];
			foreach ($data as $key => $value) {
				$templateAllFieldOption[$key]['text'] = $value['template_field_name'];
				$templateAllFieldOption[$key]['value'] = $value['template_field_id'];
			}

			return $this->returnSuccessMessage(null, $templateAllFieldOption);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}
}
