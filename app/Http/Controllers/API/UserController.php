<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\CategoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use Spatie\Permission\Models\Role;
use App\Models\SubSiteUser;
use App\Models\ModelHasRoles;
use App\Models\Category;
use App\Models\SubSitePermission;
use App\Models\SubSiteUserPermission;
use Validator;
use App\Http\Traits\GeneralTrait;
use Exception;
use Hash;
use Illuminate\Support\Str;
use App\Models\PasswordReset;
use App\Models\ClientCategoryAccess;
use App\Mail\PasswordSetRequestMail;
use App\Models\SubSite;
use Illuminate\Support\Facades\Mail;
use Mockery\Matcher\Subset;
use Illuminate\Support\Facades\DB;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
//use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
	use GeneralTrait;


	/**
	 * Get all roles
	 */
	public function getAllRole()
	{
		try {
			$roles = Role::select('name')->get();

			if ($roles) {
				$data = array();
				foreach ($roles as $key => $role) {
					$data[$key]['label'] = $role->name;
					$data[$key]['value'] = $role->name;
				}
				return $this->returnSuccessMessage(null, $data);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Get all admin user
	 */
	public function getAllAdminUser(Request $request)
	{
		try {
			$perPage  = $request->perPage;
			$search   = $request->search;
			$page     = $request->page;
			$sortBy   = isset($request->sortBy) ? $request->sortBy  : 'id';
			$sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
			$status   = $request->status;
			$role     = "Super Admin";

			if (Auth::user()->id != 1) {
				$userData = User::where('first_name', '!=', '')->whereNotIn('id', [Auth::user()->id, 1]);
			} else {
				$userData = User::where('first_name', '!=', '')->whereNot('id', Auth::user()->id);
			}

			// Role filter
			if ($role) {
				$userData = $userData->whereHas("roles", function ($q) use ($role) {
					$q->where("name", $role);
				});
			}

			// Status filter
			if ($status) {
				$userData = $userData->where('status', $status);
			}

			// Search filter
			if ($search) {
				$userData = $userData->where(function ($query) use ($search) {
					$query->orWhere('first_name', 'LIKE', '%' . $search . '%')
						->orWhere(\DB::raw('CONCAT(first_name, " ", last_name)'), 'LIKE', '%' . $search . '%')
						->orWhere('middle_name', 'LIKE', '%' . $search . '%')
						->orWhere('last_name', 'LIKE', '%' . $search . '%')
						->orWhere('client_representative_position', 'LIKE', '%' . $search . '%')
						->orWhere('email', 'LIKE', '%' . $search . '%')
						->orWhere('contact_number', 'LIKE', '%' . $search . '%');
				});
			}

			$userData = $userData->orderBy($sortBy, $sortDesc)->paginate($perPage);

			foreach ($userData as $key => $user) {
				if ($user->avatar) {
					$userData[$key]->avatar = !empty($user->avatar) ? $this->getStorageURL($user->avatar) : '';
				}

				$userData[$key]->role = $user->roles->first()->name ?? '';
			}

			$pagination = [
				"total"        => $userData->total(),
				"current_page" => $userData->currentPage(),
				"last_page"    => $userData->lastPage(),
				"from"         => $userData->firstItem(),
				"to"           => $userData->lastItem()
			];

			$data = ['users' => $userData, "total" => $userData->total(), 'pagination' => $pagination];

			return $this->returnSuccessMessage(null, $data);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Get all client user
	 */
	public function getAllClientUser(Request $request)
	{
		try {
			$perPage  = $request->perPage;
			$search   = $request->search;
			$page     = $request->page;
			$sortBy   = isset($request->sortBy) ? $request->sortBy  : 'id';
			$sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
			$status   = $request->status;
			$role     = "Admin";

            //Client List With Company Detail
            $userData = User::where('first_name', '!=', '')
            ->where('is_root', '=', '0')
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('roles')
                    ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->whereColumn('users.id', 'model_has_roles.model_id')
                    ->where('name', 'Admin')
                    ->whereNotExists(function ($query) {
                        $query->select(DB::raw(1))
                                ->from('roles')
                                ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                                ->whereColumn('users.id', 'model_has_roles.model_id')
                                ->where('name', 'User');
                    });
            })
            ->with('companyData');

			// Role filter
			if ($role) {
				$userData = $userData->whereHas("roles", function ($q) use ($role) {
					$q->where("name", $role);
				});
			}

			// Status filter
			if ($status) {
				$userData = $userData->where('status', $status);
			}

			// Search filter
			if ($search) {
				// Search filter
				$userData = $userData->where(function ($query) use ($search) {
					$query->orWhere('client_company_name', 'LIKE', '%' . $search . '%')
						->orWhere('created_at', 'LIKE', '%' . $search . '%')
						//->orWhere(\DB::raw('CONCAT(client_company_name, " (", client_abn, ")")'), 'LIKE', '%' . $search . '%')
						->orWhere('email', 'LIKE', '%' . $search . '%')
                        ->orWhereHas('companyData', function ($companyQuery) use ($search) {
                            $companyQuery->orWhere('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('contact_no', 'LIKE', '%' . $search . '%')
                            ->orWhere('abn', 'LIKE', '%' . $search . '%');
                        });
				});
			}

			$userData = $userData->orderBy($sortBy, $sortDesc)->paginate($perPage);

			foreach ($userData as $key => $user) {
				if ($user->companyData) {
					$user->companyData->logo = !empty($user->companyData->logo) ? $this->getStorageURL($user->companyData->logo) : '';
                    $user->logo = $user->companyData->logo;
                    $user->client_company_name = $user->companyData->name ?? '';
				}

				$userData[$key]->role = $user->roles->first()->name ?? '';
			}

			$pagination = [
				"total"        => $userData->total(),
				"current_page" => $userData->currentPage(),
				"last_page"    => $userData->lastPage(),
				"from"         => $userData->firstItem(),
				"to"           => $userData->lastItem()
			];

			$data = ['users' => $userData,  "total" => $userData->total(), 'pagination' => $pagination];

			return $this->returnSuccessMessage(null, $data);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Get all Site user
	 */
	public function getAllSiteUser(Request $request)
	{
		try {
			$perPage  = isset($request->perPage) ? $request->perPage : null;
			$search   = $request->search;
			$page     = $request->page;
			$sortBy   = isset($request->sortBy) ? $request->sortBy : 'id';
			$sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
			$status   = $request->status;
			$role     = ["User", "Admin"];


			$userData = User::where('first_name', '!=', '')->whereNot('id', Auth::user()->id)->where('company_id', Auth::user()->company_id);

            // $userData = User::where('first_name', '<>', '')
            // ->where('company_id', Auth::user()->company_id)
            // ->whereExists(function ($query) {
            //     $query->select(DB::raw(1))
            //         ->from('roles')
            //         ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
            //         ->whereColumn('users.id', 'model_has_roles.model_id')
            //         ->whereIn('name', ['User', 'Admin']);
            //         ->whereNotExists(function ($query) {
            //             $query->select(DB::raw(1))
            //                     ->from('roles')
            //                     ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
            //                     ->whereColumn('users.id', 'model_has_roles.model_id')
            //                     ->where('name', 'Admin');
            //         });
            // });

			// Role filter
			if ($role) {
				$userData = $userData->whereHas("roles", function ($q) use ($role) {
					$q->whereIn("name", $role);
				});
			}

			// Status filter
			if ($status) {
				$userData = $userData->where('status', $status);
			}

			// Search filter
			if ($search) {
				$userData = $userData->where(function ($query) use ($search) {
					$query->orWhere('first_name', 'LIKE', '%' . $search . '%')
						->orWhere(\DB::raw('CONCAT(first_name, " ", last_name)'), 'LIKE', '%' . $search . '%')
						->orWhere('middle_name', 'LIKE', '%' . $search . '%')
						->orWhere('last_name', 'LIKE', '%' . $search . '%')
						->orWhere('email', 'LIKE', '%' . $search . '%')
						->orWhere('contact_number', 'LIKE', '%' . $search . '%');
				});
			}
			if ($perPage) {
				$userData = $userData->orderBy($sortBy, $sortDesc)->paginate($perPage);

				foreach ($userData as $key => $user) {

                        if ($user->avatar) {
                            $userData[$key]->avatar = !empty($user->avatar) ? $this->getStorageURL($user->avatar) : '';
                        }

                        $userData[$key]->role = $user->roles->first()->name ?? '';
                        $userData[$key]->role_length = count($user->roles);

				}

				$pagination = [
					"total"        => $userData->total(),
					"current_page" => $userData->currentPage(),
					"last_page"    => $userData->lastPage(),
					"from"         => $userData->firstItem(),
					"to"           => $userData->lastItem()
				];

				$data = ['users' => $userData, "total" => $userData->total(), 'pagination' => $pagination];
			} else {
                // $userData = User::whereNotNull('first_name')->where('status', 'Y')->where('company_id', Auth::user()->company_id);

                $firstCompanyAdmin = User::where('first_name', '<>', '')->where('company_id', Auth::user()->company_id)->first()->id;

                $userData = User::where('first_name', '<>', '')
                    ->where('company_id', Auth::user()->company_id)
					->where('status', 'Y')
					->whereHas('roles', function ($query) {
						$query->whereIn('name', ['User', 'Admin']);
					});

				$userList[] = array(
                    "value" => 0,
					"text" => "Select All"
				);
				$selectedUser = [];
				$noSelectedUser[] = array("value" => null, "text" => "Please select User");
				// $subSitEUserData = User::with('subSiteUser')
				// 	->where('created_by', Auth::user()->id)->get();


                    $areaUser = $userData->get();
                    foreach ($areaUser as $key => $value) {
                        $userList[] =  array(
                            "value" => $value['id'],
                            "text" => $value['full_name']
                        );
                    }

                    if ($request->subSite_id) {
                        $searchUser   = $request->searchUser;
                        if ($searchUser) {
                            $userData = $userData->where(function ($query) use ($searchUser) {
                                $query->orWhere('first_name', 'LIKE', '%' . $searchUser . '%')
                                    ->orWhere(\DB::raw('CONCAT(first_name, " ", last_name)'), 'LIKE', '%' . $searchUser . '%')
                                    ->orWhere('middle_name', 'LIKE', '%' . $searchUser . '%')
                                    ->orWhere('last_name', 'LIKE', '%' . $searchUser . '%');
                            });
                        }
                        $userData = $userData->get();
                        foreach ($userData as $ukey => $obj) {
                            $selectedSubSite = SubSiteUser::where('ssu_sub_site_id', $request->subSite_id)->where('ssu_user_id', $obj['id'])->where('company_id', Auth::user()->company_id)->exists();
                            if ($selectedSubSite) {

                                $getCategoryModule = $this->getSubSiteUserCatModule($obj['id'], $request->subSite_id);

                                if ($getCategoryModule) {
                                    $selectedUser[] =  array(
                                        "value" => $obj['id'],
									    "text" => $obj['full_name'],
									    "data" => $getCategoryModule
								);
							} else {
								$selectedUser[] =  array(
									"value" => $obj['id'],
									"text" => $obj['full_name']
								);
							}
						} else {
							$noSelectedUser[] =  array(
								"value" => $obj['id'],
								"text" => $obj['full_name']
							);
						}
					}
				}

				$data = ['users' => $userData, 'userList' => $userList, 'selectedUser' => $selectedUser, 'noSelectedUser' => $noSelectedUser];
			}

			return $this->returnSuccessMessage(null, $data);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/** Get User Wise Category/Module */
	public function getSubSiteUserCatModule($user_id, $subSite_id)
	{
		$allData = [];
		$categoryData = Category::with('module')->where('category_name', '!=', ' ')
            ->where('company_id', Auth::user()->company_id)
			->orWhere('category_user_id', Auth::user()->id)
			->get();

		$expand = false;
		$catModule = [];


		foreach ($categoryData as $cKey => $value) {
			$modules = [];
			$checked = false;
			foreach ($value['module'] as $vKey =>  $val) {
				$selected = SubSitePermission::where('ssp_sub_site_id', $subSite_id)->where('ssp_category_id', $val['module_category_id'])->where('ssp_module_id', $val['module_id'])->where('company_id', Auth::user()->company_id)->exists();
				$subSiteUserPermission = SubSiteUserPermission::where('ssup_sub_site_id', $subSite_id)->where('ssup_user_id', $user_id)->where('ssup_category_id', $val['module_category_id'])->where('ssup_module_id', $val['module_id'])->where('company_id', Auth::user()->company_id)->exists();


				if (!$checked && $selected) {
					$checked = $selected;
				}
				if ($selected) {
					if ($subSiteUserPermission) {
						$modules[] = array(
							"id" => $val['module_id'],
							"module_category_id" => $val['module_category_id'],
							"title" => $val['module_name'],
							"checked" => $selected
						);
					} else {
						$modules[] = array(
							"id" => $val['module_id'],
							"module_category_id" => $val['module_category_id'],
							"title" => $val['module_name'],
							"checked" => false
						);
					}
				}
			}
			if ($checked) {
				if ($subSiteUserPermission) {
					$catModule[] =  array(
						"id" => $value['category_id'],
						"title" => $value['category_name'],
						"expanded" => true,
						"async" => true,
						"checked" => $checked,
						"children" => $modules,
					);
				} else {
					$catModule[] =  array(
						"id" => $value['category_id'],
						"title" => $value['category_name'],
						"expanded" => true,
						"async" => true,
						"checked" => false,
						"children" => $modules,
					);
				}
			}
		}


		$allData[] = array(
			"id" => 0,
			"title" => "Select All",
			"expanded" => true,
			"async" => true,
			"selected" => $checked,
			"children" => $catModule,
		);

		return ($allData);
	}

	/**
	 * Get all user
	 */
	public function getAllUserData(Request $request)
	{
		try {
			$perPage  = $request->perPage;
			$search   = $request->q;
			$page     = $request->page;
			$sortBy   = isset($request->sortBy) ? $request->sortBy  : 'id';
			$sortDesc = ($request->sortDesc == 'true') ? 'desc' : 'asc';
			$status = $request->status;
			$role = $request->role;

			if ($sortBy == 'user') {
				$sortBy = 'first_name';
			}

			$userData = User::where('first_name', '!=', '');

			// Role filter
			if ($role) {
				$userData = $userData->whereHas("roles", function ($q) use ($role) {
					$q->where("name", $role);
				});
			}

			// Status filter
			if ($status) {
				$userData = $userData->where('status', $status);
			}

			// Search filter
			if ($search) {
				$userData = $userData->where('first_name', 'LIKE', '%' . $search . '%')
					->orWhere('middle_name', 'LIKE', '%' . $search . '%')
					->orWhere('last_name', 'LIKE', '%' . $search . '%')
					->orWhere('email', 'LIKE', '%' . $search . '%');
			}

			$userData = $userData->orderBy($sortBy, $sortDesc)->paginate($perPage);

			foreach ($userData as $key => $user) {
				if ($user->avatar) {
					$userData[$key]->avatar = !empty($user->avatar) ? $this->getStorageURL($user->avatar) : '';
				}

				$userData[$key]->role = $user->roles->first()->name ?? '';
			}

			$pagination = [
				"total"        => $userData->total(),
				"current_page" => $userData->currentPage(),
				"last_page"    => $userData->lastPage(),
				"from"         => $userData->firstItem(),
				"to"           => $userData->lastItem()
			];

			$data = ['users' => $userData, "total" => $userData->total(), 'pagination' => $pagination];

			return $this->returnSuccessMessage(null, $data);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Get edit user data
	 */
	public function edit($id)
	{
		// try {
			$data = User::where('id', $id)->first();
			if ($data) {

				$data->avatar              = !empty($data->avatar) ? $this->getStorageURL($data->avatar) : '';
				//$data->companyData->logo = !empty($data->companyData->logo) ? $this->getStorageURL($data->companyData->logo) : '';
				$data->role                = $data->roles->first()->name ?? '';
				if ($data->role == 'Admin') {
					// Get clone category
					$categoryObj = new CategoryController;
					$clientCloneCategoryData = $categoryObj->getClientCloneCategory($id);

					// assign blank array for select multiple category
					$data->categorySelected = [];

					// Display to admin user
					if ($clientCloneCategoryData) {
						$data->clientCloneCategory = $clientCloneCategoryData;
					}
				}

				return $this->returnSuccessMessage(null, $data);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		// } catch (\Exception $e) {
		// 	return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		// }
	}

    /**
	 * Get edit user data
	 */
	public function editWithCompany($id)
	{
		// try {
			$userData = User::where('id', $id)->with('companyData')->first();
			if ($userData) {
				$userData->avatar              = !empty($userData->avatar) ? $this->getStorageURL($userData->avatar) : '';
				$userData->companyData->logo = !empty($userData->companyData->logo) ? $this->getStorageURL($userData->companyData->logo) : '';
				$userData->role                = $userData->roles->first()->name ?? '';
				if ($userData->role == 'Admin') {
					// Get clone category
					$categoryObj = new CategoryController;
					$clientCloneCategoryData = $categoryObj->getClientCloneCategory($id);

					// assign blank array for select multiple category
					$userData->categorySelected = [];

					// Display to admin user
					if ($clientCloneCategoryData) {
						$userData->clientCloneCategory = $clientCloneCategoryData;
					}
				}


                $userListData = User::where('first_name', '<>', '')
                    ->where('company_id', $userData->company_id)
                    ->where('id', '!=', $id)
                    ->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('roles')
                            ->join('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
                            ->whereColumn('users.id', 'model_has_roles.model_id')
                            ->where('name', 'Admin');
                })->with('companyData')->get();

				foreach ($userListData as $key => $user) {
                    if ($user->companyData) {
                        $user->companyData->logo = $userData->companyData->logo ?? '';
                        $user->client_company_logo = $user->companyData->logo;
                        $user->client_company_name = $userData->companyData->name ?? '';
                    }

					$modalHasRoles = ModelHasRoles::where('model_id',$user->id)->count();
					$user->converted_admin = ($modalHasRoles > 1)?'Y':'N';
				}

                $data = ['users' => $userData, 'userList' => $userListData];
				return $this->returnSuccessMessage(null, $data);
			} else {
				return $this->returnError(404, 'Oppps! No record found...');
			}
		// } catch (\Exception $e) {
		// 	return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		// }
	}



	/*
	*	Save new user
	*/
	public function store(Request $request)
	{
        $is_root = '1';
        //dd($request->role);

        if ($request->role == 'Super Admin') {

            $request->validate([
				'first_name'     => 'required|string',
				'last_name'      => 'required|string',
				'email'          => 'required|string|email|unique:users',
				'contact_number' => 'nullable|regex:/^[- +()]*[0-9][- +()0-9]*$/',
				'role'           => 'required',
				'status'         => 'required',
				'avatar'         => 'nullable|mimes:jpg,jpeg,png|max:2048',
				'client_representative_position' => 'required',

			]);
		} else if ($request->role == 'Admin') {
            $IsRoot = User::where('is_root', '=', '0')->where('company_id', $request->company_id)->exists();
            $is_root = $IsRoot ? '1' : '0';
			$regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';

			$request->validate([
				'role'                           => 'required',
				'first_name'                     => 'required|string',
				'last_name'                      => 'required|string',
				'status'                         => 'required',
				'email'                          => 'required|string|email|unique:users',
				'contact_number'                 => 'required|regex:/^[- +()]*[0-9][- +()0-9]*$/',
				'client_representative_position' => 'required',
				'avatar'                         => 'nullable|mimes:jpg,jpeg,png|max:2048',
                'company_id'                     => 'required',
				//'client_company_logo'            => 'required|mimes:jpg,jpeg,png|max:2048'

			]);
		} else if ($request->role == 'User') {
			$request->validate([
				'first_name'     => 'required|string',
				'last_name'      => 'nullable|string',
				'email'          => 'required|string|email|unique:users',
				'contact_number' => 'nullable|regex:/^[- +()]*[0-9][- +()0-9]*$/',
				'role'           => 'required',
				'status'         => 'required',
				'avatar'         => 'nullable|mimes:jpg,jpeg,png|max:2048'
			]);
		}

		// try {

			$user = new User([
				'first_name'                     => $request->first_name,
				'middle_name'                    => $request->middle_name ?? NULL,
				'last_name'                      => $request->last_name,
				'email'                          => $request->email,
                'company_id'                     => $request->company_id ?? Auth::user()->company_id,
                'is_root'                        => $is_root,
				'client_company_name'            => $request->client_company_name ?? NULL,
				'client_abn'                     => $request->client_abn ?? NULL,
				'client_uuid'                    => $request->client_uuid ?? NULL,
				'client_company_website'         => $request->client_company_website ?? NULL,
				'client_company_contact_no'      => $request->client_company_contact_no ?? NULL,
				'client_representative_position' => $request->client_representative_position ?? NULL,
				//'status'                         => $request->status,
				'contact_number'                 => $request->contact_number ?? NULL,
				'date_of_birth'                  => $request->date_of_birth ?? NULL,
				'address_line_1'                 => $request->address_line_1 ?? NULL,
				'address_line_2'                 => $request->address_line_2 ?? NULL,
				'street'                         => $request->street ?? NULL,
				'city'                           => $request->city ?? NULL,
				'state'                          => $request->state ?? NULL,
				'postal_code'                    => $request->postal_code ?? NULL,
				'country'                        => $request->country ?? NULL,
				'status'                         => $request->status ?? NULL,
				'is_confined_space'              => $request->is_confined_space ?? 'N',
				'created_by'                     => Auth::user()->id
			]);

			if ($user->save()) {

				$user->assignRole([$request->role]);

				// For user avatar upload
				if ($user->id && $request->file() && $request->avatar) {
					if ($request->file()) {
						$filePath = $this->fileUpload($request->avatar, 'image/users');
					}

					$updateUserData = array(
						'avatar' => $filePath
					);

					$user->update($updateUserData);
				}

				// For client logo upload
				if ($user->id && $request->file() && $request->client_company_logo) {

					if ($request->file()) {
						$filePath = $this->fileUpload($request->client_company_logo, 'image/users');
					}

					$updateUserData = array(
						'client_company_logo' => $filePath
					);

					$user->update($updateUserData);
				}

				// Clone category for client
				if ($user->id && $request->categorySelected) {
					$cloneCategoryIdArray = explode(',', $request->categorySelected);
					if (sizeof($cloneCategoryIdArray) > 0) {
						$categoryObj = new CategoryController;
						$categoryObj->cloneCategory($user->id, $user->company_id, $cloneCategoryIdArray);
					}
				}

				// Send email for set user password
				$passwordReset = PasswordReset::updateOrCreate(
					['email' => $user->email],
					[
						'email' => $user->email,
						'token' => Str::random(60)
					]
				);

				if ($user && $passwordReset) {
					$link = url('/set-password/' . $passwordReset->token);
					$mail = new PasswordSetRequestMail($request->email, $link, $user->first_name);
					Mail::mailer('welcome')->to($request->email)->send($mail);
				}

				return $this->returnSuccessMessage('User added successfully.');
			} else {
				return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
			}
		// } catch (\Exception $e) {
		// 	return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		// }
	}

	/**
	 * Update user data
	 */
	public function update($id, Request $request)
	{

		if ($request->role == 'Super Admin') {
			$request->validate([
				'first_name'     => 'required|string',
				'last_name'      => 'required|string',
				'email'          => 'required|string|email|unique:users,email,' . $id,
				'contact_number' => 'nullable|regex:/^[- +()]*[0-9][- +()0-9]*$/',
				'role'           => 'required',
				'status'         => 'required',
				//'avatar'         => 'required|mimes:jpg,jpeg,png|max:2048'
			]);
		} else if ($request->role == 'Admin') {
			$request->validate([
				'role'                           => 'required',
				'first_name'                     => 'required|string',
				'last_name'                      => 'required|string',
				'status'                         => 'required',
				'email'                          => 'required|string|email|unique:users,email,' . $id,
				'contact_number'                 => 'nullable|regex:/^[- +()]*[0-9][- +()0-9]*$/',
				'client_representative_position' => 'nullable',
                'company_id'                     => 'required',
				//'avatar'                         => 'nullable|mimes:jpg,jpeg,png|max:2048',
				//'client_company_logo'            => 'required|mimes:jpg,jpeg,png|max:2048'
			]);
		} else if ($request->role == 'User') {
			$request->validate([
				'first_name'     => 'required|string',
				'last_name'      => 'nullable|string',
				'email'          => 'required|string|email|unique:users,email,' . $id,
				'contact_number' => 'nullable|regex:/^[- +()]*[0-9][- +()0-9]*$/',
				'role'           => 'required',
				'status'         => 'required',
				//'avatar'         => 'nullable|mimes:jpg,jpeg,png|max:2048'
			]);
		}

		if ($request->passwordValueOld || $request->newPasswordValue || $request->retypePassword) {
			$messages = [
				'passwordValueOld.required' => 'The Current Password field is required.',
				'newPasswordValue.required' => 'The New Password field is required.',
				'retypePassword.required'   => 'The Retype New Password field is required.',
			];

			$request->validate([
				'passwordValueOld' => 'required',
				'newPasswordValue' => 'required',
				'retypePassword'   => 'required',
			], $messages);

			if (!($this->changePassword($request))) {
				return $this->returnError(200, 'Your current password does not matches with the password.');
			}
		}

		try {

			$updateUserData = array(
				'first_name'                     => $request->first_name,
				'middle_name'                    => $request->middle_name,
				'last_name'                      => $request->last_name,
				'email'                          => $request->email,
				'client_representative_position' => $request->client_representative_position,
				'contact_number'                 => $request->contact_number,
				'date_of_birth'                  => $request->date_of_birth,
                'company_id'                     => $request->company_id,
				'updated_by'                     => Auth::user()->id
			);

			// Update user data
			$user = User::find($id);
			$user->update($updateUserData);

			// // Removed existing user role
			// \DB::table('model_has_roles')->where('model_id', $id)->delete();

			// Assign new user role
			$user->assignRole([$request->role]);

			// Category access permission for client user
			if ($request->categorySelected) {
				// Clone category
				$categoryObj = new CategoryController;
				$categoryObj->cloneCategory($id, $user->company_id, $request->categorySelected);
			}

			return $this->returnSuccessMessage('You have successfully updated user.', $user);
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

    /**
	 * Update user role
	 */
    public function makeUserToAdmin(Request $request){
        //dd($request->id);
        $user = User::find($request->id);
        $user->assignRole('Admin');

        return $this->returnSuccessMessage('User successfully converted to Admin.');
    }

	/**
	 * Change user password
	 */
	public function changePassword(Request $request)
	{
		if (!(Hash::check($request->get('passwordValueOld'), Auth::user()->password))) {
			// The passwords matches
			// Your current password does not matches with the password.
			return false;
		}

		//Change Password
		$user = Auth::user('api');
		$user->password = bcrypt($request->newPasswordValue);
		$user->save();

		return true;
	}

	/**
	 * Update user avatar
	 */
	public function uploadUserAvatar($id, Request $request)
	{
		$userData = User::where('id', $id)->first();

		if ($request->file) {
			$request->validate([
				'file' => 'required|mimes:jpg,jpeg,png|max:8000'
			]);

			if ($request->file()) {
				$filePath = $this->fileUpload($request->file, 'image/users');
			}

			$updateUserData = array(
				'avatar' => $filePath
			);

			// Remove existing image on server
			if (isset($userData) && $userData->avatar) {
				$this->deleteFile($userData->avatar);
			}

			$userData->update($updateUserData);

			// Set full URL
			$userData->avatar = !empty($userData->avatar) ? $this->getStorageURL($userData->avatar) : '';

			return $this->returnSuccessMessage('You have successfully updated user avatar.', $userData);
		} else if ($request->client_company_logo) {
			$request->validate([
				'client_company_logo' => 'required|mimes:jpg,jpeg,png|max:2048'
			]);

			if ($request->file()) {
				$filePath = $this->fileUpload($request->client_company_logo, 'image/users');
			}

			$updateUserData = array(
				'client_company_logo' => $filePath
			);

			if (isset($userData) && $userData->client_company_logo) {
				$this->deleteFile($userData->client_company_logo);
			}

			$userData->update($updateUserData);

			$userData->client_company_logo = !empty($userData->client_company_logo) ? $this->getStorageURL($userData->client_company_logo) : '';

			return $this->returnSuccessMessage('You have successfully updated company logo.', $userData);
		}
	}

	/**
	 * Remove user avatar
	 */
	public function removeUserAvatar(Request $request)
	{
		if ($request->id) {
			$userData = User::where('id', $request->id)->first();

			// Remove existing image on server
			if (isset($userData) && $userData->avatar != '') {
				$this->deleteFile($userData->avatar);

				// Update user data
				$userData->update(array('avatar' => ''));

				return $this->returnSuccessMessage('You have successfully removed user avatar.');
			}
			// Remove existing image on server
			else if (isset($userData) && $userData->client_company_logo  != '') {
				$this->deleteFile($userData->client_company_logo);

				// Update user data
				$userData->update(array('client_company_logo' => ''));

				return $this->returnSuccessMessage('You have successfully removed client logo.');
			}
		}
	}

	/**
	 * Delete user data
	 *
	 * @param User $user
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request)
	{
		if ($request->id) {
            ///echo $request->id;
            $exist_userINarea = SubSiteUser::where('ssu_user_id', $request->id)->where('company_id', Auth::user()->company_id) -> exists();

            if($exist_userINarea){
                return $this->returnError(405, 'Opps! Not allow to Delete, as this User already assigned to area.');
            }else{
			$userData = User::where('id', $request->id)->first();
			$userData->delete();

			return $this->returnSuccessMessage('You have successfully deleted user.');
            }
		}

		return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
	}

	/**
	 * Change user status data
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function changeUserStatus(Request $request)
	{
		try {
			if ($request->id) {
				if ($request->status == 'Y') {
					User::where('id', $request->id)->update(['status' => 'N']);
				} else {
					User::where('id', $request->id)->update(['status' => 'Y']);
				}

				return $this->returnSuccessMessage('You have successfully changed user status.');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/**
	 * Get all client users from company
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getCompanyAllUser()
	{
		try {
			$categoryModuleArray = [];

			$companyId = Auth::user()->company_id;

			if ($companyId) {
				$userData = User::select('id', 'company_id', 'first_name', 'last_name', 'email', 'status')
					->with(['roles', 'location', 'location.area'])
					->where('company_id', $companyId)
					->whereHas('roles', function ($query) {
						$query->whereIn('name', ['User', 'Admin']);
					})
					->get()
					->toArray();

				foreach ($userData as $key => $udata) {
					// 					$data[$key]['id'] = $udata['id'];
					// 					$data[$key]['full_name'] = $udata['full_name'];
					
					$roles = collect($udata['roles'])->pluck('name')->implode(', ');
    				$userData[$key]['roles'] = $roles;

					if (isset($udata['location']) && sizeOf($udata['location']) > 0) {
						foreach ($udata['location'] as $lkey => $location) {
							if (isset($location['area']) && sizeOf($location['area']) > 0) {
								foreach ($location['area'] as $akey => $area) {
									$userData[$key]['location'][$lkey]['area'][$akey]['categoryModule'] = $this->getUserCategoryModule($udata['id'], $area['sub_site_id']);
								}
							}
						}
					}
					//$userData[$key]['categoryModule'] = $categoryModuleArray;
				}

				//$userData['categoryModule'] = $categoryModule;

				return $this->returnSuccessMessage('', $userData);
			} else {
				return $this->returnError(500, 'Oppps! Not assigned company to this user...');
			}
		} catch (\Exception $e) {
			return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
		}
	}

	/** Get User Wise Category/Module */
	public function getUserCategoryModule($user_id, $subSite_id)
	{
		$allData = [];
		$categoryData = Category::with('module')->where('category_name', '!=', ' ')
			->where('company_id', Auth::user()->company_id)
			->orWhere('category_user_id', Auth::user()->id)
			->get()->toArray();

		$expand = false;
		$catModule = [];


		foreach ($categoryData as $cKey => $value) {
			$modules = [];
			$checked = false;
			foreach ($value['module'] as $vKey =>  $val) {
				$selected = SubSitePermission::where('ssp_sub_site_id', $subSite_id)->where('ssp_category_id', $val['module_category_id'])->where('ssp_module_id', $val['module_id'])->where('company_id', Auth::user()->company_id)->exists();
				$subSiteUserPermission = SubSiteUserPermission::where('ssup_sub_site_id', $subSite_id)->where('ssup_user_id', $user_id)->where('ssup_category_id', $val['module_category_id'])->where('ssup_module_id', $val['module_id'])->where('company_id', Auth::user()->company_id)->exists();

				if (!$checked && $selected) {
					$checked = $selected;
				}
				if ($selected) {
					if ($subSiteUserPermission) {
						$modules[] = array(
							"id"                 => $user_id . '-' . $subSite_id . '-' . $val['module_id'],
							"module_category_id" => $val['module_category_id'],
							"title"              => $val['module_name'],
							"checked"            => $selected
						);
					} else {
						$modules[] = array(
							"id"                 => $user_id . '-' . $subSite_id . '-' . $val['module_id'],
							"module_category_id" => $val['module_category_id'],
							"title"              => $val['module_name'],
							"checked"            => false
						);
					}
				} else {
					// NOTE :: Currently not assigned modules without manage category -> If not assigned module via manage category then it will not showing
					$modules[] = array(
						"id"                 => $user_id . '-' . $subSite_id . '-' . $val['module_id'],
						"module_category_id" => $val['module_category_id'],
						"title"              => $val['module_name'],
						"checked"            => false
					);
				}
			}
			if ($checked) {
				if ($subSiteUserPermission) {
					$catModule[] =  array(
						"id"       => $user_id . '-' . $subSite_id . '-' . $value['category_id'],
						"title"    => $value['category_name'],
						"expanded" => true,
						"async"    => true,
						"checked"  => $checked,
						"children" => $modules,
					);
				} else {
					$catModule[] =  array(
						"id"       => $user_id . '-' . $subSite_id . '-' . $value['category_id'],
						"title"    => $value['category_name'],
						"expanded" => true,
						"async"    => true,
						"checked"  => false,
						"children" => $modules,
					);
				}
			} else {
				// NOTE :: Currently not assigned modules without manage category -> If not assigned module via manage category then it will not showing
				$catModule[] =  array(
					"id"       => $user_id . '-' . $subSite_id . '-' . $value['category_id'],
					"title"    => $value['category_name'],
					"expanded" => true,
					"async"    => true,
					"checked"  => false,
					"children" => $modules,
				);
			}
		}

		$allData[] = array(
			"id"       => '0-' . $user_id . '-' . $subSite_id,
			"title"    => "Select All",
			"expanded" => true,
			"async"    => true,
			"selected" => $checked,
			"children" => $catModule,
		);

		return ($allData);
	}

	/**
	 * User wise category/module permission store.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function saveUserPermission(Request $request)
	{
		if ($request->data && $request->user_id) {
			foreach ($request->data as $subSiteId => $data) {
				// Remove permission to unselect all
				if($subSiteId > 0) {
					// Delete existing user permission
					SubSiteUserPermission::where('ssup_sub_site_id', $subSiteId)->where('ssup_user_id', $request->user_id)->delete();
				}

				// If we get selected data then assigned new permission
				if (sizeOf($data) > 0) {
					foreach ($data as $permission) {
						if ($permission) {
							$idArray = explode("-", $permission['id']);
							$userId = $idArray[0];
							if ($userId > 0) {
								$areaId = $idArray[1];
								$moduleId  = $idArray[2];

								// Check assigned user to area for permission
								$subSiteUserExists = SubSiteUser::where('ssu_sub_site_id', $areaId)->where('ssu_user_id', $userId)->where('company_id', Auth::user()->company_id)->exists();
								
								// Give permission to user for area
								if(!$subSiteUserExists) {
									$subSiteData = SubSite::where('sub_site_id', $areaId)->where('company_id', Auth::user()->company_id)->first();

									$subSiteUsers = array(
										'ssu_site_id'     => $subSiteData->site_id,
										'ssu_sub_site_id' => $areaId,
										'ssu_user_id'     => $userId,
										'created_by'      => Auth::user()->id,
										'company_id'      => Auth::user()->company_id,
									);
				
									SubSiteUser::create($subSiteUsers);
								}

								if (isset($permission['module_category_id'])) {
									$userPermission = array(
										'ssup_sub_site_id' => $areaId,
										'ssup_user_id'     => $request->user_id,
										'ssup_category_id' => $permission['module_category_id'],
										'ssup_module_id'   => $moduleId,
										'created_by'       => Auth::user()->id,
										'company_id'       => Auth::user()->company_id,
									);

									SubSiteUserPermission::create($userPermission);

									// NOTE :: Check if category not assigned to this area and user so we assign this
									$subSitePermissionDataExist = SubSitePermission::where('ssp_sub_site_id', $areaId)->where('ssp_category_id', $permission['module_category_id'])->where('ssp_module_id', $moduleId)->where('company_id', Auth::user()->company_id)->exists();

									if(!$subSitePermissionDataExist) {
										$subSitePermission = array(
											'ssp_sub_site_id' => $areaId,
											'ssp_category_id' => $permission['module_category_id'],
											'ssp_module_id'   => $moduleId,
											'created_by'      => Auth::user()->id,
											'company_id'      => Auth::user()->company_id,
										);

										SubSitePermission::create($subSitePermission);
									}
								}
							}
						}
					}
				}
			}
		}

		return $this->returnSuccessMessage('User Permission updated Successfully.');
	}
}
