<?php

namespace App\Http\Controllers\MobileAPI;

use App\Http\Controllers\Controller;
use App\Http\Traits\MobileGeneralTrait;
use App\Models\CategoryModuleFieldValue;
use App\Models\Asset;
use App\Models\Category;
use App\Models\FieldInputType;
use App\Models\Module;
use App\Models\ModuleField;
use App\Models\Site;
use App\Models\SubSite;
use App\Models\SubSiteUserPermission;
use App\Models\User;
use App\Models\ErrorLog;
use App\Models\AssetDocument;
use App\Models\Inspection;
use App\Models\ModuleQuestion;
use App\Models\ModuleAnswer;
use App\Models\AssetActivity;
use App\Models\ConfinedSpaceQuestion;
use App\Models\ConfinedSpaceFormResult;
use App\Models\ConfinedSpaceAnswer;
use App\Models\Company;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Mail;

class ApiController extends Controller
{
    use MobileGeneralTrait;
    /**
     * Display a listing of sites
     *
     * @return \Illuminate\Http\Response
     */
    public function siteListing()
    {
        try {
            $user         = Auth::id();
            $sub_site_ids = SubSiteUserPermission::where('ssup_user_id', $user)->whereNull('deleted_at')->groupBy('ssup_sub_site_id')->pluck('ssup_sub_site_id')->toArray();
            $site_ids     = SubSite::where('sub_site_status', 'Y')->whereIn('sub_site_id', $sub_site_ids)->whereNull('deleted_at')->groupBy('site_id')->pluck('site_id')->toArray();
            $data         = Site::where('site_status', 'Y')->whereIn('site_id', $site_ids)->get();
            
            return $this->returnSuccessMessage(null, $data);
        } catch (Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
    /**
     * Display a listing of sub sites
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function subSiteListing(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'site_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->returnValidation($validator->errors());
            }

            $site_id      = $request->input('site_id');
            $user         = Auth::id();
            $sub_site_ids = SubSiteUserPermission::where('ssup_user_id', $user)->whereNull('deleted_at')->groupBy('ssup_sub_site_id')->pluck('ssup_sub_site_id')->toArray();
            $data         = SubSite::where('sub_site_status', 'Y')->whereIn('sub_site_id', $sub_site_ids)->where('site_id', $site_id)->whereNull('deleted_at')->get();
           
            return $this->returnSuccessMessage(null, $data);
        } catch (Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
    /**
     * Display a listing of categories for logged in user
     * for a particular sub site id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function subSiteCategory(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'sub_site_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->returnValidation($validator->errors());
            }

            $sub_site_id = $request->input('sub_site_id');
            $user_id = Auth::id();
            
            $category_ids = SubSiteUserPermission::where('ssup_user_id', $user_id)
                                ->where('ssup_sub_site_id', $sub_site_id)
                                ->whereNull('deleted_at')
                                ->groupBy('ssup_category_id')
                                ->pluck('ssup_category_id')
                                ->toArray();

            $data = Category::whereIn('category_id', $category_ids)->where('category_status', 'Y')->whereNull('deleted_at')->get();
            
            return $this->returnSuccessMessage(null, $data);
        } catch (Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Display a listing of modules for logged in user
     * for a particular user id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryListingById($userId)
    {
        try {
            $user = User::find($userId);
            if (!$user) {
                return $this->returnError(404, 'User not found');
            }

            $ids = SubSiteUserPermission::where('ssup_user_id', $userId)->whereNull('deleted_at')->groupBy('ssup_category_id')->pluck('ssup_category_id')->toArray();
            $data = Category::where('category_status', 'Y')->whereNull('deleted_at')->whereIn('category_id', $ids)->get();
            return $this->returnSuccessMessage(null, $data);
        } catch (Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Display a listing of modules for logged in user by category id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryModuleListing(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'category_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->returnValidation($validator->errors());
            }

            $userId = Auth::id();
            $catId = $request->input('category_id');

            $ids = SubSiteUserPermission::where('ssup_user_id', $userId)->where('ssup_category_id', $catId)->whereNull('deleted_at')->pluck('ssup_module_id')->toArray();
            $data = Module::where('module_status', 'Y')->whereNull('deleted_at')->whereIn('module_id', $ids)->get();
            
            return $this->returnSuccessMessage(null, $data);
        } catch (Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Display a listing of module fields for logged in user by module id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryModuleFieldListing(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'module_id' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->returnValidation($validator->errors());
            }
            $moduleId = $request->input('module_id');

            // $data = ModuleField::leftjoin('field_input_type', 'module_fields.mf_input_field_type_id', 'field_input_type.field_input_type_id')
            //     ->where('module_fields.mf_module_id', $moduleId)
            //     ->get(['module_fields.*', 'field_input_type.field_input_type_name', 'field_input_type.field_input_type', 'field_input_type.field_input_type_status']);

            $moduleFieldData = ModuleField::select('mf_id', 'mf_module_id', 'mf_input_field_type_id', 'mf_name', 'mf_default_val', 'mf_is_required')
                                    ->with(['fieldInputType' => function ($query) {
                                        $query->select('field_input_type_id', 'field_input_type_name', 'field_input_type');
                                    }])
                                    ->where('mf_module_id', $moduleId)
                                    ->where('mf_status', 'Y')
                                    ->whereNull('deleted_at')
                                    ->get();

            $data = array();
            foreach ($moduleFieldData as $key => $moduleField) {
                $data[$key]['mf_id']                  = $moduleField->mf_id;
                $data[$key]['mf_module_id']           = $moduleField->mf_module_id;
                $data[$key]['mf_input_field_type_id'] = $moduleField->mf_input_field_type_id;
                $data[$key]['field_input_type_name']  = $moduleField->fieldInputType->field_input_type_name;
                $data[$key]['field_input_type']       = $moduleField->fieldInputType->field_input_type;
                $data[$key]['mf_name']                = $moduleField->mf_name;
                $data[$key]['mf_default_val']         = $moduleField->mf_default_val;
                $data[$key]['mf_is_required']         = $moduleField->mf_is_required;
                $data[$key]['keyboardType']           = $this->getKeyboardType($moduleField->fieldInputType->field_input_type);
            }

            return $this->returnSuccessMessage(null, $data);
        } catch (Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /*
    *   Get keyboard type
    */
    public function getKeyboardType($fieldInputType)
    {
        switch ($fieldInputType) {
            case 'varchar':
            case 'longtext':
            case 'mediumtext':
                $data = 'Default';
                break;
            case 'int':
            case 'double':
                $data = 'Numeric';
                break;

            default:
                $data = 'Default';
                break;
        }

        return $data;
    }

    /**
     * Asset registration for logged in user by module id, sub site id, module fields value
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeAsset(Request $request)
    {
        try {
            $uid = $this->generateAssetUID($request->module_id);
            $site_id = SubSite::find($request->sub_site_id)->site_id;

            $asset = new Asset();
            $asset->asset_uid         = $uid;
            $asset->asset_nfc_uuid    = Str::uuid()->toString();
            $asset->asset_module_id   = $request->module_id;
            $asset->asset_site_id     = $site_id;
            $asset->asset_sub_site_id = $request->sub_site_id;
            $asset->asset_user_id     = Auth::user()->id;
            $asset->company_id        = Auth::user()->company_id;
            $asset->asset_latitude    = $request->asset_latitude;
            $asset->asset_longitude   = $request->asset_longitude;

            // Store Image
            if ($request->has('image')) {
                $asset->asset_image = empty($request->image) || $request->image == 'null' ? null : $this->fileUpload($request->image, 'asset_image');
            }
            $asset->save();

            $requestValues = $request->values;

            // loop to get all keys values of request
            foreach ($requestValues as $key => $value) {
                $type = FieldInputType::find(ModuleField::find($value['mf_id'])->mf_input_field_type_id)->field_input_type;
                $var = 'cmfv_value_' . $type;

                $cmfv = new CategoryModuleFieldValue();
                $cmfv->cmfv_asset_id = $asset->asset_id;
                $cmfv->cmfv_mf_id    = $value['mf_id'];
                $cmfv->$var          = $value['value'];
                $cmfv->save();
            }

            // Store inspect document
            if ($request->document_count > 0) {
                $documentData = array(
                    'documents'   => [],
                    'ad_asset_id' => $asset->asset_id,
                    'ad_name'     => $request->ad_name,
                    'ad_type'     => $request->ad_type,
                );

                for ($i = 1; $i <= $request->document_count; $i++) {
                    if ($request->files->has('document_' . $i)) {
                        $documentData['documents'][$i] = $request->file('document_' . $i);
                        $documentData['ad_name'][$i]   = $request->input('ad_name_' . $i);
                        $documentData['ad_type'][$i]   = $request->input('ad_type_' . $i);
                    }
                }
                // Store asset document
                $this->storeAssetDocument($documentData);
            }

            return $this->returnSuccessMessage('Asset created successfully', ['asset_info_url' => '/asset-info/' . $asset->asset_nfc_uuid]);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Update Asset using asset id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updateAsset(Request $request)
    {
        try {
            $assetData = array(
                'asset_latitude'  => $request->asset_latitude,
                'asset_longitude' => $request->asset_longitude,
                'updated_by'      => Auth::user()->id,
            );

            // Update asset data
            Asset::where('asset_id', $request->asset_id)->update($assetData);

            // Update dynamic value
            foreach ($request->dynamic_fields as $key => $value) {
                $categoryModuleFieldValueObj = CategoryModuleFieldValue::find($value['cmfv_id']);
                $column                      = $value['cmfv_column'];
                $existingValue               = $categoryModuleFieldValueObj->$column;
                $updatedValue                = $value['value'];

                if ($existingValue != $updatedValue) {
                    $type = null;
                    $field = FieldInputType::find(ModuleField::find($categoryModuleFieldValueObj->cmfv_mf_id)->mf_input_field_type_id)->field_input_type_name;

                    if ($field == 'Image') {
                        $type = 'image';
                    } elseif ($field == 'Color') {
                        $type = 'color';
                    }

                    // update the value here and make an entry in the asset activity table
                    $oldValue = $existingValue;
                    $newValue = $updatedValue;

                    // Assign new value in obj
                    $categoryModuleFieldValueObj->$column = $newValue;

                    // Change the value if it is date or date time
                    if ($field == 'Date') {
                        $oldValue = $oldValue ? $this->getFormattedDate($oldValue) : '';
                        $newValue = $newValue ? $this->getFormattedDate($newValue) : '';
                    } elseif ($field == 'DateTime') {
                        $oldValue = $oldValue ? $this->getFormattedDateTime($oldValue) : '';
                        $newValue = $newValue ? $this->getFormattedDateTime($newValue) : '';
                    } elseif ($field == 'Time') {
                        $oldValue = $oldValue ? $this->getFormattedTime($oldValue) : '';
                        $newValue = $newValue ? $this->getFormattedTime($newValue) : '';
                    } elseif ($field == 'Boolean') {
                        $oldValue = $oldValue == 'Y' ? 'Yes' : 'No';
                        $newValue = $newValue == 'Y' ? 'Yes' : 'No';
                    }

                    // Add activity log
                    $this->addAssetActivity($request->asset_id, $value['cmfv_id'], $type, $oldValue, $newValue);

                    // Save asset dynamic value
                    $categoryModuleFieldValueObj->save();
                }
            }

            // Checking for image input
            if ($request->has('image')) {
                $asset    = Asset::find($request->asset_id);
                $oldValue = $asset->asset_image;
                $asset->asset_image = empty($request->image) || $request->image == 'null' ? null : $this->fileUpload($request->image, 'asset_image');
                $asset->save();
                $newValue = $asset->asset_image;

                // Set activity log
                $this->addAssetActivity($request->asset_id, null, 'image', $oldValue, $newValue);
            }

            // Store inspect document
            if ($request->document_count > 0) {
                $documentData = array(
                    'documents'   => [],
                    'ad_asset_id' => $request->asset_id,
                );

                for ($i = 1; $i <= $request->document_count; $i++) {
                    if ($request->files->has('document_' . $i)) {
                        $documentData['documents'][$i] = $request->file('document_' . $i);
                        $documentData['ad_name'][$i]   = $request->input('ad_name_' . $i);
                        $documentData['ad_type'][$i]   = $request->input('ad_type_' . $i);
                    }
                }

                // Store asset document
                $this->storeAssetDocument($documentData);
            }

            return $this->returnSuccessMessage('Asset updated successfully');
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * function to insert a record to asset activity table with given old and new value
     */
    public function addAssetActivity($assetId, $cmfvId, $type, $oldValue, $newValue)
    {
        try {
            $activity = new AssetActivity();
            $activity->aa_asset_id   = $assetId;
            $activity->aa_cmfv_id    = $cmfvId;
            $activity->aa_type       = $type;
            $activity->aa_old_value  = $oldValue;
            $activity->aa_new_value  = $newValue;
            $activity->aa_updated_by = Auth::user()->id;
            $activity->save();
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /*
    *   Get Asset UID
    */
    public function generateAssetUID($modId)
    {
        $module = Module::find($modId)->module_name;
        $module = strtoupper(substr($module, 0, 3));
        $assetCount = Asset::where('asset_module_id', $modId)->withTrashed()->count();
        $assetCount = $assetCount + 1;
        $assetCount = str_pad($assetCount, 5, '0', STR_PAD_LEFT);
        return $module . " " . $assetCount;
    }
    
    /**
     * Get company details logged in user by user id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserCompanyDetail()
    {
        try {
            /*
            $companyId = Auth::user()->created_by;

            // Get company detail
            $data['company_detail'] = User::find($companyId);
            */
            
            $companyId = Auth::user()->company_id;
            $companyData = Company::find($companyId);

            // Get company detail
            $data['company_detail']['client_company_name']       = $companyData['name'];
            $data['company_detail']['client_abn']                = $companyData['abn'];
            $data['company_detail']['client_uuid']               = $companyData['client_id'];
            $data['company_detail']['client_company_website']    = $companyData['website'];
            $data['company_detail']['client_company_contact_no'] = $companyData['contact_no'];
            $data['company_detail']['client_company_logo']       = $companyData['logo'];
            $data['company_detail']['street']                    = $companyData['street'];
            $data['company_detail']['city']                      = $companyData['city'];
            $data['company_detail']['state']                     = $companyData['state'];
            $data['company_detail']['country']                   = $companyData['country'];
            $data['company_detail']['postal_code']               = $companyData['postal_code'];
            $data['company_detail']['is_confined_space']         = $companyData['is_confined_space'];

            return $this->returnSuccessMessage(null, $data);
        } catch (Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Store mobile errors
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeError(Request $request)
    {
        try {
            $userId = Auth::id();

            $errorLogData = array(
                'el_name'        => $request->el_name,
                'el_description' => $request->el_description,
                'el_type'        => 'mobile',
                'el_status'      => 'Y',
                'created_by'     => $userId
            );

            // store error log data
            ErrorLog::create($errorLogData);

            // Get all super admin email
            $superAdminEmails = User::where('first_name', '!=', '');
            $superAdminEmails = $superAdminEmails->whereHas("roles", function ($q) {
                                    $q->where("name", "Super Admin");
                                })
                                ->pluck()
                                ->toArray();
            $title = "New Error Log Recorded";

            // Send New Error Log Mail Notification to super admin
            $mail = Mail::send('reports.pdf.listingemail', array(
                        'head'           => $title,
                        'el_name'        => $request->el_name,
                        'el_description' => $request->el_description,
                        'el_type'        => 'mobile',
                        'raised'         => "By {$userId}   At " . Carbon::now()->format('d M Y H:i')
                    ), function ($message) use ($superAdminEmails, $title) {
                        $message->to($superAdminEmails)->subject($title);
                    });

            return $this->returnSuccessMessage('Successfully reported error to the admin.', []);
        } catch (Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Get asset list based on module_id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function assetListing(Request $request)
    {
        try {
            $data = array();

            $assetList = Asset::select('assets.asset_id', 'assets.asset_uid', 'assets.asset_image', 'assets.asset_latitude', 'assets.asset_longitude', 'assets.deleted_at')->with(['values', 'values.moduleField', 'values.moduleField.fieldInputType'])->whereNull('assets.deleted_at')->where('assets.asset_module_id', $request->module_id)->orderBy('assets.asset_id', 'DESC')->get();

            foreach ($assetList as $key => $asset) {
                $data[$key]['asset_id']        = $asset->asset_id;
                $data[$key]['asset_uid']       = $asset->asset_uid;
                $data[$key]['asset_image']     = $asset->asset_image;
                $data[$key]['asset_latitude']  = $asset->asset_latitude;
                $data[$key]['asset_longitude'] = $asset->asset_longitude;
                $data[$key]['asset_info_url']  = url('/asset-info/' . $asset->asset_id);

                foreach ($asset->values as $valueKey => $assetValue) {
                    // Return formatted data as per requirement
                    switch ($assetValue->moduleField->fieldInputType->field_input_type_name) {
                        case 'Color':
                            $data[$key]['color']['label'] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getColorText($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                            $data[$key]['color']['code'] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] : '';
                            break;
                        case 'Time':
                            $data[$key][$assetValue->moduleField->mf_name] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getFormattedTime($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                            break;
                        case 'Date':
                            $data[$key][$assetValue->moduleField->mf_name] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getFormattedDate($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                            break;
                        case 'Date-Time':
                            $data[$key][$assetValue->moduleField->mf_name] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getFormattedDateTime($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                            break;
                        case 'Boolean':
                            $data[$key][$assetValue->moduleField->mf_name] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] == 'Y' ? 'Yes' : 'No';
                            break;

                        default:
                            $data[$key][$this->getSLUG($assetValue->moduleField->mf_name)] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type];
                            break;
                    }
                }
            }
            return $this->returnSuccessMessage('Asset Listed Successfully.', $data);
        } catch (Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    public function getSLUG($name)
    {
        return strtolower(str_replace(' ', '_', $name));
    }

    // Temporary function to get formatted time h:i
    public function getFormattedTime($date)
    {
        return date('H:i', strtotime($date));
    }
    // Temporary function to get formatted date d-MM-Y
    public function getFormattedDate($date)
    {
        return date('d F Y', strtotime($date));
    }
    // Temporary function to get formatted datetime  d-MM-Y H:i
    public function getFormattedDateTime($date)
    {
        return date('d F Y H:i', strtotime($date));
    }

    /*
    *  Get color text based on color code
    */
    public function getColorText($color)
    {
        if ($color == 'rgb(255,0,0)') {
            return 'Jan-Mar';
        } else if ($color == 'rgb(76,175,80)') {
            return 'Apr-Jun';
        } else if ($color == 'rgb(0,0,255)') {
            return 'Jul-Sep';
        } else if ($color == 'rgb(255,255,0)') {
            return 'Oct-Dec';
        } else if ($color == 'rgb(128,128,128)') {
            return 'Others';
        }
    }

    /*
    *  Get Q color expiry date based on color code
    */
    public function getQcolorExpiry($color)
    {
        $year = now()->year;

        if ($color == 'rgb(255,0,0)') {
            // 'Jan-Mar'
            $month = 3;
        } else if ($color == 'rgb(76,175,80)') {
            // 'Apr-Jun'
            $month = 6;
        } else if ($color == 'rgb(0,0,255)') {
            //return 'Jul-Sep';
            $month = 9;
        } else if ($color == 'rgb(255,255,0)') {
            //return 'Oct-Dec';
            $month = 12;
        }
        return Carbon::create($year, $month)->lastOfMonth()->format('Y-m-d');
    }

    /**
     * Get asset list based on module_id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function assetDetail(Request $request)
    {
        //try {
            if ($request->asset_nfc_uuid != '') {
                if ($request->asset_latitude && $request->asset_longitude) {
                    // Update lat and long
                    $this->updateAssetLatLong($request);
                }


                $assetData = Asset::select('assets.asset_id', 'assets.asset_nfc_uuid', 'assets.asset_module_id', 'assets.asset_uid', 'asset_site_id', 'asset_sub_site_id', 'assets.asset_image', 'assets.asset_latitude', 'assets.asset_longitude', 'assets.asset_q_color', 'assets.created_at')
                                ->with(['values', 'module', 'site' => function ($query) {
                                    $query->select('site_id', 'site_name');
                                }, 'subSite' => function ($query) {
                                    $query->select('sub_site_id', 'sub_site_name');
                                }])->where('assets.asset_nfc_uuid', $request->asset_nfc_uuid);

                if (Auth::user()){
                    $assetData = $assetData->where('company_id', Auth::user()->company_id);
                }

                // Get asset data
                $assetData = $assetData->first();

                if ($assetData) {
                    // Site name
                    $siteName = isset($assetData->site->site_name) ? $assetData->site->site_name : '';

                    // Sub site name
                    $subSiteName = isset($assetData->subSite->sub_site_name) ? $assetData->subSite->sub_site_name : '';

                    // Module name
                    $moduleName = isset($assetData->module->module_name) ? $assetData->module->module_name : '';

                    $data['asset_id']                       = $assetData->asset_id;
                    $data['asset_site_id']                  = $assetData->asset_site_id;
                    $data['asset_site_name']                = $siteName;
                    $data['asset_sub_site_id']              = $assetData->asset_sub_site_id;
                    $data['asset_sub_site_name']            = $subSiteName;
                    $data['asset_module_id']                = $assetData->asset_module_id;
                    $data['asset_module']                   = $moduleName;
                    $data['asset_category_id']              = (isset($assetData->module->category) > 0) ? $assetData->module->category->category_id : 0;
                    $data['asset_category_name']            = (isset($assetData->module->category) > 0) ? $assetData->module->category->category_name : '';
                    $data['asset_uid']                      = $assetData->asset_uid;
                    $data['asset_image']                    = $assetData->asset_image;
                    $data['asset_latitude']                 = $assetData->asset_latitude;
                    $data['asset_longitude']                = $assetData->asset_longitude;
                    $data['asset_q_color']                  = $assetData->asset_q_color;
                    $data['asset_info_url']                 = !empty($assetData->asset_nfc_uuid) ? '/asset-info/' . $assetData->asset_nfc_uuid : '';
                    $data['created_at']                     = $assetData->created_at;
                    $data['created_at_format']              = $assetData->created_at_format;
                    $data['asset_inspection_schedule_rgby'] = $assetData->module->module_inspection_schedule_rgby;
                    $data['asset_inspection_schedule']      = $this->getInspectionScheduleText($assetData->module->module_inspection_schedule);

                    $valueKey = 0;
                    foreach ($assetData->values as $assetValue) {
                        if(isset($assetValue->moduleField)) {
                            $data['dynamic_fields'][$valueKey]['mf_id']                  = $assetValue->moduleField->mf_id;
                            $data['dynamic_fields'][$valueKey]['mf_module_id']           = $assetValue->moduleField->mf_module_id;
                            $data['dynamic_fields'][$valueKey]['mf_input_field_type_id'] = $assetValue->moduleField->mf_input_field_type_id;
                            $data['dynamic_fields'][$valueKey]['field_input_type_name']  = $assetValue->moduleField->fieldInputType->field_input_type_name;
                            $data['dynamic_fields'][$valueKey]['field_input_type']       = $assetValue->moduleField->fieldInputType->field_input_type;
                            $data['dynamic_fields'][$valueKey]['mf_name']                = $assetValue->moduleField->mf_name;
                            $data['dynamic_fields'][$valueKey]['mf_default_val']         = $assetValue->moduleField->mf_default_val;
                            $data['dynamic_fields'][$valueKey]['mf_is_required']         = $assetValue->moduleField->mf_is_required;
                            $data['dynamic_fields'][$valueKey]['keyboardType']           = $this->getKeyboardType($assetValue->moduleField->fieldInputType->field_input_type);

                            // Return formatted data as per requirement
                            switch ($assetValue->moduleField->fieldInputType->field_input_type_name) {
                                case 'Color':
                                    $data['dynamic_fields'][$valueKey]['cmfv_id']     = $assetValue->cmfv_id;
                                    $data['dynamic_fields'][$valueKey]['cmfv_column'] = 'cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type;
                                    $data['dynamic_fields'][$valueKey]['label']       = $assetValue->moduleField->mf_name;
                                    $data['dynamic_fields'][$valueKey]['value']       = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getColorText($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                                    $data['dynamic_fields'][$valueKey]['color']       = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] : '';
                                    break;
                                case 'Time':
                                    $data['dynamic_fields'][$valueKey]['cmfv_id']     = $assetValue->cmfv_id;
                                    $data['dynamic_fields'][$valueKey]['cmfv_column'] = 'cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type;
                                    $data['dynamic_fields'][$valueKey]['label']       = $assetValue->moduleField->mf_name;
                                    $data['dynamic_fields'][$valueKey]['value']       = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getFormattedTime($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                                    $data['dynamic_fields'][$valueKey]['color']       = '';
                                    break;
                                case 'Date':
                                    $data['dynamic_fields'][$valueKey]['cmfv_id']     = $assetValue->cmfv_id;
                                    $data['dynamic_fields'][$valueKey]['cmfv_column'] = 'cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type;
                                    $data['dynamic_fields'][$valueKey]['label']       = $assetValue->moduleField->mf_name;
                                    $data['dynamic_fields'][$valueKey]['value']       = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getFormattedDate($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                                    $data['dynamic_fields'][$valueKey]['color']       = '';
                                    break;
                                case 'Date-Time':
                                    $data['dynamic_fields'][$valueKey]['cmfv_id']     = $assetValue->cmfv_id;
                                    $data['dynamic_fields'][$valueKey]['cmfv_column'] = 'cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type;
                                    $data['dynamic_fields'][$valueKey]['label']       = $assetValue->moduleField->mf_name;
                                    $data['dynamic_fields'][$valueKey]['value']       = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getFormattedDateTime($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                                    $data['dynamic_fields'][$valueKey]['color']       = '';
                                    break;
                                case 'Boolean':
                                    $data['dynamic_fields'][$valueKey]['cmfv_id']     = $assetValue->cmfv_id;
                                    $data['dynamic_fields'][$valueKey]['cmfv_column'] = 'cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type;
                                    $data['dynamic_fields'][$valueKey]['label']       = $assetValue->moduleField->mf_name;
                                    $data['dynamic_fields'][$valueKey]['value']       = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] == 'Y' ? 'Yes' : 'No';
                                    $data['dynamic_fields'][$valueKey]['color']       = '';
                                    break;

                                default:
                                    $data['dynamic_fields'][$valueKey]['cmfv_id']     = $assetValue->cmfv_id;
                                    $data['dynamic_fields'][$valueKey]['cmfv_column'] = 'cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type;
                                    $data['dynamic_fields'][$valueKey]['label']       = $assetValue->moduleField->mf_name;
                                    $data['dynamic_fields'][$valueKey]['value']       = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type];
                                    $data['dynamic_fields'][$valueKey]['color']       = '';
                                    break;
                            }

                            $valueKey++;
                        }
                    }
                    
                    // Get Asset Inspection Data
                    $assetInspectionData = $this->getAssetInspectionData($assetData->asset_id);
                    $data['inspections'] = $assetInspectionData;

                    // Get Asset Document Data
                    $assetDocumentData = $this->getAssetDocumentData($assetData->asset_id);
                    $data['documents'] = $assetDocumentData;


                    return $this->returnSuccessMessage('Asset Detail Display Successfully.', $data);
                } else {
                    return $this->returnWarning(204, 'You are not authorized user for this asset');
                }
                
            } else {
                return $this->returnWarning(204, 'This asset has been deleted from the database, please register the asset again if required (remove the tag from the asset if it is no longer required)');
            }
        // } catch (Exception $e) {
        //     return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        // }
    }

    /**
     * Get asset inspection data based on asset id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getAssetInspectionData($assetId)
    {
        $assetInspectionData =  Inspection::where('inspection_asset_id', $assetId)->whereNull('deleted_at')->orderBy('inspection_id', 'DESC')->get();

        $data = array();
        foreach ($assetInspectionData as $key => $assetInspection) {
            $data[$key]['inspection_id']       = $assetInspection->inspection_id;
            $data[$key]['inspection_asset_id'] = $assetInspection->inspection_asset_id;
            $data[$key]['inspection_image_1']  = $assetInspection->inspection_image_1;
            $data[$key]['inspection_image_2']  = $assetInspection->inspection_image_2;
            $data[$key]['inspection_note']     = $assetInspection->inspection_note;
            $data[$key]['inspection_result']   = $assetInspection->inspection_result;
            $data[$key]['inspection_q_color']  = $assetInspection->inspection_q_color;
            $data[$key]['inspectedBy']         = User::find($assetInspection->created_by)->full_name;
            $data[$key]['created_at']          = $assetInspection->created_at;
        }

        return $data;
    }

    /**
     * Get asset & inspection document data based on asset id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getAssetDocumentData($assetId)
    {
        $assetDocumentData =  AssetDocument::where('ad_asset_id', $assetId)->whereNull('deleted_at')->orderBy('ad_id', 'DESC')->get();
        $data = array();
        foreach ($assetDocumentData as $key => $assetDocument) {
            $data[$key]['ad_id']       = $assetDocument->ad_id;
            $data[$key]['ad_asset_id'] = $assetDocument->ad_asset_id;
            $data[$key]['ad_name']     = $assetDocument->ad_name;
            $data[$key]['ad_type']     = $assetDocument->ad_type;
            $data[$key]['ad_link']     = $assetDocument->ad_link;
            $data[$key]['ad_file']     = $assetDocument->ad_file;
            $data[$key]['ad_size']     = $assetDocument->ad_size;
            $data[$key]['created_at']  = $assetDocument->created_at;
        }

        return $data;
    }

    /**
     * Store asset document data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeAssetDocument($data)
    {
        $documentData = array();
        foreach ($data['documents'] as $key => $document) {
            $documentData[] = array(
                'ad_asset_id' => $data['ad_asset_id'],
                'ad_name'     => $data['ad_name'][$key],
                'ad_type'     => $data['ad_type'][$key],
                'ad_size'     => round($document->getSize() / 1024, 1),
                'ad_link'     => null,
                'ad_file'     => $this->fileUpload($document, 'asset_document'),
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now(),
            );
        }
        // Store documents
        AssetDocument::insert($documentData);
    }

    /**
     * Update asset latitude and longitude data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updateAssetLatLong(Request $request)
    {
        $data = array(
            'asset_latitude' => $request->asset_latitude,
            'asset_longitude' => $request->asset_longitude
        );

        // Store documents
        Asset::where('asset_nfc_uuid', $request->asset_nfc_uuid)->update($data);
    }

    /**
     * Get asset list based on global search
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function globalAssetSearch(Request $request)
    {
        try {
            $search    = $request->search;
            $siteId    = $request->site_id;
            $subSiteId = $request->sub_site_id;
            $userId    = Auth::id();


            if ($search != '' && $userId > 0) {
                $assetData = Asset::select('assets.asset_id', 'assets.asset_nfc_uuid', 'assets.asset_module_id', 'assets.asset_uid', 'asset_site_id', 'asset_sub_site_id', 'assets.asset_image', 'assets.asset_latitude', 'assets.asset_longitude')
                                ->with([
                                    'values',
                                    'module',
                                    'site' => function ($query) {
                                        $query->select('site_id', 'site_name');
                                    }, 'subSite' => function ($query) {
                                        $query->select('sub_site_id', 'sub_site_name');
                                    },
                                    'module.category'
                                ])
                                ->whereHas('module', function ($query) {
                                    $query->where('module_status', 'Y');
                                    $query->whereNull('deleted_at');
                                })
                                ->whereHas('site', function ($query) {
                                    $query->where('site_status', 'Y');
                                    $query->whereNull('deleted_at');
                                })
                                ->whereHas('subSite', function ($query) {
                                    $query->where('sub_site_status', 'Y');
                                    $query->whereNull('deleted_at');
                                })
                                ->whereNull('deleted_at');

                // Get sub site id based on user permission
                $subSiteArray = SubSiteUserPermission::where('ssup_user_id', $userId)->whereNull('deleted_at')->groupBy('ssup_sub_site_id')->pluck('ssup_sub_site_id')->toArray();

                // Get sub site id based on user permission
                $siteIdArray = SubSite::where('sub_site_status', 'Y')->whereIn('sub_site_id', $subSiteArray)->whereNull('deleted_at')->groupBy('site_id')->pluck('site_id')->toArray();

                // Get module id based on user permission
                $moduleIdArray = SubSiteUserPermission::where('ssup_user_id', $userId)->whereNull('deleted_at')->groupBy('ssup_module_id')->pluck('ssup_module_id')->toArray();

                // If client select - show all area data
                if (sizeOf($siteIdArray) > 0) {
                    $assetData = $assetData->whereIn('asset_site_id', $siteIdArray);
                } else {
                    // If user have not sites, At that case no need to check in asset
                    return $this->returnError(204, 'Oppps! Asset not found, Please try again...');
                }

                // Filter with sub site
                if (sizeOf($subSiteArray) > 0) {
                    $assetData = $assetData->whereIn('asset_sub_site_id', $subSiteArray);
                } else {
                    // If user have not sub sites, At that case no need to check in asset
                    return $this->returnError(204, 'Oppps! Asset not found, Please try again...');
                }

                // Filter with module id
                if (sizeOf($moduleIdArray) > 0) {
                    $assetData = $assetData->whereIn('asset_module_id', $moduleIdArray);
                } else {
                    // If user have not sub sites, At that case no need to check in asset
                    return $this->returnError(204, 'Oppps! Asset not found, Please try again...');
                }

                // Filter data with site id
                if ($siteId > 0) {
                    $assetData = $assetData->where('asset_site_id', $siteId);
                }

                // Filter data with sub site id
                if ($subSiteId > 0) {
                    $assetData = $assetData->where('asset_sub_site_id', $subSiteId);
                }

                $assetData = $assetData->where(function ($masterQuery) use ($search) {
                    $masterQuery->orWhere('assets.asset_uid', 'like', '%' . $search . '%');
                    $masterQuery->orWhereHas('values', function ($query) use ($search) {
                        $query->where('cmfv_value_text', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_mediumtext', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_longtext', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_int', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_double', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_decimal', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_char', 'like', '%' . $search . '%');
                        $query->orWhere('cmfv_value_varchar', 'like', '%' . $search . '%');
                    });
                });

                // Get asset data
                $assetList = $assetData->get();
                $assetCount = $assetList->count();

                if ($assetCount > 0) {
                    $data = array(
                        'total' => $assetCount,
                        'result' => array()
                    );

                    foreach ($assetList as $key => $asset) {
                        // Site name
                        $siteName = isset($asset->site->site_name) ? $asset->site->site_name : '';

                        // Sub site name
                        $subSiteName = isset($asset->subSite->sub_site_name) ? $asset->subSite->sub_site_name : '';

                        // Module name
                        $moduleName = isset($asset->module->module_name) ? $asset->module->module_name : '';

                        $data['result'][$key]['asset_id']            = $asset->asset_id;
                        $data['result'][$key]['asset_site_id']       = $asset->asset_site_id;
                        $data['result'][$key]['asset_site_name']     = $siteName;
                        $data['result'][$key]['asset_sub_site_id']   = $asset->asset_sub_site_id;
                        $data['result'][$key]['asset_sub_site_name'] = $subSiteName;
                        $data['result'][$key]['asset_module_id']     = $asset->asset_module_id;
                        $data['result'][$key]['asset_module']        = $moduleName;
                        $data['result'][$key]['asset_category_id']   = (isset($asset->module->category) > 0) ? $asset->module->category->category_id : 0;
                        $data['result'][$key]['asset_category_name'] = (isset($asset->module->category) > 0) ? $asset->module->category->category_name : '';
                        $data['result'][$key]['asset_uid']           = $asset->asset_uid;
                        $data['result'][$key]['asset_image']         = $asset->asset_image;
                        $data['result'][$key]['asset_latitude']      = $asset->asset_latitude;
                        $data['result'][$key]['asset_longitude']     = $asset->asset_longitude;
                        $data['result'][$key]['asset_info_url']      = !empty($asset->asset_nfc_uuid) ? '/asset-info/' . $asset->asset_nfc_uuid : '';

                        foreach ($asset->values as $valueKey => $assetValue) {
                            // Return formatted data as per requirement
                            switch ($assetValue->moduleField->fieldInputType->field_input_type_name) {
                                case 'Color':
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['label'] = $assetValue->moduleField->mf_name;
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['value'] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getColorText($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['color'] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] : '';
                                    break;
                                case 'Time':
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['label'] = $assetValue->moduleField->mf_name;
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['value'] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getFormattedTime($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['color'] = '';
                                    break;
                                case 'Date':
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['label'] = $assetValue->moduleField->mf_name;
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['value'] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getFormattedDate($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['color'] = '';
                                    break;
                                case 'Date-Time':
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['label'] = $assetValue->moduleField->mf_name;
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['value'] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] ? $this->getFormattedDateTime($assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type]) : '';
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['color'] = '';
                                    break;
                                case 'Boolean':
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['label'] = $assetValue->moduleField->mf_name;
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['value'] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type] == 'Y' ? 'Yes' : 'No';
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['color'] = '';
                                    break;

                                default:
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['label'] = $assetValue->moduleField->mf_name;
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['value'] = $assetValue['cmfv_value_' . $assetValue->moduleField->fieldInputType->field_input_type];
                                    $data['result'][$key]['dynamic_fields'][$valueKey]['color'] = '';
                                    break;
                            }
                        }

                        // Get Asset Inspection Data
                        $assetInspectionData =  Inspection::select("*", DB::raw("DATE_FORMAT(created_at, '%d %M %Y') as date"), DB::raw('TIME(created_at) as time'))->with('assetWiseInspection')->where('inspection_asset_id', $request->asset_id)->orderBy('inspection_id', 'DESC')->get();
                        $data['result'][$key]['inspections'] = $assetInspectionData;

                        // Get Asset Document Data
                        $documentData = AssetDocument::select("*", DB::raw("DATE_FORMAT(created_at, '%d %M %Y') as ad_date"), DB::raw("TIME_FORMAT(created_at, '%H:%i') as ad_time"))->where('ad_asset_id', $request->asset_id)->orderBy('ad_id', 'DESC')->get();
                        $data['result'][$key]['documents'] = $documentData;
                    }

                    // Data sorting module wise
                    $assetModule = array_column($data['result'], 'asset_module');
                    array_multisort($assetModule, SORT_ASC, $data['result']);

                    $data = $this->group_by('asset_module', $data['result']);

                    return $this->returnSuccessMessage('Asset Listed Successfully.', $data);
                } else {
                    return $this->returnWarning(204, 'Oppps! Asset not found, Please try again...');
                }
            } else {
                return $this->returnError(204, 'Oppps! Asset not found, Please try again...');
            }
        } catch (Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Function that groups an array of associative arrays by some key.
     *
     * @param {String} $key Property to sort by.
     * @param {Array} $data Array that stores multiple associative arrays.
     */
    function group_by($key, $data)
    {
        $result = array();

        foreach ($data as $i => $val) {
            if (array_key_exists($key, $val)) {
                $result[$val[$key]][] = $val;
            } else {
                $result[""][] = $val;
            }
        }

        $output = array();
        foreach ($result as $name => $result) {
            $output[] = array(
                'asset_module' => $name,
                'result' => $result
            );
        }

        return $output;
    }

    /**
     * Asset inspection for logged in user by asset id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeAssetInspection(Request $request)
    {
        //try {
            $assetData = Asset::find($request->inspection_asset_id);

            if($assetData->company_id != Auth::user()->company_id) {
                return $this->returnSuccessMessage('You are not authorized user.', []);
            }

            $data = array(
                'inspection_asset_id' => $request->inspection_asset_id,
                'inspection_note'     => $request->inspection_note,
                'inspection_result'   => $request->inspection_result,
                'inspection_q_color'  => isset($request->inspection_q_color) ? $request->inspection_q_color : NULL,
                'inspection_q_expiry' => isset($request->inspection_q_color) ? $this->getQcolorExpiry($request->inspection_q_color) : NULL,
                'inspection_status'   => 'Y',
                'created_by'          => Auth::id()
            );

            $assetInspectionData = Inspection::create($data);

            // Store inspection image 1
            if ($request->has('inspection_image_1') && $assetInspectionData->inspection_id && $request->file()) {
                if ($request->file()) {
                    $filePath = $this->fileUpload($request->inspection_image_1, 'asset_inspection');
                }

                $updateInspectionData = array(
                    'inspection_image_1' => $filePath
                );

                // Update inspection data
                $assetInspectionData->update($updateInspectionData);
            }

            // Store inspection image 2
            if ($request->has('inspection_image_2') && $assetInspectionData->inspection_id && $request->file()) {
                if ($request->file()) {
                    $filePath = $this->fileUpload($request->inspection_image_2, 'asset_inspection');
                }

                $updateInspectionData = array(
                    'inspection_image_2' => $filePath
                );

                // Update inspection data
                $assetInspectionData->update($updateInspectionData);
            }

            // Store inspect document
            if ($request->document_count > 0) {
                $documentData = array(
                    'documents'   => [],
                    'ad_asset_id' => $request->inspection_asset_id,
                    'ad_name'     => $request->ad_name,
                    'ad_type'     => $request->ad_type,
                );

                for ($i = 1; $i <= $request->document_count; $i++) {
                    if ($request->files->has('document_' . $i)) {
                        $documentData['documents'][$i] = $request->file('document_' . $i);
                        $documentData['ad_name'][$i]   = $request->input('ad_name_' . $i);
                        $documentData['ad_type'][$i]   = $request->input('ad_type_' . $i);
                    }
                }
                // Store asset document
                $this->storeAssetDocument($documentData);
            }

            // Q color is selected then update in asset
            if (!empty($request->inspection_q_color)) {
                // Update Q color in asset table
                Asset::where('asset_id', $request->inspection_asset_id)->update(['asset_q_color' => $request->inspection_q_color]);
            }
            return $this->returnSuccessMessage('You have successfully inspected the asset.', ['inspection_id' => $assetInspectionData->inspection_id]);
        // } catch (\Exception $e) {
        //     return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        // }
    }

    /**
     * Get question list based on module id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getModuleQuestions(Request $request)
    {
        try {
            $moduleId = $request->module_id;
            $userId   = Auth::id();
            $data     = array();

            if ($moduleId > 0 && $userId > 0) {
                // Get Module Name
                $moduleName = Module::find($moduleId)->module_name;

                // Get all question for module
                $moduleQuestionData = ModuleQuestion::where('msq_module_id', $moduleId)->where('msq_status', 'Y')->whereNull('deleted_at')->get();

                $data['moduleName'] = $moduleName;
                foreach ($moduleQuestionData as $key => $moduleQuestion) {
                    $data['questions'][$key]['msq_id']              = $moduleQuestion->msq_id;
                    $data['questions'][$key]['msq_module_id']       = $moduleQuestion->msq_module_id;
                    $data['questions'][$key]['msq_title']           = $moduleQuestion->msq_title;
                    $data['questions'][$key]['msq_desc']            = $moduleQuestion->msq_desc;
                    $data['questions'][$key]['msq_options']         = $moduleQuestion->msq_options;
                    $data['questions'][$key]['msq_type']            = $moduleQuestion->msq_type;
                    $data['questions'][$key]['msq_type_has_button'] = $moduleQuestion->msq_type_has_button;
                    $data['questions'][$key]['msq_has_required']    = $moduleQuestion->msq_has_required;
                }
            }

            return $this->returnSuccessMessage('You have successfully get module questions.', $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Asset inspection for logged in user by asset id
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeUserModuleQuestionAnswer(Request $request)
    {
        try {
            if ($request->module_question_user_answer) {
                $siteId       = $request->site_id;
                $subSiteId    = $request->sub_site_id;
                $inspectionId = $request->inspection_id;

                foreach ($request->module_question_user_answer as $key => $moduleQuestionUserAnswer) {
                    $data[] = array(
                        'msa_question_id'   => $moduleQuestionUserAnswer['msq_id'],
                        'msa_site_id'       => $siteId,
                        'msa_sub_site_id'   => $subSiteId,
                        'msa_inspection_id' => $inspectionId,
                        'msa_module_id'     => $moduleQuestionUserAnswer['msq_module_id'],
                        'msa_user_id'       => Auth::id(),
                        'msa_value'         => $moduleQuestionUserAnswer['value'],
                        'created_at'        => Carbon::now(),
                        'updated_at'        => Carbon::now(),
                    );
                }

                // Store module answer
                ModuleAnswer::insert($data);

                return $this->returnSuccessMessage('You have successfully add question answer.');
            }
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Get confined space question list
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getConfinedSpaceQuestions(Request $request)
    {
        try {
            $data = array();

            // Get all confined space question
            $confinedSpaceQuestionData = ConfinedSpaceQuestion::with('childrenQuestion')->get();

            foreach ($confinedSpaceQuestionData as $key => $confinedQuestion) {
                $data['questions'][$key]['csq_id']        = $confinedQuestion->csq_id;
                $data['questions'][$key]['csq_parent_id'] = $confinedQuestion->csq_parent_id;
                $data['questions'][$key]['csq_title']     = $confinedQuestion->csq_title;

                if (sizeOf($confinedQuestion->childrenQuestion) > 0) {
                    foreach ($confinedQuestion->childrenQuestion as $childeKey => $value) {
                        $data['questions'][$key]['childrenQuestion'][$childeKey]['csq_id']        = $value->csq_id;
                        $data['questions'][$key]['childrenQuestion'][$childeKey]['csq_parent_id'] = $value->csq_parent_id;
                        $data['questions'][$key]['childrenQuestion'][$childeKey]['csq_title']     = $value->csq_title;
                    }
                }
            }

            return $this->returnSuccessMessage('You have successfully get module questions.', $data);
        } catch (\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
     * Store confined space form
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeConfinedSpaceForm(Request $request)
    {
        //try {
            $data = array(
                'csfr_site_id'                  => $request->csfr_site_id,
                'csfr_sub_site_id'              => $request->csfr_sub_site_id,
                'csfr_user_id'                  => Auth::id(),
                'csfr_department'               => $request->csfr_department,
                'csfr_date'                     => $request->csfr_date,
                'csfr_position'                 => $request->csfr_position,
                'csfr_location_of_space'        => $request->csfr_location_of_space,
                'csfr_description_of_space'     => $request->csfr_description_of_space,
                'csfr_is_confined_space_number' => $request->csfr_is_confined_space_number,
                'csfr_confined_space_number'    => $request->csfr_confined_space_number,
                'csfr_result'                   => $request->csfr_result,
                'created_by'                    => Auth::id()
            );

            // Store confined space form result
            $confinedSpaceQuestionData = ConfinedSpaceFormResult::create($data);

            // Question Answer store data
            $questionAnswerData = $request->questions;

            $answerData = array();
            foreach ($questionAnswerData as $key => $confinedQuestion) {
                $answerData[] = array(
                    'csa_site_id'     => $request->csfr_site_id,
                    'csa_sub_site_id' => $request->csfr_sub_site_id,
                    'csa_csfr_id'     => $confinedSpaceQuestionData->csfr_id,
                    'csa_csq_id'      => $confinedQuestion['csq_id'],
                    'csa_value'       => $confinedQuestion['csa_value'],
                    'created_at'      => Carbon::now(),
                    'updated_at'      => Carbon::now(),
                );
            }

            // Store question answer
            ConfinedSpaceAnswer::insert($answerData);

            return $this->returnSuccessMessage('You have successfully submitted confined space form.', $data);
        // } catch (\Exception $e) {
        //     return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        // }
    }

    /**
     * Move asset to another location, area, category and module 
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function moveAsset(Request $request) {
        try {
            if(sizeOf($request->assetId) > 0 && $request->locationId > 0 && $request->areaId > 0 && $request->categoryId > 0 && $request->moduleId > 0) {
                $userId = $request->userId ?? Auth::user()->id;

                if($userId > 0) {
                    //New Value for Activity
                    $newValue = 'Location -> '. Site::find($request->locationId)->site_name.' Area -> '.SubSite::find($request->areaId)->sub_site_name.' Category -> '.Category::find($request->categoryId)->category_name.' Module -> '.Module::find($request->moduleId)->module_name;

                    foreach ($request->assetId as $key => $value) {
                        // Generate Asset UID
                        $moduleIdArray[$key] = $request->moduleId;
                        $newUID[$key]        = $this->generateAssetUID($moduleIdArray[$key]);

                        // Old Value for Activity
                        $oldAssetValue = Asset::where('asset_id', $value)->first(['asset_id','asset_site_id', 'asset_sub_site_id', 'asset_module_id']);
                        $oldValue = 'Location -> '.Site::find($oldAssetValue->asset_site_id)->site_name.' Area -> '.SubSite::find($oldAssetValue->asset_sub_site_id)->sub_site_name.' Category -> '.$oldAssetValue->module->category->category_name.' Module -> '.Module::find($oldAssetValue->asset_module_id)->module_name;

                        // Replicate Asset
                        $newAsset = Asset::find($value)->replicate();
                        $newAsset->asset_uid = $newUID[$key];

                        // Updating the user id with the current login user id & duplicate_from with the asset id
                        $newAsset->asset_user_id        = $userId;
                        $newAsset->created_by           = $userId;
                        $newAsset->asset_duplicate_from = NULL;

                        // Update asset_site_id, asset_sub_site_id, asset_module_id
                        $newAsset->asset_site_id     = $request->locationId;
                        $newAsset->asset_sub_site_id = $request->areaId;
                        $newAsset->asset_module_id   = $request->moduleId;

                        // Update created_at and updated_at
                        $newAsset->created_at = Carbon::now();
                        $newAsset->updated_at = Carbon::now();
                        $newAsset->save();

                        // Update Asset Document
                        AssetDocument::where('ad_asset_id', $oldAssetValue->asset_id)->update(['ad_asset_id' => $newAsset->asset_id]);

                        // Update Asset Activity
                        AssetActivity::where('aa_asset_id', $oldAssetValue->asset_id)->update(['aa_asset_id' => $newAsset->asset_id]);

                        // Update Asset Inspection
                        Inspection::where('inspection_asset_id', $oldAssetValue->asset_id)->update(['inspection_asset_id' => $newAsset->asset_id]);

                        $activity   = new AssetActivity();
                        $activity->aa_asset_id   = $newAsset->asset_id;
                        $activity->aa_cmfv_id    = null;
                        $activity->aa_type       = 'move_to';
                        $activity->aa_old_value  = $oldValue;
                        $activity->aa_new_value  = $newValue;
                        $activity->aa_updated_by = $userId;
                        $activity->save();

                        // Delete old asset
                        $oldAssetValue->delete();
                    }

                    return $this->returnSuccessMessage('Asset Move successfully');
                }
            } else {
                return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
            }
        } catch(\Exception $e) {
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }
}
