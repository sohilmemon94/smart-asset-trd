<?php

namespace App\Http\Controllers\MobileAPI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\MobileGeneralTrait;
use App\Models\User;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use URL;

class AuthController extends Controller
{
    use MobileGeneralTrait;

    /**
     * Login user by email and password
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'email'    => 'required|email|regex:/(.+)@(.+)\.(.+)/i',
                'password' => 'required',
            ]);

            if($validator->fails()){
                return $this->returnValidation($validator->errors());
            }

            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
                $roleId = config('constant.roleIds.site_manager');
                $user   =  Auth::user();

                if($user->roles->last()->id != $roleId){
                    return $this->returnError(401,'You are not allowed to login!');
                }

                $success['token']             = $user->createToken($user->name)->accessToken;
                $success['asset_path']        = URL::to('storage/');
                $success['is_confined_space'] = isset($user->companyData) ? $user->companyData->is_confined_space : 'N';
                $success['user']              = $user;
                //$success['company_detail']    = User::find($user->created_by);

                $companyId = $user->company_id;
                $companyData = Company::find($companyId);

                // Get company detail
                $success['company_detail']['client_company_name']       = $companyData['name'];
                $success['company_detail']['client_abn']                = $companyData['abn'];
                $success['company_detail']['client_uuid']               = $companyData['client_id'];
                $success['company_detail']['client_company_website']    = $companyData['website'];
                $success['company_detail']['client_company_contact_no'] = $companyData['contact_no'];
                $success['company_detail']['client_company_logo']       = $companyData['logo'];
                $success['company_detail']['street']                    = $companyData['street'];
                $success['company_detail']['city']                      = $companyData['city'];
                $success['company_detail']['state']                     = $companyData['state'];
                $success['company_detail']['country']                   = $companyData['country'];
                $success['company_detail']['postal_code']               = $companyData['postal_code'];
                $success['company_detail']['is_confined_space']         = $companyData['is_confined_space'];

                // storage_path()
                return $this->returnSuccessMessage('Login successfully.',$success);
            }else{
                return $this->returnError(401, 'Invalid login credentials, Please try again...');
            }
        }catch(Exception $e){
            return $this->returnError(500, 'Oppps! Something went wrong, Please try again...');
        }
    }

    /**
	 * Logout user (Revoke the token)
	 *
	 * @return [string] message
	 */
	public function logout(Request $request)
	{
		try {
			$user = Auth::user()->token();
			$user->revoke();

			return $this->returnSuccessMessage('You have successfully logged out.', []);
		} catch (Exception $e) {
			// $e->getMessage();
			return $this->returnError(500, 'Opps! Something went wrong, Please try again...');
		}
	}
}
