<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;
use App;

class TestController extends Controller
{
    // Welcome Email Function
    public function sendWelcomeMail($email){
        $toEmail = $email;
        $data    = 'This is welcome email...';

        Mail::mailer('smtp2')->send('email', array('data' => $data), function($message) use ($toEmail,$data){
            $message->to($toEmail)->subject('Welcome Email');
        });

        echo "Welcome Email Send Successfully!";
        exit;
    }
    // Report Email Function
    public function sendReportMail($email){
        $toEmail = $email;
        $data    = 'This is report email...';

        Mail::mailer('smtp3')->send('email', array('data' => $data), function($message) use ($toEmail,$data){
            $message->to($toEmail)->subject('Report Email');
        });

        echo "Report Email Send Successfully!";
        exit;
    }
    // Support Email Function
    public function sendSupportMail($email){
        $toEmail = $email;
        $data    = 'This is support email...';

        Mail::mailer('smtp4')->send('email', array('data' => $data), function($message) use ($toEmail,$data){
            $message->to($toEmail)->subject('Support Email');
        });

        echo "Support Email Send Successfully!";
        exit;
    }

}
