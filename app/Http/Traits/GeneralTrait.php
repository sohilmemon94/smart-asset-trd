<?php

namespace App\Http\Traits;

use App\Models\ModuleField;
use App\Models\Site;
use App\Models\SubSite;
use App\Models\SubSiteUser;
use Exception;
use Illuminate\Support\Facades\Storage;
use File;
use DateTime;
use DateTimeZone;
use Carbon\Carbon;

trait GeneralTrait
{
    /*
        Call when get success response
    */
    public function returnSuccessMessage($msg = "", $data = [])
    {
        // Write data into file - start
        /*$filePath    = storage_path('logs/vue.log');
        $currentDate = date('Y-m-d');

        if(File::exists($filePath)){
            // chmod($filePath, 0777);
            $lastModified = date('Y-m-d',filemtime($filePath));
            $fp = ($currentDate != $lastModified)?fopen($filePath, 'w'):fopen($filePath, 'a');
            fwrite($fp,json_encode($data).PHP_EOL);
            fclose($fp);
        }*/
        // Write data into file - end
        //print_r($msg);
        //print_r();
        //$status = true;
        $user        = request()->user();
        //print_r($user->error);
        $tokenResult = !empty($user) ? $user->createToken('Personal Access Token') : '';
        //$token       = $tokenResult->token;

        return response()->json([
            'status'      => true,
            'message'     => $msg,
            'data'        => $data,
            'accessToken' => !empty($tokenResult) ? $tokenResult->accessToken : '',
			'setRefreshToken'      => env('refreshTokenSecret'),
        ], 200);
    }

    /*
        Call when get error response
    */
    public function returnError($errNum = 401, $msg)
    {
        return response()->json([
            'status'  => false,
            'message' => $msg,
            'type'    => 'error',
            'accessToken'     => env('secret'),
				'setRefreshToken' => env('refreshTokenSecret'),
        ], $errNum);
    }

    /*
        Call when get warning response
    */
    public function returnWarning($errNum = 401, $msg)
    {
        return response()->json([
            'status'  => false,
            'message' => $msg,
            'type'    => 'warning',
            'accessToken'     => env('secret'),
				'setRefreshToken' => env('refreshTokenSecret'),
        ], $errNum);
    }

    /*
        Call when not found error response
    */
    public function notFoundError($statusCode, $msg)
    {
        return response()->json([
            'status'  => false,
            'message' => $msg,
            'accessToken'     => env('secret'),
				'setRefreshToken' => env('refreshTokenSecret'),
        ], $statusCode);
    }

    /*
        Call when return data response
    */
    public function returnData($key, $value, $msg = "")
    {
        // Write data into file - start
        /*$filePath    = storage_path('logs/vue.log');
        $currentDate = date('Y-m-d');

        if(File::exists($filePath)){
            // chmod($filePath, 0777);
            $lastModified = date('Y-m-d',filemtime($filePath));
            $fp = ($currentDate != $lastModified)?fopen($filePath, 'w'):fopen($filePath, 'a');
            fwrite($fp,json_encode($value).PHP_EOL);
            fclose($fp);
        }*/
        // Write data into file - end

        return response()->json([
            'status'  => true,
            'message' => $msg,
            $key      => $value,
            'accessToken'     => env('secret'),
				'setRefreshToken' => env('refreshTokenSecret'),
        ]);
    }

    /*
        Call when return validation response
    */
    public function returnValidation($errors)
    {
        $errorsData = [];
        foreach ($errors->messages() as $key => $value) {
            $errorsData[$key] = $value[0];
        }


        return response()->json([
            'status'      => false,
            'message'     => 'Oppps! Something went wrong, Please try again...',
            'errors'      => $errorsData,
            'accessToken' => env('secret'),
			'setRefreshToken'      => env('refreshTokenSecret'),
        ], 422);
    }

    /*
        Call when upload any file
    */
    public static function fileUpload($file, $path)
    {
        try {
            if (config('global.file_upload') == 's3') {
                $path = $path . "/" . date('FY');
                $path = Storage::disk(config('global.file_upload'))->put($path, $file);
                return $path;
            } else {
                $fileExtension = $file->getClientOriginalExtension();
                $fileName = time() . rand(15, 100) . '.' . $fileExtension;
                $path ="upload/" . $path ;
                $file->storeAs($path, $fileName, 'public');
                return $path . "/" . $fileName;
            }
        } catch (Exception $e) {
            return "";
        }
    }

    /*
        Call when delete uploaded file
    */
    public static function deleteFile($path)
    {
        try {
            if (\Storage::disk('public')->exists($path))
                {
                    \Storage::disk('public')->delete($path);
                    return "";
                }
        }
        catch (Exception $e) {
            return "";
        }
    }

    /*
        Call when get the file URL
    */
    public static function getStorageURL($filename)
    {
        if(config('global.file_upload') == 's3'){
            $disk = Storage::disk('s3');
            if ($disk->exists($filename)) {
                $client = $disk->getDriver()->getAdapter()->getClient();
                $bucket = \Config::get('filesystems.disks.s3.bucket');

                $command = $client->getCommand('GetObject', [
                    'Bucket' => $bucket,
                    'Key' => $filename
                ]);

                $request = $client->createPresignedRequest($command, '+20 minutes');
                return (string) $request->getUri();
            }
        }else{
            return asset('storage/' . $filename);
        }
    }

    /**
     * function to get the list of sites and subsites of the login user.
     * This function will be used to return the sites and subsites of the login user.
     */
    public function getSiteAndSubSiteList($user_id)
    {
        // For the association of user with the sites & subsites we will use the sub_sites_users table.
        $sites = SubSiteUser::select('ssu_site_id')->where('ssu_user_id', $user_id)->distinct()->pluck('ssu_site_id')->toArray();
        //dd($sites);
        $subSites = SubSiteUser::select('ssu_sub_site_id')->where('ssu_user_id', $user_id)->distinct()->pluck('ssu_sub_site_id')->toArray();

        $siteOptions = [];
        foreach ($sites as $key => $value) {
            $site = Site::find($value);
            if($site->site_status == 'Y'){
                $siteOptions[$key]['label'] = $site->site_name;
                $siteOptions[$key]['value'] = $value;
            }
        }
        $subSiteOptions = [];
        foreach ($subSites as $key => $value) {
            $subsite = SubSite::find($value);
            $site    = Site::find($subsite->site_id);
            // dd($subsite->sub_site_status, $site->site_status);
            if($subsite->sub_site_status == 'Y' && $site->site_status == 'Y') {
                $subSiteOptions[$key]['site']  = $subsite->site_id;
                $subSiteOptions[$key]['label'] = $subsite->sub_site_name;
                $subSiteOptions[$key]['value'] = $value;
            }
        }
        return ['sites' => array_values($siteOptions), 'subSites' => array_values($subSiteOptions)];
    }

    // function to get formatted time h:i
    public function getFormattedTime($date)
    {
        return date('H:i', strtotime($date));
    }
    // function to get formatted date d-MM-Y
    public function getFormattedDate($date)
    {
        return date('d F Y', strtotime($date));
    }
    // function to get formatted datetime  d-MM-Y H:i
    public function getFormattedDateTime($date)
    {
        return date('d F Y H:i', strtotime($date));
    }

    public function getExpiryMFids() {
        $mfIds = ModuleField::where('mf_status', 'Y')->where('mf_has_expiry_date', 'Y')->pluck('mf_id')->toArray();
        return $mfIds;
    }

    // Bhautik
    public function convertUTCtime($userTimeZone = 'Asia/Kolkata'){
        $dateTime = new DateTime('2023-03-02 04:43:00', new DateTimeZone('UTC'));
        $dateTime->setTimezone(new DateTimeZone($userTimeZone));

        return $dateTime->format('Y-m-d H:i:s');
    }

    /**
     * Below is a common trait function that will be used to convert time from source timezone to destination timezone
     */
    public function convertTimeZone($sourceTZ, $destinationTZ, $dateTime) {
        $dateTime = new DateTime($dateTime, new DateTimeZone($sourceTZ));
        $dateTime->setTimezone(new DateTimeZone($destinationTZ));
        // return datetime to str
        return $dateTime->format('Y-m-d H:i:s');
    }

    /*
    *  Get Q color expiry date based on color code
    */
    public function getQcolorExpiry($color)
    {
        $year = now()->year;

        if ($color == 'rgb(255,0,0)') {
            // 'Jan-Mar'
            $month = 3;
        } else if ($color == 'rgb(76,175,80)') {
            // 'Apr-Jun'
            $month = 6;
        } else if ($color == 'rgb(0,0,255)') {
            //return 'Jul-Sep';
            $month = 9;
        } else if ($color == 'rgb(255,255,0)') {
            //return 'Oct-Dec';
            $month = 12;
        }
        return Carbon::create($year, $month)->lastOfMonth()->format('Y-m-d');
    }
}


