<?php

namespace App\Http\Traits;
use Exception;
use Illuminate\Support\Facades\Storage;
use File;

trait MobileGeneralTrait
{
    public function returnSuccessMessage($msg = "", $data = [])
    {
        return response()->json([
            'status'  => true,
            'message' => $msg,
            'data'    => $data,
        ], 200);
    }

    public function returnError($errNum = 401, $msg)
    {
        return response()->json([
            'status'  => false,
            'message' => $msg,
            'type'    => 'error'
        ], $errNum);
    }

    public function returnWarning($errNum = 401, $msg)
    {
        return response()->json([
            'status'  => false,
            'message' => $msg,
            'type'    => 'warning'
        ], $errNum);
    }

    public function notFoundError($statusCode, $msg)
    {
        return response()->json([
            'status'  => false,
            'message' => $msg,
        ], $statusCode);
    }

    public function returnData($key, $value, $msg = "")
    {
        return response()->json([
            'status'  => true,
            'message' => $msg,
            $key      => $value,
        ]);
    }

    public function returnValidation($errors)
    {
        $errorsData = [];

        foreach ($errors->messages() as $key => $value) {
            $errorsData[$key] = $value[0];
        }

        return response()->json([
            'status'  => false,
            'message' => null,
            'errors'  => $errorsData,
        ], 422);
    }

    /*
        Call when upload any file
    */
    public static function fileUpload($file, $path)
    {
        try {
            if (config('global.file_upload') == 's3') {
                $path = $path . "/" . date('FY');
                $path = Storage::disk(config('global.file_upload'))->put($path, $file);
                return $path;
            } else {
                //$fileExtension = $file->getClientOriginalExtension();
                //$fileName = time() . rand(15, 100) . '.' . $fileExtension;
                $fileName = $file->getClientOriginalName();
                $path ="upload/" . $path ;
                $file->storeAs($path, $fileName, 'public');
                return $path . "/" . $fileName;
            }
        } catch (Exception $e) {
            return "";
        }
    }
    
    /*
        Call when get the file URL
    */
    public static function getStorageURL($filename)
    {
        if(config('global.file_upload') == 's3'){
            $disk = Storage::disk('s3');
            if ($disk->exists($filename)) {
                $client = $disk->getDriver()->getAdapter()->getClient();
                $bucket = \Config::get('filesystems.disks.s3.bucket');

                $command = $client->getCommand('GetObject', [
                    'Bucket' => $bucket,
                    'Key' => $filename
                ]);

                $request = $client->createPresignedRequest($command, '+20 minutes');
                return (string) $request->getUri();
            }
        }else{
            return asset('storage/' . $filename);
        }
    }

    /*
        Call when get the inspection schedule text
    */
    public static function getInspectionScheduleText($value) {
        if ($value === 7) return 'Weekly';
        if ($value === 30) return 'Monthly';
        if ($value === 90) return 'Every 3 Months';
        if ($value === 'RGBY') return 'Quarterly with RGBY';
        if ($value === 180) return 'Every 6 Months';
        if ($value === 360) return 'Every 12 Months';
        if ($value === 540) return 'Every 18 Months';
        if ($value === 720) return 'Every 24 Months';
        return 'N/A';
    }
}


