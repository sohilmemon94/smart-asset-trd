<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AssetReportIssueMail extends Mailable //implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $toEmail, $assetId, $location, $area, $note, $name, $img;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($toEmail, $assetId, $location, $area, $note, $name, $img)
    {
        $this->toEmail  = $toEmail;
        $this->assetId  = $assetId;
        $this->location = $location;
        $this->area     = $area;
        $this->note     = $note;
        $this->name     = $name;
        $this->img      = $img;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.assetReportIssueMail')
            ->subject("Asset Report Issue")
            ->from(config('setting.FROM_EMAIL'))
            ->with([
                'toEmail'  => $this->toEmail,
                'assetId'  => $this->assetId,
                'location' => $this->location,
                'area'     => $this->area,
                'note'     => $this->note,
                'name'     => $this->name,
                'img'      => $this->img
            ]);
    }
}
