<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordResetSuccessMail extends Mailable //implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $toEmail, $name, $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($toEmail, $name, $link)
    {
        $this->toEmail = $toEmail;
        $this->name    = $name;
        $this->link    = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.passwordResetSuccessMail')
            ->subject("Password Reset Success")
            ->from(config('setting.FROM_EMAIL'))
            ->with([
                'toEmail' => $this->toEmail,
                'name'    => $this->name,
                'link'    => $this->link
            ]);
    }
}
