<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordSetRequestMail extends Mailable //implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $toEmail, $link, $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($toEmail, $link, $name)
    {
        $this->toEmail = $toEmail;
        $this->link    = $link;
        $this->name    = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.passwordSetRequestMail')
            ->subject("Please set your account password")
            ->from(config('setting.WELCOME_EMAIL'))
            ->with([
                'toEmail' => $this->toEmail,
                'link'    => $this->link,
                'name'    => $this->name
            ]);
    }
}
