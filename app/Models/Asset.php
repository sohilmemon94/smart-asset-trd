<?php

namespace App\Models;

use App\Http\Traits\GeneralTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Asset extends Model
{
    use HasFactory, SoftDeletes, GeneralTrait;
    protected $primaryKey = 'asset_id';

    protected $appends = ['created_at_format', 'updated_at_format'];

    //protected $dates = ['deleted_at'];

    /**
	 * Get date as per require format.
	 */
	public function getCreatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->created_at))->format('d F Y H:i');
	}

    public function getUpdatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->updated_at))->format('d F Y H:i');
	}

    public function values()
    {
        return $this->hasMany(CategoryModuleFieldValue::class, 'cmfv_asset_id', 'asset_id');
    }

    public function documents()
    {
        return $this->hasMany(AssetDocument::class, 'ad_asset_id', 'asset_id');
    }

    public function activity()
    {
        return $this->hasMany(AssetActivity::class, 'aa_asset_id', 'asset_id');
    }

    /*
    *   Get asset module data
    */
    public function module()
    {
        return $this->hasOne(Module::class, 'module_id', 'asset_module_id');
    }

    /*
    *   Get asset site data
    */
    public function site()
    {
        return $this->hasOne(Site::class, 'site_id', 'asset_site_id');
    }

    /*
    *   Get asset sub site data
    */
    public function subSite()
    {
        return $this->hasOne(SubSite::class, 'sub_site_id', 'asset_sub_site_id');
    }

    /*
    *   Get asset inspection data
    */
    public function inspections()
    {
        // using has one here because needs to take one instance and that is the last one of the asset
        return $this->hasMany(Inspection::class, 'inspection_asset_id', 'asset_id');
    }
}

