<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetActivity extends Model
{
    use HasFactory;
    protected $primaryKey = 'aa_id';

    public function asset()
    {
        return $this->hasOne(Asset::class, 'asset_id', 'aa_asset_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'aa_updated_by');
    }

    public function values()
    {
        return $this->hasOne(CategoryModuleFieldValue::class, 'cmfv_id', 'aa_cmfv_id');
    }

}
