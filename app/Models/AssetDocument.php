<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AssetDocument extends Model
{
    use HasFactory, SoftDeletes;
    protected $primaryKey = 'ad_id';
}
