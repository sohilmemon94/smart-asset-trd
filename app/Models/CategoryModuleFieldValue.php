<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryModuleFieldValue extends Model
{
    use HasFactory, SoftDeletes;
    protected $primaryKey = 'cmfv_id';

    public function moduleField()
    {
        return $this->hasOne(ModuleField::class, 'mf_id', 'cmfv_mf_id');
    }

}
