<?php

namespace App\Models;

use App\Http\Traits\GeneralTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ClientCloneCategory extends Model
{
    use HasFactory;
    use SoftDeletes;
    use GeneralTrait;

    protected $primaryKey = 'cca_id';
    protected $table = 'client_clone_category';

    protected $fillable = [
        'cca_user_id', 'cca_clone_category_id', 'company_id', 'cca_new_category_id', 'created_by', 'updated_by'
    ];


    protected $appends = ['created_at_format', 'updated_at_format'];

    /**
	 * Get date as per require format.
	 */
	public function getCreatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->created_at))->format('d M Y');
	}

    /**
	 * Get date as per require format.
	 */
    public function getUpdatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->updated_at))->format('d M Y');
	}

}
