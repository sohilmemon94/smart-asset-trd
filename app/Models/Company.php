<?php

namespace App\Models;

use App\Http\Traits\GeneralTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Models\Module;
use App\Models\User;



class Company extends Model
{
    use HasFactory;
    use SoftDeletes;
    use GeneralTrait;

    protected $primaryKey = 'id';
    protected $table = 'company';

    protected $fillable = [
        'name', 'abn', 'client_id', 'website', 'contact_no', 'logo', 'street', 'city', 'state', 'country', 'postal_code', 'is_confined_space', 'commence_date', 'created_by', 'updated_by'
    ];


    protected $appends = ['created_at_format', 'updated_at_format', 'commence_at_format'];

    /**
	 * Get date as per require format.
	 */
	public function getCreatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->created_at))->format('d F Y');
	}

    public function getUpdatedAtFormatAttribute()
	{
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->updated_at))->format('d F Y');
	}

    public function getCommenceAtFormatAttribute()
    {
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->commence_date))->format('d F Y');
    }

    public function admins() {
        return $this->hasMany(User::class);
    }

    /*
    *   Get company root Admin User 
	*/
	public function firstAdminUser()
    {
        return $this->hasOne(User::class)->orderBy('created_at');
    }

}
