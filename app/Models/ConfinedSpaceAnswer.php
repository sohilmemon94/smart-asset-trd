<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ConfinedSpaceAnswer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'csa_id';
    protected $table      = 'confined_space_answer';

    protected $fillable = [
        'csa_site_id', 'csa_sub_site_id', 'csa_csq_id', 'csa_csfr_id', 'csa_value', 'created_by', 'updated_by'
    ];

    public function question()
    {
        return $this->hasOne(ConfinedSpaceQuestion::class, 'csq_id', 'csa_csq_id');
    }
}
