<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ConfinedSpaceQuestion extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'csq_id';
    protected $table      = 'confined_space_question';

    protected $fillable = [
        'csq_parent_id', 'csq_title', 'created_by', 'updated_by'
    ];

    public function childrenQuestion()
    {
        return $this->hasMany(ConfinedSpaceQuestion::class, 'csq_parent_id');
    }
}
