<?php

namespace App\Models;

use App\Http\Traits\GeneralTrait;
use Carbon\Carbon;
use App\Models\Category;
use App\Models\SubSiteUserPermission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;



class Module extends Model
{
    use HasFactory;
    use SoftDeletes;
    use GeneralTrait;

    protected $primaryKey = 'module_id';
    protected $table = 'modules';

    protected $fillable = [
        'module_user_id', 'module_category_id', 'module_name', 'company_id', 'module_prefix', 'module_desc', 'module_icon', 'module_is_default', 'module_has_activity', 'module_has_image', 'module_allow_duplicates','module_inspection_schedule', 'module_inspection_schedule_rgby', 'module_status', 'created_by', 'updated_by'
    ];

    protected $appends = ['created_at_format', 'updated_at_format'];

    /**
     * Get date as per require format.
     */
    public function getCreatedAtFormatAttribute()
    {
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->created_at))->format('d F Y');
    }

    public function getUpdatedAtFormatAttribute()
    {
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->updated_at))->format('d F Y');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'category_id', 'module_category_id');
    }

    public function userWiseModule(){
        return $this->hasMany(SubSiteUserPermission::class, 'ssup_module_id', 'module_id');
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }
}
