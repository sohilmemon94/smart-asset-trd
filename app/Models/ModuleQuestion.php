<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ModuleQuestion extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'msq_id';
    protected $table      = 'module_survey_questions';

    protected $fillable = [
        'msq_module_id', 'msq_user_id', 'company_id', 'msq_title', 'msq_desc', 'msq_options', 'msq_type', 'msq_is_default', 'msq_type_has_button', 'msq_has_required', 'msq_status', 'created_by', 'updated_by'
    ];

    protected $appends = ['type'];

    /**
	 * Get date as per require format.
	 */
	public function getTypeAttribute()
	{
        if ($this->msq_type == 'radio') {
            return 'One option selection';
        } else {
            return 'Multiple option selection';
        }
	}
}
