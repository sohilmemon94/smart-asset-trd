<?php

namespace App\Models;

use App\Http\Traits\GeneralTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Carbon\Carbon;

class Site extends Model
{
    use HasFactory;
    use SoftDeletes;
    use GeneralTrait;

    protected $primaryKey = 'site_id';
    protected $table = 'locations';

    protected $fillable = [
        'site_name', 'site_uuid', 'site_description', 'company_id', 'site_status', 'created_by', 'updated_by'
    ];

    protected $appends = ['created_at_format', 'updated_at_format'];

    public function getCreatedAtFormatAttribute()
    {
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->created_at))->format('d F Y');
    }

    public function getUpdatedAtFormatAttribute()
    {
        $timeZone = config('constant.timeZone');
		return Carbon::parse($this->convertTimeZone('UTC', $timeZone, $this->updated_at))->format('d F Y');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function area()
    {
        return $this->hasMany(SubSite::class, 'site_id', 'site_id')->withTrashed();
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
