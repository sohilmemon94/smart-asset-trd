<?php

namespace App\Models;
use App\Models\Site;
use App\Models\SubSiteUser;
use App\Models\SubSiteUserPermission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SubSite extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'sub_site_id';
    protected $table = 'areas';

    protected $fillable = [
        'sub_site_uuid', 'site_id', 'sub_site_name','company_id', 'sub_site_description', 'sub_site_status','created_by','updated_by'
    ];

    // public function user()
    // {
    //     return $this->hasMany(User::class, 'id', 'user_id');
    // }
    public function subSiteUser(){
        return $this->hasMany(SubSiteUser::class, 'ssu_sub_site_id', 'sub_site_id');
    }

    public function site()
    {
        return $this->hasOne(Site::class, 'site_id', 'site_id');
    }

    public function subSiteUserPermission(){
        return $this->hasMany(SubSiteUserPermission::class, 'ssup_sub_site_id ', 'sub_site_id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }
}
