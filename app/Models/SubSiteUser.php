<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SubSiteUser extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'ssu_id';
    protected $table      = 'sub_sites_users';

    protected $fillable = [
        'ssu_site_id', 'ssu_sub_site_id', 'ssu_user_id', 'company_id', 'created_by', 'updated_by'
    ];

    public function siteManager()
    {
        return $this->belongsTo('App\Models\User', 'ssu_user_id', 'id');
    }


}
