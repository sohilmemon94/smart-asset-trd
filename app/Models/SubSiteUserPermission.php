<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class SubSiteUserPermission extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $primaryKey = 'ssup_id';
    protected $table      = 'sub_site_user_permission';

    protected $fillable = [
        'ssup_sub_site_id', 'ssup_user_id', 'ssup_category_id', 'ssup_module_id', 'company_id', 'created_by', 'updated_by'
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'ssup_category_id', 'category_id');
    }

    public function module()
    {
        return $this->belongsTo('App\Models\Module', 'ssup_module_id', 'module_id');
    }
}
