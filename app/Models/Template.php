<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\TemplateField;

class Template extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'template_id';
    protected $table = 'templates';

    protected $fillable = [
        'company_id', 'template_name', 'template_description', 'template_is_default', 'template_status', 'created_by', 'updated_by'
    ];

    public function templateField(){
        return $this->hasMany(TemplateField::class, 'template_field_template_id', 'template_id');
    }
}
