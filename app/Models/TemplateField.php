<?php

namespace App\Models;

use App\Models\FieldInputType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TemplateField extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $primaryKey = 'template_field_id';
    protected $table      = 'template_fields';

    protected $fillable = [
        'template_field_template_id', 'template_field_input_field_type_id', 'template_field_name', 'template_field_default_val', 'template_field_is_show_on_grid','template_field_has_expiry_date', 'template_field_is_show_on_details', 'company_id', 'template_field_is_required', 'template_field_status', 'created_by', 'updated_by'
    ];

    /*
     *  Get field input type data
     */
    public function fieldInputType()
    {
        return $this->hasOne(FieldInputType::class, 'field_input_type_id', 'template_field_input_field_type_id');
    }
}
