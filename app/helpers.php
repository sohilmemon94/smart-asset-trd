<?php

function angleToRotate($w, $h, $mode) {
    if($w>$h) {
        if($mode=='portrait'){
            return '90deg';
        }
        else {
            return '0deg';
        }
    }
    else {
        if($mode=='landscape'){
            return '-90deg';
        }
        else {
            return '0deg';
        }
    }
}

function getInspectionScheduleText($value) {
    if ($value === 7) return 'Weekly';
    if ($value === 30) return 'Monthly';
    if ($value === 90) return 'Every 3 Months';
    if ($value === 'RGBY') return 'Quarterly with RGBY';
    if ($value === 180) return 'Every 6 Months';
    if ($value === 360) return 'Every 12 Months';
    if ($value === 540) return 'Every 18 Months';
    if ($value === 720) return 'Every 24 Months';
    return 'N/A';
}
