<?php

return [
    'roleIds' => [
        'admin'  => 1,              //Super Admin
        'client' => 2,              //Admin
        'site_manager' => 3         //Client
    ],
    'fieldsType' => [
        'text'     => ['text', 'string', 'varchar', 'char'],
        'number'   => ['int'],
        'decimal'  => ['decimal', 'double'],
        'textarea' => ['mediumtext', 'longtext'],
        'time'     => ['time'],
        'date'     => ['date'],
        'datetime' => ['datetime'],
        'boolean'  => ['boolean'],
    ],
    'timeZone'     => 'UTC'
];
