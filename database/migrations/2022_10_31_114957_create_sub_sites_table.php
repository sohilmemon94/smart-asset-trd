<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->id('sub_site_id');
            $table->string('sub_site_uuid')->nullable();
            $table->unsignedBigInteger('site_id');
            $table->string('sub_site_name', 50);
            $table->text('sub_site_description')->nullable();
            $table->enum('sub_site_status', ['Y', 'N'])->default('Y')->comment('Y => Active, N => Inactive');;
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // Foreign Key
            $table->foreign('site_id')->references('site_id')->on('locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
};
