<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->id('module_id');
            $table->unsignedBigInteger('module_user_id');
            $table->unsignedBigInteger('module_category_id');
            $table->string('module_name');
            $table->string('module_prefix');
            $table->text('module_desc')->nullable();
            $table->string('module_icon')->nullable();
            $table->enum('module_is_default', ['Y', 'N'])->default('N')->comment('Y => Yes, N => No');
            $table->enum('module_has_activity', ['Y', 'N'])->default('Y')->comment('Y => Active, N => Inactive');
            $table->enum('module_allow_duplicates', ['Y', 'N'])->default('N')->comment('Y => Active, N => Inactive');
            $table->enum('module_status', ['Y', 'N'])->default('Y')->comment('Y => Active, N => Inactive');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // Foreign Key
            $table->foreign('module_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('module_category_id')->references('category_id')->on('category')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
};
