<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_fields', function (Blueprint $table) {
            $table->id('template_field_id');
            $table->unsignedBigInteger('template_field_template_id');
            $table->unsignedBigInteger('template_field_input_field_type_id');
            $table->string('template_field_name', 50);
            $table->text('template_field_default_val')->nullable();
            $table->enum('template_field_is_show_on_grid', ['Y', 'N'])->default('Y')->comment('Y => Yes, N => No');
            $table->enum('template_field_is_show_on_details', ['Y', 'N'])->default('Y')->comment('Y => Yes, N => No');
            $table->enum('template_field_is_required', ['Y', 'N'])->default('Y')->comment('Y => Yes, N => No');
            $table->enum('template_field_status', ['Y', 'N'])->default('Y')->comment('Y => Active, N => Inactive');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('template_field_template_id')->references('template_id')->on('templates')->onDelete('cascade');
            $table->foreign('template_field_input_field_type_id')->references('field_input_type_id')->on('field_input_type')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_fields');
    }
};
