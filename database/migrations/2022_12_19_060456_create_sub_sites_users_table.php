<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_sites_users', function (Blueprint $table) {
            $table->id('ssu_id');
            $table->unsignedBigInteger('ssu_site_id');
            $table->unsignedBigInteger('ssu_sub_site_id');
            $table->unsignedBigInteger('ssu_user_id');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();

            // Foreign Key
            $table->foreign('ssu_site_id')->references('site_id')->on('locations')->onDelete('cascade');
            $table->foreign('ssu_sub_site_id')->references('sub_site_id')->on('areas')->onDelete('cascade');
            $table->foreign('ssu_user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_sites_users');
    }
};
