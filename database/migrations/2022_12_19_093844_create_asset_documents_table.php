<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_documents', function (Blueprint $table) {
            $table->id('ad_id');
            $table->unsignedBigInteger('ad_asset_id');
            $table->string('ad_name');
            $table->string('ad_type');
            $table->string('ad_link')->nullable();
            $table->string('ad_file')->nullable();
            $table->double('ad_size')->comment("Size in KB")->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('ad_asset_id')->references('asset_id')->on('assets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_documents');
    }
};
