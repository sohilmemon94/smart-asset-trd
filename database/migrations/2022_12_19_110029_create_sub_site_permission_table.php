<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_site_permission', function (Blueprint $table) {
            $table->id('ssp_id');
            $table->unsignedBigInteger('ssp_sub_site_id');
            $table->unsignedBigInteger('ssp_category_id');
            $table->unsignedBigInteger('ssp_module_id');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();


            // Foreign Key
            $table->foreign('ssp_sub_site_id')->references('sub_site_id')->on('areas')->onDelete('cascade');
            $table->foreign('ssp_category_id')->references('category_id')->on('category')->onDelete('cascade');
            $table->foreign('ssp_module_id')->references('module_id')->on('modules')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_site_permission');
    }
};
