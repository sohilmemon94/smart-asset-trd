<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_module_field_values', function (Blueprint $table) {
            $table->id('cmfv_id');
            $table->unsignedBigInteger('cmfv_asset_id');
            $table->unsignedBigInteger('cmfv_mf_id');
            $table->text('cmfv_value_text')->nullable();
            $table->mediumText('cmfv_value_mediumtext')->nullable();
            $table->longText('cmfv_value_longtext')->nullable();
            $table->integer('cmfv_value_int')->nullable();
            $table->double('cmfv_value_double')->nullable();
            $table->double('cmfv_value_decimal')->nullable();
            $table->char('cmfv_value_char')->nullable();
            $table->date('cmfv_value_date')->nullable();
            $table->time('cmfv_value_time')->nullable();
            $table->dateTime('cmfv_value_date_time')->nullable();
            $table->string('cmfv_value_varchar')->nullable();
            $table->enum('cmfv_value_boolean', ['Y', 'N'])->default('N');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cmfv_mf_id')->references('mf_id')->on('module_fields');
            $table->foreign('cmfv_asset_id')->references('asset_id')->on('assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_module_field_values');
    }
};
