<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_module_field_values', function (Blueprint $table) {
            $table->renameColumn('cmfv_value_date_time', 'cmfv_value_datetime')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_module_field_values', function (Blueprint $table) {
            $table->renameColumn('cmfv_value_datetime', 'cmfv_value_date_time')->nullable()->change();
        });
    }
};
