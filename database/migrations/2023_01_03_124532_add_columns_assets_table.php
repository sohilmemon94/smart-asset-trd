<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->string('asset_image')->nullable()->after('asset_module_id');
            $table->string('asset_latitude')->nullable()->after('asset_image');
            $table->string('asset_longitude')->nullable()->after('asset_latitude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('asset_image');
            $table->dropColumn('asset_latitude');
            $table->dropColumn('asset_longitude');
        });
    }
};
