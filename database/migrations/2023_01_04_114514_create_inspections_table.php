<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspections', function (Blueprint $table) {
            $table->id('inspection_id');
            $table->unsignedBigInteger('inspection_asset_id');
            $table->string('inspection_image_1')->nullable();
            $table->string('inspection_image_2')->nullable();
            $table->text('inspection_note')->nullable();
            $table->enum('inspection_result', ['pass', 'fail', 'maintenance_required', 'inspect'])->comment('pass => Pass, fail => Fail, maintenance_required => Maintenance Required, inspect => Inspection');
            $table->enum('inspection_status', ['Y', 'N'])->default('Y')->comment('Y => Active, N => Inactive');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();

            // Foreign Key
            $table->foreign('inspection_asset_id')->references('asset_id')->on('assets')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspections');
    }
};
