<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_activities', function (Blueprint $table) {
            $table->id('aa_id');
            $table->unsignedBigInteger('aa_asset_id');
            $table->unsignedBigInteger('aa_cmfv_id');
            $table->unsignedBigInteger('aa_updated_by');
            $table->longText('aa_old_value')->nullable();
            $table->longText('aa_new_value')->nullable();
            $table->string('aa_type', 20)->nullable();
            $table->timestamps();

            $table->foreign('aa_asset_id')->references('asset_id')->on('assets');
            $table->foreign('aa_cmfv_id')->references('cmfv_id')->on('category_module_field_values');
            $table->foreign('aa_updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_activities');
    }
};
