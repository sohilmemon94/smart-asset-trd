<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_activities', function (Blueprint $table) {
            // modify cmfv_id column to nullable
            $table->unsignedBigInteger('aa_cmfv_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_activities', function (Blueprint $table) {
            $table->unsignedBigInteger('aa_cmfv_id')->nullable(false)->change();
        });
    }
};
