<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('module_fields', function (Blueprint $table) {
            $table->enum('mf_has_expiry_date', ['Y', 'N'])->default('N')->comment('Y => Yes, N => No');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('module_fields', function (Blueprint $table) {
            $table->dropColumn('mf_has_expiry_date');
        });
    }
};
