<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_survey_answers', function (Blueprint $table) {
            $table->id('msa_id');
            $table->unsignedBigInteger('msa_question_id');
            $table->unsignedBigInteger('msa_site_id');
            $table->unsignedBigInteger('msa_sub_site_id');
            $table->unsignedBigInteger('msa_module_id');
            $table->unsignedBigInteger('msa_user_id');
            $table->string('msa_value');
            $table->timestamps();

            // Foreign Key
            $table->foreign('msa_question_id')->references('msq_id')->on('module_survey_questions')->onDelete('cascade');
            $table->foreign('msa_site_id')->references('site_id')->on('locations')->onDelete('cascade');
            $table->foreign('msa_sub_site_id')->references('sub_site_id')->on('areas')->onDelete('cascade');
            $table->foreign('msa_module_id')->references('module_id')->on('modules')->onDelete('cascade');
            $table->foreign('msa_user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_survey_answers');
    }
};
