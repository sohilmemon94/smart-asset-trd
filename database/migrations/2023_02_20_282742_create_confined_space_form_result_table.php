<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confined_space_form_result', function (Blueprint $table) {
            $table->id('csfr_id');
            $table->unsignedBigInteger('csfr_site_id');
            $table->unsignedBigInteger('csfr_sub_site_id');
            $table->unsignedBigInteger('csfr_user_id');
            $table->string('csfr_department');
            $table->dateTime('csfr_date');
            $table->string('csfr_position');
            $table->string('csfr_location_of_space');
            $table->longText('csfr_description_of_space');
            $table->enum('csfr_is_confined_space_number', ['Y', 'N'])->default('N');
            $table->string('csfr_confined_space_number');
            $table->string('csfr_result');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('csfr_site_id')->references('site_id')->on('locations')->onDelete('cascade');
            $table->foreign('csfr_sub_site_id')->references('sub_site_id')->on('areas')->onDelete('cascade');
            $table->foreign('csfr_user_id')->references('id')->on('users')->onDelete('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confined_space_form_result');
    }
};
