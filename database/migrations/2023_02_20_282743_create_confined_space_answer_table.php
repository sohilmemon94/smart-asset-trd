<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confined_space_answer', function (Blueprint $table) {
            $table->id('csa_id');
            $table->unsignedBigInteger('csa_site_id');
            $table->unsignedBigInteger('csa_sub_site_id');
            $table->unsignedBigInteger('csa_csq_id');
            $table->unsignedBigInteger('csa_csfr_id');
            $table->enum('csa_value', ['Y', 'N'])->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('csa_site_id')->references('site_id')->on('locations');
            $table->foreign('csa_sub_site_id')->references('sub_site_id')->on('areas');
            $table->foreign('csa_csq_id')->references('csq_id')->on('confined_space_question');
            $table->foreign('csa_csfr_id')->references('csfr_id')->on('confined_space_form_result');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confined_space_answer');
    }
};
