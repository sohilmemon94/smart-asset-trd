<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('module_survey_answers', function (Blueprint $table) {
            $table->unsignedBigInteger('msa_inspection_id')->after('msa_module_id');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('msa_inspection_id');

            // Foreign Key
            $table->foreign('msa_inspection_id')->references('inspection_id')->on('inspections')->onDelete('cascade');
        });
    }
};
