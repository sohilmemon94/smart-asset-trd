<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->boolean('is_notified_24hrs')->default(false)->after('asset_longitude')->comment("1->Notification has been sent for this asset. 0->It's not.");
            $table->boolean('is_notified_7days')->default(false)->after('is_notified_24hrs')->comment("1->Notification has been sent for this asset. 0->It's not.");
            $table->boolean('is_notified_30days')->default(false)->after('is_notified_7days')->comment("1->Notification has been sent for this asset. 0->It's not.");
            $table->boolean('is_notified_90days')->default(false)->after('is_notified_30days')->comment("1->Notification has been sent for this asset. 0->It's not.");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('is_notified_24hrs');
            $table->dropColumn('is_notified_7days');
            $table->dropColumn('is_notified_30days');
            $table->dropColumn('is_notified_90days');
        });
    }
};
