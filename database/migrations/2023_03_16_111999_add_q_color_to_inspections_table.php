<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspections', function (Blueprint $table) {
            $table->string('inspection_q_color', '20')->nullable()->after('inspection_result');
            $table->date('inspection_q_expiry')->nullable()->after('inspection_q_color');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspections', function (Blueprint $table) {
            $table->dropColumn('inspection_q_color');
            $table->dropColumn('inspection_q_expiry');
        });
    }
};
