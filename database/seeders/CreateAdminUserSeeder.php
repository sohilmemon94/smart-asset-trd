<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'Super',
            'last_name'  => 'Admin',
            'email'      => 'superadmin@gmail.com',
            'password'   => bcrypt('superadmin@123')
        ]);

        // Create super admin role
        $role = Role::create(['name' => 'Super Admin', 'guard_name' => 'api']);

        // Get all permission
        $permissions = Permission::pluck('id','id')->all();

        // Assign all permission to super admin role
        $role->syncPermissions($permissions);

        // Assign role to user
        $user->assignRole([$role->id]);
    }
}
