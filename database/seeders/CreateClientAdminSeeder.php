<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateClientAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'company_id' => 1,
            'first_name' => 'Client',
            'last_name'  => 'Admin',
            'email'      => 'clientadmin@gmail.com',
            'password'   => bcrypt('clientadmin@123')
        ]);

        // Create client admin role
        $role = Role::create(['name' => 'Admin', 'guard_name' => 'api']);

        // Assign role to user
        $user->assignRole([$role->id]);
    }
}
