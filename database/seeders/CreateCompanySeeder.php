<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Company;

class CreateCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = Company::create([
            'name'              => 'Smart Asset',
            'abn'               => 'abn12345',
            'client_id'         => 'SA1',
            'website'           => 'http://smartassetrig.com',
            'contact_no'        => '40404040040',
            'logo'              => 'upload/image/users/default_company.png',
            'street'            => 'North Ewingar Road',
            'city'              => 'Sydney',
            'state'             => 'New South Wales',
            'country'           => 'Australia',
            'postal_code'       => '2533',
            'is_confined_space' => 'N',
        ]);
    }
}
