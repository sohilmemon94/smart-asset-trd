<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateSiteManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clientAdmin = User::where('email','clientadmin@gmail.com')->first();

        $user = User::create([
            'company_id' => 1,
            'first_name' => 'Site',
            'last_name'  => 'Manager',
            'email'      => 'sitemanager@gmail.com',
            'password'   => bcrypt('sitemanager@123'),
            'created_by' => ($clientAdmin)?$clientAdmin->id:null
        ]);

        // Create User role
        $role = Role::create(['name' => 'User', 'guard_name' => 'api']);

        // Assign role to user
        $user->assignRole([$role->id]);
    }
}
