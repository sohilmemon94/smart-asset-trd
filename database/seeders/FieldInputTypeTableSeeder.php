<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\FieldInputType;

class FieldInputTypeTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fieldData = [
            'Text'              => 'varchar',
            'Long Text'         => 'mediumtext',
            'Very Long Text'    => 'longtext',
            'Numeric - Integer' => 'int',
            'Numeric - Double'  => 'double',
            'Date'              => 'date',
            'Time'              => 'time',
            'DateTime'          => 'datetime',
            'Color'             => 'char',
            'Boolean'           => 'boolean',
            // 'Percentage'        => 'decimal',
            // 'Password'          => 'varchar',
            // 'Code'              => 'longtext',
            // 'Token'             => 'varchar',
        ];

        foreach ($fieldData as $key => $data) {
            $status = ($key == 'Color') ? 'N' : 'Y';

            FieldInputType::create(['field_input_type_name' => $key, 'field_input_type' => $data, 'field_input_type_status' => $status]);
        }
    }
}
