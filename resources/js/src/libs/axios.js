import Vue from 'vue'

// axios
import axios from 'axios'

import store from '@/store';
import { initialAbility } from '@/libs/acl/config'
import useJwt from '@/auth/jwt/useJwt'
import Router from '../router';

const accessToken = localStorage.getItem('access_token');

const axiosIns = axios.create({
    // You can add your headers here
    // ================================
    // baseURL: 'https://some-domain.com/api/',
    // timeout: 1000,
    // headers: {'X-Custom-Header': 'foobar'}
    baseURL: window.BASE_URL + "/api/",
    headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + accessToken
    }
})

// Initial loading
const appLoading = document.getElementById('loading-bg')

axiosIns.interceptors.request.use(config => {
    // Display Loader
    if(config.url!=='dashboard') {
        store.commit('app/UPDATE_LOADER_FLAG', true)
    }

    var timeZone = store.state.app.timeZone;


    // // Add timeZone request to every API
    if(config.method == "get"){
        // Add variable in params
        if(!config.params) {
            config.params = {};
        }
        config.params.timeZone = timeZone;
    } else if(config.method == "post"){
        // Add variable in data on the basis of object or formdata
        if(config.data instanceof FormData) {
            config.data.append('timeZone', timeZone);
        }
        else if(typeof config.data === 'object') {
            config.data.timeZone = timeZone;
        }
    }

    if (config.method == "get" || config.method == "post" || config.method == "delete" || config.method == "put" || config.loader == true) {
        config.has_loader = true;
        store.dispatch('app/openRequestIncrementCount');
    }

    if (appLoading && store.getters['app/openRequestCount'] > 0 && store.getters['app/loaderFlag']) {
        appLoading.style.display = 'block'
    }

    if(config.url==='dashboard') {
        store.commit('app/UPDATE_LOADER_FLAG', false)
    }

    return config
});


axiosIns.interceptors.response.use(function (response) {
    if (response.config.has_loader) {
        store.dispatch('app/openRequestDecrementCount');
    }

    // Remove loader
    if (appLoading && store.getters['app/openRequestCount'] <= 0) {
        appLoading.style.display = 'none'

        // Remove error on success
        store.commit('app/UPDATE_ERRORS', []);
    }

    // Any status code within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
}, function (error) {

    // Handel 500 error
    if (error.response.status == 500) {
        Router.push({ name: 'error-404' });
    }

    if (!!error && !!error.response && !!error.response.data.message && error.response.data.message == "Unauthenticated.") {
        localStorage.removeItem(
            useJwt.jwtConfig.storageTokenKeyName
        );
        localStorage.removeItem(
            useJwt.jwtConfig.storageRefreshTokenKeyName
        );

        // Remove userData from localStorage
        localStorage.removeItem("userData");

        // Redirect to login page
        Router.push({ name: "auth-login" });
    }

   // if (response.config.has_loader) {
        store.dispatch('app/openRequestDecrementCount');
    //}

    // Remove loader
    if (appLoading && store.getters['app/openRequestCount'] <= 0) {
        appLoading.style.display = 'none'
    }
    // Server side file size validation error handling
    if(error.response.status == 413 && error.response.data.errors && error.response.data.errors.file) {
        // toastr message logic in vue file.
    }
    // Any status codes outside the range of 2xx cause this function to trigger
    // Do something with response error
    else if (error.response && error.response.data && error.response.data.errors) {
        store.commit('app/UPDATE_ERRORS', error.response.data.errors);
    } else {
        store.commit('app/UPDATE_ERRORS', []);
    }

    return Promise.reject(error);
});

Vue.prototype.$http = axiosIns

export default axiosIns
