import { mapGetters, mapActions } from "vuex";
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'

export default {
    data() {
        const languageOptions = ['English', 'Spanish', 'French', 'Russian', 'German', 'Arabic', 'Sanskrit']

        const genderOptions = [
            { text: 'Male', value: 'male' },
            { text: 'Female', value: 'female' },
        ]

        const statusOptions = [
            { label: 'Active', value: 'Y' },
            { label: 'Inactive', value: 'N' },
        ]

        const colorOptions = [
            { label: 'Jan-Mar', value: 'rgb(255,0,0)' },
            { label: 'Apr-Jun', value: 'rgb(76,175,80)' },
            { label: 'Jul-Sep', value: 'rgb(0,0,255)' },
            { label: 'Oct-Dec', value: 'rgb(255,255,0)' },
            { label: 'Other', value: 'rgb(128,128,128)' },
        ]

        const confinedSpaceOptions = [
            { label: 'Yes', value: 'Y' },
            { label: 'No', value: 'N' },
        ]
        return {
            languageOptions,
            genderOptions,
            statusOptions,
            colorOptions,
            confinedSpaceOptions,
        }
    },
    mounted() {
        this.clearError();
    },
    computed: {
        ...mapGetters('app', ["getError"]),
    },
    methods: {
        // Clear all errors
        ...mapActions('app', [
            'clearError',
        ]),

        /**
         * This function is used to redirect to the dashboard page for different user role
         * @param {string} role
         */
        goToDashboard(role) {
            if(role === 'Super Admin') {
                this.$router.push({ name: 'admin-dashboard' });
            } else if(role === 'Admin') {
                this.$router.push({ name: 'client-dashboard' });
            } else if(role === 'User') {
                this.$router.push({ name: 'dashboard' });
            }
        },

        /* This function is used to redirect to the detail listing page from dashboard
        *  @param {string} role - role of the user
        *  @param {object} params - params to be passed to the route
        */
        goToDashboardDetails(role, params) {
            // check if params object has time property
            if(params.time) {
                this.$router.push({ name: `${role}-dashboard-details-time`, params: params });
                if(role === 'Super Admin') {
                    this.$router.push({ name: 'admin-dashboard-details-time', params: params });
                } else if(role === 'Admin') {
                    this.$router.push({ name: 'client-dashboard-details-time', params: params });
                } else if(role === 'User') {
                    this.$router.push({ name: 'dashboard-details-time', params: params });
                }
            } else {
                if(role === 'Super Admin') {
                    this.$router.push({ name: 'admin-dashboard-details', params: params });
                } else if(role === 'Admin') {
                    this.$router.push({ name: 'client-dashboard-details', params: params });
                } else if(role === 'User') {
                    this.$router.push({ name: 'dashboard-details', params: params });
                }
            }

        },

        /* This function is used to redirect to the detail listing page from dashboard
        *  @param {string} role - role of the user
        *  @param {object} params - params to be passed to the route
        */
        goToDashboardAssetDetail(role, params) {
            if(role === 'Super Admin') {
                this.$router.push({ name: 'admin-dashboard-asset-details', params: params });
            } else if(role === 'Admin') {
                this.$router.push({ name: 'client-dashboard-asset-details', params: params });
            } else if(role === 'User') {
                this.$router.push({ name: 'dashboard-asset-details', params: params });
            }
        },

        // move to top
        moveUp() {
            window.scrollTo(0, 0);
        },

        googleMapLinkLatLong(lat, long) {
            let link = `https://www.google.com/maps/place/${lat},${long}/@${lat},${long},17z`;
            return link;
        },

        googleMapLinkCSV(csv) {
            let link = `https://www.google.com/maps/place/${csv}/@${csv},17z`;
            return link;
        },

        // Show Toast Message
        showToast(title, icon, variant, text) {
            this.$toast({
                component: ToastificationContent,
                position: 'top-right',
                props: {
                    title: title,
                    icon: icon,
                    variant: variant,
                    text: text,
                }
            })
        },
        // Get status value
        getStatusValue(status) {
            if (status == 'Y') {
              return 'Active'
            } else if (status == 'N') {
              return 'Inactive'
            } else if (status == 'P') {
              return 'Pending'
            }
        },

        getColorText(color) {
            if (color == 'rgb(255,0,0)') {
                return 'Jan-Mar'
            } else if (color == 'rgb(76,175,80)') {
                return 'Apr-Jun'
            } else if (color == 'rgb(0,0,255)') {
                return 'Jul-Sep'
            } else if (color == 'rgb(255,255,0)') {
                return 'Oct-Dec'
            } else if (color == 'rgb(128,128,128)') {
                return 'Other'
            }
        },

        adminBaseURL() {
            return window.BASE_URL + '/assets' || '';
        },
        actualURL() {
            return window.BASE_URL + '/' || '';
        },
        storageBaseURL(){
            return window.BASE_URL + '/storage/' || '';

        },
        preventLeadingSpace(e) {
            // only prevent the keypress if the value is blank
            if (!e.target.value) e.preventDefault();
            // otherwise, if the leading character is a space, remove all leading white-space
            else if (e.target.value[0] == " ")
                e.target.value = e.target.value.replace(/^\s*/, "");
        },
        preventNumericInput($event) {

            var keyCode = $event.keyCode ? $event.keyCode : $event.which;
            if (keyCode > 47 && keyCode < 58) {
                $event.preventDefault();
            }
        },
        onlyNumber($event) {
            let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
            if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
                $event.preventDefault();
            }
        },
        strictNumber($event) {
            let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
            if (keyCode < 48 || keyCode > 57) { // 46 is dot
                $event.preventDefault();
            }
        },

        formatDate(date, format = "-") {
            if (date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;

                return [day, month, year].join(format);
            } else {
                return "";
            }
        },
        formatMonth(date, format = "-") {

            if (date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    year = d.getFullYear();


                return [month, year].join(format);
            } else {
                return "";
            }
        },
        sorting(value) {
            this.tableFillter.sort = value;
            if (this.tableFillter.order[value] == "asc") {
                this.tableFillter.order[value] = "desc";
                this.tableFillter.direction = "desc";
            } else {
                this.tableFillter.order[value] = "asc";
                this.tableFillter.direction = "asc";
            }
            this.tableFillter.table++;
        },
        paginate(page) {
            this.tableFillter.table++;
            this.tableFillter.page = page;
        },
        changeTime(time) {
            if (time) {
                if (time.HH != "" && time.mm != "" && time.ss != "") {
                    var hour = time.HH;
                    var min = time.mm;
                    var ss = time.ss;

                    var stringTime = hour + ":" + min + ":" + ss;
                    return stringTime;
                } else {
                    return "";
                }
            } else {
                return "";
            }
        },
        // *===============================================---*
        // *--------- UI ---------------------------------------*
        // *===============================================---*

        resolveUserRoleVariant(role) {
            if (role === 'Super Admin') return 'primary'
            if (role === 'Admin') return 'warning'
            if (role === 'User') return 'success'
            if (role === 'editor') return 'info'
            if (role === 'admin') return 'danger'

            return 'primary'
        },

        resolveUserRoleIcon(role) {
            if (role === 'subscriber') return 'UserIcon'
            if (role === 'Admin') return 'SettingsIcon'
            if (role === 'User') return 'DatabaseIcon'
            if (role === 'editor') return 'Edit2Icon'
            if (role === 'admin') return 'ServerIcon'

            return 'UserIcon'
        },

        resolveUserStatusVariant(status) {
            if (status === 'P') return 'secondary'
            if (status === 'Y') return 'success'
            if (status === 'N') return 'warning'

            return 'primary'
        },

        resolveInspectionStatusVariant(status) {
            if (status === 'fail') return 'danger'
            if (status === 'pass') return 'success'
            if (status === 'maintenance_required') return 'warning'
            if (status === 'inspect') return 'secondary'

            return 'primary'
        },

        getInspectionScheduleText(value) {
            if (value === 7) return 'Weekly'
            if (value === 30) return 'Monthly'
            if (value === 90) return 'Every 3 Months'
            if (value === 'RGBY') return 'Quarterly with RGBY'
            if (value === 180) return 'Every 6 Months'
            if (value === 360) return 'Every 12 Months'
            if (value === 540) return 'Every 18 Months'
            if (value === 720) return 'Every 24 Months'
            return 'N/A'
        }
    }

}

window.roleIds = {
    admin: 1,
    client: 2,
    site_manager: 3
};
