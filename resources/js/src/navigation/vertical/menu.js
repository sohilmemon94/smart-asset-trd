import { getUserData } from '@/auth/utils'
import store from '@/store';

const userData = getUserData() ?? store.state.auth.authUserData;
let menuData = [];

// Menu set as per the user role
if (userData.role == 'Super Admin') {
  menuData = store.state.verticalMenu.superAdminMenuData;
} else if (userData.role == 'Admin') {
  if (userData.is_confined_space == 'Y') {
    store.commit("verticalMenu/UPDATE_CLIENT_ADMIN_MENU_DATA", {
      data: [{
        title: 'Confined Space',
        icon: 'InfoIcon',
        route: 'client-confined-space-list'
      }], index: 7
    });
  }

  menuData = store.state.verticalMenu.clientAdminMenuData;
} else if (userData.role == 'User') {
  menuData = store.state.verticalMenu.siteManagerMenuData;
}

export default [
  ...menuData
]
