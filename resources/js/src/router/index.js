import Vue from 'vue'
import VueRouter from 'vue-router'

// Routes
import { canNavigate } from '@/libs/acl/routeProtection'
import { isUserLoggedIn, getUserData, getHomeRouteForLoggedInUser } from '@/auth/utils'
import superAdmin from './routes/super-admin'
import clientAdmin from './routes/client-admin'
import siteManager from './routes/site-manager'
//import uiElements from './routes/ui-elements/index'
import authentication from './routes/authentication'
import miscellaneous from './routes/miscellaneous'
import print from './routes/print'
import store from '@/store'
import Http from '@/libs/axios';
//import chartsMaps from './routes/charts-maps'
//import formsTable from './routes/forms-tables'
//import others from './routes/others'

Vue.use(VueRouter)

const userData = getUserData()

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    //{ path: '/', redirect: { name: 'dashboard' } },
    ...superAdmin,
    ...clientAdmin,
    ...siteManager,
    ...authentication,
    ...miscellaneous,
    ...print,
    //...chartsMaps,
    //...formsTable,
    //...uiElements,
    //...others,
    {
      path: '*',
      redirect: 'error-404',
    },
  ],
})

router.beforeEach((to, _, next) => {
  const isLoggedIn = isUserLoggedIn()

    if(window.dashboardTimer) {
        // destroy timer
        clearInterval(window.dashboardTimer);
    }

    // Checking & Setting TimeZone Offset
    var tz = store.state.app.timeZone;
    if(tz==null || tz=='null') {
        // var offset = -(new Date().getTimezoneOffset())/60;
        var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone
        store.dispatch('app/setTimeZone', timeZone);
    } else {
    }

  if (!canNavigate(to)) {
    // Redirect to login if not logged in
    if (!isLoggedIn) return next({ name: 'auth-login' })

    // If logged in => not authorized
    return next({ name: 'misc-not-authorized' })
  }

  // Redirect if logged in
  if (to.meta.redirectIfLoggedIn && isLoggedIn) {
    next(getHomeRouteForLoggedInUser(userData ? userData.role : null))
  }

  return next()
})

// ? For splash screen
// Remove afterEach hook if you are not using splash screen
router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')
  if (appLoading) {
    appLoading.style.display = 'none'
  }
})

export default router
