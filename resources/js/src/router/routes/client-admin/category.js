export default [
    // *===============================================---*
    // *--------- CATEGORY ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/client/category/list',
      name: 'client-category-list',
      component: () => import('@/views/client-admin/category/category-list/CategoryList.vue'),
      meta: {
        pageTitle: 'Category List',
        breadcrumb: [
          {
            text: 'Category',
          },
          {
            text: 'Category List',
            active: true,
          },
        ],
      },
    },

  ]
