export default [
    {
        path: '/client/dashboard',
        name: 'client-dashboard',
        component: () => import('@/views/common/dashboard/home/Dashboard.vue'),
    },
    {
        path: '/client/dashboard/details/:apiKey',
        props: true,
        name: 'client-dashboard-details',
        component: () => import('@/views/common/dashboard/detail-listing/Index.vue'),
        meta: {
            navActiveLink: 'client-dashboard',
            // pageTitle: 'Detail Listing'
            breadcrumb: [
                {
                    text: 'Dashboard',
                    to: { name: 'client-dashboard' }
                },
                {
                    text: 'Details',
                    active: true,
                },
            ],
        },
    },
    {
        path: '/client/dashboard/details/:apiKey/:time',
        props: true,
        name: 'client-dashboard-details-time',
        component: () => import('@/views/common/dashboard/detail-listing/Index.vue'),
        meta: {
            navActiveLink: 'client-dashboard',
            // pageTitle: 'Detail Listing'
            breadcrumb: [
                {
                    text: 'Dashboard',
                    to: { name: 'client-dashboard' }
                },
                {
                    text: 'Details',
                    active: true,
                },
            ],
        },
    },
    {
        path: '/client/dashboard/asset-info/:id',
        props: true,
        name: 'client-dashboard-asset-details',
        component: () => import('@/views/common/dashboard/detail-info/Index.vue'),
        meta: {
            navActiveLink: 'client-dashboard',
            pageTitle: 'Asset Info',
            breadcrumb: [
                {
                    text: 'Dashboard',
                    to: { name: 'client-dashboard' }
                },
                {
                    text: 'Details'
                },
                {
                    text: 'Asset Info',
                    active: true,
                },
            ],
        },
    }
]
