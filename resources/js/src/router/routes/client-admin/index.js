import dashboard from './dashboard'
// import report from './report'
import site from './site'
import subSite from './sub-site'
import template from './template'
import category from './category'
import module from './module'
import user from './user'
import confinedSpace from './confined-space'
import support from './support'
import userPermission from './user-permission'

const data = [...dashboard, ...site, ...subSite, ...template, ...category, ...module, ...user, ...confinedSpace, ...support, ...userPermission]
// const data = [...dashboard, ...report, ...site, ...subSite, ...template, ...category, ...module, ...user, ...support]

export default data
