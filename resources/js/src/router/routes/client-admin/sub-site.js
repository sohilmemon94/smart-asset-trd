export default [
    // *===============================================---*
    // *--------- SUB-SITE ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/client/area/list/',
        name: 'client-sub-site-list',
        component: () =>
            import ('@/views/client-admin/area/area-list/AreaList.vue'),
        meta: {
            pageTitle: 'Area List',
            breadcrumb: [{
                    text: 'Area',
                },
                {
                    text: 'Area List',
                    active: true,
                },
            ],
        },
    },
    {
        path: '/client/area/list/:id?',
        name: 'client-site-sub-site-list',
        component: () => import('@/views/client-admin/area/area-list/AreaList.vue'),
        meta: {
          navActiveLink: 'client-sub-site-list',
          pageTitle: 'Area List',
          breadcrumb: [
            {
              text: 'Location',
              to: { name: 'client-location-list' }

            },
            {
              text: 'Area',
            },
            {
              text: 'Area List',
              active: true,
            },
          ],
        },
    },
    // {
    //     path: '/client/sub-site/list/',
    //     name: 'client-sub-site-list',
    //     component: () =>
    //         import ('@/views/client-admin/sub-site/sub-site-list/SubSiteList.vue'),
    //     meta: {
    //         pageTitle: 'Sub Site List',
    //         breadcrumb: [{
    //                 text: 'Sub Site',
    //             },
    //             {
    //                 text: 'Sub Site List',
    //                 active: true,
    //             },
    //         ],
    //     },
    // },
    // *===============================================---*
    // *--------- SUB-SITE - Category/Module Permissions ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/client/area/category-module-permission/:id?',
        name: 'client-category-module-permission',
        component: () =>
            import ('@/views/client-admin/area/area-list/CategoryModulePermission.vue'),
        meta: {
            navActiveLink: 'client-sub-site-list',
            pageTitle: 'Category/Module Permission',
            breadcrumb: [{
                    text: 'Area',
                },
                {
                    text: 'Category/Module Permission',
                    active: true,
                },
            ],
        },
    },
    // *===============================================---*
    // *--------- SUB-SITE - Users Permissions ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/client/area/users-permission/:id?',
        name: 'client-users-permission',
        component: () =>
            import ('@/views/client-admin/area/area-list/UsersPermission.vue'),
        meta: {
            navActiveLink: 'client-sub-site-list',
            pageTitle: 'Users Permission',
            breadcrumb: [{
                    text: 'Area',
                },
                {
                    text: 'Users Permission',
                    active: true,
                },
            ],
        },
    },
    // *===============================================---*
    // *--------- SUB-SITE - Go to Site ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/client/area/asset-manage/:id?',
        name: 'client-asset-manage',
        component: () =>
            import ('@/views/client-admin/area/area-list/GotoSite.vue'),
        meta: {
            navActiveLink: 'client-sub-site-list',
            pageTitle: 'Manage Asset',
            breadcrumb: [{
                    text: 'Area',
                },
                {
                    text: 'Manage Asset',
                    active: true,
                },
            ],
        },
    }
]
