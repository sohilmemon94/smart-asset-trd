export default [
  // *===============================================---*
  // *--------- TEMPLATE ---- ---------------------------------------*
  // *===============================================---*
  {
    path: '/client/template/list',
    name: 'client-template-list',
    component: () => import('@/views/client-admin/template/template-list/TemplateList.vue'),
    meta: {
      pageTitle: 'Template List',
      breadcrumb: [
        {
          text: 'Template',
        },
        {
          text: 'Template List',
          active: true,
        },
      ],
    },
  },
  // *===============================================---*
    // *--------- TEMPLATE FIELD LIST ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/client/template/list/:id?',
        name: 'client-template-field',
        component: () => import('@/views/client-admin/template/template-field-list/TemplateFieldList.vue'),
        meta: {
        navActiveLink: 'client-template-list',
        //navActiveLink: 'client-sub-site-list',
          pageTitle: 'Template Field List',
          breadcrumb: [
            {
              text: 'Template Field',
            },
            {
              text: 'Template Field List',
              active: true,
            },
          ],
        },
      },
]
