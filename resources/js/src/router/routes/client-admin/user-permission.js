export default [
    // *===============================================---*
    // *--------- USER PERMISSION ---------------------------------------*
    // *===============================================---*
    {
        path: '/client/userPermission',
        name: 'client-user-permission',
        component: () =>
            import ('@/views/client-admin/user-permission/index.vue'),
        meta: {
            pageTitle: 'User Permission List',
            breadcrumb: [{
                    text: 'User Permission',
                },
                {
                    text: 'User Permission List',
                    active: true,
                },
            ],
        },
    },
]
