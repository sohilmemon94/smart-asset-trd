export default [
    // *===============================================---*
    // *--------- USER ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/client/my-profile',
        name: 'client-admin-myProfile',
        component: () =>
            import ('@/views/client-admin/my-profile/MyProfile.vue'),
        meta: {
            pageTitle: 'My Profile',
            breadcrumb: [{
                text: 'My Profile',
                active: true,
            }, ],
        },
    },
    {
        path: '/client/user/list',
        name: 'client-user-list',
        component: () =>
            import ('@/views/client-admin/user/user-list/UserList.vue'),
        meta: {
            pageTitle: 'User List',
            breadcrumb: [{
                    text: 'User',
                },
                {
                    text: 'User List',
                    active: true,
                },
            ],
        },
    },
    {
        path: '/client/user/add',
        name: 'client-user-add',
        component: () =>
            import ('@/views/client-admin/user/user-add/create.vue'),
        meta: {
            navActiveLink: 'client-user-list',
            pageTitle: 'Add User',
            breadcrumb: [{
                    text: 'User',
                    to: { name: 'client-user-list' }
                },
                {
                    text: 'Add User',
                    //active: true,
                },
            ],
        },
    },
    {
        path: '/client/user/view/:id',
        name: 'client-user-view',
        component: () =>
            import ('@/views/client-admin/user/user-view/UserView.vue'),
        meta: {
            navActiveLink: 'client-user-list',
            pageTitle: 'User View',
            breadcrumb: [{
                    text: 'User',
                    to: { name: 'client-user-list' }
                },
                {
                    text: 'User View',
                    active: true,
                },
            ],
        },
    },
    {
        path: '/client/user/edit/:id',
        name: 'client-user-edit',
        component: () =>
            import ('@/views/client-admin/user/user-edit/UserEdit.vue'),
        meta: {
            navActiveLink: 'client-user-list',
            pageTitle: 'User Edit',
            breadcrumb: [{
                    text: 'User',
                    to: { name: 'client-user-list' }
                },
                {
                    text: 'User Edit',
                    active: true,
                },
            ],
        },
    },
]
