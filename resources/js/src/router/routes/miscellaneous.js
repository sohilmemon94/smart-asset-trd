export default [
  {
    path: '/miscellaneous/coming-soon',
    name: 'misc-coming-soon',
    component: () => import('@/views/miscellaneous/ComingSoon.vue'),
    meta: {
      layout: 'full',
    },
  },
  {
    path: '/miscellaneous/not-authorized',
    name: 'misc-not-authorized',
    component: () => import('@/views/miscellaneous/NotAuthorized.vue'),
    meta: {
      layout: 'full',
      resource: 'Auth',
    },
  },
  {
    path: '/miscellaneous/under-maintenance',
    name: 'misc-under-maintenance',
    component: () => import('@/views/miscellaneous/UnderMaintenance.vue'),
    meta: {
      layout: 'full',
    },
  },
  {
    path: '/miscellaneous/error',
    name: 'misc-error',
    component: () => import('@/views/miscellaneous/Error.vue'),
    meta: {
      layout: 'full',
    },
  },
  {
    path: '/asset-info/:asset_nfc_uuid?',
    name: 'asset-information',
    component: () => import('@/views/common/asset-information/AssetInformation.vue'),
    meta: {
        layout: 'full',
        resource: 'Auth',
      },
  },
  // *===============================================---*
  // *--------- Error ---- ---------------------------------------*
  // *===============================================---*
  {
    path: '/admin/error/list',
    name: 'admin-error-list',
    component: () => import('@/views/error/error-list/ErrorList.vue'),
    meta: {
      pageTitle: 'Error Log',
      breadcrumb: [
        {
          text: 'Error',
        },
        {
          text: 'Error List',
          active: true,
        },
      ],
    },
  },
]
