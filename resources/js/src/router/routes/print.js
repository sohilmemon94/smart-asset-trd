export default [
    {
        path: '/print-list',
        name: 'print-list',
        component: () => import('@/views/common/dashboard/detail-listing/Print.vue'),
        meta: {
            layout: 'full',
        },
    },
    {
        path: '/print-detail',
        name: 'print-detail',
        component: () => import('@/views/common/dashboard/detail-info/Print.vue'),
        meta: {
            layout: 'full',
        },
    }
]
