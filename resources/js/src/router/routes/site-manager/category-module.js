export default [
  {
    path: '/category-module/:id',
    name: 'category-module',
    props: true,
    component: () => import('@/views/site-manager/category-module/Index.vue'),
    meta: (route) => ({
      module_id: route.params.id          // <--- This is the module_id (needs to be dynamic)
    })
  },
]
