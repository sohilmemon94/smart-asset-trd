export default [
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import('@/views/common/dashboard/home/Dashboard.vue'),
  },
    {
        path: '/dashboard/:id?',
        name: 'dashboard-with-id',
        component: () => import('@/views/common/dashboard/home/Dashboard.vue'),
        meta: {
          navActiveLink: 'dashboard',
        },
    },
    {
        path: '/dashboard/details/:apiKey',
        props: true,
        name: 'dashboard-details',
        component: () => import('@/views/common/dashboard/detail-listing/Index.vue'),
        meta: {
            navActiveLink: 'dashboard',
            // pageTitle: 'Detail Listing'
            breadcrumb: [
                {
                    text: 'Dashboard',
                    to: { name: 'dashboard' }
                },
                {
                    text: 'Details',
                    active: true,
                },
            ],
        },
    },
    {
        path: '/dashboard/details/:apiKey/:time',
        props: true,
        name: 'dashboard-details-time',
        component: () => import('@/views/common/dashboard/detail-listing/Index.vue'),
        meta: {
            navActiveLink: 'dashboard',
            // pageTitle: 'Detail Listing'
            breadcrumb: [
                {
                    text: 'Dashboard',
                    to: { name: 'dashboard' }
                },
                {
                    text: 'Details',
                    active: true,
                },
            ],
        },
    },
    {
        path: '/dashboard/asset-info/:id',
        props: true,
        name: 'dashboard-asset-details',
        component: () => import('@/views/common/dashboard/detail-info/Index.vue'),
        meta: {
            navActiveLink: 'dashboard',
            pageTitle: 'Asset Info',
            breadcrumb: [
                {
                    text: 'Dashboard',
                    to: { name: 'dashboard' }
                },
                {
                    text: 'Details'
                },
                {
                    text: 'Asset Info',
                    active: true,
                },
            ],
        },
    }
]
