import dashboard from './dashboard'
// import report from './report'
import user from './user'
import categoryModule from './category-module'

const data = [...dashboard, ...user, ...categoryModule]
// const data = [...dashboard, ...report, ...user, ...categoryModule]

export default data
