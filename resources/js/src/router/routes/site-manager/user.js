export default [
    // *===============================================---*
    // *--------- USER ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/my-profile',
      name: 'site-manager-myProfile',
      component: () => import('@/views/site-manager/my-profile/MyProfile.vue'),
      meta: {
        pageTitle: 'My Profile',
        breadcrumb: [
          {
            text: 'My Profile',
            active: true,
          },
        ],
      },
    }
]