export default [
  // *===============================================---*
  // *--------- Category ---- ---------------------------------------*
  // *===============================================---*
  {
    path: '/admin/category/list',
    name: 'admin-category-list',
    component: () => import('@/views/super-admin/category/category-list/CategoryList.vue'),
    meta: {
      pageTitle: 'Category List',
      breadcrumb: [
        {
          text: 'Category',
        },
        {
          text: 'Category List',
          active: true,
        },
      ],
    },
  },
  // *===============================================---*
  // *--------- CATEGORY MODULE LIST ---- ---------------------------------------*
  // *===============================================---*
  // {
  //   path: '/admin/category/list/:id?',
  //   name: 'admin-category-module',
  //   component: () => import('@/views/super-admin/category/category-module-list/CategoryModuleList.vue'),
  //   meta: {
  //     navActiveLink: 'admin-category-list',
  //     pageTitle: 'Category Module List',
  //     breadcrumb: [
  //       {
  //         text: 'Category Module',
  //       },
  //       {
  //         text: 'Category Module List',
  //         active: true,
  //       },
  //     ],
  //   },
  // },
]