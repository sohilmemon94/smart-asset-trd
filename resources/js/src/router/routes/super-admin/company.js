export default[
    // *===============================================---*
    // *--------- COMPANY ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/admin/company/list',
        name: 'admin-company-list',
        component: () => import('@/views/super-admin/company/list/List.vue'),
        meta: {
          pageTitle: 'Client List',
          breadcrumb: [
            {
              text: 'Client',
            },
            {
              text: 'Client List',
              active: true,
            },
          ],
        },
      },
      {
        path: '/admin/company/add',
        name: 'admin-company-add',
        component: () => import('@/views/super-admin/company/add/Create.vue'),
        meta: {
          navActiveLink: 'admin-company-list',
          pageTitle: 'Add Client',
          breadcrumb: [
            {
              text: 'Client',
              to : { name: 'admin-company-list'}
            },
            {
              text: 'Add Client',
              active: true,
            },
          ],
        },
      },
      {
        path: '/admin/company/edit/:id',
        name: 'admin-company-edit',
        component: () => import('@/views/super-admin/company/edit/Edit.vue'),
        meta: {
          navActiveLink: 'admin-company-list',
          pageTitle: 'Client Edit',
          breadcrumb: [
            {
              text: 'Client',
              to : { name: 'admin-company-list'}
            },
            {
              text: 'Client Edit',
              active: true,
            },
          ],
        },
      },
      {
        path: '/admin/company/view/:id',
        name: 'admin-company-view',
        component: () => import('@/views/super-admin/company/view/View.vue'),
        meta: {
          navActiveLink: 'admin-company-list',
          pageTitle: 'Client View',
          breadcrumb: [
            {
              text: 'Company',
              to : { name: 'admin-company-list'}
            },
            {
              text: 'Client View',
              active: true,
            },
          ],
        },
      },

]
