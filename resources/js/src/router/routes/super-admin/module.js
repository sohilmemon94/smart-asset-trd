export default [
    // *===============================================---*
    // *--------- MODULE ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/admin/module/list',
      name: 'admin-module-list',
      component: () => import('@/views/super-admin/module/module-list/ModuleList.vue'),
      meta: {
        pageTitle: 'Module List',
        breadcrumb: [
          {
            text: 'Module',
          },
          {
            text: 'Module List',
            active: true,
          },
        ],
      },
    },
    // *===============================================---*
    // *--------- MODULE FIELD LIST ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/admin/module/list/:id?',
      name: 'admin-module-field',
      component: () => import('@/views/super-admin/module/module-field-list/ModuleFieldList.vue'),
      meta: {
        navActiveLink: 'admin-module-list',
        pageTitle: 'Module Field List',
        breadcrumb: [
          {
            text: 'Module Field',
          },
          {
            text: 'Module Field List',
            active: true,
          },
        ],
      },
    },
    // *===============================================---*
    // *--------- MODULE QUESTION LIST ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/admin/module/:id/question',
      name: 'admin-module-question',
      component: () => import('@/views/super-admin/module/module-question-list/ModuleQuestionList.vue'),
      meta: {
        navActiveLink: 'admin-module-list',
        pageTitle: 'Module Question List',
        breadcrumb: [
          {
            text: 'Module Question'
          },
          {
            text: 'Module Question List',
            active: true,
          },
        ],
      },
    },
    {
      path: '/admin/module/:id/question/add',
      name: 'admin-module-question-add',
      component: () => import('@/views/super-admin/module/module-question-add/ModuleQuestionAdd.vue'),
      meta: {
        navActiveLink: 'admin-module-list',
        pageTitle: 'Add Module Question',
        breadcrumb: [
          {
            text: 'Module Question',
            to : { name: 'admin-module-list'}
          },
          {
            text: 'Add Module Question',
            active: true,
          },
        ],
      },
    },
    {
      path: '/admin/module/:id/question/edit/:msq_id',
      name: 'admin-module-question-edit',
      component: () => import('@/views/super-admin/module/module-question-edit/ModuleQuestionEdit.vue'),
      meta: {
        navActiveLink: 'admin-module-list',
        pageTitle: 'Module Question Edit',
        breadcrumb: [
          {
            text: 'Module Question',
            to : { name: 'admin-module-list'}
          },
          {
            text: 'Module Question Edit',
            active: true,
          },
        ],
      },
    },
    {
      path: '/admin/category/:category_id/module/list/',
      name: 'admin-category-module-list',
      component: () => import('@/views/super-admin/module/module-list/ModuleList.vue'),
      meta: {
        navActiveLink: 'admin-module-list',
        pageTitle: 'Module List',
        breadcrumb: [
          {
            text: 'Category',
            to : {name: 'admin-category-list'}
          },
          {
            text: 'Module',
            to : {name: 'admin-module-list'}
          },
          {
            text: 'Module List',
            active: true,
          },
        ],
      },
    },
    // *===============================================---*
    // *--------- MODULE QUESTION LIST ---- ---------------------------------------*
    // *===============================================---*
    {
        path: '/admin/question-answer',
        name: 'admin-question-answer',
        component: () => import('@/views/super-admin/module/answer-list/AnswerList.vue'),
        meta: {
          //navActiveLink: 'admin-module-list',
          pageTitle: 'Answer List',
          breadcrumb: [
            {
              text: 'Answer'
            },
            {
              text: 'Answer List',
              active: true,
            },
          ],
        },
      },
  ]
