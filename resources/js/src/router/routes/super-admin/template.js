export default [
    // *===============================================---*
    // *--------- TEMPLATE ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/admin/template/list',
      name: 'admin-template-list',
      component: () => import('@/views/super-admin/template/template-list/TemplateList.vue'),
      meta: {
        pageTitle: 'Template List',
        breadcrumb: [
          {
            text: 'Template',
          },
          {
            text: 'Template List',
            active: true,
          },
        ],
      },
    },
    // *===============================================---*
    // *--------- TEMPLATE FIELD LIST ---- ---------------------------------------*
    // *===============================================---*
    {
      path: '/admin/template/list/:id?',
      name: 'admin-template-field',
      component: () => import('@/views/super-admin/template/template-field-list/TemplateFieldList.vue'),
      meta: {
        navActiveLink: 'admin-template-list',
        pageTitle: 'Template Field List',
        breadcrumb: [
          {
            text: 'Template Field',
          },
          {
            text: 'Template Field List',
            active: true,
          },
        ],
      },
    },
  ]