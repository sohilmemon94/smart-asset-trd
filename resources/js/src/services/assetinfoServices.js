import Http from '@/libs/axios';

class AssetInfo {

    async getAssetInfo(payload, config){
        //console.log(payload);
        return await Http.get('asset-info/' + payload, config).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async submitAssetReport(payload, config) {
        return await Http
            .post('/assetReportIssue', payload, config)
            .then(response => {
                if (response.status) {
                    return response.data;
                } else {
                    return {};
                }
            }).catch((error) => {
                return Promise.reject(error.response);
            });
    }
}

export default new AssetInfo();
