import Http from '@/libs/axios';

class Auth {

    // login from api and store user token in to local storage and vuex
    async login(payload){
        return await Http.post('auth/login', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    // logout from api and destroy token from local storage and vuex.
    async logout(){
        return await Http.get('auth/logout').then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    // My profile from api
    async myProfile(){
        return await Http.get('auth/myProfile').then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    // My profile from api
    async createPasswordReset(payload) {
        return await Http.post('auth/createPasswordReset', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    // Check password token exist and not expired
    async findTokenPasswordReset(payload) {
        return await Http.post('auth/findTokenPasswordReset', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    // Reset password and send success email send to user
    async resetPassword(payload) {
        return await Http.post('auth/resetPassword', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    // Set new password and send success email send to user
    async setPassword(payload) {
        return await Http.post('auth/setPassword', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    
}

export default new Auth();