import Http from '@/libs/axios';

class CategoryServices {

    // Get Client Panel Site in system via API call
    async getAllCategory(filleter) {
        return await Http.get('/category', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get all category option in system via API call
    async getAllCategoryOption(filleter) {
        return await Http.get('/getAllCategory').then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get all default category list in system via API call
    async getDefaultCategoryList() {
        return await Http.get('/getDefaultCategoryList').then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Add Client Panel Site in system via API call
    async saveCategory(payload) {
        return await Http
            .post('/category', payload)
            .then(response => {
                if (response.status) {
                    return response.data;
                } else {
                    return {};
                }
            }).catch((error) => {
                return Promise.reject(error.response);
            });
    }

    // Delete Client Panel Site API call
    async deleteCategoryData(payload) {
        return await Http.post(`/deleteCategory`, { id: payload }).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response;
        });
    }

    // Get Client Panel Site in system via API call
    async getCategoryData(payload) {
        return await Http.get(`category/${payload}/edit`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update Client Panel Site in system via API call
    async updateCategoryData(payload) {
        return await Http.put(`category/${payload.id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update category in system via API call
    async updateCategory(payload) {
        return await Http.put(`category/${payload.category_id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Change Category status API call
    async changeStatus(payload) {
        return await Http.post(`changeCategoryStatus`, payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

}

export default new CategoryServices();
