import Http from '@/libs/axios';

class CompanyServices{

    // Get Company in system via API call
    async getAllCompany(filleter) {
        return await Http.get('company', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Add Super Admin Panel company in system via API call
    async saveCompany(payload) {
        return await Http
            .post('/storeCompany', payload)
            .then(response => {
                if (response.status) {
                    return response.data;
                } else {
                    return {};
                }
            }).catch((error) => {
                return Promise.reject(error.response);
            });
    }

    // Get Module in system via API call
    async getCompanyData(payload) {
        return await Http.get(`company/${payload}/edit`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get all default company list in system via API call
    async getDefaultCompanyList() {
        return await Http.get('/getDefaultCompanyList').then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update company in system via API call
    async updateCompanyData(payload) {
        return await Http.post(`company/${payload.id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Delete company API call
    async deleteCompanyData(payload) {
        return await Http.post(`deleteCompany`, { id: payload }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Upload company avatar API call
    async uploadCompanyAvatar(companyAvatarData, payload, config) {
        //console.log(payload);
        return await Http.post(`uploadCompanyAvatar/${companyAvatarData.id}`, payload, config).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Remove company avatar API call
    async removeCompanyAvatar(payload) {
        //console.log(payload);
        return await Http.post(`removeCompanyAvatar`, { id: payload }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Change Category status API call
    async changeStatus(payload) {
        return await Http.post(`changeCompanyStatus`, payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }
}

export default new CompanyServices();
