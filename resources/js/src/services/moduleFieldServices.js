import Http from '@/libs/axios';

class ModuleFieldServices {

    // Get all input field in system via API call
    async getAllInputField() {
        return await Http.get('getAllInputField').then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get Client Panel Template in system via API call
    async getModuleFieldList(filleter) {
        return await Http.get('moduleField', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Save module field in system via API call
    async saveModuleField(payload) {
        return await Http
            .post('/moduleField', payload)
            .then(response => {
                if (response.status) {
                    return response.data;
                } else {
                    return {};
                }
            }).catch((error) => {
                return Promise.reject(error.response);
            });
    }

    // Get Client Panel Template in system via API call
    async getModuleFieldData(payload) {
        return await Http.get(`moduleField/${payload}/edit`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update Template Field in system via API call
    async updateModuleField(payload) {
        return await Http.put(`moduleField/${payload.mf_id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Change Template Field status API call
    async changeModuleFieldStatus(payload) {
        return await Http.post(`changeModuleFieldStatus`, payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    // Delete Template field API call
    async deleteModuleField(payload) {
        return await Http.post(`/deleteModuleField`, { id: payload }).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response;
        });
    }
    // Save Template field for module API call
    async saveTemplateFieldForModule(payload) {
        return await Http.post(`/saveTemplateFieldForModule`, payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response;
        });
    }


    
}

export default new ModuleFieldServices();
