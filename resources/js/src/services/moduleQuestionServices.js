import Http from '@/libs/axios';

class ModuleQuestionServices {

    // Get Client Panel in system via API call
    async getModuleQuestionList(filleter) {
        return await Http.get('moduleQuestion', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Save module Question in system via API call
    async saveModuleQuestion(payload) {
        return await Http
            .post('/moduleQuestion', payload)
            .then(response => {
                if (response.status) {
                    return response.data;
                } else {
                    return {};
                }
            }).catch((error) => {
                return Promise.reject(error.response);
            });
    }

    // Get Client Panel in system via API call
    async getModuleQuestionData(payload) {
        return await Http.get(`moduleQuestion/${payload}/edit`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update Question in system via API call
    async updateModuleQuestion(payload) {
        return await Http.put(`moduleQuestion/${payload.msq_id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Change Question status API call
    async changeModuleQuestionStatus(payload) {
        return await Http.post(`changeModuleQuestionStatus`, payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    // Delete Question API call
    async deleteModuleQuestion(payload) {
        return await Http.post(`/deleteModuleQuestion`, { id: payload }).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response;
        });
    }

    
}

export default new ModuleQuestionServices();
