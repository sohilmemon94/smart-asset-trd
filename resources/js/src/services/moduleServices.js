import Http from '@/libs/axios';

class ModuleServices {

    // Get Module in system via API call
    async getAllModule(filleter) {
        return await Http.get('/module', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Add Module in system via API call
    async saveModule(payload, config) {
        return await Http
            .post('/modules', payload, config)
            .then(response => {
                if (response.status) {
                    return response.data;
                } else {
                    return {};
                }
            }).catch((error) => {
                return Promise.reject(error.response.data);
            });
    }

    // Delete Module API call
    async deleteModuleData(payload) {
        return await Http.post(`/deleteModule`, { id: payload }).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response;
        });
    }

    // Get Module in system via API call
    async getModuleData(payload) {
        return await Http.get(`module/${payload}/edit`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update Module in system via API call
    async updateModuleData(id, formData, config) {
        return await Http.post(`/module/${id}`, formData, config).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update Module Icon in system via API call
    async uploadModuleIcon(payload, config) {
        return await Http.post(`/uploadModuleIcon`, payload, config).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update Module in system via API call
    async removeModuleIcon(payload) {
        return await Http.post(`/removeModuleIcon`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Change Module status API call
    async changeModuleStatus(payload) {
        return await Http.post(`changeModuleStatus`, payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

}

export default new ModuleServices();