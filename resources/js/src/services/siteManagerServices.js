import Http from '@/libs/axios';

class siteManagerServices {

    async getFileSizeLimit(){
        return await Http.get('site-panel/file-size-limit').then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async getSiteManagerDynamicMenu(payload){
        return await Http.post('site-panel/vertical-menu', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async getClientPanelCategory(payload){
        return await Http.post('site-panel/category-list', {sub_site_id : payload.sub_site_id, user_id: payload.user_id}).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async getClientPanelModule(payload){
        //console.log(payload);
        return await Http.post('site-panel/module-list', {sub_site_id : payload.sub_site_id, category_id: payload.category_id, user_id: payload.user_id, asset_id: payload.asset_id}).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async getAssetsList(payload) {
        return await Http.get('site-panel/assets', {params: payload}).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async getClientAssetsList(payload) {
        return await Http.get('site-panel/assets/client', {params: payload}).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async storeAsset(payload) {
        return await Http.post('site-panel/assets/store', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async duplicateAsset(payload) {
        return await Http.post('site-panel/assets/duplicate', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async moveAsset(payload) {
        return await Http.post('site-panel/assets/moveAsset', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async getSitesAndSubSitesList() {
        return await Http.get('site-panel/sites-and-sub-sites-list').then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async getDocumentsList(payload) {
        return await Http.get('site-panel/assets/documents', {params: payload}).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async deleteBatchAssets(payload) {
        return await Http.post('site-panel/assets/asset-batch-delete', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }


    async getAssetDetails(payload) {
        return await Http.get('site-panel/assets/details/'+payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async saveAssetDocument(payload) {
        return await Http.post('site-panel/assets/save-document', payload).then((response) => {
            return response;
        }).catch((error) => {
            return error;
        });
    }

    async getAssetDocument(payload) {
        return await Http.get('site-panel/assets/document/'+payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async deleteBatchDocuments(payload) {
        return await Http.post('site-panel/assets/document-batch-delete', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async getFieldsList(payload) {
        return await Http.get('site-panel/fields-list', {params: payload}).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async updateAsset(payload) {
        return await Http.post('site-panel/assets/update', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async fetchAssetActivity(payload) {
        return await Http.get('site-panel/assets/activity', {params: payload}).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async getAssetInspectionList(payload) {
        return await Http.get('site-panel/assets/inspection', {params: payload}).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async deleteBatchInspections(payload) {
        return await Http.post('site-panel/assets/inspection-batch-delete', payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async moduleInfo(payload) {
        return await Http.get('site-panel/module-info/'+payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    async getSubSiteOption(payload) {
        return await Http.get('site-panel/all-sub-sites',  {params: payload}).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }



}

export default new siteManagerServices();
