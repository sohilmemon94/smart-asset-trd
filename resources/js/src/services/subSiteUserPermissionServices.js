import Http from '../libs/axios';


class SubSiteUserPermissionServices {

    // Update Client Panel subSite user permission in system via API call
    async updateSubSiteUser(payload) {
        return await Http.post(`updateSubSiteUser/${payload.id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update Client Panel subSite user permission in system via API call
    async saveSubSiteUserCat(payload) {
        return await Http.post(`saveSubSiteUser/${payload.id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get Client Panel Category/Module Selected List in system via API call
    async getSelectedCatMod(filleter) {
        return await Http.get(`/selectedCategory`, {
            params: filleter
        }).then((response) => {
         //   console.log(response);
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Delete Client User Management Panel subSite API call
    async deleteSubSiteUserData(payload) {
        return await Http.post(`/deleteSubSiteUser/${payload.id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response);
        });
    }
}

export default new SubSiteUserPermissionServices();
