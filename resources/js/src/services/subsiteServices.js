import Http from '../libs/axios';

class SubSiteServices {

    // Get Client Panel subSite in system via API call
    async getAllSubSite(filleter) {
        //console.warn(filleter);
        // console.log(filleter.id);
        return await Http.get(`/subsite`, {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Add Client Panel subSite in system via API call
    async storeSubSite(payload) {
        return await Http
            .post('/subsite', payload)
            .then(response => {
                if (response.status) {
                    return response.data;
                } else {
                    return {};
                }
            }).catch((error) => {
                return Promise.reject(error.response);
            });
    }

    // Delete Client Panel subSite API call
    async deleteSubSiteData(payload) {
        return await Http.post(`/deleteSubsite`, { id: payload }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response);
        });
    }

    // Get Client Panel subSite in system via API call
    async getSubSiteData(payload) {
        return await Http.get(`subsite/${payload}/edit`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update Client Panel subSite in system via API call
    async updateSubSiteData(payload) {
        return await Http.put(`subsite/${payload.id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Change Sub Site status API call
    async changeSubSiteStatus(payload) {
        return await Http.post(`changeSubSiteStatus`, payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    // Get Client Panel subSite API call
    async getSiteOnSubSite(payload) {
        return await Http.post(`/getSiteOnSubSite`, { id: payload }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response);
        });
    }

    // Get Client Panel subSite API call
    async getUserOnSubSite(payload) {
        return await Http.post(`/getUserOnSubSite`, { site_id : payload.site_id, sub_site_id:  payload.sub_site_id }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response);
        });
    }

}

export default new SubSiteServices();
