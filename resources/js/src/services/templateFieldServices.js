import Http from '@/libs/axios';

class TemplateFieldServices {

    // Get all input field in system via API call
    async getAllInputField() {
        return await Http.get('getAllInputField').then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get Client Panel Template in system via API call
    async getTemplateFieldList(filleter) {
        return await Http.get('templateField', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Save Template field in system via API call
    async saveTemplateField(payload) {
        return await Http
            .post('/templateField', payload)
            .then(response => {
                if (response.status) {
                    return response.data;
                } else {
                    return {};
                }
            }).catch((error) => {
                return Promise.reject(error.response);
            });
    }

    // Get Client Panel Template in system via API call
    async getTemplateFieldData(payload) {
        return await Http.get(`templateField/${payload}/edit`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update Template Field in system via API call
    async updateTemplateField(payload) {
        return await Http.put(`templateField/${payload.template_field_id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Change Template Field status API call
    async changeTemplateFieldStatus(payload) {
        return await Http.post(`changeTemplateFieldStatus`, payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }

    // Delete Template field API call
    async deleteTemplateField(payload) {
        return await Http.post(`/deleteTemplateField`, { id: payload }).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response;
        });
    }

    // Get Template All Fields
    // Get Client Panel Template in system via API call
    async getTemplateAllFields(payload) {
        return await Http.get('getTemplateAllFields', {
            params: payload
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }
}

export default new TemplateFieldServices();
