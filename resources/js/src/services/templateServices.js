import Http from '@/libs/axios';

class TemplateServices {

    // Get Template in system via API call
    async getAllTemplate(filleter) {
        return await Http.get('/template', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get User Template in system via API call
    async getUserTemplateOptions() {
        return await Http.get('/getUserTemplateOptions').then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Add Template in system via API call
    async saveTemplate(payload) {
        return await Http
            .post('/template', payload)
            .then(response => {
                if (response.status) {
                    return response.data;
                } else {
                    return {};
                }
            }).catch((error) => {
                return Promise.reject(error.response);
            });
    }

    // Delete Template API call
    async deleteTemplateData(payload) {
        return await Http.post(`/deleteTemplate`, { id: payload }).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response;
        });
    }

    // Duplicate Template API call
    async duplicateTemplateData(payload) {
        return await Http.post(`/duplicateTemplate`, { id: payload }).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response;
        });
    }

    // Get Template in system via API call
    async getTemplateData(payload) {
        return await Http.get(`template/${payload}/edit`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update Template in system via API call
    async updateTemplateData(payload) {
        return await Http.put(`template/${payload.template_id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Change template status API call
    async changeTemplateStatus(payload) {
        return await Http.post(`changeTemplateStatus`, payload).then((response) => {
            return response.data;
        }).catch((error) => {
            return error.response.data;
        });
    }
    
}

export default new TemplateServices();
