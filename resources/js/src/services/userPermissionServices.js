import Http from '@/libs/axios';

class UserPermissionServices {
    // Get all user from company in system via API call
    async getCompanyAllUser(filleter) {
        return await Http.get('getCompanyAllUser').then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Save User wise Category/Module Permission data in database
    async saveUserPermission(payload) {
        return await Http.post(`saveUserPermission`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

}

export default new UserPermissionServices();
