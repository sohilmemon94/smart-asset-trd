import Http from '@/libs/axios';

class UserServices {
    // Get admin user in system via API call
    async getAllAdminUser(filleter) {
        return await Http.get('getAllAdminUser', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get client user in system via API call
    async getAllClientUser(filleter) {
        return await Http.get('getAllClientUser', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get site user in system via API call
    async getAllSiteUser(filleter) {
        return await Http.get('getAllSiteUser', {
            params: filleter
        }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get admin client with company in system via API call
    async getUserWithCompanyData(payload) {
        return await Http.get(`editCompanyWithUserData/${payload}`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }


    // Get admin user in system via API call
    async getUserData(payload) {
        return await Http.get(`editUserData/${payload}`).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Get all role in system via API call
    async getAllRole() {
        return await Http.get('getAllRole').then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
           return Promise.reject(error.response.data);
        });
    }

    // Add user in system via API call
    async storeNewUser(payload, config) {
        return await Http.post('storeNewUser', payload, config).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
           return Promise.reject(error.response.data);
        });
    }

    // Update user in system via API call
    async updateUserData(payload) {
        return await Http.post(`updateUserData/${payload.id}`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Update user update role in system via API call
    async updateUserToAdmin(payload) {
        return await Http.post(`makeUserToAdmin`, { id: payload }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Upload user avatar API call
    async uploadUserAvatar(userAvatarData, payload, config) {
        //console.log(payload);
        return await Http.post(`uploadUserAvatar/${userAvatarData.id}`, payload, config).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Remove user avatar API call
    async removeUserAvatar(payload) {
        //console.log(payload);
        return await Http.post(`removeUserAvatar`, { id: payload }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Delete user API call
    async deleteUserData(payload) {
        return await Http.post(`deleteUser`, { id: payload }).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

    // Change user status API call
    async changeStatus(payload) {
        return await Http.post(`changeUserStatus`, payload).then((response) => {
            if (response.status) {
                return response.data;
            } else {
                return {};
            }
        }).catch((error) => {
            return Promise.reject(error.response.data);
        });
    }

}

export default new UserServices();
