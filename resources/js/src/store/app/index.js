import { $themeConfig, $themeBreakpoints } from '@themeConfig'
import AppServices from '@/services/appServices';

export default {
  namespaced: true,
  state: {
    appName: (localStorage.getItem("userData") ? ((JSON.parse(localStorage.getItem("userData")).client_company_name != "") ? JSON.parse(localStorage.getItem("userData")).client_company_name : $themeConfig.app.appName) : $themeConfig.app.appName),
    appLogoImage: (localStorage.getItem("userData") ? ((JSON.parse(localStorage.getItem("userData")).client_company_logo != null && JSON.parse(localStorage.getItem("userData")).client_company_logo != '') ? JSON.parse(localStorage.getItem("userData")).client_company_logo : $themeConfig.app.appLogoImage) : $themeConfig.app.appLogoImage),
    windowWidth: 0,
    shallShowOverlay: false,
    openRequestCount: 0,
    errors: [],
    errorList: {
      errors: {
        data: [],
        to: 0,
        from: 0
      }
    },
    loaderFlag: true,         // flag for the loader for dashboard.
    timeZone: null
  },
  getters: {
    appName: state => {
      const { appName } = state
      return appName;
    },
    appLogoImage: state => {
      const { appLogoImage } = state
      return appLogoImage;
    },
    currentBreakPoint: state => {
      const { windowWidth } = state
      if (windowWidth >= $themeBreakpoints.xl) return 'xl'
      if (windowWidth >= $themeBreakpoints.lg) return 'lg'
      if (windowWidth >= $themeBreakpoints.md) return 'md'
      if (windowWidth >= $themeBreakpoints.sm) return 'sm'
      return 'xs'
    },
    openRequestCount: state => {
      const { openRequestCount } = state
      return openRequestCount;
    },
    getError: state => {
      const { errors } = state
      return errors;
    },
    errorList: state => {
      const { errorList } = state
      return errorList;
    },
    loaderFlag: state => {
        const { loaderFlag } = state
        return loaderFlag;
    },
    timeZone: state => {
        const { timeZone } = state
        return timeZone;
    }
  },
  mutations: {
    UPDATE_APP_NAME(state, val) {
      state.appName = val
    },
    UPDATE_APP_LOGO_IMAGE(state, val) {
      state.appLogoImage = val
    },
    UPDATE_WINDOW_WIDTH(state, val) {
      state.windowWidth = val
    },
    TOGGLE_OVERLAY(state, val) {
      state.shallShowOverlay = val !== undefined ? val : !state.shallShowOverlay
    },
    UPDATE_OPEN_REQUEST_COUNT(state, val) {
      state.openRequestCount = val
    },
    UPDATE_ERRORS(state, val) {
      state.errors = val
    },
    UPDATE_ERROR_LIST(state, val) {
      state.errorList = val
    },
    UPDATE_LOADER_FLAG(state, val) {
        state.loaderFlag = val
    },
    UPDATE_TIME_ZONE(state, val) {
        state.timeZone = val
    }
  },
  actions: {
    clearError({ commit }) {
      commit('UPDATE_ERRORS', []);
    },
    clearGlobalConfig({ commit }) {
      commit('UPDATE_APP_NAME', $themeConfig.app.appName);
      commit('UPDATE_APP_LOGO_IMAGE', $themeConfig.app.appLogoImage);
    },
    // Increment open request count
    openRequestIncrementCount({ commit, state }) {
      commit('UPDATE_OPEN_REQUEST_COUNT', state.openRequestCount + 1);
    },
    // Decrement open request count
    openRequestDecrementCount({ commit, state }) {
        commit('UPDATE_OPEN_REQUEST_COUNT', state.openRequestCount - 1);
    },
    // Get all client user data in database
    getAllError({ commit }, payload) {
      return AppServices.getAllError(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_ERROR_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          commit('UPDATE_ERROR_LIST', {});
          return Promise.reject(error);
        }
      );
    },
    setLoaderFlag({commit}, payload) {
        commit('UPDATE_FLAG', payload);
    },
    setTimeZone({commit}, payload) {
        commit('UPDATE_TIME_ZONE', payload);
    }
  },
}
