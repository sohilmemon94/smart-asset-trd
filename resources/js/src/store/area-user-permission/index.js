import subSiteUserPermissionServices from '../../services/subSiteUserPermissionServices';


export default {
    namespaced: true,
    state :{
        templateName: '',
        subSiteUserData: {
            sub_site_name: '',
            sub_site_uuid: '',
            sub_site_description: '',
            sub_site_status: true,
            created_by: '',
            site_id: '',
            user_id: ''
          },
    },
    mutations: {
        UPDATE_SUB_SITE_USER_DATA(state, payload) {
            state.subSiteUserData = payload;
          },
          UPDATE_TEMPLATE_NAME(state, payload) {

            state.templateName = payload;
          },
    },
    actions: {
        // update subSite data in database
        updateSubSiteUserPermission({ commit, dispatch }, payload) {
            return subSiteUserPermissionServices.updateSubSiteUser(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {

                    }
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },

        // Save User wise Category/Module Permission data in database
        saveSubSiteUserPermission({ commit, dispatch }, payload) {
            return subSiteUserPermissionServices.saveSubSiteUserCat(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {

                    }
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },

        // Get Selected subSite Category/Module data
        getSelectedCategoryModule({ commit }, payload) {
            return subSiteUserPermissionServices.getSelectedCatMod(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {
                        commit('UPDATE_TEMPLATE_NAME', response.data.subSites.sub_site_name);

                      }
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },

         // Delete subSite in database
         deleteSubSiteUser({ commit, dispatch }, payload) {
            return subSiteUserPermissionServices.deleteSubSiteUserData(payload).then(
            response => {
                return Promise.resolve(response);
            },
            error => {
                return Promise.reject(error);
            }
            );
        },
    },
    getters: {

        templateName(state) {
            return state.templateName;
          },
    },
}
