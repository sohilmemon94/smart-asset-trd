import CategoryModuleServices from '../../services/categorymoduleServices';


export default {
    namespaced: true,
    state: {
        templateName: '',
        categoryModuleList: {
            categoryWithModules: {
                data: []
            }
        },
        CategoryModule: {
            category_name: '',
            category_desc: '',
            category_status: ''
        },
    },
    mutations: {

        UPDATE_CATEGORY_MODULE_LIST(state, val) {
            state.categoryModuleList = val
        },
        UPDATE_TEMPLATE_NAME(state, payload) {

            state.templateName = payload;
          },
    },
    actions: {
        // Get all Category/Module data in database
        getAllCategoryModule({ commit }, payload) {
            //console.log(payload);
            return CategoryModuleServices.getAllCategoryModule(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {
                        commit('UPDATE_CATEGORY_MODULE_LIST', response.data);
                        commit('UPDATE_TEMPLATE_NAME', response.data.subSites.sub_site_name);
                    }
                    return Promise.resolve(response);
                },
                error => {
                    //commit('UPDATE_CATEGORY_MODULE_LIST', {});
                    return Promise.reject(error);
                }
            );
        },

        // Category/Module Permission data in database
        updateCategory({ commit, dispatch }, payload) {
            if (payload.id) {
            return CategoryModuleServices.saveCategoryModulePermission(payload).then(
                response => {
                return Promise.resolve(response);
                },
                error => {
                return Promise.reject(error);
                }
            );
            }else {
            return CategoryServices.updateCategory(payload).then(
                response => {
                return Promise.resolve(response);
                },
                error => {
                return Promise.reject(error);
                }
            );
            }

        },
        // Store site data
        // saveSite({ commit }, payload) {
        //     return SiteServices.saveSite(payload).then(
        //         response => {
        //             commit('UPDATE_STORE_SITE', {
        //                 site_name: '',
        //                 site_uuid: '',
        //                 site_description: '',
        //                 site_status: '',
        //                 created_by: ''
        //             });
        //             return Promise.resolve(response);
        //         },
        //         error => {
        //             return Promise.reject(error);
        //         }
        //     )
        // },
        // Get specific site data
        // getSiteData({ commit }, payload) {
        //     return SiteServices.getSiteData(payload).then(
        //         response => {

        //             return Promise.resolve(response);
        //         },
        //         error => {
        //             return Promise.reject(error);
        //         }
        //     );
        // },

        // update user data in database
        // updateSite({ commit, dispatch }, payload) {
        //     return SiteServices.updateSiteData(payload).then(
        //         response => {
        //             if (response.status == true || response.status == 200) {

        //             }
        //             return Promise.resolve(response);
        //         },
        //         error => {
        //             return Promise.reject(error);
        //         }
        //     );
        // },

    },
    getters: {
        categoryModuleList: state => {
            const { categoryModuleList } = state
            return categoryModuleList;
        },
        templateName(state) {
            return state.templateName;
          },
    },

}
