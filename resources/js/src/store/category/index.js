import CategoryServices from '@/services/categoryServices';


export default {
  namespaced: true,
  state: {
    categoryList: {
      categories: {
        data: [],
        to: 0,
        from: 0
      }
    },
    userCategoryOptions : [],
    defaultCategoryList : [],
    categories: [],
    category: {
      category_id: '',
      category_name: '',
      category_desc: '',
      category_is_default: '',
      category_status: 'Y',
      created_at: '',
      updated_at: '',
    },

    categoryData: {},
    userListOverlay: false,
    siteListOverlay: false,
    editMode: false,
    updateId: null,
    modalButtonName: 'Add',
    viewMode: false,
  },
  mutations: {
    UPDATE_USER_CATEGORY_OPTIONS(state, payload) {
      state.userCategoryOptions = payload;
    },
    UPDATE_DEFAULT_CATEGORY_LIST(state, payload) {
      state.defaultCategoryList = payload;
    },
    UPDATE_CATEGORY_LIST(state, val) {
      state.categoryList = val
    },
    UPDATE_CATEGORY_DATA(state, val) {
      state.categoryData = val
    },
    UPDATE_CATEGORY(state, payload) {
      let category_status;
      if (payload.category_status == 'Y' || payload.category_status == true) {
        category_status = true;
      } else if (payload.category_status == 'N') {
        category_status = false;
      }
      state.category.category_id = payload.category_id;
      state.category.category_name = payload.category_name;
      state.category.category_desc = payload.category_desc;
      state.category.category_status = category_status;
      state.category.created_by = payload.created_by;
      state.category.updated_at = payload.updated_at;
    },
    UPDATE_MODAL_BUTTON_NAME(state, val) {
      state.modalButtonName = val
    },
    UPDATE_EDIT_MODE(state, val) {
      state.editMode = val
    },
    UPDATE_VIEW_MODE(state, val) {
      state.viewMode = val
    },
  },
  actions: {


    //Sites
    // Get all client user data in database
    getAllCategory({ commit }, payload) {
      return CategoryServices.getAllCategory(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_CATEGORY_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_CATEGORY_LIST', {});
          return Promise.reject(error);
        }
      );
    },
    // Get auth user category option list for dropdown
    getUserCategoryOptions({ commit }) {
      return CategoryServices.getAllCategoryOption().then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_USER_CATEGORY_OPTIONS', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_USER_CATEGORY_OPTIONS', []);
          return Promise.reject(error);
        }
      );
    },
    // Get all default category
    getDefaultCategoryList({ commit }, payload) {
      return CategoryServices.getDefaultCategoryList().then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_DEFAULT_CATEGORY_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_DEFAULT_CATEGORY_LIST', []);
          return Promise.reject(error);
        }
      );
    },
    // Get specific category data
    getCategoryData({ commit }, payload) {
      return CategoryServices.getCategoryData(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_CATEGORY_DATA', response.data);
            commit('UPDATE_CATEGORY', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_CATEGORY_DATA', {});
          //commit('UPDATE_CATEGORY', {});
          return Promise.reject(error);
        }
      );
    },
    saveCategory({ commit, dispatch }, payload) {
      return CategoryServices.saveCategory(payload).then(
        response => {
          dispatch('getAllCategory', { perPage: 25, sortBy: 'category_id', sortDesc: 'true', page: '1' });
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      )
    },
    // update category data in database
    updateCategory({ commit, dispatch }, payload) {
      if (payload.id) {
        return CategoryServices.updateCategoryData(payload).then(
          response => {
            return Promise.resolve(response);
          },
          error => {
            return Promise.reject(error);
          }
        );
      }else {
        return CategoryServices.updateCategory(payload).then(
          response => {
            return Promise.resolve(response);
          },
          error => {
            return Promise.reject(error);
          }
        );
      }
      
    },

  },
  getters: {
    categoryList: state => {
      const { categoryList } = state
      return categoryList;
    },
    userCategoryOptions: state => {
      const { userCategoryOptions } = state
      return userCategoryOptions;
    },
    defaultCategoryList: state => {
      const { defaultCategoryList } = state
      return defaultCategoryList;
    },
    categories(state) {
      return state.categories;
    },
    categoryData: state => {
      const { categoryData } = state
      return categoryData;
    },
    modalButtonName: state => {
      const { modalButtonName } = state
      return modalButtonName;
    },
    editMode: state => {
      const { editMode } = state
      return editMode;
    },
    category: state => {
      const { category } = state
      return category;
    },
    viewMode: state => {
      const { viewMode } = state
      return viewMode;
    },
  },

}
