import CompanyServices from "@/services/companyServices";

export default {
    namespaced: true,

    state:{
        companyList: {
            companys: {
              data: [],
              to: 0,
              from: 0
            }
        },
        companyData: {},
        defaultCompanyList : [],
    },
    mutations: {
        UPDATE_COMPANY_LIST(state, payload) {
            state.companyList = payload;
        },
        UPDATE_COMPANY_DATA(state, val) {
            state.companyData = val
        },
        UPDATE_DEFAULT_COMPANY_LIST(state, payload) {
            state.defaultCompanyList = payload;
        },
    },
    actions: {
        // Get all company data in database
        getAllCompany({ commit }, payload) {
            return CompanyServices.getAllCompany(payload).then(
            response => {
                if (response.status == true || response.status == 200) {
                commit('UPDATE_COMPANY_LIST', response.data);
                }
                return Promise.resolve(response);
            },
            error => {
                commit('UPDATE_COMPANY_LIST', {});
                return Promise.reject(error);
            }
            );
        },

        // Get all default company for option
        getDefaultCompanyList({ commit }, payload) {
            return CompanyServices.getDefaultCompanyList().then(
            response => {
                if (response.status == true || response.status == 200) {
                commit('UPDATE_DEFAULT_COMPANY_LIST', response.data);
                }
                return Promise.resolve(response);
            },
            error => {
                commit('UPDATE_DEFAULT_COMPANY_LIST', []);
                return Promise.reject(error);
            }
            );
        },

        // Store Company data in database
        saveCompany({ commit }, data) {
            return CompanyServices.saveCompany(data.payload, data.config).then(
            response => {
                return Promise.resolve(response);
            },
            error => {
                return Promise.reject(error);
            }
            )
        },

        // Get specific company data
        getCompanyData({ commit }, payload) {
            return CompanyServices.getCompanyData(payload).then(
            response => {
                if (response.status == true || response.status == 200) {
                commit('UPDATE_COMPANY_DATA', response.data);
                }
                return Promise.resolve(response);
            },
            error => {
                commit('UPDATE_COMPANY_DATA', {});
                return Promise.reject(error);
            }
            );
        },

        // update user data in database
        updateCompany({ commit, dispatch }, payload) {
            return CompanyServices.updateCompanyData(payload).then(
            response => {
                if (response.status == true || response.status == 200) {

                }
                return Promise.resolve(response);
            },
            error => {
                return Promise.reject(error);
            }
            );
        },
    },
    getters: {
        companyList: state => {
            const { companyList } = state
            return companyList;
        },
        companyData: state => {
            const { companyData } = state
            return companyData;
        },
        defaultCompanyList: state => {
            const { defaultCompanyList } = state
            return defaultCompanyList;
        },
    },
}
