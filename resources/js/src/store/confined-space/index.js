import ConfinedSpaceServices from '@/services/confinedSpaceServices';


export default {
  namespaced: true,
  state: {
    confinedSpaceList: {
      confinedSpace: {
        data: [],
        to: 0,
        from: 0
      }
    },
    confinedSpaceData: {},
  },
  mutations: {
    UPDATE_CONFINED_SPACE_LIST(state, val) {
      state.confinedSpaceList = val
    },
    UPDATE_CONFINED_SPACE_DATA(state, val) {
      state.confinedSpaceData = val
    },
  },
  actions: {
    // Get all confined space data in database
    getAllConfinedSpace({ commit }, payload) {
      return ConfinedSpaceServices.getAllConfinedSpace(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_CONFINED_SPACE_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_CONFINED_SPACE_LIST', {});
          return Promise.reject(error);
        }
      );
    },
    // Get specific module field data
    getModuleFieldData({ commit }, payload) {
      return ConfinedSpaceServices.getConfinedSpaceData(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_CONFINED_SPACE_DATA', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_CONFINED_SPACE_DATA', {});
          return Promise.reject(error);
        }
      );
    },
  },
  getters: {
    confinedSpaceList: state => {
      const { confinedSpaceList } = state
      return confinedSpaceList;
    },
    confinedSpaceData: state => {
      const { confinedSpaceData } = state
      return confinedSpaceData;
    },
  }
}
