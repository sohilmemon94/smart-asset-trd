export default {
    namespaced: true,
    state: {
        asset: {},
        loginUserRole: localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')).role : null
    },
    getters: {
        asset: state => {
            const { asset } = state
            return asset;
        },
        loginUserRole: state => {
            const { loginUserRole } = state
            return loginUserRole;
        }
    },
    mutations: {
        UPDATE_ASSET(state, payload) {
            state.asset = payload
        },
        UPDATE_LOGIN_USER_ROLE(state) {
            state.loginUserRole = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')).role : null
        }
    },
    actions: {
        setupPrintDataAction({commit}, payload) {
            commit('UPDATE_ASSET', payload);
            commit('UPDATE_LOGIN_USER_ROLE');
        }
    },
}
