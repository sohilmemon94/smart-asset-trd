import dashboardServices from '@/services/dashboardServices'

export default {
    namespaced: true,
    state: {
        clientOptions: [],
        locationOptions: [],
        areaOptions: [],
        clientName: 'All Clients',
        locationName: 'All Locations',
        areaName: 'All Areas',
        assetSummary: {},
        currentInspectionCompliance: {},
        lastInspection: {},
        lastRegisteredAsset: {},
        lastFailedAsset: {},
        detailedInfo: {},
        summaryInfoChart: [],
        complianceInfoChart: [],
        clientId: localStorage.getItem('clientId') ? localStorage.getItem('clientId') : 0,
        companyId: localStorage.getItem('companyId') ? localStorage.getItem('companyId') : 0,
        locationId: localStorage.getItem('locationId') ? localStorage.getItem('locationId') : 0,
        areaId: localStorage.getItem('areaId') ? localStorage.getItem('areaId') : 0,
        globalAssetSearch:[],
    },
    getters: {
        clientOptions: state => {
            const { clientOptions } = state
            return clientOptions;
        },
        locationOptions: state => {
            const { locationOptions } = state
            return locationOptions;
        },
        areaOptions: state => {
            const { areaOptions } = state
            return areaOptions;
        },
        clientName: state => {
            const { clientName } = state
            return clientName;
        },
        locationName: state => {
            const { locationName } = state
            return locationName;
        },
        areaName: state => {
            const { areaName } = state
            return areaName;
        },
        assetSummary: state => {
            const { assetSummary } = state
            return assetSummary;
        },
        currentInspectionCompliance: state => {
            const { currentInspectionCompliance } = state
            return currentInspectionCompliance;
        },
        lastInspection: state => {
            const { lastInspection } = state
            return lastInspection;
        },
        lastRegisteredAsset: state => {
            const { lastRegisteredAsset } = state
            return lastRegisteredAsset;
        },
        lastFailedAsset: state => {
            const { lastFailedAsset } = state
            return lastFailedAsset;
        },
        detailedInfo: state => {
            const { detailedInfo } = state
            return detailedInfo;
        },
        summaryInfoChart: state => {
            const { summaryInfoChart } = state
            return summaryInfoChart;
        },
        complianceInfoChart: state => {
            const { complianceInfoChart } = state
            return complianceInfoChart;
        },
        clientId: state => {
            const { clientId } = state
            return clientId;
        },
        companyId: state => {
            const { companyId } = state
            return companyId;
        },
        locationId: state => {
            const { locationId } = state
            return locationId;
        },
        areaId: state => {
            const { areaId } = state
            return areaId;
        },
        globalAssetSearch:state => {
            const { globalAssetSearch } = state
            return globalAssetSearch;
        }
    },
    mutations: {
        UPDATE_CLIENT_OPTIONS(state, payload) {
            state.clientOptions = payload
        },
        UPDATE_LOCATION_OPTIONS(state, payload) {
            state.locationOptions = payload
        },
        UPDATE_AREA_OPTIONS(state, payload) {
            state.areaOptions = payload
        },
        UPDATE_CLIENT_NAME(state, payload) {
            state.clientName = payload
        },
        UPDATE_LOCATION_NAME(state, payload) {
            state.locationName = payload
        },
        UPDATE_AREA_NAME(state, payload) {
            state.areaName = payload
        },
        UPDATE_ASSET_SUMMARY(state, payload) {
            state.assetSummary = payload
            state.summaryInfoChart = [];
            state.summaryInfoChart.push(payload.info.pass);
            state.summaryInfoChart.push(payload.info.fail);
            state.summaryInfoChart.push(payload.info.maintanance_required);
            state.summaryInfoChart.push(payload.info.inspection_due);
        },
        UPDATE_CURRENT_INSPECTION_COMPLIANCE(state, payload) {
            state.currentInspectionCompliance = payload
            state.complianceInfoChart = [];
            state.complianceInfoChart.push(payload.current_inspections);
            state.complianceInfoChart.push(payload.overdue_inspections);
            state.complianceInfoChart.push(payload.inspections_due);
        },
        UPDATE_LAST_INSPECTION(state, payload) {
            state.lastInspection = payload
        },
        UPDATE_LAST_REGISTERED_ASSET(state, payload) {
            state.lastRegisteredAsset = payload
        },
        UPDATE_LAST_FAILED_ASSET(state, payload) {
            state.lastFailedAsset = payload
        },
        UPDATE_DETAILED_INFO(state, payload) {
            state.detailedInfo = payload
        },
        UPDATE_GLOBAL_ASSET_SEARCH(state, payload) {
            state.globalAssetSearch = payload
        },
    },
    actions: {
        getFilterOptions({ commit }) {
            return new Promise((resolve, reject) => {
                dashboardServices.getFilterOptions().then((response) => {console.log(response.data);
                    commit('UPDATE_CLIENT_OPTIONS', response.data.clientAdmins);
                    commit('UPDATE_LOCATION_OPTIONS', response.data.locations);
                    commit('UPDATE_AREA_OPTIONS', response.data.areas);
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        setClientName({ commit }, payload) {
            commit('UPDATE_CLIENT_NAME', payload);
        },
        setLocationName({ commit }, payload) {
            commit('UPDATE_LOCATION_NAME', payload);
        },
        setAreaName({ commit }, payload) {
            commit('UPDATE_AREA_NAME', payload);
        },
        loadDashboardData({ commit, state }, payload) {
            // storing the selected area Array so that it can be used in the dashboardListing module
            commit('dashboardListing/UPDATE_AREA_ARRAY', payload.areaArr, { root: true });
            state.clientId = payload.clientId;
            state.locationId = payload.locationId;
            state.areaId = payload.areaId;
            state.companyId = payload.companyId;
            // setting to localstorage
            localStorage.setItem('clientId', payload.clientId);
            localStorage.setItem('companyId', payload.companyId);
            localStorage.setItem('locationId', payload.locationId);
            localStorage.setItem('areaId', payload.areaId);
            localStorage.setItem('areaArr', JSON.stringify(payload.areaArr));
            return new Promise((resolve, reject) => {
                dashboardServices.loadDashboardData(payload).then((response) => {
                    commit('UPDATE_ASSET_SUMMARY', response.data.summary);
                    commit('UPDATE_CURRENT_INSPECTION_COMPLIANCE', response.data.currentInspectionCompliance);
                    commit('UPDATE_LAST_INSPECTION', response.data.lastInspection);
                    commit('UPDATE_LAST_REGISTERED_ASSET', response.data.lastRegisteredAsset);
                    commit('UPDATE_LAST_FAILED_ASSET', response.data.lastFailedAsset);
                    commit('UPDATE_DETAILED_INFO', response.data.detailedInfo);
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        getGlobalAssetSearch({ commit }, payload) {
            return new Promise((resolve, reject) => {
                dashboardServices.getGlobalAssetSearch(payload).then((response) => {
                    if(response.data){
                        commit('UPDATE_GLOBAL_ASSET_SEARCH', response.data);
                    } else {
                        commit('UPDATE_GLOBAL_ASSET_SEARCH', []);
                    }

                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
    },
}
