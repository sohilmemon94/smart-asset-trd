export default {
    namespaced: true,
    state: {
        assetList: [],
        pageTitle: '',
        columns: [],
        clientName: '',
        locationName: '',
        areaName: '',
        apiKey: null,
        time: null,
        loginUserRole: localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')).role : null,
        flag: false
    },
    getters: {
        assetList: state => {
            const { assetList } = state
            return assetList;
        },
        pageTitle: state => {
            const { pageTitle } = state
            return pageTitle;
        },
        columns: state => {
            const { columns } = state
            return columns;
        },
        clientName: state => {
            const { clientName } = state
            return clientName;
        },
        locationName: state => {
            const { locationName } = state
            return locationName;
        },
        areaName: state => {
            const { areaName } = state
            return areaName;
        },
        apiKey: state => {
            const { apiKey } = state
            return apiKey;
        },
        time: state => {
            const { time } = state
            return time;
        },
        loginUserRole: state => {
            const { loginUserRole } = state
            return loginUserRole;
        },
        flag: state => {
            const { flag } = state
            return flag;
        }
    },
    mutations: {
        UPDATE_ASSET_LIST(state, payload) {
            state.assetList = payload
        },
        UPDATE_PAGE_TITLE(state, payload) {
            state.pageTitle = payload
        },
        UPDATE_COLUMNS(state, payload) {
            if(state.flag) {
                payload.pop();
            }
            state.columns = payload
        },
        UPDATE_CLIENT_NAME(state, payload) {
            state.clientName = payload
        },
        UPDATE_LOCATION_NAME(state, payload) {
            state.locationName = payload
        },
        UPDATE_AREA_NAME(state, payload) {
            state.areaName = payload
        },
        UPDATE_API_KEY(state, payload) {
            state.apiKey = payload
        },
        UPDATE_TIME(state, payload) {
            state.time = payload
        },
        UPDATE_LOGIN_USER_ROLE(state) {
            state.loginUserRole = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')).role : null
        },
        UPDATE_FLAG(state, payload) {
            state.flag = payload;
        }
    },
    actions: {
        setupPrintDataAction({commit}, payload) {
            commit('UPDATE_FLAG', payload.flag);
            commit('UPDATE_ASSET_LIST', payload.assetList);
            commit('UPDATE_PAGE_TITLE', payload.pageTitle);
            commit('UPDATE_COLUMNS', payload.columns);
            commit('UPDATE_CLIENT_NAME', payload.clientName);
            commit('UPDATE_LOCATION_NAME', payload.locationName);
            commit('UPDATE_AREA_NAME', payload.areaName);
            commit('UPDATE_API_KEY', payload.api);
            commit('UPDATE_TIME', payload.time);
            commit('UPDATE_LOGIN_USER_ROLE');
        }
    },
}
