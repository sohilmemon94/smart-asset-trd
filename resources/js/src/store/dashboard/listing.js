import dashboardServices from '@/services/dashboardServices'

export default {
    namespaced: true,
    state: {
        assetList: [],
        pageTitle: '',
        table: {
            page: 1,
            perPage: 25,
            total: 0,
            sortBy: 'id',
            sortDesc: false,
            areaArray: localStorage.getItem('areaArr') ? JSON.parse(localStorage.getItem('areaArr')) : [],
        }
    },
    getters: {
        assetList: state => {
            const { assetList } = state
            return assetList;
        },
        pageTitle: state => {
            const { pageTitle } = state
            return pageTitle;
        },
        areaArray: state => {
            const { areaArray } = state.table
            return areaArray;
        }
    },
    mutations: {
        UPDATE_ASSET_LIST(state, payload) {
            state.assetList = payload
        },
        UPDATE_PAGE_TITLE(state, payload) {
            state.pageTitle = payload
        },
        UPDATE_AREA_ARRAY(state, payload) {
            state.table.areaArray = payload
        }
    },
    actions: {
        fetchDashboardListing({ commit, state }, payload) {
            commit('UPDATE_ASSET_LIST', []);
            payload = {...payload, ...state.table}
            return new Promise((resolve, reject) => {
                dashboardServices.fetchDashboardListing(payload).then((response) => {
                    commit('UPDATE_ASSET_LIST', response.data.assetList);
                    commit('UPDATE_PAGE_TITLE', response.data.pageTitle);
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        exportDashboardListingData({commit}, payload) {
            return new Promise((resolve, reject) => {
                dashboardServices.exportDashboardListingData(payload).then((response) => {
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        }
    },
}
