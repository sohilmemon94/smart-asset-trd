import SiteServices from '../../services/siteServices';


export default {
    namespaced: true,
    state: {
        siteList: {
            sites: {
                data: [],
                to: 0,
                from: 0
            }
        },
        siteData: {
            site_name: '',
            site_uuid: '',
            site_description: '',
            site_status: '',
          },
        siteButtonName: 'Add',
        editMode: false,
    },
    mutations: {
        UPDATE_SITE_BUTTON_NAME(state, val) {
            state.siteButtonName = val
          },
          UPDATE_SITE_DATA(state, payload) {
            state.siteData = payload;
          },
          UPDATE_EDIT_MODE(state, val) {
            state.editMode = val
          },
        UPDATE_SITE_LIST(state, val) {
            state.siteList = val
        }
    },
    actions: {
        // Get all Sites data in database
        getAllSite({ commit }, payload) {
            return SiteServices.getAllSite(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {
                        commit('UPDATE_SITE_LIST', response.data);
                        commit('UPDATE_SITE_DATA', response.data);
                    }
                    return Promise.resolve(response);
                },
                error => {
                    //commit('UPDATE_SITE_LIST', {});
                    return Promise.reject(error);
                }
            );
        },
        // Store site data
        saveSite({ commit }, payload) {
            return SiteServices.saveSite(payload).then(
                response => {

                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            )
        },
        // Get specific site data
        getSiteData({ commit }, payload) {
            return SiteServices.getSiteData(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {
                        response.data.site_status = (response.data.site_status == 'Y') ? true : false;

                        commit('UPDATE_SITE_DATA', response.data);
                      }

                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },

        // update user data in database
        updateSite({ commit, dispatch }, payload) {
            return SiteServices.updateSiteData(payload).then(
                response => {
                    if (response.status == true || response.status == 200) {

                    }
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        // Change Site Status in database
        changeSiteStatus({ commit, dispatch }, payload) {
            return SiteServices.changeSiteStatus(payload).then(
            response => {
                return Promise.resolve(response);
            },
            error => {
                return Promise.reject(error);
            }
            );
        },
        // Delete Site in database
    deleteSite({ commit, dispatch }, payload) {
        return SiteServices.deleteSiteData(payload).then(
          response => {
            return Promise.resolve(response);
          },
          error => {
            return Promise.reject(error);
          }
        );
      },

    },
    getters: {
        siteButtonName: state => {
            const { siteButtonName } = state
            return siteButtonName;
          },
          siteData: state => {
            const { siteData } = state
            return siteData;
          },
          editMode: state => {
            const { editMode } = state
            return editMode;
          },
        siteList: state => {
            const { siteList } = state
            return siteList;
        }
    },

}
