import ModuleQuestionServices from '@/services/moduleQuestionServices';

export default {
  namespaced: true,
  state: {
    moduleQuestionList: {
      moduleQuestion: {
        data: [],
        to: 0,
        from: 0
      }
    },
    moduleName: '',
    moduleQuestionData: {
        msq_id: '',
        msq_module_id: '',
        msq_title: '',
        msq_desc: '',
        msq_options: '',
        msq_type: '',
        msq_type_has_button: '',
        msq_has_required: '',
        msq_status: true,
        created_by: '',
    },
    modalButtonName: 'Add',
    editMode: false,
    viewMode: false,
    modalButtonDisable: false,
  },
  mutations: {
    UPDATE_MODULE_NAME(state, payload) {
      state.moduleName = payload;
    },
    UPDATE_MODULE_QUESTION_LIST(state, payload) {
      state.moduleQuestionList = payload;
    },
    UPDATE_MODULE_QUESTION_DATA(state, payload) {
      state.moduleQuestionData = payload;
    },
    UPDATE_MODAL_BUTTON_NAME(state, val) {
      state.modalButtonName = val
    },
    UPDATE_EDIT_MODE(state, val) {
      state.editMode = val
    },
    UPDATE_VIEW_MODE(state, val) {
      state.viewMode = val
    },
    UPDATE_MODAL_BUTTON_DISABLE(state, val) {
      state.modalButtonDisable = val
    },
  },
  getters: {
    moduleName(state) {
      return state.moduleName;
    },
    moduleQuestionList: state => {
      const { moduleQuestionList } = state
      return moduleQuestionList;
    },
    moduleQuestionData: state => {
      const { moduleQuestionData } = state
      return moduleQuestionData;
    },
    modalButtonName: state => {
      const { modalButtonName } = state
      return modalButtonName;
    },
    editMode: state => {
      const { editMode } = state
      return editMode;
    },
    viewMode: state => {
      const { viewMode } = state
      return viewMode;
    },
    modalButtonDisable: state => {
      const { modalButtonDisable } = state
      return modalButtonDisable;
    },
  },
  actions: {
    // Get all question based on module id
    getModuleQuestionList({ commit }, payload) {
      return ModuleQuestionServices.getModuleQuestionList(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            // Set module name
            commit('UPDATE_MODULE_NAME', response.data.moduleName);

            commit('UPDATE_MODULE_QUESTION_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          commit('UPDATE_MODULE_QUESTION_LIST', {});
          return Promise.reject(error);
        }
      );
    },
    // Save module question data in database
    saveModuleQuestion({ commit, dispatch }, payload) {
      return ModuleQuestionServices.saveModuleQuestion(payload).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      )
    },
    // Get specific module question data
    getModuleQuestionData({ commit }, payload) {
      return ModuleQuestionServices.getModuleQuestionData(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            response.data.msq_type_has_button = (response.data.msq_type_has_button == 'Y') ? true : false;
            response.data.msq_has_required = (response.data.msq_has_required == 'Y') ? true : false;

            commit('UPDATE_MODULE_QUESTION_DATA', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          commit('UPDATE_MODULE_QUESTION_DATA', {});
          return Promise.reject(error);
        }
      );
    },
    // update module question data in database
    updateModuleQuestion({ commit, dispatch }, payload) {
      return ModuleQuestionServices.updateModuleQuestion(payload).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // Change module question Status in database
    changeModuleQuestionStatus({ commit, dispatch }, payload) {
      return ModuleQuestionServices.changeModuleQuestionStatus(payload).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    // Delete module question Status in database
    deleteModuleQuestion({ commit, dispatch }, payload) {
      return ModuleQuestionServices.deleteModuleQuestion(payload).then(
        response => {
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
  },
}
