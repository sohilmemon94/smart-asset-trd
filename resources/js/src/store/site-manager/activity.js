// This store is made to manage all the state of the User panel.
import siteManagerServices from '../../services/siteManagerServices.js';

export default {
    namespaced: true,
    state: {
        activityList: [],
        table: {
            asset_id: null,
            sortBy: 'aa_id',
            sortDesc: true,
            perPage: 25,
            search: null,
            page: 1,
        }
    },
    mutations: {
        UPDATE_ACTIVITY_LIST(state, payload) {
            state.activityList = payload;
        },
    },
    actions: {
        fetchAssetActivity({ commit }, assetId) {
            let payload = {asset_id: assetId};
            return new Promise((resolve, reject) => {
                siteManagerServices.fetchAssetActivity(payload).then((response) => {
                    commit('UPDATE_ACTIVITY_LIST', response.data);
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
    },
    getters: {
        activityList: state => {
            const { activityList } = state;
            return activityList;
        },
        table: state => {
            const { table } = state;
            return table;
        }
    },
}
