// This store is made to manage all the state of the User panel.
import siteManagerServices from '../../services/siteManagerServices.js';

export default {
    namespaced: true,
    state: {
        categoryName: null,
        moduleName: null,
        activeModuleId: null,
        activeModuleHasImage: false,
        activeModuleAllowDuplicate: true,
        editMode: false,
        modalButtonName: 'Add',
        viewMode: false,
        defaultAsset: {
        },
        fieldsList: [],
        tableColumns: [],
        activeFieldsList: [],
        table: {
            module_id: null,
            sub_site_id: localStorage.getItem('defaultSubSite'),
            sortBy: null,
            sortDesc: true,
            perPage: 25,
            search: null,
            role: '',
            status: '',
            page: 1,
        },
    },
    mutations: {
        UPDATE_CATEGORY_NAME(state, payload) {
            state.categoryName = payload;
        },
        UPDATE_MODULE_NAME(state, payload) {
            state.moduleName = payload;
        },
        UPDATE_ACTIVE_MODULE_ID(state, payload) {
            state.activeModuleId = payload;
        },
        UPDATE_ACTIVE_MODULE_HAS_IMAGE(state, payload) {
            state.activeModuleHasImage = payload;
        },
        UPDATE_ACTIVE_MODULE_ALLOW_DUPLICATE(state, payload) {
            state.activeModuleAllowDuplicate = payload;
        },
        UPDATE_EDIT_MODE(state, payload) {
            state.editMode = payload;
        },
        UPDATE_MODAL_BUTTON_NAME(state, payload) {
            state.modalButtonName = payload;
        },
        UPDATE_VIEW_MODE(state, payload) {
            state.viewMode = payload;
        },
        UPDATE_DEFAULT_ASSET(state, payload) {
            state.defaultAsset = payload;
        },
        UPDATE_FIELDS_LIST(state, payload) {
            state.fieldsList = payload;
            state.tableColumns = [];
            // adding checkbox to the table headers
            state.tableColumns.push({
                label: '',
                key: 'checkbox',
            });
            // adding UID to the table headers
            state.tableColumns.push({
                label: 'UID',
                key: 'asset_uid',
                tdClass: 'p-0',
                sortable: true
            });
            // adding dynamic field to the table headers
            payload.forEach(element => {
                if(element.onGrid) {
                    state.tableColumns.push({
                        label: element.name,
                        key: element.key,
                        sortable: false
                    });
                }
            });
            // adding image on condition to the table headers
            if(state.activeModuleHasImage) {
                state.tableColumns.push({
                    label: 'Image',
                    key: 'asset_image',
                });
            }
            // adding location to the table headers
            state.tableColumns.push({
                label: 'Location to Map',
                key: 'asset_location_map',
            });
            // adding status to the table headers
            state.tableColumns.push({
                label: 'Status',
                key: 'status',
            });
            // adding created_at to the table headers
            state.tableColumns.push({
                label: 'Created At',
                key: 'created_at',
            });
            // adding action to the table headers
            state.tableColumns.push({
                label: 'Actions',
                key: 'actions',
                thClass: 'text-center',
            });
            // adding duplicate on condition to the table headers
            // if(state.activeModuleAllowDuplicate) {
            //     state.tableColumns.push({
            //         label: 'Duplicate',
            //         key: 'duplicate',
            //     });
            // }

            let list = [];
            payload.forEach(element => {
                if(element.isActive) {
                    list.push(element);
                }
            });
            state.activeFieldsList = list;
        },
        UPDATE_TABLE(state, payload) {
            state.table = payload;
        },
    },
    actions: {
        setActiveModuleId({ commit, dispatch }, payload) {
            commit('UPDATE_ACTIVE_MODULE_ID', payload);
            dispatch('getActiveModuleInfo',  payload);
        },
        getActiveModuleInfo({commit}, modId) {
            return new Promise((resolve, reject) => {
                siteManagerServices.moduleInfo(modId).then((response) => {
                    commit('UPDATE_CATEGORY_NAME', response.data.categoryName);
                    commit('UPDATE_MODULE_NAME', response.data.moduleName);
                    commit('UPDATE_ACTIVE_MODULE_HAS_IMAGE', response.data.module_has_image);
                    commit('UPDATE_ACTIVE_MODULE_ALLOW_DUPLICATE', response.data.module_allow_duplicate);
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });

        },
        updateEditMode({ commit }, payload) {
            commit('UPDATE_EDIT_MODE', payload);
        },
        updateModalButtonName({ commit }, payload) {
            commit('UPDATE_MODAL_BUTTON_NAME', payload);
        },
        updateViewMode({ commit }, payload) {
            commit('UPDATE_VIEW_MODE', payload);
        },
        storeAsset({ dispatch, state, commit }, payload) {
            return new Promise((resolve, reject) => {
                siteManagerServices.storeAsset(payload).then((response) => {
                    // call getAssetsList action from siteManagerAssets store
                    let requestData = {
                        module_id: state.activeModuleId,
                        sub_site_id: localStorage.getItem('defaultSubSite'),
                        sortBy: 'asset_id',
                        sortDesc: true,
                        perPage: 25,
                        search: null,
                        role: '',
                        status: '',
                        page: 1
                    };
                    commit('UPDATE_TABLE', requestData);
                    dispatch('siteManager/getAssetsList', requestData, {root: true});
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        duplicateAsset({ dispatch, state, commit }, payload) {
            return new Promise((resolve, reject) => {
                siteManagerServices.duplicateAsset(payload).then((response) => {
                    // call getAssetsList action from siteManagerAssets store
                    let requestData = {
                        module_id: state.activeModuleId,
                        sub_site_id: localStorage.getItem('defaultSubSite'),
                        sortBy: 'asset_id',
                        sortDesc: true,
                        perPage: 25,
                        search: null,
                        role: '',
                        status: '',
                        page: 1
                    };
                    commit('UPDATE_TABLE', requestData);
                    dispatch('siteManager/getAssetsList', requestData, {root: true});
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        getAssetDetails({ commit }, payload) {
            return new Promise((resolve, reject) => {
                siteManagerServices.getAssetDetails(payload).then((response) => {

                    commit('UPDATE_DEFAULT_ASSET', response.data);
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        getFieldsList({ commit }, payload) {
            return siteManagerServices.getFieldsList(payload).then((response) => {
                commit('UPDATE_FIELDS_LIST', response.data.fieldsList);
                commit('UPDATE_DEFAULT_ASSET', response.data.defaultAsset);
                return Promise.resolve(response);
            }).catch((error) => {
                return Promise.reject(error);
            });
        },
        deleteBatchAssets({ dispatch, state }, payload) {
            return new Promise((resolve, reject) => {
                siteManagerServices.deleteBatchAssets(payload).then((response) => {
                    // call getAssetsList action from siteManagerAssets store
                    let requestData = {
                        module_id: state.activeModuleId,
                        sub_site_id: localStorage.getItem('defaultSubSite'),
                        sortBy: 'asset_id',
                        sortDesc: false,
                        perPage: 25,
                        search: null,
                        role: '',
                        status: '',
                    };
                    dispatch('siteManager/getAssetsList', requestData, {root: true});
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        updateAsset({ dispatch, state, commit }, payload) {
            return new Promise((resolve, reject) => {
                siteManagerServices.updateAsset(payload).then((response) => {
                    // call getAssetsList action from siteManagerAssets store
                    let requestData = {
                        module_id: state.activeModuleId,
                        sub_site_id: localStorage.getItem('defaultSubSite'),
                        sortBy: 'asset_id',
                        sortDesc: true,
                        perPage: 25,
                        search: null,
                        role: '',
                        status: '',
                        page: 1,
                    };
                    commit('UPDATE_TABLE', requestData);
                    commit('UPDATE_DEFAULT_ASSET', response.data);
                    dispatch('siteManager/getAssetsList', requestData, {root: true});
                    dispatch('activity/fetchAssetActivity', null, {root: true});
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        moveAsset({dispatch, state, commit}, payload){
            return new Promise((resolve, reject) => {
                siteManagerServices.moveAsset(payload).then((response) => {
                    // call getAssetsList action from siteManagerAssets store
                    let requestData = {
                        module_id: state.activeModuleId,
                        sub_site_id: localStorage.getItem('defaultSubSite'),
                        sortBy: 'asset_id',
                        sortDesc: true,
                        perPage: 25,
                        search: null,
                        role: '',
                        status: '',
                        page: 1
                    };
                    commit('UPDATE_TABLE', requestData);
                    dispatch('siteManager/getAssetsList', requestData, {root: true});
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        }
    },
    getters: {
        categoryName(state){
            return state.categoryName;
        },
        moduleName(state){
            return state.moduleName;
        },
        activeModuleId: state => {
            const { activeModuleId } = state;
            return activeModuleId;
        },
        activeModuleHasImage: state => {
            const { activeModuleHasImage } = state;
            return activeModuleHasImage;
        },
        editMode(state) {
            return state.editMode;
        },
        modalButtonName(state) {
            return state.modalButtonName;
        },
        viewMode(state) {
            return state.viewMode;
        },
        defaultAsset(state) {
            return state.defaultAsset;
        },
        siteOptions(state) {
            return state.siteOptions;
        },
        subSiteOptions(state) {
            return state.subSiteOptions;
        },
        fieldsList(state) {
            return state.fieldsList;
        },
        tableColumns(state) {
            return state.tableColumns;
        },
        activeFieldsList(state) {
            return state.activeFieldsList;
        },
        table(state) {
            return state.table;
        },
    },
}
