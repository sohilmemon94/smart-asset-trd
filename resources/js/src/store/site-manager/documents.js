// This store is made to manage all the state of the User panel.
import siteManagerServices from '../../services/siteManagerServices.js';


export default {
    namespaced: true,
    state: {
        documentList: {
            documents: {
                data: [],
                to: 0,
                from: 0
            }
        },
        assetId: null,
        defaultDocument: {
            ad_id: null
        },

        modalButtonName: 'Add',
    },
    mutations: {
        UPDATE_DOCUMENT_LIST(state, payload) {
            state.documentList = payload;
        },
        UPDATE_ASSET_ID(state, payload) {
            state.assetId = payload;
        },
        UPDATE_MODAL_BUTTON_NAME(state, payload) {
            state.modalButtonName = payload;
        },
        UPDATE_DEFAULT_DOCUMENT(state, payload) {
            state.defaultDocument = payload;
        }
    },
    actions: {
        getDocumentsList({ commit }, payload) {
            return siteManagerServices.getDocumentsList(payload).then((response) => {
                commit('UPDATE_ASSET_ID', payload.asset_id);
                commit('UPDATE_DOCUMENT_LIST', response.data);
                return Promise.resolve(response);
            }).catch((error) => {
                return Promise.reject(error);
            });
        },
        setModalButtonName({ commit }, payload) {
            commit('UPDATE_MODAL_BUTTON_NAME', payload);
        },
        saveAssetDocument({ state, dispatch }, payload) {
            return new Promise((resolve, reject) => {
                siteManagerServices.saveAssetDocument(payload).then((res) => {
                    if(res && res.response && res.response.status == 413) {
                        // This is the server side file size error handling.
                        // Toast msg will be shown from vue file itself.
                        return reject(res.response.data);
                    }
                    let requestData = {
                        asset_id: state.assetId,
                        sortBy: 'ad_id',
                        sortDesc: false,
                        perPage: 25,
                        search: null,
                        role: '',
                        status: '',
                    };
                    dispatch('getDocumentsList', requestData).then((response) => {
                        return resolve(response);
                    }).catch((error) => {
                        return reject(error);
                    });
                    resolve(res.data);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        getAssetDocument({ commit }, payload) {
            return new Promise((resolve, reject) => {
                siteManagerServices.getAssetDocument(payload).then((response) => {
                    commit('UPDATE_DEFAULT_DOCUMENT', response.data);
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        deleteBatchDocuments({ state, dispatch }, payload) {
            return new Promise((resolve, reject) => {
                siteManagerServices.deleteBatchDocuments(payload).then((response) => {
                    let requestData = {
                        asset_id: state.assetId,
                        sortBy: 'ad_id',
                        sortDesc: false,
                        perPage: 25,
                        search: null,
                        role: '',
                        status: '',
                    };
                    dispatch('getDocumentsList', requestData).then((response) => {
                        return resolve(response);
                    }).catch((error) => {
                        return reject(error);
                    });
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        }
    },
    getters: {
        documentList: state => state.documentList,
        assetId: state => state.assetId,
        modalButtonName: state => state.modalButtonName,
        defaultDocument: state => state.defaultDocument,
    },

}
