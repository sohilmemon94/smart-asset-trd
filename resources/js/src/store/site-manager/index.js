// This store is made to manage all the state of the User panel.
import SiteManagerServices from '../../services/siteManagerServices';


export default {
    namespaced: true,
    state: {
        maxFileSize: 0,
        assetsList: {
            assets: {
                data: [],
                to: 0,
                from: 0
            }
        },
        siteOptions: localStorage.getItem('sites') ? JSON.parse(localStorage.getItem('sites')) : [],
        subSiteOptions: localStorage.getItem('subSites') ? JSON.parse(localStorage.getItem('subSites')) : [],
        defaultSite: localStorage.getItem('defaultSite') ? JSON.parse(localStorage.getItem('defaultSite')) : null,
        defaultSubSite: localStorage.getItem('defaultSubSite') ? JSON.parse(localStorage.getItem('defaultSubSite')) : null,
        sideBar: false
    },
    mutations: {
        UPDATE_MAX_FILE_SIZE(state, payload) {
            //console.log('fileSizeServer=',payload);
            state.maxFileSize = payload;
        },
        UPDATE_ASSETS_LIST(state, payload) {
            state.assetsList = payload;
        },
        UPDATE_SITE_OPTIONS(state, payload) {
            state.siteOptions = payload;
        },
        UPDATE_SUB_SITE_OPTIONS(state, payload) {
            state.subSiteOptions = payload;
        },
        UPDATE_DEFAULT_SITE(state, payload) {
            state.defaultSite = payload;
        },
        UPDATE_DEFAULT_SUB_SITE(state, payload) {
            state.defaultSubSite = payload;
        },
        UPDATE_SIDE_BAR(state, payload) {
            state.sideBar = payload;
        }
    },
    actions: {
        getMaxFileSize({ commit }) {
            return SiteManagerServices.getFileSizeLimit().then((response) => {
                commit('UPDATE_MAX_FILE_SIZE', response.data);
                return Promise.resolve(response);
            }).catch((error) => {
                return Promise.reject(error);
            });
        },
        updateSiteManagerSitesAndSubSites({ commit }) {
            return SiteManagerServices.getSitesAndSubSitesList().then((response) => {
                commit('UPDATE_SITE_OPTIONS', response.data.sites);
                commit('UPDATE_SUB_SITE_OPTIONS', response.data.subSites);
                // updating values in the local storage
                localStorage.removeItem('sites');
                localStorage.removeItem('subSites');
                localStorage.setItem('sites', JSON.stringify(response.data.sites));
                localStorage.setItem('subSites', JSON.stringify(response.data.subSites));
                return Promise.resolve(response);
            }).catch((error) => {
                return Promise.reject(error);
            });
        },

        getAssetsList({ commit }, payload) {
            return SiteManagerServices.getAssetsList(payload).then((response) => {
                if(response.status == false) {
                    // START :: if category not found then redirect to dashboard
                    if(response.message == 'Access Forbidden') {
                        window.location.href = "/"
                    }
                    // END :: if category not found then redirect to dashboard

                    commit('UPDATE_ASSETS_LIST', {});
                    return Promise.reject(response);
                }
                commit('UPDATE_ASSETS_LIST', response.data);
                return Promise.resolve(response);
            }).catch((error) => {
                return Promise.reject(error);
            });
        },
        getSitesAndSubSitesList({ commit,state }, moduleId) {
            return new Promise((resolve, reject) => {
                SiteManagerServices.getSitesAndSubSitesList(moduleId).then((response) => {
                    if(state.assetsList && state.assetsList.assets && state.assetsList.assets.data.length > 0) {
                        commit('UPDATE_ASSETS_LIST', {});
                    }
                    commit('UPDATE_SITE_OPTIONS', response.data.sites);
                    commit('UPDATE_SUB_SITE_OPTIONS', response.data.subSites);
                    commit('UPDATE_DEFAULT_SITE', response.data.defaultSite);
                    commit('UPDATE_DEFAULT_SUB_SITE', response.data.defaultSubSite);
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                });
            });
        },
        setSideBar({ commit }, payload) {
            commit('UPDATE_SIDE_BAR', payload);
        },
        getClientPanelCategory({ commit }, payload) {
            return SiteManagerServices.getClientPanelCategory(payload).then((response) => {
                return Promise.resolve(response);
            }).catch((error) => {
                // console.log(error);
                return Promise.reject(error);
            });
        }
    },
    getters: {
        maxFileSize: state => {
            const { maxFileSize } = state;
            return maxFileSize;
        },
        assetsList: state => {
            const { assetsList } = state;
            return assetsList;
        },
        siteOptions: state => {
            const { siteOptions } = state;
            return siteOptions;
        },
        subSiteOptions: state => {
            const { subSiteOptions } = state;
            return subSiteOptions;
        },
        defaultSite: state => {
            const { defaultSite } = state;
            return defaultSite;
        },
        defaultSubSite: state => {
            const { defaultSubSite } = state;
            return defaultSubSite;
        },
        sideBar: state => {
            const { sideBar } = state;
            return sideBar;
        }
    },

}
