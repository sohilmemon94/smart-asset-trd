import SiteManagerServices from '../../services/siteManagerServices';

export default {
    namespaced: true,
    state: {
        subSiteOptions : [],
        categoryOptions : [],
        moduleOptions : []
    },

    mutations: {
        UPDATE_SUB_SITE_OPTIONS(state, payload) {
            state.subSiteOptions = payload;
        },
        UPDATE_CATEGORY_OPTIONS(state, payload){
            state.categoryOptions = payload;
        },
        UPDATE_MODULE_OPTIONS(state, payload){
            state.moduleOptions = payload;
        }
    },
    actions: {
        getSubSiteOption({ commit }, payload){
            return SiteManagerServices.getSubSiteOption(payload).then((response) => {
                commit('UPDATE_SUB_SITE_OPTIONS', response.data);
                return Promise.resolve(response);
            }).catch((error) => {
                return Promise.reject(error);
            });
        },
        getCategoryOption({ commit }, payload){
            return SiteManagerServices.getClientPanelCategory(payload).then((response) => {
                commit('UPDATE_CATEGORY_OPTIONS', response.data);
                return Promise.resolve(response);
            }).catch((error) => {
                return Promise.reject(error);
            });
        },
        getModuleOption({ commit }, payload){
            return SiteManagerServices.getClientPanelModule(payload).then((response) => {
                commit('UPDATE_MODULE_OPTIONS', response.data);
                return Promise.resolve(response);
            }).catch((error) => {
                return Promise.reject(error);
            });
        }
    },
    getters: {
        subSiteOptions: state => {
            const { subSiteOptions } = state;
            return subSiteOptions;
        },
        categoryOptions: state => {
            const { categoryOptions } = state;
            return categoryOptions;
        },
        moduleOptions: state => {
            const { moduleOptions } = state;
            return moduleOptions;
        },
    }

}
