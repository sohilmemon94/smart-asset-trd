import TemplateFieldServices from "@/services/templateFieldServices";

export default {
    namespaced: true,
    state: {
        inputFieldType: [],
        templateFieldList: {
            templateField: {
                data: [],
                to: 0,
                from: 0,
            },
        },
        templateName: "",
        templateFieldData: {
            template_field_id: "",
            template_field_template_id: "",
            template_field_name: "",
            template_field_default_val: "",
            template_field_input_field_type_id: "",
            template_field_is_show_on_grid: "",
            template_field_is_show_on_details: "",
            template_field_has_expiry_date: "",
            template_field_is_required: "",
            template_field_status: true,
            created_by: "",
        },
        templateAllFields: [],
        modalButtonName: "Add",
        editMode: false,
        viewMode: false,
        noTemplateFields: false,
    },
    mutations: {
        UPDATE_INPUT_FIELD_TYPE(state, payload) {
            state.inputFieldType = payload;
        },
        UPDATE_TEMPLATE_NAME(state, payload) {
            state.templateName = payload;
        },
        UPDATE_TEMPLATE_FIELD_LIST(state, payload) {
            state.templateFieldList = payload;
        },
        UPDATE_TEMPLATE_FIELD_DATA(state, payload) {
            state.templateFieldData = payload;
        },
        UPDATE_TEMPLATE_ALL_FIELDS(state, payload) {
            state.templateAllFields = payload;
        },
        UPDATE_MODAL_BUTTON_NAME(state, val) {
            state.modalButtonName = val;
        },
        UPDATE_EDIT_MODE(state, val) {
            state.editMode = val;
        },
        UPDATE_VIEW_MODE(state, val) {
            state.viewMode = val;
        },
        UPDATE_NO_TEMPLATE_FIELDS(state, val) {
            state.noTemplateFields = val;
        },
    },
    getters: {
        inputFieldType(state) {
            return state.inputFieldType;
        },
        templateName(state) {
            return state.templateName;
        },
        templateFieldList: (state) => {
            const { templateFieldList } = state;
            return templateFieldList;
        },
        templateFieldData: (state) => {
            const { templateFieldData } = state;
            return templateFieldData;
        },
        templateAllFields: (state) => {
            const { templateAllFields } = state;
            return templateAllFields;
        },
        modalButtonName: (state) => {
            const { modalButtonName } = state;
            return modalButtonName;
        },
        editMode: (state) => {
            const { editMode } = state;
            return editMode;
        },
        viewMode: (state) => {
            const { viewMode } = state;
            return viewMode;
        },
        noTemplateFields: (state) => {
            const { noTemplateFields } = state;
            return noTemplateFields;
        },
    },
    actions: {
        // Get all input field type in database
        getAllInputField({ commit }, payload) {
            return TemplateFieldServices.getAllInputField(payload).then(
                (response) => {
                    if (response.status == true || response.status == 200) {
                        commit("UPDATE_INPUT_FIELD_TYPE", response.data);
                    }
                    return Promise.resolve(response);
                },
                (error) => {
                    commit("UPDATE_INPUT_FIELD_TYPE", []);
                    return Promise.reject(error);
                }
            );
        },
        // Get all field based on template id
        getTemplateFieldList({ commit }, payload) {
            return TemplateFieldServices.getTemplateFieldList(payload)
                .then((response) => {
                    if (response.status === true || response.status === 200) {
                        commit(
                            "UPDATE_TEMPLATE_NAME",
                            response.data.templateName
                        );
                        commit("UPDATE_TEMPLATE_FIELD_LIST", response.data);
                    }
                    return response;
                })
                .catch((error) => {
                    //commit('UPDATE_TEMPLATE_FIELD_LIST', {});
                    return Promise.reject(error);
                });
        },
        // Save template field data in database
        saveTemplateField({ commit, dispatch }, payload) {
            return TemplateFieldServices.saveTemplateField(payload).then(
                (response) => {
                    return Promise.resolve(response);
                },
                (error) => {
                    return Promise.reject(error);
                }
            );
        },
        // Get specific template field data
        getTemplateFieldData({ commit }, payload) {
            return TemplateFieldServices.getTemplateFieldData(payload).then(
                (response) => {
                    if (response.status == true || response.status == 200) {
                        response.data.template_field_is_show_on_grid =
                            response.data.template_field_is_show_on_grid == "Y"
                                ? true
                                : false;
                        response.data.template_field_is_show_on_details =
                            response.data.template_field_is_show_on_details ==
                            "Y"
                                ? true
                                : false;
                        response.data.template_field_is_required =
                            response.data.template_field_is_required == "Y"
                                ? true
                                : false;
                        response.data.template_field_has_expiry_date =
                            response.data.template_field_has_expiry_date == "Y"
                                ? true
                                : false;
                        response.data.template_field_status =
                            response.data.template_field_status == "Y"
                                ? true
                                : false;

                        commit("UPDATE_TEMPLATE_FIELD_DATA", response.data);
                    }
                    return Promise.resolve(response);
                },
                (error) => {
                    commit("UPDATE_TEMPLATE_FIELD_DATA", {});
                    return Promise.reject(error);
                }
            );
        },
        // update template field data in database
        updateTemplateField({ commit, dispatch }, payload) {
            return TemplateFieldServices.updateTemplateField(payload).then(
                (response) => {
                    return Promise.resolve(response);
                },
                (error) => {
                    return Promise.reject(error);
                }
            );
        },
        // Change Template Field Status in database
        changeTemplateFieldStatus({ commit, dispatch }, payload) {
            return TemplateFieldServices.changeTemplateFieldStatus(
                payload
            ).then(
                (response) => {
                    return Promise.resolve(response);
                },
                (error) => {
                    return Promise.reject(error);
                }
            );
        },
        // Delete Template Field Status in database
        deleteTemplateField({ commit, dispatch }, payload) {
            return TemplateFieldServices.deleteTemplateField(payload).then(
                (response) => {
                    return Promise.resolve(response);
                },
                (error) => {
                    return Promise.reject(error);
                }
            );
        },
        // Get specific template all field data
        getTemplateAllFields({ commit }, payload) {
            return TemplateFieldServices.getTemplateAllFields(payload).then(
                (response) => {
                    if (response.status == true || response.status == 200) {
                        commit("UPDATE_TEMPLATE_ALL_FIELDS", response.data);
                    }
                    return Promise.resolve(response);
                },
                (error) => {
                    commit("UPDATE_TEMPLATE_ALL_FIELDS", []);
                    return Promise.reject(error);
                }
            );
        },
    },
};
