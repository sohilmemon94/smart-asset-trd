import TemplateServices from '@/services/templateServices';


export default {
  namespaced: true,
  state: {
    templateList: {
      templates: {
        data: [],
        to: 0,
        from: 0
      }
    },
    userTemplateOptions: [],
    cloneTemplateId: '',
    templates: [],
    storeTemplate: {
      template_name: '',
      template_description: '',
      template_status: '',
      created_by: ''
    },
    defaultTemplate: {
      template_id: '',
      template_name: '',
      template_description: '',
      template_is_default: 'Y',
      template_status: 'Y',
      created_by: ''
    },
    templateData: {},
    userListOverlay: false,
    templateListOverlay: false,
    editMode: false,
    updateId: null,
    modalButtonName: 'Add',
    viewMode: false,
  },
  mutations: {
    UPDATE_STORE_TEMPLATE_MUTATION(state, payload) {
      state.storeTemplate.template_name = payload.template_name;
      state.storeTemplate.template_description = payload.template_description;
      state.storeTemplate.template_status = (payload.template_status == 1) ? true : false;
      state.storeTemplate.created_by = payload.created_by;
    },
    UPDATE_DEFAULT_TEMPLATE(state, payload) {
      let template_status;
      if (payload.template_status == 'Y' || payload.template_status == true) {
        template_status = true;
      } else if (payload.template_status == 'N') {
        template_status = false;
      }
      state.defaultTemplate.template_id = payload.template_id;
      state.defaultTemplate.template_name = payload.template_name;
      state.defaultTemplate.template_description = payload.template_description;
      state.defaultTemplate.template_is_default = payload.template_is_default;
      state.defaultTemplate.template_status = template_status;
      state.defaultTemplate.created_by = payload.created_by;
    },
    UPDATE_TEMPLATES_MUTATION(state, payload) {
      state.templates = payload;
    },
    UPDATE_TEMPLATE_LIST_MUTATION(state, payload) {
      state.templateListOverlay = payload;
    },
    UPDATE_TEMPLATE_LIST(state, val) {
      state.templateList = val
    },
    UPDATE_CLONE_TEMPLATE_ID(state, val) {
      state.cloneTemplateId = val
    },
    UPDATE_USER_TEMPLATE_OPTIONS(state, val) {
      state.userTemplateOptions = val
    },
    UPDATE_TEMPLATE_DATA(state, val) {
      state.templateData = val
    },
    UPDATE_MODAL_BUTTON_NAME(state, val) {
      state.modalButtonName = val
    },
    UPDATE_EDIT_MODE(state, val) {
      state.editMode = val
    },
    UPDATE_VIEW_MODE(state, val) {
      state.viewMode = val
    },
  },
  actions: {
    // Get all template data with filter in database
    getAllTemplate({ commit }, payload) {
      return TemplateServices.getAllTemplate(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_TEMPLATE_LIST', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          //commit('UPDATE_TEMPLATE_LIST', {});
          return Promise.reject(error);
        }
      );
    },
    getUserTemplateOptions({ commit }) {
      return TemplateServices.getUserTemplateOptions().then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_USER_TEMPLATE_OPTIONS', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          commit('UPDATE_USER_TEMPLATE_OPTIONS', []);
          return Promise.reject(error);
        }
      );
    },
    SAVE_TEMPLATES_ACTION({ commit }, payload) {
      return TemplateServices.saveTemplate(payload).then(
        response => {
          commit('UPDATE_STORE_TEMPLATE_MUTATION', {
            Template_name: '',
            Template_uuid: '',
            Template_description: '',
            Template_status: '',
            created_by: ''
          });
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      )
    },
    saveTemplate({ commit, dispatch }, payload) {
      return TemplateServices.saveTemplate(payload).then(
        response => {
          dispatch('getAllTemplate', { perPage: 25, sortBy: 'template_id', sortDesc: 'true', page: '1', templateIsDefault: 'Y' });
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      )
    },
    // Get specific template data
    getTemplateData({ commit }, payload) {
      return TemplateServices.getTemplateData(payload).then(
        response => {
          if (response.status == true || response.status == 200) {
            commit('UPDATE_TEMPLATE_DATA', response.data);
            commit('UPDATE_DEFAULT_TEMPLATE', response.data);
          }
          return Promise.resolve(response);
        },
        error => {
          commit('UPDATE_TEMPLATE_DATA', {});
          commit('UPDATE_DEFAULT_TEMPLATE', {});
          return Promise.reject(error);
        }
      );
    },

    // update user data in database
    updateTemplate({ commit, dispatch }, payload) {
      return TemplateServices.updateTemplateData(payload).then(
        response => {
          if (response.status == true || response.status == 200) {

          }
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },

  },
  getters: {
    templateList: state => {
      const { templateList } = state
      return templateList;
    },
    cloneTemplateId: state => {
      const { cloneTemplateId } = state
      return cloneTemplateId;
    },
    userTemplateOptions: state => {
      const { userTemplateOptions } = state
      return userTemplateOptions;
    },
    templates(state) {
      return state.templates;
    },
    GET_TEMPLATE_LIST_OVERLAY(state) {
      return state.templateListOverlay;
    },
    templateData: state => {
      const { templateData } = state
      return templateData;
    },
    modalButtonName: state => {
      const { modalButtonName } = state
      return modalButtonName;
    },
    editMode: state => {
      const { editMode } = state
      return editMode;
    },
    defaultTemplate: state => {
      const { defaultTemplate } = state
      return defaultTemplate;
    },
    viewMode: state => {
      const { viewMode } = state
      return viewMode;
    },
  },

}
