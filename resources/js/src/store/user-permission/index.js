import UserPermissionServices from "@/services/userPermissionServices";

export default {
    namespaced: true,
    state: {
        companyAllUser: [],
    },
    getters: {
        companyAllUser(state) {
            const { companyAllUser } = state;
            return companyAllUser;
        },
    },
    mutations: {
        UPDATE_COMPANY_ALL_USER(state, payload) {
            state.companyAllUser = payload;
        },
    },
    actions: {
        // Get all user data from company in database
        getCompanyAllUser({ commit }) {
            return UserPermissionServices.getCompanyAllUser().then(
                (response) => {
                    if (response.status == true || response.status == 200) {
                        commit("UPDATE_COMPANY_ALL_USER", response.data);
                    }
                    return Promise.resolve(response);
                },
                (error) => {
                    return Promise.reject(error);
                }
            );
        },

        // Save User wise Category/Module Permission data in database
        saveUserPermission({ commit, dispatch }, payload) {
            return UserPermissionServices.saveUserPermission(payload).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
    },
};
