import { $themeConfig } from '@themeConfig'
import siteManagerServices from '@/services/siteManagerServices'

export default {
    namespaced: true,
    state: {
        isVerticalMenuCollapsed: $themeConfig.layout.menu.isCollapsed,
        navMenuItems: [],
        superAdminMenuData: [{
            title: 'Dashboards',
            icon: 'HomeIcon',
            route: 'admin-dashboard',
        },
        // {
        //     title: 'Report',
        //     icon: 'PieChartIcon',
        //     route: 'admin-report',
        // },
        {
            title: 'Super Admin Users',
            icon: 'UserIcon',
            route: 'admin-user-list',
        },
        {
            title: 'Clients',
            icon: 'TrelloIcon',
            route: 'admin-company-list',
        },
        {
            title: 'Client Admin Users',
            icon: 'UsersIcon',
            route: 'admin-client-list',
        },
        {
            title: 'Categories',
            icon: 'LayersIcon',
            route: 'admin-category-list',
        },
        {
            title: 'Modules',
            // icon: 'ClipboardIcon',
            route: 'admin-module-list',
        },
        {
            title: 'Templates',
            icon: 'LayoutIcon',
            route: 'admin-template-list'
        },
        {
            title: 'Error Log',
            icon: 'AlertOctagonIcon',
            route: 'admin-error-list'
        },
        {
            title: 'Support',
            icon: 'MessageCircleIcon',
            href: 'https://www.smartassettag.com.au/support-portal',
        }
        ],
        clientAdminMenuData: [{
            title: 'Dashboards',
            icon: 'HomeIcon',
            route: 'client-dashboard',
        },
        // {
        //     title: 'Report',
        //     icon: 'PieChartIcon',
        //     route: 'client-report',
        // },
        {
            title: 'Users',
            icon: 'UserIcon',
            route: 'client-user-list'
        },
        {
            title: 'Users Permission',
            icon: 'ShieldIcon',
            route: 'client-user-permission'
        },
        {
            title: 'Locations',
            icon: 'LayersIcon',
            route: 'client-site-list'
        },
        {
            title: 'Areas',
            icon: 'MapPinIcon',
            route: 'client-sub-site-list'
        },
        {
            title: 'Categories',
            icon: 'LayersIcon',
            route: 'client-category-list'
        },
        {
            title: 'Modules',
            // icon: 'ClipboardIcon',
            route: 'client-module-list'
        },
        {
            title: 'Templates',
            icon: 'LayoutIcon',
            route: 'client-template-list'
        },
        {
            title: 'Support',
            icon: 'MessageCircleIcon',
            href: 'https://www.smartassettag.com.au/support-portal',
        }
        ],
        resetClientAdminMenuData: [{
            title: 'Dashboards',
            icon: 'HomeIcon',
            route: 'client-dashboard',
        },
        // {
        //     title: 'Report',
        //     icon: 'PieChartIcon',
        //     route: 'client-report',
        // },
        {
            title: 'Users',
            icon: 'UserIcon',
            route: 'client-user-list'
        },
        {
            title: 'Users Permission',
            icon: 'ShieldIcon',
            route: 'client-user-permission'
        },
        {
            title: 'Locations',
            icon: 'LayersIcon',
            route: 'client-site-list'
        },
        {
            title: 'Areas',
            icon: 'MapPinIcon',
            route: 'client-sub-site-list'
        },
        {
            title: 'Categories',
            icon: 'LayersIcon',
            route: 'client-category-list'
        },
        {
            title: 'Modules',
            // icon: 'ClipboardIcon',
            route: 'client-module-list'
        },
        {
            title: 'Templates',
            icon: 'LayoutIcon',
            route: 'client-template-list'
        },
        // {
        //     title: 'Users',
        //     icon: 'UserIcon',
        //     route: 'client-user-list'
        // },
        {
            title: 'Support',
            icon: 'MessageCircleIcon',
            href: 'https://www.smartassettag.com.au/support-portal',
        }
        ],
        siteManagerMenuData: [{
            title: 'Dashboards',
            icon: 'HomeIcon',
            route: 'dashboard',
        },
            // {
            //     title: 'Report',
            //     icon: 'PieChartIcon',
            //     route: 'report',
            // }
        ],
        menuChange: false,
    },
    getters: {
        navMenuItems: state => {
            const { navMenuItems } = state
            return navMenuItems;
        },
        superAdminMenuData: state => {
            const { superAdminMenuData } = state
            return superAdminMenuData;
        },
        clientAdminMenuData: state => {
            const { clientAdminMenuData } = state
            return clientAdminMenuData;
        },
        siteManagerMenuData: state => {
            const { siteManagerMenuData } = state
            return siteManagerMenuData;
        },
        menuChange: state => {
            const { menuChange } = state
            return menuChange;
        }
    },
    mutations: {
        UPDATE_VERTICAL_MENU_COLLAPSED(state, val) {
            state.isVerticalMenuCollapsed = val
        },
        UPDATE_NAV_MENU_ITEMS(state, val) {
            state.navMenuItems = val
        },
        UPDATE_SUPER_ADMIN_MENU_DATA(state, val) {
            state.superAdminMenuData = val
        },
        UPDATE_CLIENT_ADMIN_MENU_DATA(state, { data, index }) {
            state.clientAdminMenuData.splice(index, 0, ...data);
        },
        RESET_CLIENT_ADMIN_MENU_DATA(state, val) {
            state.resetClientAdminMenuData = val
        },
        // UPDATE_CLIENT_ADMIN_MENU_DATA(state, val) {
        //     state.clientAdminMenuData = val
        // },
        UPDATE_SITE_MANAGER_MENU_DATA(state, val) {
            state.siteManagerMenuData = val
        },
        UPDATE_SITE_MANAGER_DYNAMIC_MENU_DATA(state, val) {
            state.siteManagerMenuData = state.siteManagerMenuData.concat(val);
        },
        RESET_SITE_MANAGER_DYNAMIC_MENU_DATA(state) {
            state.siteManagerMenuData = [{
                title: 'Dashboards',
                icon: 'HomeIcon',
                route: 'dashboard',
            },
                // {
                //     title: 'Report',
                //     icon: 'PieChartIcon',
                //     route: 'report',
                // }
            ];
        },
        UPDATE_MENU_CHANGE(state, val) {
            state.menuChange = val
        }
    },
    actions: {
        updateSiteManagerVerticalMenu({ commit }) {
            return siteManagerServices.getSiteManagerDynamicMenu({}).then((response) => {
                commit('UPDATE_SITE_MANAGER_DYNAMIC_MENU_DATA', response.data)
                commit('UPDATE_MENU_CHANGE', true)
                return Promise.resolve(response);
            }).catch((error) => {
                return Promise.reject(error);
            });
        },

    },
}
