<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $head }}</title>
    <style>
        @page {
            margin: 10px;
            size: A4;
            /*or width x height 150mm 50mm*/
        }

        body {
            margin: 24px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin: 0 auto;
            font-family: Arial, Helvetica, sans-serif;
        }

        table tr td,
        table tr th {
            padding: 0px 8px;
            margin: 0;
            box-sizing: border-box;
        }

        table.data-table tr td {
            padding: 0px 12px;
            margin: 0;
            box-sizing: border-box;
        }

        table,
        th,
        td {
            border: 1px solid #f1f2f3;
        }

        .report-brand,
        .report-brand li {
            padding: 0;
            margin: 0;
            /* margin-top: 12px; */
        }

        .report-brand li {
            display: inline-block;
            vertical-align: middle;
        }

        .report-brand-name {
            font-size: 32px;
            padding-left: 10px;
            color: #005abe;
        }

        @media(max-width:767px) {
            table {
                width: 100%;
                overflow-x: auto;
            }
        }
    </style>
</head>

<body>
    <table style="border-bottom:none !important;">
        <thead>
            <tr style="">
                <td style="border:none; padding-top: 5px; width:50%">
                    <h3 style="margin:0px;">{{ $head }}</h3>
                </td>
                <td rowspan="2" style="border:none; text-align:right; width:50%">
                    <ul class="report-brand">
                        <li><img src="{{ url($clientLogo) }}" width="60px"></li>
                    </ul>
                </td>
            </tr>
            <tr style="">
                <td style="border:none;padding-top: 5px;">
                    <p style="margin:0px"><strong>As on:</strong> {{ $datetime }}</p>
                </td>
            </tr>
        </thead>
    </table>
    <table style="font-size: 12px;" class="data-table">
        <thead style="background:#cccccc;">
            <tr>
                <th colspan="12">Current Record</th>
            </tr>
            <tr>
                <th>Row No.</th>
                @foreach ($columns as $column)
                    @if ($column['key'] != 'actions' && $column['key'] != 'row')
                        <th>{{ $column['label'] }}</th>
                    @endif
                @endforeach
            </tr>
        </thead>
        <tbody>
            @forelse ($newData as $item)
                <tr style="background-color:#ffffff ;">
                    <td style="text-align:center;">{{ $loop->iteration }}</td>
                    @foreach ($columns as $column)
                        <?php $key = $column['key']; ?>
                        @if ($key != 'actions' && $key != 'row')
                            @if ($key == 'q')
                                <td style="text-align: center; width: 90px;">
                                    @if ($item[$key] == 'rgb(255,0,0)')
                                        <img src="{{ url('pdf_image/logo/jan_mar.png') }}" width="80px">
                                    @elseif ($item[$key] == 'rgb(76,175,80)')
                                        <img src="{{ url('pdf_image/logo/apr_jun.png') }}" width="80px">
                                    @elseif ($item[$key] == 'rgb(0,0,255)')
                                        <img src="{{ url('pdf_image/logo/jul_sep.png') }}" width="80px">
                                    @elseif ($item[$key] == 'rgb(255,255,0)')
                                        <img src="{{ url('pdf_image/logo/oct_dec.png') }}" width="80px">
                                    @elseif ($item[$key] == 'rgb(128,128,128)')
                                        <img src="{{ url('pdf_image/logo/other.png') }}" width="80px">
                                    @else
                                        N/A
                                    @endif

                                </td>
                            @elseif ($key == 'status')
                                <td>
                                    @if ($item[$key] == 'Pass')
                                        <span style="color:#64c832">Pass</span>
                                    @elseif ($item[$key] == 'Fail')
                                        <span style="color:#ea5455">Fail</span>
                                    @elseif ($item[$key] == 'Inspection Due')
                                        <span style="color:#82868b">Inspection Due</span>
                                    @else
                                        <span style="color:#ff9f43">Maintenance Required</span>
                                    @endif
                                </td>
                            @else
                                <td <?php if ($key == 'expires' || $key == 'last_inspection') {
                                    echo "style='text-align: center'";
                                } ?>>
                                    @if (isset($item[$key]) && !empty($item[$key]))
                                        {{ $item[$key] }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                            @endif
                        @endif
                    @endforeach
                </tr>
            @empty
                <tr>
                    <td colspan="12" style="text-align:center;">No Record Found</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    <table style="font-size: 12px;" class="data-table">
        <thead style="background:#cccccc;">
            <tr>
                <th colspan="12">Previous Record</th>
            </tr>
            <tr>
                <th>Row No.</th>
                @foreach ($columns as $column)
                    @if ($column['key'] != 'actions' && $column['key'] != 'row')
                        <th>{{ $column['label'] }}</th>
                    @endif
                @endforeach
            </tr>
        </thead>
        <tbody>
            @forelse ($oldData as $item)
                <tr style="background-color:#ffffff ;">
                    <td style="text-align:center;">{{ $loop->iteration }}</td>
                    @foreach ($columns as $column)
                        <?php $key = $column['key']; ?>
                        @if ($key != 'actions' && $key != 'row')
                            @if ($key == 'q')
                                <td style="text-align: center; width: 90px;">
                                    @if ($item[$key] == 'rgb(255,0,0)')
                                        <img src="{{ url('pdf_image/logo/jan_mar.png') }}" width="80px">
                                    @elseif ($item[$key] == 'rgb(76,175,80)')
                                        <img src="{{ url('pdf_image/logo/apr_jun.png') }}" width="80px">
                                    @elseif ($item[$key] == 'rgb(0,0,255)')
                                        <img src="{{ url('pdf_image/logo/jul_sep.png') }}" width="80px">
                                    @elseif ($item[$key] == 'rgb(255,255,0)')
                                        <img src="{{ url('pdf_image/logo/oct_dec.png') }}" width="80px">
                                    @elseif ($item[$key] == 'rgb(128,128,128)')
                                        <img src="{{ url('pdf_image/logo/other.png') }}" width="80px">
                                    @else
                                        N/A
                                    @endif

                                </td>
                            @elseif ($key == 'status')
                                <td>
                                    @if ($item[$key] == 'Pass')
                                        <span style="color:#64c832">Pass</span>
                                    @elseif ($item[$key] == 'Fail')
                                        <span style="color:#ea5455">Fail</span>
                                    @elseif ($item[$key] == 'Inspection Due')
                                        <span style="color:#82868b">Inspection Due</span>
                                    @else
                                        <span style="color:#ff9f43">Maintenance Required</span>
                                    @endif
                                </td>
                            @else
                                <td <?php if ($key == 'expires' || $key == 'last_inspection') {
                                    echo "style='text-align: center'";
                                } ?>>
                                    @if (isset($item[$key]) && !empty($item[$key]))
                                        {{ $item[$key] }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                            @endif
                        @endif
                    @endforeach
                </tr>
            @empty
                <tr>
                    <td colspan="12" style="text-align:center;">No Record Found</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</body>

</html>
