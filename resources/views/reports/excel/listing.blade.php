<table>
    <thead style="background:#cccccc;">
        <tr>
            <th>Row No.</th>
            @foreach ($columns as $column)
                @if ($column->key != 'actions' && $column->key != 'row')
                    <th>{{ $column->label }}</th>
                @endif
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($items as $item)
            <tr style="background-color:#ffffff ;">
                <td style="text-align:center;">{{ $loop->iteration }}</td>
                @foreach ($columns as $column)
                    <?php $key = $column->key; ?>
                    @if ($key=='q' || $key=='asset_q_color')
                        <td>
                            @if ($key=='q')
                                @if ($item->$key == 7)
                                <span>Weekly</span>
                                @elseif ($item->$key == 30)
                                    <span>Monthly</span>
                                @elseif ($item->$key == 90)
                                    <span>Every 3 Months</span>
                                @elseif ($item->$key == 'RGBY')
                                    <span>Quarterly with RGBY</span>
                                @elseif ($item->$key == 180)
                                    <span>Every 6 Months</span>
                                @elseif ($item->$key == 360)
                                    <span>Every 12 Months</span>
                                @elseif ($item->$key == 540)
                                    <span>Every 18 Months</span>
                                @elseif ($item->$key == 720)
                                    <span>Every 24 Months</span>
                                @else
                                    N/A
                                @endif
                            @else
                                @if ($item->$key == 'rgb(255,0,0)')
                                Jan-Mar
                                @elseif ($item->$key == 'rgb(76,175,80)')
                                    Apr-Jun
                                @elseif ($item->$key == 'rgb(0,0,255)')
                                    Jul-Sep
                                @elseif ($item->$key == 'rgb(255,255,0)')
                                    Oct-Dec
                                @elseif ($item->$key == 'rgb(128,128,128)')
                                    Other
                                @else
                                    N/A
                                @endif
                            @endif
                        </td>
                    @endif
                    @if ($key != 'actions' && $key != 'q' && $key != 'asset_q_color' && $key != 'row')
                        <td>
                            @if ($key == 'status')
                                @if ($item->$key == 'Pass')
                                    <span style="color:#64c832">Pass</span>
                                @elseif ($item->$key == 'Fail')
                                    <span style="color:#ea5455">Fail</span>
                                @elseif ($item->$key == 'Inspection Due')
                                    <span style="color:#82868b">Inspection Due</span>
                                @else
                                    <span style="color:#ff9f43">Maintenance Required</span>
                                @endif
                            @else
                                @if (isset($item->$key) && !empty($item->$key))
                                    {{ $item->$key }}
                                @else
                                    N/A
                                @endif
                            @endif
                        </td>
                    @endif
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
