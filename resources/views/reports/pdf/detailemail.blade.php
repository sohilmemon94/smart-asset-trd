<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Asset Info</title>
    <style>
        @page {
            margin: 10px;
            size: A4;
            /*or width x height 150mm 50mm*/
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        table {
            width: 100%;
            box-sizing: border-box;
            margin: 0 auto;
            margin-bottom: 0px;
            padding: 0;
            border: 0px;
            border: 1px solid #f1f2f3;

        }

        table tr td,
        table tr th {
            padding: 8px 15px;
            margin: 0;
            border: none;
            box-sizing: border-box;
        }

        table,
        th,
        td {
            padding: 0;
        }

        table tr,
        table tbody {
            margin: 0px;
        }

        table tr td {
            margin-bottom: 0px;
        }

        table td p {
            margin-bottom: 0px;
            margin-top: 0px;
            white-space: normal;
            word-break: break-all;
            font-size: 10px
        }
        .report-brand , .report-brand li {
            padding: 0;
            margin: 0;
        }
        .report-brand li {
            display : inline-block;
            vertical-align: middle;
        }
        .report-brand-name {
            font-size: 32px;
            padding-left: 10px;
            color: #005abe;
        }

        .badge {
            display: inline-block;
            font-size: 85%;
            font-weight: 600;
            line-height: 1;
            white-space: nowrap;
            vertical-align: baseline;
            /* padding: 0.3rem 0.5rem; */
            padding: 2px 4px;
            text-align: center;
            color: #fff;
            border-radius: 2px;
        }

        .badge-success {
            background-color: #28a745;
        }

        .badge-danger {
            background-color: #dc3545;
        }

        .badge-warning {
            background-color: #ffc107;
        }

        .badge-secondary {
            background-color: #6c757d;
        }

        @media(max-width:767px) {
            table {
                width: 100%;
                overflow-x: auto;
            }
        }
    </style>
</head>

<body>
    <table style="border-bottom:none !important;">
        <thead>
            <tr style="">
                <td style="border:none; padding-top: 5px;">
                    <h3 style="margin:0px;">Asset Info</h3>
                </td>
                <td rowspan="2" style="border:none; text-align:right">
                    @if ($isClientAdmin || $isUserAdmin)
                        <ul class="report-brand">
                            <li><img src="{{url($clientLogo)}}" width="60px"></li>
                            {{-- <li><span class="report-brand-name">{{$clientCompanyName}}</span></li> --}}
                        </ul>
                    @else
                        <img src="{{ url('pdf_image/logo/logo.png') }}" width="180px">
                    @endif
                </td>
            </tr>
            <tr style="">
                <td style="border:none;padding-top: 5px;">
                    <p style="margin:0px"><strong>As on:</strong> {{ $datetime }}</p>
                </td>
            </tr>
        </thead>
    </table>
    <table style="border-radius: 4px 4px 0 0; margin:0px;">
        <tbody>
            <tr>
                <td>
                    <p style="color: #005abe; font-weight: bold; margin-bottom:0px;">Location:</p>
                    <p>{{ $data->location }}</p>
                </td>

                <td>
                    <p style="color: #005abe; font-weight: bold; margin-bottom:0px;">Area:</p>
                    <p>{{ $data->area }}</p>
                </td>

                <td>
                    <p style="color: #005abe; font-weight: bold; margin-bottom:0px;">Asset ID:</p>
                    <p>{{ $data->asset_uid }}</p>
                </td>
                <td>
                    <p style="color: #005abe; font-weight: bold; margin-bottom: 5px">Status:</p>
                    <p>
                        @if ($data->status == 'Pass')
                            <span class="badge badge-success">Pass</span>
                        @elseif ($data->status == 'Fail')
                            <span class="badge badge-danger">Fail</span>
                        @elseif ($data->status == 'Maintenance Required')
                            <span class="badge badge-warning">Maintenance Required</span>
                        @else
                            <span class="badge badge-secondary">Inspection Pending</span>
                        @endif
                    </p>
                </td>
                {{-- @foreach (array_slice($data->dynamic_fields,0,2) as $field)
                <td>
                    <p style="color: #005abe; font-weight: bold; margin-bottom:0px;">{{$field->isColor?'Inspection Schedule':$field->label}}:</p>
                    @if ($field->isColor)
                        @if ($field->value == 'rgb(255,0,0)')
                            <img src="{{ url('pdf_image/logo/jan_mar.png') }}" width="80px">
                        @elseif ($field->value == 'rgb(76,175,80)')
                            <img src="{{ url('pdf_image/logo/apr_jun.png') }}" width="80px">
                        @elseif ($field->value == 'rgb(0,0,255)')
                            <img src="{{ url('pdf_image/logo/jul_sep.png') }}" width="80px">
                        @elseif ($field->value == 'rgb(255,255,0)')
                            <img src="{{ url('pdf_image/logo/oct_dec.png') }}" width="80px">
                        @elseif ($field->value == 'rgb(128,128,128)')
                            <img src="{{ url('pdf_image/logo/other.png') }}" width="80px">
                        @else
                            N/A
                        @endif
                    @else
                        <p>{{$field->value}}</p>
                    @endif
                </td>
                @endforeach --}}
            </tr>
            <?php $limit = (int)((count($data->dynamic_fields))/4)+1; ?>
            @for ($i=1; $i<=$limit; $i++)
                <tr>
                    @foreach (array_slice($data->dynamic_fields, ($i-1)*4, 4) as $field)
                    <td>
                        <p style="color: #005abe; font-weight: bold; margin-bottom:0px;">{{$field->isColor?'Inspection Schedule':$field->label}}:</p>
                        @if ($field->isColor)
                            @if ($field->value == 'rgb(255,0,0)')
                                <img src="{{ url('pdf_image/logo/jan_mar.png') }}" width="80px">
                            @elseif ($field->value == 'rgb(76,175,80)')
                                <img src="{{ url('pdf_image/logo/apr_jun.png') }}" width="80px">
                            @elseif ($field->value == 'rgb(0,0,255)')
                                <img src="{{ url('pdf_image/logo/jul_sep.png') }}" width="80px">
                            @elseif ($field->value == 'rgb(255,255,0)')
                                <img src="{{ url('pdf_image/logo/oct_dec.png') }}" width="80px">
                            @elseif ($field->value == 'rgb(128,128,128)')
                                <img src="{{ url('pdf_image/logo/other.png') }}" width="80px">
                            @else
                                N/A
                            @endif
                        @else
                            <p>{{$field->value}}</p>
                        @endif
                    </td>
                    @endforeach
                </tr>
            @endfor
            <tr>
                <td>
                    <p style="color: #005abe; font-weight: bold; margin-bottom:0px;">Last Inspection:</p>
                    <p>{{ $data->last_inspection }}</p>
                </td>
                <td>
                    <p style="color: #005abe; font-weight: bold; margin-bottom:0px;">Last Inspection By:</p>
                    <p>{{ $data->last_inspection_by ?? '' }}</p>
                </td>
                <td>
                    <p style="color: #005abe; font-weight: bold; margin-bottom:0px;">Last Inspection Notes:</p>
                    <p>{{ $data->last_inspection_notes ?? '' }}</p>
                </td>
                <td>
                    <p style="color: #005abe; font-weight: bold; margin-bottom:5px;">Last Inspection Result:</p>
                    {{-- <p>{{ $data->last_inspection_result ?? '' }}</p> --}}
                    <p>
                        @if ($data->last_inspection_result == 'Pass')
                            <span class="badge badge-success">Pass</span>
                        @elseif ($data->last_inspection_result == 'Fail')
                            <span class="badge badge-danger">Fail</span>
                        @elseif ($data->last_inspection_result == 'Maintenance Required')
                            <span class="badge badge-warning">Maintenance Required</span>
                        @else
                            <span class="badge badge-secondary">Inspection Pending</span>
                        @endif
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="color: #005abe; font-weight: bold; margin-bottom:0px;">Inspection Schedule:</p>
                    @if ($data->inspection_schedule==7)
                        <p>Weekly</p>
                    @elseif ($data->inspection_schedule==30)
                        <p>Monthly</p>
                    @elseif ($data->inspection_schedule==90)
                        <p>Every 3 Months</p>
                    @elseif ($data->inspection_schedule=='RGBY')
                        <p>Quarterly with RGBY</p>
                    @elseif ($data->inspection_schedule==180)
                        <p>Every 6 Months</p>
                    @elseif ($data->inspection_schedule==360)
                        <p>Every 12 Months</p>
                    @elseif ($data->inspection_schedule==540)
                        <p>Every 18 Months</p>
                    @elseif ($data->inspection_schedule==720)
                        <p>Every 24 Months</p>
                    @else
                        <p>N/A</p>
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
    <table style="border-radius: 4px 4px 0 0; margin:0px; padding-bottom: 40px; width: 600px;" width="600">
        <tbody>
            <tr>
                <td>
                    <h5 style="color: #005abe; font-weight: bold; margin-bottom:30px;">Asset Photo</h5>
                </td>
                <td>
                    <h5 style="color: #005abe; font-weight: bold; margin-bottom:30px;">Last Inspection Photo</h5>
                </td>
            </tr>
            <tr>
                <td style="padding: 0;">
                    <div class="border text-center">
                        @if ($data->photo)
                            <img src="{{ $data->photo }}" width="400">
                        @else
                            <p>Not Available</p>
                        @endif
                    </div>
                </td>
                <td>
                    <div class="border text-center">
                        @if ($data->last_inspection_photo)
                            <img src="{{ $data->last_inspection_photo }}" width="400">
                        @else
                            <p>Not Available</p>
                        @endif
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="border-radius: 4px 4px 0 0; margin:0px;">
        <tbody style="margin: 0;">
            <tr>
                <td style="width:50%; vertical-align:0px">
                    <h5 style="color: #005abe; font-weight: bold; margin-bottom:0px;">Location</h5>
                    <h6>
                        @if ($data->latitude && !empty($data->latitude))
                            <?php
                            // $src = 'abv.com';
                            // $api_key = env('GOOGLE_MAPS_API_KEY');
                            // $latlong = explode('#', $data->loc);
                            // $latitude = $latlong[0];
                            // $longitude = $latlong[1];
                            $latitude = $data->latitude;
                            $longitude = $data->longitude;
                            $src = 'http://www.google.com/maps/place/' . $latitude . ',' . $longitude . '/' . @$latitude . ',' . $longitude . ',17z';
                            ?>
                            <a href="{{ $src }}" target="_blank" style="color: #28a745;">Click here to view the asset location</a>
                        @else
                            <a role="button">Not Available</a>
                        @endif
                    </h6>
                </td>

                <td style="width:50%">
                    <p style="color: #005abe; font-weight: bold; font-size: 14px; margin-bottom:0px;">Documents:</p>
                    <p>
                        @forelse ($data->documents as $doc)
                            {{-- {{ pathinfo($doc->url)['extension'] }} --}}
                            @if (!empty($doc->url) && in_array(pathinfo($doc->url)['extension'], ['pdf', 'jpg', 'png', 'jpeg']))
                                <br><a href="{{$doc->url}}" target="_blank" style="color: #28a745;">{{$doc->name}}</a><br>
                            @else
                                <br><a href="{{$doc->url}}" download style="color: #28a745;">{{$doc->name}}</a><br>
                            @endif
                        @empty
                            <span>No Documents Found</span>
                        @endforelse
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
