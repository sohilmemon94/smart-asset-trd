<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $head }}</title>
    <style>
        @page {
            margin: 10px;
            size: A4;
            /*or width x height 150mm 50mm*/
        }

        body {
            margin: 24px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin: 0 auto;
            font-family: Arial, Helvetica, sans-serif;
        }

        table tr td,
        table tr th {
            padding: 0px 8px;
            margin: 0;
            box-sizing: border-box;
        }
        table.data-table tr td {
            padding: 0px 12px;
            margin: 0;
            box-sizing: border-box;
        }

        table,
        th,
        td {
            border: 1px solid #f1f2f3;
        }

        .report-brand , .report-brand li {
            padding: 0;
            margin: 0;
            /* margin-top: 12px; */
        }
        .report-brand li {
            display : inline-block;
            vertical-align: middle;
        }
        .report-brand-name {
            font-size: 32px;
            padding-left: 10px;
            color: #005abe;
        }

        @media(max-width:767px) {
            table {
                width: 100%;
                overflow-x: auto;
            }
        }
    </style>
</head>

<body>
    <table style="border-bottom:none !important;">
        <thead>
            <tr style="">
                <td style="border:none; padding-top: 5px; width:50%">
                    <h3 style="margin:0px;">{{ $head }}</h3>
                </td>
                
                <td rowspan="3" style="border:none; text-align:right; width:50%; padding-right: 30px;">
                    @if ($isClientAdmin || $isUserAdmin)
                        <img src="{{url($clientLogo)}}" width="130">
                    @else
                        <img src="{{url('pdf_image/logo/logo.png')}}" width="130" style="width: 130px; margin-bottom: 10px; padding-right: 10px;">
                    @endif
                </td>
            </tr>
            <tr style="">
                <td style="border:none;padding-top: 5px;">
                    <p style="margin:0px"><strong>As on:</strong> {{ $datetime }}</p>
                </td>
            </tr>
            <tr style="">
                <td style="padding-top: 5px; padding-bottom: 5px; border:none">
                    {{-- @if ($displayClient == true)
                        <span style="color:#005abe; font-size: 12px">{{$client}}&nbsp;&#x3e;&nbsp;</span>
                    @endif --}}
                    <p style="font-size: 14px; margin:0px">
                        Location: <span style="color:#005abe; margin-right: 20px"><?php if($location == 'All Locations') echo 'All'; else echo $location ?></span>
                        Area: <span style="color:#005abe;"><?php if($area == 'All Areas') echo 'All'; else echo $area; ?></span>
                    </p>
                    {{-- <span style="color:#005abe; font-size: 12px">{{$area}}</span> --}}
                </td>
            </tr>
        </thead>
    </table>
    <table style="font-size: 12px;" class="data-table">
        <thead style="background:#cccccc;">
            <tr>
                <th>Row No.</th>
                @foreach ($columns as $column)
                    @if ($column->key != 'actions' && $column->key != 'row')
                        <th>{{ $column->label }}</th>
                    @endif
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach ($items as $item)
                {{-- <tr>{{print_r($item)}}</tr> --}}
                <tr style="background-color:#ffffff ;">
                    {{-- @foreach ($item as $data)
                        <td style="text-align:center;">{{ $data }}</td>
                        @endforeach --}}
                    <td style="text-align:center;">{{ $loop->iteration }}</td>
                    @foreach ($columns as $column)
                        <?php $key = $column->key; ?>
                        @if ($key != 'actions' && $key != 'row')
                            {{-- <td style="text-align:center;">{{ $data }}</td> --}}
                            @if ($key == 'q' || $key == 'asset_q_color')
                                <td style="text-align: center; width: 90px;">
                                    @if ($key == 'q')
                                        @if ($item->$key == 7)
                                        <span>Weekly</span>
                                        @elseif ($item->$key == 30)
                                            <span>Monthly</span>
                                        @elseif ($item->$key == 90)
                                            <span>Every 3 Months</span>
                                        @elseif ($item->$key == 'RGBY')
                                            <span>Quarterly with RGBY</span>
                                        @elseif ($item->$key == 180)
                                            <span>Every 6 Months</span>
                                        @elseif ($item->$key == 360)
                                            <span>Every 12 Months</span>
                                        @elseif ($item->$key == 540)
                                            <span>Every 18 Months</span>
                                        @elseif ($item->$key == 720)
                                            <span>Every 24 Months</span>
                                        @else
                                            N/A
                                        @endif
                                    @else
                                        {{-- {{ $item->$key }} --}}
                                        @if ($item->$key == 'rgb(255,0,0)')
                                            <img src="{{url('pdf_image/logo/jan_mar.png')}}" width="70">
                                        @elseif ($item->$key == 'rgb(76,175,70)')
                                            <img src="{{url('pdf_image/logo/apr_jun.png')}}" width="70">
                                        @elseif ($item->$key == 'rgb(0,0,255)')
                                            <img src="{{url('pdf_image/logo/jul_sep.png')}}" width="70">
                                        @elseif ($item->$key == 'rgb(255,255,0)')
                                            <img src="{{url('pdf_image/logo/oct_dec.png')}}" width="70">
                                        @elseif ($item->$key == 'rgb(128,128,128)')
                                            <img src="{{url('pdf_image/logo/other.png')}}" width="70">
                                        @else
                                            N/A
                                        @endif
                                    @endif
                                </td>
                            @elseif ($key == 'status')
                                <td style="text-align:center;">
                                    @if ($item->$key == 'Pass')
                                        <span style="color:#64c832">Pass</span>
                                    @elseif ($item->$key == 'Fail')
                                        <span style="color:#ea5455">Fail</span>
                                    @elseif ($item->$key == 'Inspection Due')
                                        <span style="color:#82868b">Inspection Due</span>
                                    @else
                                        <span style="color:#ff9f43">Maintenance Required</span>
                                    @endif
                                </td>
                            @else
                                <td <?php if($key=='expires'||$key=='last_inspection') echo "style='text-align: center'"; ?>>
                                    @if (isset($item->$key) && !empty($item->$key))
                                        {{ $item->$key }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                            @endif
                        @endif
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
