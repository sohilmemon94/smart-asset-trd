<?php

use App\Http\Controllers\API\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\SiteController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\SubSiteController;
use App\Http\Controllers\API\TemplateController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\CompanyController;
use App\Http\Controllers\API\DashboardController;
use App\Http\Controllers\API\DashboardDetailAsset;
use App\Http\Controllers\API\DashboardDetailListing;
use App\Http\Controllers\API\ModuleController;
use App\Http\Controllers\API\TemplateFieldController;
use App\Http\Controllers\API\ModuleFieldController;
use App\Http\Controllers\API\ModuleQuestionController;
use App\Http\Controllers\API\SitePanelController;
use App\Http\Controllers\API\ConfinedSpaceController;

use App\Http\Controllers\MobileAPI\AuthController as MobileAuthController;
use App\Http\Controllers\MobileAPI\UserController as MobileUserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Mobile API's Start
// Route::group(['prefix' => 'mobile', 'middleware' => ['auth:api']], function(){
// End
Route::get('delete-fields',[ApiController::class, 'deletefields']);
Route::get('update-fields',[ApiController::class, 'updatefields']);


Route::group(['prefix' => 'auth'], function () {
	Route::post('login', [AuthController::class, 'login']);
	Route::post('register', [AuthController::class, 'register']);

	// password reset functionality
	Route::post('createPasswordReset', [AuthController::class, 'createPasswordReset']);
	Route::post('findTokenPasswordReset', [AuthController::class, 'findTokenPasswordReset']);
	Route::post('resetPassword', [AuthController::class, 'resetPassword']);
	Route::post('setPassword', [AuthController::class, 'setPassword']);

	Route::group(['middleware' => 'auth:api'], function () {
		Route::get('logout', [AuthController::class, 'logout']);
		Route::get('myProfile', [AuthController::class, 'myProfile']);
	});
});

Route::get('asset-info/{assetUUID}', [SitePanelController::class, 'getAssetInfo']);
Route::post('assetReportIssue', [SitePanelController::class, 'submitReportIssue']);

Route::group(['middleware' => ['auth:api', 'check_expired_token']], function () {
	Route::get('getAllAdminUser', [UserController::class, 'getAllAdminUser']);
	Route::get('getAllClientUser', [UserController::class, 'getAllClientUser']);
	Route::get('getAllSiteUser', [UserController::class, 'getAllSiteUser']);

	// Get all user from company
	Route::get('getCompanyAllUser', [UserController::class, 'getCompanyAllUser']);
	// Save user permission
	Route::post('saveUserPermission', [UserController::class, 'saveUserPermission']);

	Route::get('getAllRole', [UserController::class, 'getAllRole']);
	Route::get('getAllUserData', [UserController::class, 'getAllUserData']);
	Route::post('storeNewUser', [UserController::class, 'store']);
	Route::get('editUserData/{id}', [UserController::class, 'edit']);
	Route::get('editCompanyWithUserData/{id}', [UserController::class, 'editWithCompany']);
	Route::post('updateUserData/{id}', [UserController::class, 'update']);
	Route::post('makeUserToAdmin', [UserController::class, 'makeUserToAdmin']);
	Route::post('uploadUserAvatar/{id}', [UserController::class, 'uploadUserAvatar'])->name('uploadUserAvatar');
	Route::post('removeUserAvatar', [UserController::class, 'removeUserAvatar'])->name('removeUserAvatar');
	Route::post('deleteUser', [UserController::class, 'destroy'])->name('deleteUser');
	Route::post('changeUserStatus', [UserController::class, 'changeUserStatus'])->name('changeUserStatus');

	// site
	Route::resource('site', SiteController::class);
	Route::post('deleteSite', [SiteController::class, 'destroy'])->name('deleteSite');
	Route::post('changeSiteStatus', [SiteController::class, 'changeSiteStatus'])->name('changeSiteStatus');

	// subsite
	Route::resource('subsite', SubSiteController::class);
	Route::post('deleteSubsite', [SubSiteController::class, 'destroy'])->name('deleteSubsite');
	Route::post('changeSubSiteStatus', [SubSiteController::class, 'changeSubSiteStatus'])->name('changeSubSiteStatus');
	Route::post('changeSubSitePermission/{id}', [SubSiteController::class, 'subSitePermission'])->name('subSitePermission');
	Route::post('updateSubSiteUser/{id}', [SubSiteController::class, 'updateSubSiteUser'])->name('updateSubSiteUser');
	Route::post('deleteSubSiteUser/{id}', [SubSiteController::class, 'destroyUserPermission'])->name('destroyUserPermission');
	Route::post('saveSubSiteUser/{id}', [SubSiteController::class, 'userPermission'])->name('userPermission');
	Route::post('getSiteOnSubSite', [SubSiteController::class, 'getSiteOnSubSite'])->name('getSiteOnSubSite');
	Route::post('getUserOnSubSite', [SubSiteController::class, 'getUserOnSubSite'])->name('getUserOnSubSite');


	// template
	Route::resource('template', TemplateController::class);
	Route::get('getUserTemplateOptions', [TemplateController::class, 'getUserTemplateOptions']);
	Route::post('deleteTemplate', [TemplateController::class, 'destroy'])->name('deleteTemplate');
	Route::post('changeTemplateStatus', [TemplateController::class, 'changeTemplateStatus'])->name('changeTemplateStatus');
	Route::post('duplicateTemplate', [TemplateController::class, 'duplicateTemplate'])->name('duplicateTemplate');

	// Template Field
	Route::resource('templateField', TemplateFieldController::class);
	Route::get('getAllInputField', [TemplateFieldController::class, 'getAllInputField']);
	Route::post('changeTemplateFieldStatus', [TemplateFieldController::class, 'changeTemplateFieldStatus'])->name('changeTemplateFieldStatus');
	Route::post('deleteTemplateField', [TemplateFieldController::class, 'destroy'])->name('deleteTemplateField');
	Route::get('getTemplateAllFields', [TemplateFieldController::class, 'getTemplateAllFields']);

	// category
	Route::resource('category', CategoryController::class);
	Route::get('getAllCategory', [CategoryController::class, 'getAllCategory']);
	Route::post('deleteCategory', [CategoryController::class, 'destroy'])->name('deleteCategory');
    Route::post('changeCategoryStatus', [CategoryController::class, 'changeCategoryStatus'])->name('changeCategoryStatus');
    Route::get('categoryModule', [CategoryController::class, 'relatedCategoryModule'])->name('relatedCategoryModule');
	Route::get('getDefaultCategoryList', [CategoryController::class, 'getDefaultCategoryList'])->name('getDefaultCategoryList');
    Route::get('selectedCategory', [CategoryController::class, 'subSiteSelectedCatMod'])->name('subSiteSelectedCatMod');

    //company
    Route::get('company', [CompanyController::class, 'index']);
	Route::post('storeCompany', [CompanyController::class, 'store']);
	Route::get('company/{id}/edit', [CompanyController::class, 'edit']);
	Route::post('company/{id}', [CompanyController::class, 'update']);
    Route::post('uploadCompanyAvatar/{id}', [CompanyController::class, 'uploadCompanyAvatar'])->name('uploadCompanyAvatar');
	Route::post('removeCompanyAvatar', [CompanyController::class, 'removeCompanyAvatar'])->name('removeCompanyAvatar');
	Route::get('getDefaultCompanyList', [CompanyController::class, 'getAllCompany'])->name('getDefaultCompanyList');
	Route::post('deleteCompany', [CompanyController::class, 'destroy'])->name('deleteCompany');
    Route::post('changeCompanyStatus', [CompanyController::class, 'changeCompanyStatus'])->name('changeCompanyStatus');


	// module
	//Route::resource('module', ModuleController::class);
    Route::get('module', [ModuleController::class, 'index']);
	Route::post('modules', [ModuleController::class, 'store']);
	Route::get('module/{id}/edit', [ModuleController::class, 'edit']);
	Route::post('module/{id}', [ModuleController::class, 'update']);
	Route::post('uploadModuleIcon', [ModuleController::class, 'uploadModuleIcon']);
	Route::post('removeModuleIcon', [ModuleController::class, 'removeModuleIcon']);
	Route::post('deleteModule', [ModuleController::class, 'destroy'])->name('deleteModule');
	Route::post('changeModuleStatus', [ModuleController::class, 'changeModuleStatus'])->name('changeModuleStatus');

	// Module Field
	Route::resource('moduleField', ModuleFieldController::class);
	Route::get('getAllInputField', [ModuleFieldController::class, 'getAllInputField']);
	Route::post('changeModuleFieldStatus', [ModuleFieldController::class, 'changeModuleFieldStatus'])->name('changeModuleFieldStatus');
	Route::post('deleteModuleField', [ModuleFieldController::class, 'destroy'])->name('deleteModuleField');
	Route::post('saveTemplateFieldForModule', [ModuleFieldController::class, 'saveTemplateFieldForModule'])->name('saveTemplateFieldForModule');

	// Module Question
	Route::resource('moduleQuestion', ModuleQuestionController::class);
	Route::post('changeModuleQuestionStatus', [ModuleQuestionController::class, 'changeModuleQuestionStatus'])->name('changeModuleQuestionStatus');
	Route::post('deleteModuleQuestion', [ModuleQuestionController::class, 'destroy'])->name('deleteModuleQuestion');

    //Module Answer
    Route::get('moduleAnswer', [ModuleQuestionController::class, 'getAnswerList']);

	// Confined Space
	Route::resource('confinedSpace', ConfinedSpaceController::class);

    // Route group for site-panel apis
    Route::group(['prefix' => 'site-panel'], function () {
        Route::get('file-size-limit', [SitePanelController::class, 'fileSizeLimit']);
        Route::post('vertical-menu', [SitePanelController::class, 'getSitePanelVerticalMenu']);
        Route::post('category-list', [SitePanelController::class, 'getClientPanelCategory']);
        Route::post('module-list', [SitePanelController::class, 'getClientPanelModule']);
        Route::get('sites-and-sub-sites-list', [SitePanelController::class, 'getSitesAndSubSitesList']);
        Route::get('all-sub-sites', [SitePanelController::class, 'getAllSubSites']);
        Route::get('fields-list', [SitePanelController::class, 'getFieldsList']);
        Route::get('module-info/{modId}', [SitePanelController::class, 'moduleInfo']);
        // Route::get('assets', [SitePanelController::class, 'getSitePanelAssets']);
        Route::group(['prefix' => 'assets'], function() {
            Route::get('/', [SitePanelController::class, 'getSitePanelAssets']);
            Route::post('store', [SitePanelController::class, 'storeAsset']);
            Route::post('duplicate', [SitePanelController::class, 'duplicateAsset']);
            Route::post('moveAsset', [SitePanelController::class, 'moveAsset']);
            Route::post('update', [SitePanelController::class, 'updateAsset']);
            Route::post('asset-batch-delete', [SitePanelController::class, 'deleteBatchAssets']);
            Route::get('documents', [SitePanelController::class, 'getAssetDocuments']);
            Route::get('details/{assetId}', [SitePanelController::class, 'getAssetDetails']);
            Route::post('save-document', [SitePanelController::class, 'saveAssetDocument']);
            Route::get('document/{assetId}', [SitePanelController::class, 'getAssetDocument']);
            Route::post('document-batch-delete', [SitePanelController::class, 'deleteBatchDocuments']);
            Route::get('activity', [SitePanelController::class, 'getAssetActivity']);
            Route::get('inspection', [SitePanelController::class, 'getInspection']);
            Route::post('inspection-batch-delete', [SitePanelController::class, 'deleteBatchInspection']);
        });
    });

    // Route group for dashboard apis
    Route::group(['prefix' => 'dashboard'], function () {
        Route::post('/', [DashboardController::class, 'index']);
        Route::get('filter-options', [DashboardController::class, 'getFilterOptions']);
        Route::get('listing', [DashboardDetailListing::class, 'fetchDashboardListing']);
        Route::post('listing-export', [DashboardDetailListing::class, 'exportDashboardListingData']);
        Route::get('asset-detail/{assetUID}', [DashboardDetailAsset::class, 'getAssetDetail']);
        Route::post('asset-detail-export', [DashboardDetailAsset::class, 'exportDashboardAssetDetailData']);
		Route::post('global-asset-search', [DashboardController::class, 'getGlobalAssetSearch']);
    });

	// Get error log
	Route::get('error-log', [ApiController::class, 'getErrorLog']);
});
