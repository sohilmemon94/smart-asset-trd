<?php

use App\Http\Controllers\MobileAPI\ApiController as MobileApiController;
use App\Http\Controllers\MobileAPI\AuthController as MobileAuthController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'mobile'], function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', [MobileAuthController::class, 'login']);
        
        Route::group(['middleware' => 'auth:api'], function () {
            Route::get('logout', [MobileAuthController::class, 'logout']);
        });
    });

    Route::group(['middleware' => 'auth:api'], function () {
        // Store errors
        Route::post('storeError', [MobileApiController::class, 'storeError']);
        
        Route::get('getUserCompanyDetail', [MobileApiController::class, 'getUserCompanyDetail']);
        Route::get('site/listing', [MobileApiController::class, 'siteListing']);
        Route::get('sub-site/listing', [MobileApiController::class, 'subSiteListing']);
        Route::get('sub-site/category', [MobileApiController::class, 'subSiteCategory']);
        Route::get('category/listing/{userId}', [MobileApiController::class, 'categoryListingById']);
        Route::get('category/module/listing', [MobileApiController::class, 'categoryModuleListing']);
        Route::get('category/module/field/listing', [MobileApiController::class, 'categoryModuleFieldListing']);
        Route::post('asset/store', [MobileApiController::class, 'storeAsset']);
        Route::post('updateAsset', [MobileApiController::class, 'updateAsset']);
        Route::get('asset/listing', [MobileApiController::class, 'assetListing']);
        Route::get('asset/detail', [MobileApiController::class, 'assetDetail']);
        Route::get('globalAssetSearch', [MobileApiController::class, 'globalAssetSearch']);
        Route::post('storeAssetInspection', [MobileApiController::class, 'storeAssetInspection']);

        // Get all question based on module id
        Route::get('getModuleQuestions', [MobileApiController::class, 'getModuleQuestions']);

        // Store user answer in question 
        Route::post('storeUserModuleQuestionAnswer', [MobileApiController::class, 'storeUserModuleQuestionAnswer']);
        
        // Get all confined space question
        Route::get('getConfinedSpaceQuestions', [MobileApiController::class, 'getConfinedSpaceQuestions']);

        // Store confined space result 
        Route::post('storeConfinedSpaceResult', [MobileApiController::class, 'storeConfinedSpaceForm']);

        // Move asset to another location, area, category and module 
        Route::post('moveAsset', [MobileApiController::class, 'moveAsset']);
    });
});
