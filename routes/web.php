<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\MobileAPI\ApiController as MobileApiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Test Email Routes
Route::get('welcome_mail/{email}',[TestController::class,'sendWelcomeMail']);
Route::get('report_mail/{email}',[TestController::class,'sendReportMail']);
Route::get('support_mail/{email}',[TestController::class,'sendSupportMail']);

Route::get('/reset-db', function() {
    // Remove all migration and run again
    \Artisan::call('migrate:refresh');

    // Run seeder
    \Artisan::call('db:seed');

    return 'Database refresh successfully';
});


Route::get('/clear-cache', function() {
    // Routes cache cleared
    \Artisan::call('route:cache');

    // Config cache cleared
    \Artisan::call('config:cache');

    // Clear application cache
    \Artisan::call('cache:clear');

    // Clear view cache
    \Artisan::call('view:clear');

    // Clear cache using optimized class
    \Artisan::call('optimize:clear');

    return 'View cache cleared';
});

// Get json data if pass token param in mobile app
Route::get('asset-information/{asset_nfc_uuid}', [MobileApiController::class, 'assetDetail']);

Route::get('/{any}', [ApplicationController::class, 'index'])->where('any', '.*');
